
<h4><i class="fa fa-users"></i> Buku Keterangan Waris</h4>
<?foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>

<?php echo $output; ?>
<script type="text/javascript">
	$(document).ready(()=>{
		gc.FormDef = {
			m_buket_waris : <?=json_encode($conf)?>
		}
	});
</script>