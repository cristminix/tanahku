<?php  defined('BASEPATH') OR exit('No direct script access allowed');
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
class Buket_waris extends Theme_Controller {
	public $_page_title = 'Buku Keterangan Waris';
	public function custom_grid_data()
    {
        
        $this->load->model('m_buket_waris','model');
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $action = "<td class=\"actions\"> 
            <div class='btn-group'>  
            <button href=\"".site_url('pengolahan_v2/buket_waris/index/edit/'.$field->id)."/".slugify($field->nama_waris)."\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-edit\" aria-hidden=\"true\"></i> 
            </button> "."
            <button onclick=\"javascript: return delete_row('".site_url('pengolahan_v2/buket_waris/index/delete/'.$field->id)."',-1,this)\" href=\"javascript:;\" class=\"gc-bt-delete delete_button btn btn-sm btn-icon btn-pure btn-danger\" role=\"button\"> <i class=\"fa fa-trash\" aria-hidden=\"true\"></i> 
            </button>
            </div>
            </td>";
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->nama_waris;
            		$row[]=$field->id;
		$row[]=$field->dt_surat;
		$row[]=$field->no_surat;
		$row[]=$field->nama_waris;
		$row[]=$field->dt_created;
		$row[]=$field->dt_edit;
		$row[]=$field->user_id;

            $user = $this->m_login->get_by_id($field->id_user);
            $row[] =date('d-m-Y',strtotime($field->tgl_entry));
            $row[] = date('d-m-Y',strtotime($field->tgl_update));
            $row[] = $user->name;
            $row[] = $action; 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('pengolahan_v2/buket_waris/index/add')];
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';
        $this->load->view('_pengolahan_v2/dt_buket_waris.php',$tdata);
    }
    private function _customGrid(){
        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('pengolahan_v2/buket_waris/index/add')];
        $data['output'] = $this->load->view('_pengolahan_v2/dt_buket_waris.php',$tdata,true);
        $target_yaml = APP . '/form/m_buket_waris.yml';
        $buffer      = file_get_contents($target_yaml);
        $data['conf']        = Yaml::parse($buffer);
        $data['unique_hash'] = md5(date('YmdHis-Unit'));
        $data['is_admin']  = $this->cms_user_group()=='admin';
        $this->view('_pengolahan_v2/buket_waris.php', $data );
    }
    public function index()
    {
        $target_yaml = APP . '/form/m_buket_waris.yml';
        $buffer      = file_get_contents($target_yaml);
        $conf        = Yaml::parse($buffer);
        $crud = $this->new_crud($conf);
        $view_folder = './modules/'.$this->cms_module_path().'/views/gc_templates';
        // die($view_folder);
        $crud->set_view_file_base($view_folder);
        $state = $crud->getState();
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_model('m_buket_waris');
        $crud->callback_after_insert(array($this,'_update_detail')); 
        $data = $crud->render();
        $data->state = $state;
        $data->conf = $conf;
        if($return === true){
            return [
                'data' => $data,
                'view' => 'gc',
                'gc' => $crud
            ];  
        }
        $this->view('gc',$data);
    }
    public function _index()
    {
        $target_yaml = APP . '/form/m_buket_waris.yml';
        $buffer = file_get_contents($target_yaml);
        $_SERVER['FORM_DEF'] = Yaml::parse($buffer);
        $crud = $this->new_crud();
        $crud->set_subject('Keterangan Waris');
        $state = $crud->getState();
        switch ($state) {
            case 'list':
                $args = func_get_args();
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            default:
                # code...
                break;
        }
        $crud->unset_jquery();
        // $crud->unset_export();
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_table('m_buket_waris');
		$crud->set_model('m_buket_waris');
		$crud->set_theme('datatables');
        $crud->field_type('tgl_entry','hidden');
        $crud->field_type('tgl_update','hidden');
        $crud->field_type('id_user','hidden');
        $crud->callback_before_insert(array($this,'_set_tgl_entry'));
        $crud->callback_before_update(array($this,'_set_tgl_update'));
    	$id_user = $this->cms_user_id();
		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	$id_user = $this->cms_user_id();
        $crud->callback_after_insert(array($this,'_update_detail')); 
        $data = $crud->render();
		$data->is_admin  = $this->cms_user_group()=='admin';
		$this->view('_pengolahan_v2/buket_waris.php',$data);
    }
    function _set_tgl_entry($post_array){
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_entry'] = $dt;
        $post_array['tgl_update'] = $dt;
        $post_array['id_user'] = $this->cms_user_id();
        return $post_array;
    }
    function _set_tgl_update($post_array){
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_update'] = $dt;
        return $post_array;
    } 
    public function list_ahli_waris($fk_id)
    {
 
        $results   = [
            'success' => false,
            'data'=> []
        ];
 
        $cmd = $this->input->get('cmd');
        $post_keys = ["parent_id","nama_ahli_waris","alamat","keterangan"];
        $post_data = [];

        foreach ($post_keys as $name) {
            $post_data[$name] = $this->input->post($name);
        }
        switch ($cmd) {
            case 'add':
                $this->session->set_userdata('m_buket_ahli_waris_tmp_fk',$fk_id);
                $post_data['is_tmp'] = 1;
                $results['success'] = true;
                $this->db->insert('m_buket_ahli_waris',$post_data);
                $results['data'] = $post_data;
                $results['data']['id'] = $this->db->insert_id();
                break;
            case 'edit':
                $post_data['is_tmp'] = 0;
                $results['success'] = true;
                $pk = $this->input->post('id');
                $this->db->where('id',$pk)->update('m_buket_ahli_waris',$post_data);
                $results['data'] = $post_data;
                $results['data']['id'] = $pk;
                break;
            case 'delete':
                $temporal = $this->input->get('temporal') == 'true';
                $results['success'] = true;
                if($temporal){
                    $this->db->where('parent_id',$fk_id)->where('is_tmp',1)->delete('m_buket_ahli_waris');
                    $this->session->unset_userdata('m_buket_ahli_waris_tmp_fk');

                }else{
                    $pk = $this->input->post('id');
                    $this->db->where('id',$pk)->delete('m_buket_ahli_waris');
                }
                
                break;    
            case 'list':
                $results['success'] = true;
                $results['data'] = $this->db->where('parent_id',$fk_id)
                                            ->get('m_buket_ahli_waris')
                                            ->result_array();
                $results['sql'] = $this->db->last_query();                            
                break;
        }
        echo json_encode($results);
    }
    function _update_detail($post_data,$pk){
        $old_fk = $this->session->userdata('m_buket_ahli_waris_tmp_fk');
        $new_fk = $pk;
        $row = [
            'parent_id' => $new_fk,
            'is_tmp' => 0
        ];

        // print_r($row);
        $this->db->where('parent_id',$old_fk)->where('is_tmp',1)->update('m_buket_ahli_waris',$row);
        $this->session->unset_userdata('m_buket_ahli_waris_tmp_fk');
    }
}
