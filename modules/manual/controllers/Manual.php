<?php  defined('BASEPATH') OR exit('No direct script access allowed');
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
class Manual extends Theme_Controller {
	public $_page_title = 'Daftar Manual';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('m_manual');
    }

	public function custom_grid_data()
    {
        // $this->load->model('account/m_login');
        $this->load->model('m_manual','model');
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $action = "<td class=\"actions\"> 
            <div class='btn-group'>  
            <button href=\"".site_url('manual/manual/index/edit/'.$field->id)."/".slugify($field->nama_1)."\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-edit\" aria-hidden=\"true\"></i> 
            </button> "."
            <button onclick=\"javascript: return delete_row('".site_url('manual/manual/index/delete/'.$field->id)."',-1,this)\" href=\"javascript:;\" class=\"gc-bt-delete delete_button btn btn-sm btn-icon btn-pure btn-danger\" role=\"button\"> <i class=\"fa fa-trash\" aria-hidden=\"true\"></i> 
            </button>
            </div>
            </td>";
            $no++;
            $row = array();
            $row[] = $no;
       ;
            $row[] = '<h4>'. $field['title'].'</h4>'.$field['content'];
            // $user = $this->m_login->get_by_id($field->id_user);
            // $row[] =date('d-m-Y',strtotime($field->tgl_entry));
            // $row[] = date('d-m-Y',strtotime($field->tgl_update));
            // $row[] = $user->name;
            // $row[] = $action; 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('manual/manual/index/add')];
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';
        $this->load->view('_manual/dt_manual.php',$tdata);
    }
    private function _customGrid(){
        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('manual/manual/index/add')];
        $data['output'] = $this->load->view('_manual/dt_manual.php',$tdata,true);
        $data['unique_hash'] = md5(date('YmdHis-Unit'));
        $data['is_admin']  = $this->cms_user_group()=='admin';
        $this->view('_manual/manual.php', $data );
    }
    public function index()
    {
        $target_yaml = APP . '/form/tb_m_manual.yml';
        $buffer = file_get_contents($target_yaml);
        $_SERVER['FORM_DEF'] = Yaml::parse($buffer);
        $crud = $this->new_crud();
        $crud->set_subject('Manual');
        $state = $crud->getState();
        switch ($state) {
            case 'list':
                $args = func_get_args();
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            default:
                # code...
                break;
        }
        $crud->unset_jquery();
        // $crud->unset_export();
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_table('tb_m_manual');
		$crud->set_model('m_manual');
		$crud->set_theme('datatables');
        $crud->field_type('tgl_entry','hidden');
        $crud->field_type('tgl_update','hidden');
        $crud->field_type('id_user','hidden');
        $crud->callback_before_insert(array($this,'_set_tgl_entry'));
        $crud->callback_before_update(array($this,'_set_tgl_update'));
    	$id_user = $this->cms_user_id();
		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	$id_user = $this->cms_user_id();
        $data = $crud->render();
		$data->is_admin  = $this->cms_user_group()=='admin';
		$this->view('_manual/manual.php',$data);
    }
    function _set_tgl_entry($post_array){
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_entry'] = $dt;
        $post_array['tgl_update'] = $dt;
        $post_array['id_user'] = $this->cms_user_id();
        return $post_array;
    }
    function _set_tgl_update($post_array){
        $dt = date('Y-m-d H:i:s');
        $post_array['tgl_update'] = $dt;
        return $post_array;
    } 

    public function resources()
    {
        $this->_page_title = 'Resources & Utility';
        $data = [
        ];
        $this->view('resources',$data);
    }

    public function m_manual($method)
    {
        if(method_exists($this->m_manual, $method)){
            $results = $this->m_manual->$method();

            echo json_encode($results);
        }
    }
    public function list_fields($table)
    {
        $field_data = $this->db->field_data($table);
        $fields = [$table=>[
            'available_fields' => [],
            'sort_fields'=>[],
            'search_fields'=>[],
            'accordions'=>[]
        ]];

        foreach ($field_data as $field) {
            $fields[$table]['available_fields'][] = $field->name;
        }
        $target_yaml = APP . '/form/'.$table.'.yml';
        if(file_exists($target_yaml)){
            $target_yaml = APP . '/form/'.$table.date('YmdHis').'.yml';
        }
        file_put_contents($target_yaml,Yaml::dump($fields, 6, 4, Yaml::DUMP_EMPTY_ARRAY_AS_SEQUENCE  ));
        $results = [
            'message' => "WRITE : $target_yaml\n"
        ];
        echo json_encode($results);
    }
    public function table_headers($table)
    {
        $field_data = $this->db->field_data($table);
        $headers = []; 
        $table_headers = '<tr>'."\n";
        foreach ($field_data as $field) {
            if($field->name == 'tgl_entry'){
                $caption = 'Tanggal Buat';
            }
            else if($field->name == 'tgl_update'){
                $caption = 'Tanggal Ubah';
            }
            else if($field->name == 'id_user'){
                $caption = 'Dibuat';
            }
            else $caption = title_case(str_replace('_',' ',$field->name));
       
      
            $table_headers .= '<th class="'.$field->name.'" >'. $caption .= '</th>'."\n";
         }
        $results = [
            'message' => $table_headers,
            'headers' => $headers
        ];
        echo json_encode($results);
    }
    public function yml_list()
    {
        $results = glob(APP.'/modules/*yml');
        echo json_encode($results);

    }
    public function init_modules($module_name)
    {
        $results = [
            'message'=> $module_name,
            'filename' => APP. '/modules/'.$module_name.'.yml'
        ];

        echo json_encode($results);
    }
}
