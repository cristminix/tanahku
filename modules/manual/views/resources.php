<script type="text/javascript" src="{{ theme_assets }}global/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{ theme_assets }}global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
<script type="text/javascript" src="{{ theme_assets }}pages/scripts/form-wizard.min.js"></script>
<div id="resources">
	<ul class="nav nav-tabs">
        <li class="active">
            <a href="#tab_crud" data-toggle="tab"> CRUD Helper </a>
        </li>
        <li>
            <a href="#tab_1_2" data-toggle="tab"> Important GC method </a>
        </li>
        
    </ul>
    <div class="tab-content">
    <div class="tab-pane fade active in" id="tab_crud">
        <?$this->load->view('resources/accordions')?>
    </div>
    <div class="tab-pane fade" id="tab_1_2">
        <code>
        	$crud->set_view_file_base($dir);
        	//contoh:
        	// salah satu file template harus ada dalam direktori
        	// add.php
        	// edit.php
        	// list.php
        	// list_template.php
        	// read.php
        	$view_folder = './modules/'.$this->cms_module_path().'/views/gc_templates';
        	$crud->set_view_file_base($view_folder);
        </code>
    </div>
    <div class="tab-pane fade" id="tab_1_3">
       tab_1_3
    </div>
    <div class="tab-pane fade" id="tab_1_4">
       tab_1_4
    </div>
</div>
</div>

<script type="text/javascript">
$(document).ready(()=>{
	window.resources = new Vue({
		el: '#resources',
		data:{
			table_list : [],
			select_table:'',
			yml_list:[],
			message_1:'',
			message_2:'',
			message_3:'',
			module_name:'pengaturan_konsumen_dokumen_konsumen',
			alias:'dok-konsumen',
			klass:'Dok_konsumen',
			path:'dok_konsumen',
			route:'pengaturan',
			page_title:'Daftar Kelengkapan Dokumen Konsumen',
			subject:'Dokumen Konsumen',
			tab_filename:'tab',
			ctl_alias: '$alias',
			ctl_class: '$class',
			ctl_ctl: '$class.php',
			step:1,
		},
		mounted(){
			this.$nextTick(()=>{
				setTimeout(()=>{
					this.initTableList();
					this.initYmlList();
				},100);
			});
		},
		watch:{
			select_table(a,b){
				console.log(a,b);
				this.module_name = a;
			}
		},
		methods:{
			initTableList(){
				$.get(site_url()+`manual/m_manual/list_table`,(res)=>{
					this.table_list = res;
				},'json');
			},
			initYmlList(){
				$.get(site_url()+`manual/yml_list`,(res)=>{
					this.yml_list = res;
				},'json');
			},
			dbListFields(){
				var postData = {

				};
				var table = this.select_table;
				if(table.length <= 1){
					swal('Silahkan Pilih Tabel');
					return;
				}
				$.post(site_url()+`manual/list_fields/${table}`,postData,(res)=>{
					this.message_1 = res.message;
				},'json');
			},
			genTableHeaders(){
				var postData = {

				};
				var table = this.select_table;
				if(table.length <= 1){
					swal('Silahkan Pilih Tabel');
					return;
				}
				$.post(site_url()+`manual/table_headers/${table}`,postData,(res)=>{
					this.message_2= res.message;
				},'json');
			},
			initModule(){
				var postData = {};
				$.post(site_url()+`manual/init_modules/${this.module_name}`,postData,(res)=>{
					this.message_1 = res.message;
				},'json');
			},
			generateModule(){
				console.log(this.module_name)
			},
			next(){
				$('#tab1').hide()
				$('#tab2').show()
				$('.progress-bar').css('width','50%');
				$('a[href*=tab2]').attr('aria-expanded',true);
				$('a[href*=tab2]').closest('li').addClass('active');
				var step = this.step += 1;
				if(step==3){
					setTimeout(()=>{
					if($('.progress-bar').attr('style')=='width: 50%;'){
							console.log('teope');
							$('#tab2').hide()
							$('#tab3').show()
							$('.progress-bar').css('width','75%');
							$('a[href*=tab3]').attr('aria-expanded',true);
							$('a[href*=tab3]').closest('li').addClass('active');
						}
					},1000);
				}
				
			}
		}

	});
});
</script>

<style type="text/css">
	code{
		white-space: pre-line;
	}
</style>