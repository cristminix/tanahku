
<div class="page-header navbar navbar-fixed-top" id="page_header_navbar">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="{{ homepage }}">
                            <img src="{{ base_url }}pub/img/logo-big.png"  alt="logo" class="logo-default" /> </a>
                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- <form class="search-form" action="extra_search.html" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search..." name="query">
                            <span class="input-group-btn">
                                <a href="javascript:;" class="btn submit">
                                    <i class="icon-magnifier"></i>
                                </a>
                            </span>
                        </div>
                    </form> -->
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar0">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-bell"></i>
                                    <span class="badge badge-default"> 1 </span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="external">
                                        <h3>
                                            <span class="bold">1 pending</span> notifications</h3>
                                        <a href="javascript:void()">view all</a>
                                    </li>
                                </ul>
                            </li> -->
                            <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar01">
                                <a href="{{ site_url }}pengolahan/riwayat-tanah-group" class="btn btn-menu">
                                    <span>Pengolahan</span>
                                </a>
                                
                            </li>

                            <!-- BEGIN NOTIFICATION DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after "dropdown-extended" to change the dropdown styte -->
                            <!-- DOC: Apply "dropdown-hoverable" class after below "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
                            <!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
                            <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" v-if="notifications!=null">
                                    <i class="icon-bell"></i>
                                    <span class="badge badge-default" v-text="notifications.counts.unread" v-if="notifications.counts.unread>0"> </span>
                                </a>
                                <ul class="dropdown-menu" v-if="notifications != null">
                                    <li class="external" v-if="notifications.counts.unread>0">
                                        <h3>
                                            <span class="bold"><span v-text="notifications.counts.unread"></span> Belum dibaca</span></h3>
                                        <a href="{{ site_url }}notifikasi">Lihat Semua</a>
                                    </li>
                                    <li>
                                        <ul  class="dropdown-menu-list scroller" data-handle-color="#637283">
                                            <li v-for="n in notifications.latest.records" :class="{'unread':!n.is_read}">
                                                <a href="javascript:;">
                                                    <span class="details">
                                                        
                                                     <span @click="gotoNotif(n)" style="font-weight: bold"> {{ n.title }} </span>
                                                     <span style="display: block;font-size: 90%">{{n.dt_ago}}</span>   
                                                </a>
                                            </li>
                                            <li v-if="notifications.latest.records.length==0" >
                                                <a href="javascript:;">
                                                    <span class="details">
                                                        <span class="label  label-icon" style="color: coral">
                                                            <i class="fa fa-exclamation-circle"></i>
                                                        </span> Tidak ada notifikasi</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <!-- END NOTIFICATION DROPDOWN -->
                            <!-- BEGIN INBOX DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            
                            <!-- END INBOX DROPDOWN -->
                            <!-- BEGIN TODO DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                          
                            <!-- END TODO DROPDOWN -->
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <span class="username username-hide-on-mobile" v-text="'{{ display_name }}'"> </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <?foreach(MENU_YML['top_right'] as $item): ?>
                                    <li v-if='jQuery.inArray("{{ user_group }}",<?=json_encode($item['credentials'],true)?>) != -1'>
                                        <a href="{{ base_url }}<?=$item['url']?>">
                                            <i class="icon-user"></i> <span><?=$item['title']?></span></a>
                                    </li>
                                    <?endforeach?> 
                                    <li class="divider"> </li>
                                    
                                    <li>
                                        <a href="{{ base_url }}logout">
                                            <i class="icon-close"></i> Keluar</a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <!-- <li class="dropdown dropdown-quick-sidebar-toggler">
                                <a href="javascript:;" class="dropdown-toggle">
                                    <i class="icon-logout"></i>
                                </a>
                            </li> -->
                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
 
            </div>
<script type="text/javascript">
    $(document).ready(function(){
        window.$vmph = new Vue({
            el:"#page_header_navbar",
            data:{
                user_avatar:'{{ user_avatar }}',
                user_real_name:'{{ user_real_name }}',
                notifications: null
              

            },
          
            methods:{
                gotoNotif(n){
                    document.location.href = site_url() + `notifikasi/detail/${n.id}/${n.title}`
                }
            },
            ready:function(){

                var updated_real_name = '<?=$this->input->cookie('cms_user_real_name',TRUE)?>';
                if(updated_real_name.length > 0){
                    this.user_real_name = updated_real_name;
                }
                var cms_user_avatar = Cookies.get('cms_user_avatar');
                if(typeof cms_user_avatar != 'undefined'){
                    this.user_avatar = cms_user_avatar;
                }
            }
        });
    });
</script>
<style type="text/css">
    li.unread{
         background: #f8f9fa;
    }
</style>