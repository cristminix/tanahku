<!--[if lt IE 9]>
<script src="{{ theme_assets }}/global/plugins/respond.min.js"></script>
<script src="{{ theme_assets }}/global/plugins/excanvas.min.js"></script> 
<script src="{{ theme_assets }}/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        
<!-- <script src="{{ site_url }}www_static/assets/bottom/js" type="text/javascript"></script> -->
<?=js_assets('bottom_js')?>
<script src="{{ site_url }}pub/js/sync_account.js" type="text/javascript"></script>
        
      


<style type="text/css">
.dataTables_wrapper .dataTables_paginate .paginate_button{
    display: inline-block;
    padding: 0 0 0 1px;
}            

.btn.refresh-data{
    display: none;
}
th.no{
    width: 10px;
    text-align: right;
}
th.actions{
    width: 105px;
    text-align: center;
}
td.actions{
    text-align: center;
}
.btn-menu{
    padding: .5em;
    color: #fff !important;
    font-weight: bold;
    height: 10px !important;
    line-height: 0 !important;
    margin-top: 10px;
}
.btn-menu:hover{
    border-radius: 12px;
    color: #fff !important;
    font-weight: bold;
    background: #666 !important;

}
th.actions.sorting,
th.no.sorting_asc,
th.no.sorting_desc{
    background: none !important;
}
</style>