<div class="page-footer" style="text-align: center;padding: 1em">
    <div class="page-footer-inner" style="line-height: 0.7"><?=APP_YML['app_name']?> &copy; <?=date('Y')?> Rumahapp - Sistem Informasi Pertanahan
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>