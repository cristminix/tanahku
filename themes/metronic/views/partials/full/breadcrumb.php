
<ul class="page-breadcrumb" id="page_breadcrumbs">
  
    <li v-for="(b,index) in breadcrumbs[active_menu]">
        <span v-if="index == (breadcrumbs[active_menu].length-1)" v-text="b.title"></span>
        <a v-if="index != (breadcrumbs[active_menu].length-1)" v-bind:href="b.url?'{{ site_url }}'+b.url:'javascript:;'"><span v-text="b.title"></span></a>
        <i v-if="index != (breadcrumbs[active_menu].length-1)" v-bind:class="{'fa':1, 'fa-circle':1}"></i>
    </li>
</ul>
<script type="text/javascript">
	$(document).ready(function(){
        window.breadcrumb=new Vue({
            el:'#page_breadcrumbs',
            data:{
                breadcrumbs:<?=json_encode(BREADCRUMB_YML)?>,
                active_menu:'<?=$this->uri->segment(1)?>',
                active_item:'<?=$this->uri->segment(1).'/'.$this->uri->segment(2)?>',
                has_segment_2: <?= !empty($this->uri->segment(2)) ?'true':'false'?>,
                segment_2: '<?=$this->uri->segment(2)?>'
            },
            mounted(){
                
                if(this.has_segment_2){
                    this.active_menu = this.active_menu+'_'+this.segment_2;
                }
                this.active_menu = this.active_menu.replace(/-/g,'_');
                console.log(this.active_menu)
            }
        });
    });
</script>