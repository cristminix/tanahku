<div class="page-sidebar-wrapper" id="page_sidebar_wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse ">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-closed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" >
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            
<li class="heading">
                                <h3 class="uppercase">MENU</h3>
                            </li>
<!-- START LOOP -->
<li v-bind:class="{'nav-item':true,'start':(index==0), 'active':(menu.url==active_menu) ,'open':(menu.url==active_menu)}" v-for="(menu,index) in menus" v-if="jQuery.inArray('{{ user_group }}',menu.credentials) != -1">
    <a :href="$.isArray(menu.items)?'javascript:;':'{{ base_url }}'+menu.url" class="nav-link nav-toggle" v-bind:uri="'{{ base_url }}'+menu.url">
        <i v-bind:class="menu.icon"></i> <span class="title" v-text="menu.title"></span>
        <span v-bind:class="{'selected':(menu.url==active_menu)}"></span>
        <span v-bind:class="{'arrow':$.isArray(menu.items),'open':(menu.url==active_menu)}"></span>
    </a>
    <ul class="sub-menu" v-if="$.isArray(menu.items)">
        <li v-bind:class="{'nav-item':true,'start':(sub.url==active_item), 'active':(sub.url==active_item||($.inArray(active_item,sub.grup_url) != -1)), 'open':(sub.url==active_item)}" v-for="sub in menu.items">
            <a v-bind:href="'{{ base_url }}'+sub.url" class="nav-link ">
                <i v-bind:class="sub.icon"></i> <span class="title" v-text="sub.title"></span>
            </a>
        </li>

    </ul>
</li>
<!-- END LOOP -->
</ul>
<!-- END SIDEBAR MENU -->
<!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR -->
</div>
<script type="text/javascript">
    $(document).ready(function(){
        window.sidebar = new Vue({
            el:'#page_sidebar_wrapper',
            data:{
                menus:<?=json_encode(MENU_YML['sidebar'])?>,
                active_menu:'<?=$this->uri->segment(1)?>',
                active_item:'<?=$this->uri->segment(1).'/'.$this->uri->segment(2)?>',
                jQuery: jQuery
            },
            mounted(){
                var segment_3 = '<?=$this->uri->segment(3)?>';
                if(this.active_menu=='pengolahan_v2'){
                    this.active_menu = 'pengolahan';
                }
                if(this.active_menu.match(/\//)){
                    var tmp = this.active_menu.split('/');
                    this.active_menu=tmp[0];
                }
                if(segment_3.length>0){
                    // this.active_item += `/${segment_3}`;
                }
                console.log(this.active_menu,this.active_item)
            }
        })
    });
</script>
