
<!DOCTYPE html>
 

<html lang="en">
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->

    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Pendaftaran Anggota PPSL</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Pendaftaran PPSL " name="description" />
        <meta content="PERUMDAM TKR" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
<script type="text/javascript">
    APP_YML_ENABLE_GOGLE_CAPTCHA = <?=APP_YML['app_enable_google_captcha']?'true':'false'?>;
</script>        
<?if(APP_YML['app_enable_google_captcha']):?>

<script src='https://www.google.com/recaptcha/api.js' async defer></script>
<?endif?>
        <!-- <link href="http:/fonts.googleapis.com/css?family=Open+Sans:4]00,300,600,700&subset=all" rel="stylesheet" type="text/css" /> -->
        <link href="{{ site_url }}themes/metronic/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ site_url }}themes/metronic/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ site_url }}themes/metronic/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ site_url }}themes/metronic/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{ site_url }}themes/metronic/assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ site_url }}themes/metronic/assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{ site_url }}themes/metronic/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ site_url }}themes/metronic/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="{{ site_url }}themes/metronic/assets/pages/css/login-5.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="{{ base_url }}pub/img/favicon.ico">
        
    </head>
    <!-- END HEAD -->
     <style type="text/css">
    .abs{
        position: absolute;
    right: 88px;
    background: red;
    border-radius: 2% !important;
    padding: 1px 9px;
    z-index: 3;
    margin-top: -10px;
    }
    .fix.abs{
            position: static;
    clear: both;
    /*width: 358px;*/
    padding: 1em;
    }
    .abs > p{
        margin: 0;
        color: #fff !important;
    }
    .user-login-5 .form-group.has-error{
        border: none !important;
    }
    span.required{
        color: red;
    }
    a,a:hover{
        text-decoration: none;
    }
    .btn-unduh,.btn-unduh-info,
    #nik_file{
        cursor: pointer;
    }
</style>
<script type="text/javascript" src="{{ site_url }}/www_static/js/main_js"></script>
        <script src="{{ site_url }}themes/metronic/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        
    <body class=" login">
        <!-- END : LOGIN PAGE 5-1 -->
        <?= $template['body']; ?>

        <!--[if lt IE 9]>
<script src="{{ site_url }}themes/metronic/assets/global/plugins/respond.min.js"></script>
<script src="{{ site_url }}themes/metronic/assets/global/plugins/excanvas.min.js"></script> 
<script src="{{ site_url }}themes/metronic/assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{ site_url }}themes/metronic/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{ site_url }}themes/metronic/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="{{ site_url }}themes/metronic/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="{{ site_url }}themes/metronic/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="{{ site_url }}themes/metronic/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{ site_url }}themes/metronic/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="{{ site_url }}themes/metronic/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="{{ site_url }}themes/metronic/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="{{ site_url }}themes/metronic/assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{ site_url }}themes/metronic/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <!-- <script src="{{ site_url }}themes/metronic/assets/pages/scripts/login-5.min.js" type="text/javascript"></script> -->
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
        <script>
            // Restricts input for the given textbox to the given inputFilter function.
function setInputFilter(textbox, inputFilter) {

  ["input", "keydown", "keyup", "mousedown", "mouseup", "select", "contextmenu", "drop"].forEach(function(event) {
    if(textbox !== null)
    textbox.addEventListener(event, function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  });
}
            $(document).ready(function()
            {

                setInputFilter(document.getElementById("nik"), function(value) {
                  return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
                });
                setInputFilter(document.getElementById("no_hp"), function(value) {
                  return /^\d*\.?\d*$/.test(value); // Allow digits and '.' only, using a RegExp
                });

                $('div.help-block').each(function(k,v){
                    let child = $(this);

                    let text  = child.text();

                    if(text.length > 2){
                        child.addClass('abs');
                        let p = child.closest('.form-group') .addClass('has-error');
                        console.log(p);
                    }
                    // console.log(child)
                });

                $('#nik_file').change(function(){
                    console.log(this.value);
                    $('.btn-unduh.ktp').hide()
                    $('.btn-unduh-info.ktp').html('<i class="fa fa-edit"></i> Ubah').show()
                });
                $('#ktp_selfi_file').change(function(){
                    console.log(this.value);
                    $('.btn-unduh.ktp_selfi').hide()
                    $('.btn-unduh-info.ktp_selfi').html('<i class="fa fa-edit"></i> Ubah').show()
                });
            })
        </script>
        <script type="text/javascript" src="{{ theme_assets }}js/register_page.js?_nocache=<?=md5(microtime())?>"></script>
    </body>

</html>

