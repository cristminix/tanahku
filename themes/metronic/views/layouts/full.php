<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="id">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title><?php echo $template['title'];?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <?php echo $template['metadata'];?>
        
         <!-- Le styles -->
        <?php
            $asset = new Asset();
            // $asset->add_cms_css('bootstrap/css/bootstrap.min.css');
            // $asset->add_themes_css('bootstrap.min.css', '{{ used_theme }}', 'default');
            // $asset->add_themes_css('style.css', '{{ used_theme }}', 'default');
            // echo $asset->compile_css();
        ?>
        <!-- Le fav and touch icons -->
        <link rel="shortcut icon" href="{{ base_url }}pub/img/favicon.ico">
        <!-- {{ widget_name:section_custom_script }} -->

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
            <?= $this->template->load_view('partials/full/assets-top.php'); ?>
        
    
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" >
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <!-- full/page-header.php -->
            <?= $this->template->load_view('partials/full/page-header.php'); ?>

            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <!-- BEGIN SIDEBAR -->
                <!-- full/left-sidebar.php -->
                <?= $this->template->load_view('partials/full/left-sidebar.php'); ?>
                <!-- END SIDEBAR -->
                <!-- BEGIN CONTENT -->
                <div class="page-content-wrapper">
                    <!-- BEGIN CONTENT BODY -->
                    <div class="page-content">
                        <!-- BEGIN PAGE HEADER-->
                        <!-- BEGIN THEME PANEL -->
                        <!-- theme-panel -->
                        <!-- END THEME PANEL -->
                        <!-- BEGIN PAGE BAR -->
                        <div class="page-bar">
                            <!-- breadcrumb -->
                            <?= $this->template->load_view('partials/full/breadcrumb.php'); ?>

                            <div class="page-toolbar">
                            <?= $this->template->load_view('partials/full/page-toolbar.php'); ?>
                                
                            </div>
                        </div>
                        
                        <!-- END PAGE BAR -->
                        
                        <?= $template['body']; ?>

                     
                        
                    </div>
                    <!-- END CONTENT BODY -->
                </div>
                
                <!-- END CONTENT -->

                <!-- BEGIN QUICK SIDEBAR -->
                <?= $this->template->load_view('partials/full/quick-sidebar.php'); ?>
                <!-- END QUICK SIDEBAR -->
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN FOOTER -->
            <?= $this->template->load_view('partials/full/footer.php'); ?>
            <!-- END FOOTER -->
        </div>
        <!-- BEGIN QUICK NAV -->
            <?= $this->template->load_view('partials/full/quick-nav.php'); ?>
        
        <!-- END QUICK NAV -->
        <!-- script-buttom -->
            <?= $this->template->load_view('partials/full/script-bottom.php'); ?>
            <?= $this->template->load_view('partials/full/firebase-script-bottom.php'); ?>

    </body>
<link rel="stylesheet" type="text/css" href="{{ site_url }}pub/css/ppsl.css">
</html>

<style type="text/css">
.page-header.navbar .page-logo .logo-default {
   /* margin-left: 24px;
    width: 108px;*/
}
#alert_cnt p a{
    display: none;
}
.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
    /*border-top: none !important; */
}
table.dataTable{
    border-collapse: collapse !important;
}

.dataTables_scrollHead{
    /*border-bottom-width: 1px !important;*/

}
table.dataTable thead td, table.dataTable thead th{
    border-bottom: none !important;
}
.modal-open{
    overflow-y: hidden !important;
}
.modal-dialog.modal-lg{
    width: 98%;
}
span.field-view{
    /*border: solid 1px #555;*/
    text-decoration: underline;
    padding: .5em;
    line-height: 2.2em;
}
.nav-item span.title{
    display: block;
    padding-left: 1.8em;
    margin-top: -1.4em;
}
a.open-file {
    color: #fff;
    font-weight: bold;
    text-decoration: none;
    /*border: solid 1px #555;*/
}
a.delete-anchor{
    color: #fff !important;
}
</style>
<script type="text/javascript">
    $(document).ready(()=>{
        const toggle_menu = '<?=$this->session->userdata('toggle_session_menu')?>';
        if(toggle_menu == 'true'){
            $('.menu-toggler').click();
        }
        const state = toggle_menu == 'true' ? 'false':'true';
        setTimeout(()=>{
            $('.menu-toggler').click(()=>{
                    $.post(site_url()+'dashboard/set_toggle_session_menu/'+state,{},(res)=>{

                    });
                });
            });
        },1000);
        
</script>