<?
/**
 * nama_lengkap, username, passwd, last_login, nip, is_active, email, avatar, unit_kerja
 */
class M_account_v2 extends CI_Model
{
	public function get_all($ids=[])
	{
		if(is_array($ids)){
			if(count($ids)>0){
				$this->db->where_in('acc.id',$ids);
			}
		}
		return $this->db->select('
			acc.id,
			rk.gelar_depan,
			rk.gelar_nonakademis,
			rk.gelar_belakang,
			rk.nama_pegawai,
			rp.nomor_hp,
			rk.nip_baru,
			rk.id_unor,
			acc.username,
			acc.passwd,
			acc.id_pegawai,
			acc.last_login,
			acc.active
		')
						->join('rekap_peg rk','rk.id_pegawai=acc.id_pegawai','left')
						->join('r_pegawai rp','rp.id_pegawai=acc.id_pegawai','left')
						->get('account acc')
						->result_array();
	}
                                    //$user_id, $encrypted_passwd, $new_passwd,$am
	public function change_password($user_id,$old_passwd_encrypted, $new_passwd,$am)
	{
		// $this->load->model('m_api_simpeg');
		$data = [
			'passwd' => md5($this->config->item('encryption_key').$new_passwd.'manis-legi'.$new_passwd)
		];
		switch ($am->t) {
			case '0': // adm
				$this->db->where('id',$am->parent_id)->update('account_adm',$data);
				
				break; 
			case '2': // PEGAWAI
				// $data = [
				// 	'passwd' => md5(md5('$notimetodie$').$new_passwd)
				// ];
				
				// // do change remote passwd
				// $this->m_api_simpeg->update_password($am->parent_id,$old_passwd_encrypted,$data['passwd']);
				// $this->db->where('id',$am->parent_id)->update('account',$data);
				
				break;
			case '1': // non
				$this->db->where('id',$am->parent_id)->update('account_register',$data);
				# code...
				break;  
		}
		return true;
	}
	public function change_profile($user_id, $nama, $email, $nomor_hp, $foto ,$am)
	{
		$this->load->model('m_api_simpeg');

		switch ($am->t) {
			case '1': // non
				
				$data = [
					'nama_lengkap' => $nama,
					'email' => $email,
					'nomor_hp' => $nomor_hp
				];

				$this->db->where('id',$am->parent_id)->update('account_register',$data);
				break; 
			case '2': // PEGAWAI
				$data = [];
				
				
				if(!empty($email)){
					$data['email'] = $email;
					$this->db->where('id',$am->parent_id)->update('account',$data);
				}

				 
				$account = $this->db->where('id',$am->parent_id)->get('account')->row();
				$this->m_api_simpeg->update_profile($account->id_pegawai,$nomor_hp);
				
				break;
			case '0': // adm
				$data = [];
				
				
				if(!empty($email)){
					$data['email'] = $email;
				}
				if(!empty($nama)){
					$data['nama_lengkap'] = $nama;
				}
				

				$this->db->where('id',$am->parent_id)->update('account_adm',$data);

				if(!empty($nomor_hp)){

					$user_info = $this->db->where('user_id',$user_id)
										  ->where('key','nomor_hp')
										  ->get('user_info')
										  ->row();
					if(!empty($user_info)){
						$this->db->where('user_id',$user_id)
								 ->where('key','nomor_hp')
								 ->update('user_info',['value'=>$nomor_hp]);
					}else{
						$this->db->insert('user_info',[
							'key' => 'nomor_hp',
							'value' => $nomor_hp,
							'user_id' => $user_id
						]);	
					}
					 
				}
				break;  
		}
		return true;
	}

	public function get_row($group_id,$user_id,$account_id,$account_only=false)
	{
		$this->load->config('ppsl');

		$table_maps = [
			'0' => 'account_adm',
			'1' => 'account_register',
			'2' => 'account'
		];

		$table_name = $table_maps[$group_id];
		$account = $this->db->where('id',$account_id)->get($table_name)->row();

		if($account_only){
			return $account;
		}
		$default_photo_url = $this->config->item('ppsl_base_url').'themes/metronic/assets/pages/media/profile/profile_user.png';
		$account->photo_url = $default_photo_url;
		$account->photo_thumb_url = $default_photo_url;

		$account->alamat = '';
		// $account->nomor_hp = '';
		
		$account->sql = $this->db->last_query();

		if($account->email == '0'){
            $account->email = '';
        }
		// ADMIN
		if($group_id === '0'){
			// GET FROM FILES => json_decode if exists
			// GET nomor_hp
			$user_info = $this->db->select('value')
										  ->where('user_id',$user_id)
										  ->where('key','nomor_hp')
										  ->get('user_info')
										  ->row();
			if(!empty($user_info)){
				$account->nomor_hp = $user_info->value;
			}

			$upload = $this->db->where('user_id',$user_id)
							   ->where('rules','photo_profile')
							   ->where('owner',$user_id)
							   ->get('uploads')
							   ->row();
			if(!empty($upload)){
				$account->photo_url = $this->config->item('ppsl_base_url') . $upload->file_path;
				$account->photo_thumb_url = $account->photo_url;
			}				   
		}
		// NON
		if($group_id === '1'){
			$upload = $this->db->where('user_id',$user_id)
							   ->where('rules','photo_profile')
							   ->where('owner',$user_id)
							   ->get('uploads')
							   ->row();
			if(!empty($upload)){
				$account->photo_url = $this->config->item('ppsl_base_url') . $upload->file_path;
				$account->photo_thumb_url = $account->photo_url;
			}
		}
		// PEGAWAI
		if($group_id === '2'){
			$profile_pegawai = $this->m_api_simpeg->get_profile($account->id_pegawai);
			// GET FROM AVATAR FIELD => json_decode if exists

			// FETCH PHOTO PROFILE
			$account->photo_url = !empty($profile_pegawai->photo_url) ? $profile_pegawai->photo_url : $default_photo_url;
			$account->photo_thumb_url = !empty($profile_pegawai->thumb_url) ? $profile_pegawai->thumb_url : $default_photo_url;
			$account->nomor_hp = $profile_pegawai->nomor_hp;
			
		}

		return $account;
	}
	public function unique_nomor_hp_for_edit($nomor_hp,$group_id,$user_id,$account)
	{
		$valid_nomor_hp_unique = false;
		$this->load->model('m_api_simpeg');
		// STEP 1
        // PEGAWAI
        if($group_id == 2){
            $r = $this->m_api_simpeg->check_nomor_hp($account->id_pegawai, $nomor_hp);
            $valid_nomor_hp_unique = $r->success;
        } else {
            $r = $this->m_api_simpeg->check_nomor_hp('', $nomor_hp);
            $valid_nomor_hp_unique = $r->success;
        }
        // STEP 2
        if($valid_nomor_hp_unique){
            // check in account_register
            // NON
            if($group_id == 1){
                $this->db->where('id !=', $account->id);
            }
            $valid_nomor_hp_unique = $this->db->select("COUNT(id) _count")
                                          ->where('nomor_hp',$nomor_hp)
                                          ->get('account_register')
                                          ->row()->_count == 0;
  
        }
        // STEP 3
        if($valid_nomor_hp_unique){
            // check in user_info
            // NON
            if($group_id == 0){

                $this->db->where('user_id !=', $account->id);
            }
            $valid_nomor_hp_unique = $this->db->select("COUNT(*) _count")
                                          ->where('value',$nomor_hp)
                                          ->where('key','nomor_hp')
                                          ->get('user_info')
                                          
                                          ->row()->_count == 0;
            
        }

        return $valid_nomor_hp_unique;
	}

	public function unique_email_for_edit($email,$group_id,$user_id,$account)
	{
		$valid_email_unique = false;
		$log = '';
		// STEP 1
        // PEGAWAI
        if($group_id == 2){
        	$this->db->where('id !=',$account->id);
        }    
        $valid_email_unique = $this->db->select("COUNT(id) _count")
                                          ->where('email',$email)
                                          ->get('account')
                                          ->row()->_count == 0;
                                        
        // die($valid_email_unique);
        // STEP 2
        if($valid_email_unique){
            // check in account_register
            // NON
            if($group_id == 1){
                $this->db->where('id !=', $account->id);
            }
            $valid_email_unique = $this->db->select("COUNT(id) _count")
                                          ->where('email',$email)
                                          ->get('account_register')
                                          ->row()->_count == 0;
  			
        }

        // STEP 3
        if($valid_email_unique){
            // check in user_info
            // NON
            if($group_id == 0){

                $this->db->where('id !=', $account->id);
            }
            $valid_email_unique = $this->db->select("COUNT(id) _count")
                                          ->where('email',$email)
                                          ->get('account_adm')
                                          ->row()->_count == 0;
            
        }
        // die($valid_email_unique);
        return $valid_email_unique;
	}
}