<?php
require_once __DIR__ . '/Grocery_CRUD_Model.php';
class M_kutipan_c_detail  extends Grocery_CRUD_Model  {
	var $table = 'p_kutipan_c'; //nama tabel dari database
    var $form_yml_name = 'p_kutipan_c_detail';
    
    private function _get_datatables_query()
    {
        $this->db->select("a.*")
                 ->from($this->table . ' a');
        $i = 0;
        $search_queries = $_POST['search']['value'];
        $search_queries = explode(',', $search_queries);
        if(count($search_queries) > 0){
            foreach ($this->column_search as $item) // looping awal
            {
                if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
                {
                    if($i===0) // looping awal
                    {
                        $this->db->group_start(); 
                        $this->db->like($item, $_POST['search']['value']);
                    }
                    else
                    {
                        $this->db->or_like($item, $_POST['search']['value']);
                    }
                    if(count($this->column_search) - 1 == $i) 
                        $this->db->group_end(); 
                }
                $i++;
            }
        }
        if(isset($_POST['order'])) 
        {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;

            $this->db->order_by(key($order), $order[key($order)]);
        }
        $parent_id = $this->input->get('parent_id');
        if(!empty($parent_id)){
            $this->db->where('a.parent_id',$parent_id);
        }
        $session_search = $this->session->userdata('session_search_kutipan_c');

        if(is_array($session_search)){
           
            foreach ($session_search as $key => $value) {
                $value = trim($value);
                if(!empty($value)){
                    if(in_array($key,['a.d_sbb_tgl_ubah','a.s_sbb_tgl_ubah'])){
                        $this->db->like($key,$value,'%%');
                    }else{
                        $this->db->where($key,$value);
                    }
                    
                }
            }
        }
    }
    public function get_min_max_order($gid="")
    {
        $parent_id = $this->input->get('parent_id');
        if(!empty($parent_id)){
            $parent_id = $gid;
            $this->db->where('a.parent_id',$parent_id);
        }
        $mmo = $this->db->select("MIN(a.order) _min,MAX(a.order) _max")
                        ->get($this->table.' a')
                        ->row(); 
        $mmo->_min += 0;      
        $mmo->_max += 0;

        return $mmo;      
    }
    function get_count_by_group($parent_id){
        if(!empty($parent_id)){
            $this->db->where('a.parent_id',$parent_id);
        }
        return $this->db->select("COUNT(a.id) _cx")
                        ->get($this->table.' a')
                        ->row()->_cx + 0; 
    }
    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    public function count_all()
    {
        $this->db->from($this->table);
        $parent_id = $this->input->get('parent_id');
        if(!empty($parent_id)){
            $this->db->where('a.parent_id',$parent_id);
        }
        $session_search = $this->session->userdata('session_search_kutipan_c');
        if(is_array($session_search)){
            $session_search = [];
            foreach ($session_search as $key => $value) {
                $value = trim($value);
                if(!empty($value)){
                     if(in_array($key,['a.d_sbb_tgl_ubah','a.s_sbb_tgl_ubah'])){
                        $this->db->like($key,$value,'%%');
                    }else{
                        $this->db->where($key,$value);
                    }
                }
            }
        }
        return $this->db->select("COUNT(a.id) _cx")
                        ->get($this->table.' a')
                        ->row()->_cx + 0;
    }
}
