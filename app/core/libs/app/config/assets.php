<?php
foreach (ASSETS_YML as $key => $value) {
	$config[$key] = $value;
}
$config['assets_top_js'] = [
	"{{ theme_assets }}/global/plugins/jquery.min.js",
	"pub/gc/js/jquery_plugins/jquery-migrate-1.1.0.min.js",
	"pub/js/vuejs2/axios.min.js",
	"pub/js/vuejs2/vue.min.js",
];
$config['assets_top_css'] = [
	"{{ theme_assets }}global/plugins/font-awesome/css/font-awesome.min.css",
	"{{ theme_assets }}global/plugins/font-awesome/css/font-awesome.min.css",
	"{{ theme_assets }}global/plugins/web-icons/web-icons.min.css",
	"{{ theme_assets }}global/plugins/simple-line-icons/simple-line-icons.min.css",
	"{{ theme_assets }}global/plugins/bootstrap/css/bootstrap.min.css",
	"{{ theme_assets }}global/plugins/bootstrap-switch/css/bootstrap-switch.min.css",
	"{{ theme_assets }}global/plugins/select2/css/select2.min.css",
	"{{ theme_assets }}global/plugins/select2/css/select2-bootstrap.min.css",
	"{{ theme_assets }}global/plugins/bootstrap-summernote/summernote.css",
	"{{ theme_assets }}global/plugins/bootstrap-fileinput/bootstrap-fileinput.css",
	"{{ theme_assets }}global/plugins/datatables/datatables.min.css",
	"{{ theme_assets }}global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css",
	"{{ theme_assets }}global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css",
	"{{ theme_assets }}global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css",
	"{{ theme_assets }}global/plugins/typeahead/typeahead.css",
	"{{ theme_assets }}global/plugins/icheck/skins/all.css",
	"{{ theme_assets }}global/plugins/bootstrap-sweetalert/sweetalert.css",
	"{{ theme_assets }}global/css/components.min.css",
	"{{ theme_assets }}global/css/plugins.min.css",
	"{{ theme_assets }}pages/css/profile.min.css",
	"{{ theme_assets }}apps/css/ticket.min.css",
	"{{ theme_assets }}layouts/layout/css/layout.min.css",
	"{{ theme_assets }}layouts/layout/css/themes/darkblue.min.css",
	"{{ theme_assets }}layouts/layout/css/custom.min.css",
];

$config['assets_bottom_js']=[
	"{{ theme_assets }}/global/plugins/bootstrap/js/bootstrap.min.js",
	"{{ theme_assets }}/global/plugins/js.cookie.min.js",
	"{{ theme_assets }}/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js",
	"{{ theme_assets }}/global/plugins/jquery.blockui.min.js",
	"{{ theme_assets }}/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js",
	"{{ theme_assets }}/global/plugins/select2/js/select2.full.min.js",

	"{{ theme_assets }}/global/plugins/bootstrap-summernote/summernote.js",
	"{{ theme_assets }}/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js",
	"{{ theme_assets }}/global/plugins/bootstrap-datepicker/js/locales/bootstrap-datepicker.id.min.js",
	"{{ theme_assets }}/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js",
	"{{ theme_assets }}/global/plugins/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.id.js",

	"{{ theme_assets }}/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js",
	"{{ theme_assets }}/global/plugins/jquery.sparkline.min.js",
	"{{ theme_assets }}/global/scripts/datatable.js",
	"{{ theme_assets }}/global/plugins/datatables/datatables.min.js",
	"{{ theme_assets }}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js",
	"{{ theme_assets }}/global/plugins/typeahead/handlebars.min.js",
	"{{ theme_assets }}/global/plugins/typeahead/typeahead.bundle.min.js",
	"{{ theme_assets }}/global/plugins/bootstrap-sweetalert/sweetalert.min.js",
	"{{ theme_assets }}/global/scripts/app.min.js",
	"{{ theme_assets }}/pages/scripts/profile.min.js",
	"{{ theme_assets }}/pages/scripts/table-datatables-managed.min.js",
	"{{ theme_assets }}/layouts/layout/scripts/layout.min.js",
	"{{ theme_assets }}/layouts/layout/scripts/demo.min.js",
	"{{ theme_assets }}/layouts/global/scripts/quick-sidebar.min.js",
	"{{ theme_assets }}/layouts/global/scripts/quick-nav.min.js",

];