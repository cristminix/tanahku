<script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
<script crossorigin src="https://cdnjs.cloudflare.com/ajax/libs/babel-standalone/6.26.0/babel.min.js"></script>

<script type="text/babel"  src="<?=site_url()?>pub/react/my_user_info/SimpleReactFileUpload.jsx"></script>
<script type="text/babel">
ReactDOM.render(
	<React.StrictMode>
		<SimpleReactFileUpload user_id={<?=$am['user_id']?>} foto={'<?=$foto?>'} group_id={<?=$am['t']?>}/>
	</React.StrictMode>, 
document.getElementById('root'));
</script>
<div class="actions" style="
    position: absolute;
    margin: -38px 0 0 0;
    right: 0;
    margin-right: 40px;
    display: none;
    "><a onclick="kembalilah()" class="btn btn-info btn-sm"><i class="fa fa-times"></i> Tutup</a></div>
<script type="text/javascript">
    const kembalilah = function(){
    history.back()
}
</script>    
<div class="row">

    <div class="col-md-12">
        <!-- BEGIN PROFILE SIDEBAR -->
        <div class="profile-sidebar" style="width: 400px">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet ">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                <div id="root"></div>
                    

                    
                </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"> <?=$am['nama_lengkap']?></div>
                    <div class="profile-usertitle-job"> <?=$am['t'] == 0 ? 'Admin' : 'Marketing'?> </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
                <!-- SIDEBAR BUTTONS -->
                <div class="profile-userbuttons" style="display: none;">
                    
                </div>
                <!-- END SIDEBAR BUTTONS -->
                <!-- SIDEBAR MENU -->
                <div class="profile-usermenu" style="display: none;">
                    
                </div>
                <!-- END MENU -->
            </div>
            <!-- END PORTLET MAIN -->
            <!-- PORTLET MAIN -->
            <div class="portlet light " style="display: none">
                <!-- STAT -->
                <div class="row list-separated profile-stat">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="uppercase profile-stat-title"> <?=$am['prospek']?> </div>
                        <div class="uppercase profile-stat-text"> Prospek </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="uppercase profile-stat-title"> <?=$am['survey']?> </div>
                        <div class="uppercase profile-stat-text"> Survey </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="uppercase profile-stat-title"> <?=$am['pelanggan']?> </div>
                        <div class="uppercase profile-stat-text"> Pelanggan </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="uppercase profile-stat-title"> <?=$am['batal']?> </div>
                        <div class="uppercase profile-stat-text"> Batal </div>
                    </div>
                </div>
                <!-- END STAT -->
                <div style="display: none">
                    
                </div>
            </div>
            <!-- END PORTLET MAIN -->
        </div>
        <!-- END BEGIN PROFILE SIDEBAR -->
        <!-- BEGIN PROFILE CONTENT -->
        <div class="profile-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light ">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Akun Profil</span>
                            </div>
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_1" data-toggle="tab">Info Pribadi</a>
                                </li>
                                <li>
                                    <a href="#tab_1_2" data-toggle="tab">Kata Sandi</a>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <div class="tab-pane " id="tab_1_2">
                                    <form id="form_password" role="form" action="{{ site_url }}account/changePassword" method="post">
                                        <div class="form-group">
                                            <label class="control-label" style="font-weight: bold;line-height: 22px;">Kata Sandi Lama</label>
                                            <input type="password" name="old_password" class="form-control" style="border-radius: 9px !important;"  />   
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" style="font-weight: bold;line-height: 22px;">Kata Sandi Baru</label>
                                            <input type="password" name="new_password" class="form-control" style="border-radius: 9px !important;"  />   
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" style="font-weight: bold;line-height: 22px;">Ulang Kata Sandi Baru</label>
                                            <input type="password" name="repeat_new_password" class="form-control" style="border-radius: 9px !important;"  />   
                                        </div>
                                        <div class="form-group" style="padding: 1em; text-align: right;">
                                            <button class="btn btn-success"><i class="fa fa-save"></i> Ubah Kata Sandi</button>
                                        </div>
                                    </form>
                                </div>

                                <!-- PERSONAL INFO TAB -->
                                <div class="tab-pane active" id="tab_1_1">
                                    <form id="form_profile" role="form" action="{{ site_url }}account/updateProfile" method="post">
                                        <div class="form-group">
                                            <label class="control-label" style="font-weight: bold;line-height: 22px;">Nama Lengkap</label>
                                            <?if($am['t']==2):?>
                                            <div class="form-control" style="border-radius: 9px !important;"><?=$account['nama_lengkap']?></div>
                                            <?else:?>
                                            <input type="text" name="nama" class="form-control" style="border-radius: 9px !important;" value="<?=($account['nama_lengkap'])?>" />    

                                            <?endif?>
                                        <!-- <div class="form-group">
                                            <label class="control-label" style="font-weight: bold;line-height: 22px;">NIP/NIK</label>
                                            <div class="form-control" style="border-radius: 9px !important;"><?=$am['nip_nik']?></div>
                                        <div class="form-group">
                                            <label class="control-label" style="font-weight: bold;line-height: 22px;">Alamat</label>
                                            <div class="form-control" style="border-radius: 9px !important;"><?=$account['alamat']?></div> -->
                                        <div class="form-group">
                                            <label class="control-label" style="font-weight: bold;line-height: 22px;">Email</label>
                                            <input type="email" id="email" name="email" class="form-control" style="border-radius: 9px !important;" value="<?=($account['email'])?>" />
                                        <div class="form-group">
                                            <label class="control-label" style="font-weight: bold;line-height: 22px;">Nomor HP</label>
                                            <input type="text" class="form-control" style="border-radius: 9px !important;" value="<?=($account['nomor_hp'])?>" name="nomor_hp" id="nomor_hp"/>    
                                        <div class="form-group" style="padding: 1em; text-align: right;">
                                            <button class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                                        </div>       
                                        </form>
                                </div>
                                <!-- END PERSONAL INFO TAB -->
                                <!-- CHANGE AVATAR TAB -->
                                 
                                <!-- END CHANGE AVATAR TAB -->
                                <!-- CHANGE PASSWORD TAB -->
                                 
                                <!-- END CHANGE PASSWORD TAB -->
                                <!-- PRIVACY SETTINGS TAB -->
                                
                                <!-- END PRIVACY SETTINGS TAB -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PROFILE CONTENT -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#form_profile').submit(function(e){
            try{e.preventDefault();}catch(e){console.log(e)}

            const url = this.action;
            let formData = new FormData();
            const user_id = <?=$am['user_id']?>;
            formData.append('user_id',user_id);
            const email = $('input#email').val();
            if(email.length>0)
                formData.append('email',email);
            const nomor_hp = $('input#nomor_hp').val();
            if(nomor_hp.length > 0)            
                formData.append('nomor_hp',nomor_hp);
            <?if($am['t'] != 2):?>
            const nama = $('input[name=nama]').val();
            formData.append('nama',nama);
            <?endif?>
            App.startPageLoading({animate:true});
            axios.post(url,formData).then((res)=>{
                console.log(res)

                if(res.data.success){
                    swal('Edit Profil Sukses !')
                }else{
                    swal(res.data.message);
                }
                App.stopPageLoading();
            }).catch((err)=>{
                swal(err);
                App.stopPageLoading();

            })

            return false;
        });

        $('#form_password').submit(function(e){
            try{e.preventDefault();}catch(e){console.log(e)}

            const url = this.action;
            let formData = new FormData();
            const user_id = <?=$am['user_id']?>;
            formData.append('user_id',user_id);
            const old_password = $('input[name=old_password]').val();
            formData.append('old_password',old_password);
            const new_password = $('input[name=new_password]').val();
            formData.append('new_password',new_password);
            const repeat_new_password = $('input[name=repeat_new_password]').val();
            formData.append('repeat_new_password',repeat_new_password);
            App.startPageLoading({animate:true});
            axios.post(url,formData).then((res)=>{
                console.log(res)

                if(res.data.success){
                    swal('Ubah Kata Sandi Sukses !')
                }else{
                    swal(res.data.message);
                }
                App.stopPageLoading();
            }).catch((err)=>{
                swal(err);
                App.stopPageLoading();

            })

            return false;
        });
    });
</script>

<style>
#changeAvaBtn{
    position: absolute;
    margin: -41px 3px 8px 227px;
    border: none;
    border-radius: 50% !important;
    background: #eee;
    padding: 4px 8px;
}
#percentage{
    position: absolute;
    margin: -100px 0px 8px 196px;
}
#fileInput{
    position: absolute;
    margin: -38px 0px 8px 196px;
    opacity:0;
}
</style>