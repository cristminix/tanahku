<div class="panel panel-danger" style="width: 80%">
<div v-if="detail!= null && form.mode != 'edit'" >
	<div class="panel-header">
		<div class="row" style="padding: 1em">
			<div class="col-md-8">
				<h4 class="title" v-if="form.mode=='edit'"><i class="fa fa-edit"></i> Ubah Pengguna</h4>
				<h4 class="title" v-if="form.mode=='delete'"><i class="fa fa-trash"></i> Hapus Pengguna</h4>
			</div>
			<div class="col-md-4" style="text-align: right;">
				<button class="btn btn-default btn-xs" @click="backToGrid()"><i class="fa fa-close"></i></button>
			</div>
		</div>
	</div>
	<div class="panel-body" style="padding: 1em">
	<div class="row">
		<div class="form-group">
			<label class="col-md-4">Nama Pengguna</label>
			<div class="col-md-8">
				<input type="text" name="nama_lengkap" v-model="detail.nama_lengkap" class="form-control">
				
			</div>
		</div>
	</div>
	<div class="row">
		<div class="form-group">
			<label class="col-md-4">Email</label>
			<div class="col-md-8">
				<input type="text" name="email" v-model="detail.email" class="form-control">
				
			</div>
		</div>
	</div>
	<div class="row">
		<div class="form-group">
			<label class="col-md-4">Jenis</label>
			<div class="col-md-8">
				<input type="text" name="jenis" v-model="detail.jenis" class="form-control">
				
			</div>
		</div>
	</div>
	<div class="row">
		<div class="form-group">
			<div class="button-group" style="padding: 1em">
				<button class="btn btn-default" @click="backToGrid()"><i class="fa fa-close"></i> Batal</button>				
				<button class="btn btn-success" v-if="form.mode!='delete'"><i class="fa fa-save"></i> Simpan</button>				
				<button class="btn btn-danger" v-if="form.mode=='delete'" @click="formSubmit(detail)"><i class="fa fa-trash"></i> Hapus</button>				
			</div>
		</div>
	</div>
	</div>
</div>
</div>