<script type="text/javascript">
	const app = {
		data : <?=json_encode($app_data)?>,
		vm   : {},
		site_url : (path)=>{return `<?=site_url()?>${path}`},
		link : {
			pegawai_aktif : '<?=site_url('')?>'
		}
	};
</script>
<link rel="stylesheet" type="text/css" href="<?=site_url('pub/css/snackbar.css')?>">
<script type="text/javascript">
	function snackbar(text,tm) {
	  tm = tm || 3000;
	  // Get the snackbar DIV
	  var x = document.getElementById("snackbar");
	  x.innerHTML=text;
	  // Add the "show" class to DIV
	  x.className = "show";

	  // After 3 seconds, remove the show class from DIV
	  setTimeout(function(){ x.className = x.className.replace("show", ""); }, tm);
	}
</script>
<div id="snackbar"></div>
<div id="app">
	<div class="row">
		<div class="col-lg-12">
			<h3 class="page-header">{{sub_title}}</h3>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<div id="content-wrapper">
		<div id="pensiun_grid" v-show="state.grid_shown">
			<!-- <div class="row top-nav">
				<div class="col-lg-12">
					<div class="btn-group pull-right">
						<a class="btn btn-primary btn-xs" href="javascript:;" v-on:click="goBack()"><i class="fa fa-fast-backward fa-fw"></i> Kembali</a>
					</div>
				</div>
			</div> -->
			<div class="row">
				<div class="col-lg-12">
					<div class="panel panel-default">
						<div class="panel-heading">
							<div class="row">
								<div class="col-lg-6">
									<div class="dropdown-non">
										<button class="btn btn-primary dropdown-toggle btn-xs" type="button" id="ddMenuX" data-toggle="dropdown">
										<span class=" icon-user"></span>
										</button>
										<!-- <ul class="dropdown-menu" role="menu" aria-labelledby="ddMenuX">
											<li><a href="javascript:;" @click="formAdd();return false;"><i class="fa fa-plus fa-fw"></i> Tambah Data</a></li>
											<li role="presentation" class="divider">
											</li>
											<li><a href="javascript:;" @click="printExcel();return false;"><i class="fa fa-print fa-fw"></i> Cetak Daftar</a></li>
										</ul> -->
										{{sub_title}}
									</div>
								</div>
								<!-- <div class="col-lg-6 c-opsi">
									<div class="btn-group pull-right">
										<button class="btn btn-primary btn-xs" type="button"  v-on:click="toggleFilter();"><i v-bind:class="{'fa fa-fw':1,'fa-caret-down':!config.filter_open,'fa-caret-up':config.filter_open}"></i></button>
									</div>
								</div> -->
							</div>
							<div class="row filter-cnt" v-show="config.filter_open">
								<div class="col-lg-6">
									<div class="panel panel-default">
										<div class="panel-body">
											<div class="form-group">
												<label>Jenis Pengguna:</label>
												<select v-model="filter.jenis_pengguna" class="form-control" v-on:change="onFilterChanged()">
													<option value="">Semua...</option>
													<option v-for="(i,j) in filter_data.jenis_pengguna" v-bind:value="i">{{j}}</option>
												</select>
												<!--./form-group-->
											</div>
											<!--./panel-body-->
										</div>
										<!--./panel panel-default-->
									</div>
									<!--./col-->
								</div>
								<div class="col-lg-6">
									<div class="panel panel-default">
										<div class="panel-body">
											<div class="form-group">
												<label>Status:</label>
												<select v-model="filter.status_pengguna" class="form-control" v-on:change="onFilterChanged()">
													<option value="">Semua...</option>
													<option v-for="(nama,id) in filter_data.status_pengguna" v-bind:value="id">{{nama}}</option>
												</select>
											</div>
										</div>
									</div>
									<!--./col-->
								</div>
								<!-- ./row -->
							</div>
							<!-- ./panel-heading -->
						</div>
						<!--  -->
						<div class="panel-body grid-data">
							<!-- <div class="row">
								<div class="col-lg-6 per-page">
									
								</div>
								<div class="col-lg-6 search-cnt">
									<div class="input-group search-grp">
										<input v-model="filter.search_query" v-on:change="onGridSearch()" type="text" class="form-control" placeholder="Masukkan kata kunci..." value="<?='';?>">
										<span class="input-group-btn">
											<button class="btn btn-default" type="button" v-on:click="onGridSearch()">
											<i class="fa fa-search"></i>
											</button>
										</span>
									</div>
									<div class="search-caption">Cari:</div>
								</div>
							</div> -->
							<!--  -->
							<div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th class="t-no t-mid-ctr">No.</th>
											<th class="t-act pad0 t-mid-ctr">AKSI</th>
											<th class="t-foto pad0 t-mid-ctr">Nama Pengguna</th>
											<th class="t-ktr t-mid-ctr">Username,Email</th>
											<!-- <th class="t-det t-mid-ctr">Jenis</th> -->
											<th class="t-jbt t-mid-ctr">Status</th>
											
										</tr>
									</thead>
									<tbody id="list">
										<tr v-for="(item,index) in grid_data">
											<td class="t-no">{{index +1}}.</td>
											<td>
												<div class="dropdown">
													<button class="btn btn-default dropdown-toggle btn-xs" type="button" data-toggle="dropdown">
													<i class="fa fa-caret-down fa-fw"></i>
													</button>
													<ul class="dropdown-menu" role="menu">
														<li role="presentation" class="cpo">
															<a role="menuitem" tabindex="-1" @click="formEdit(item)">
															<i class="fa fa-edit fa-fw"></i> Edit Data</a>
														</li>
														<li role="presentation" class="cpo">
															<a role="menuitem" tabindex="-1" @click="formDelete(item)">
															<i class="fa fa-trash fa-fw"></i> Hapus Data</a>
														</li>
														<!-- <li role="presentation" class="divider"></li> -->
														<li role="presentation" class="cpo" v-show="item.jenis=='non'" v-show="item.is_active!=1">
															<a role="menuitem" tabindex="-1" @click="activateUser(item)">
															<i class="fa fa-envelope fa-fw"></i> Aktifkan Pengguna</a>
														</li>
													</ul>
												</div>
											</td>
											<td>{{item.nama_lengkap}}</td>
											<td>
												{{item.email}}
											</td>
											<!-- <td>{{item.jenis}} </td> -->
											<td><a v-if="item.is_active==1" class="user-status">Aktif</a>
												<a v-if="item.is_verified==1" class="user-status">Verified</a>
												<a v-if="item.is_active!=1" class="user-status">Inaktif</a>
												<a v-if="item.is_verified!=1" class="user-status">Unverified</a>
											</td>
											
										</tr>
										<tr v-if="grid_data.length==0">
											<td colspan="6">No Records</td>
										</tr>
									</tbody>
								</table>
								<!--./ table-responsive -->
							</div>
							<div id="paging" v-if="grid_data.length>0">
								<div class="page-text">Hal.</div>
								<div id="bhal" class="b-hal">
									<input id="inputpaging" type="text" class="input-page" size="2"
									v-model = "pager.input_page"
									@blur  = "inputPagingBlur"
									@focus ="inputPagingFocus"
									@change="goPaging(pager.input_page)" value="1">
								</div>
								<div class="page-text-mid">dari</div>
								<div id="bhalmax" class="page-text-mid">{{pager.page_count}}</div>
								<div class="btn-group paging-frame">
									<div class="btn btn-default active" v-if="!pager.has_prev">Prev</div>
									<div class="btn btn-default" @click="goPaging(pager.prev_page);" v-if="pager.has_prev">Prev</div>
									<div v-for="page in pager.page_to_display" v-bind:class="{'btn btn-default':1,'active':page==pager.current_page}" @click="goPaging(page);">{{page}}</div>
									<div class="btn btn-default" @click="goPaging(pager.next_page);" v-if="pager.has_next">Next</div>
									<div class="btn btn-default active"v-if="!pager.has_next">Next</div>
								</div>
							</div>
							<!-- ./panel-body -->
						</div>
						<!--  -->
					</div>
				</div>
				<!-- ./row -->
			</div>
		</div>
		<!-- #content-wraper -->
		<?=$form?>
		<?=$detail?>
		<div id="dl" v-if="dl.enable">
			<iframe v-bind:src="dl.src" style="display: none"></iframe>
		</div>
	</div>
	<!-- #app -->
</div>
<script type="text/javascript" src="<?=site_url()?>pub/js/management_user.js?nocache=<?=md5(microtime())?>"></script>
<link rel="stylesheet" type="text/css" href="<?=site_url()?>pub/css/management_user.css">

<style type="text/css">
	.user-status{
		background: pink;
		border-radius: 4px !important;
		margin: 0 1em;
		padding: 1px 4px;
		/*display: block;*/
		border: solid 1px #ddd; 
	}
</style>