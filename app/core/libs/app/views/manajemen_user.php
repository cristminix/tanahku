

<h2>Manajemen Pengguna</h2>

<div id="root"></div>


<!-- Load React. -->
<!-- Note: when deploying, replace "development.js" with "development.min.js". -->
<script crossorigin src="<?=site_url()?>pub/js/react.development.js"></script>
<script crossorigin src="<?=site_url()?>pub/js/react-dom.development.js"></script>
<script crossorigin src="https://cdnjs.cloudflare.com/ajax/libs/babel-standalone/6.26.0/babel.min.js"></script>
<!-- Load our React component. -->

<script type="text/babel"  src="<?=site_url()?>pub/react/manajemen_user/Context.jsx"></script>
<script type="text/babel"  src="<?=site_url()?>pub/react/manajemen_user/AddUser.jsx"></script>
<script type="text/babel"  src="<?=site_url()?>pub/react/manajemen_user/GetUsers.jsx"></script>
<script type="text/babel"  src="<?=site_url()?>pub/react/manajemen_user/Actions.jsx"></script>
<script type="text/babel"  src="<?=site_url()?>pub/react/manajemen_user/AppManagementUser.jsx"></script>
<script type="text/babel"  src="<?=site_url()?>pub/react/manajemen_user/index.jsx"></script>

<script type="text/javascript">
	$(document).ready(function(){
		// App = App||{};
		// App.getViewPort = function(){return window}
	});
function get_unor(){
	// return <?=json_encode($unor_list)?>;
}	
</script>

<style type="text/css">
	a.orange{
		color: orange;
	}
	a.orange:hover{
		color: maroon;
	}
	a.red{
		color: maroon;
	}
	a.red:hover{
		color: maroon;
	}
	.quick-nav{
		display: none !important;
	}
</style>