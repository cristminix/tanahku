<script src="{{ theme_assets }}global/plugins/highcharts/js/highcharts.js" type="text/javascript"></script>
<script src="{{ theme_assets }}global/plugins/highcharts/js/highcharts-3d.js" type="text/javascript"></script>
<script src="{{ theme_assets }}global/plugins/highcharts/js/highcharts-more.js" type="text/javascript"></script>
<div class="row">
	<div class="col-md-12">
		<h4>&nbsp;</h4>
		<div id="chart_container"></div>
	</div>
</div>

<script type="text/javascript">

$(document).ready(function(){
	// Build the chart
	$('#chart_container').highcharts({
	    chart: {
	        plotBackgroundColor: null,
	        plotBorderWidth: true,
	        plotShadow: true,
	        type: 'pie'
	    },
	    title: {
	        text: 'Rekap Data Sporadik'
	    },
	    tooltip: {
	        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
	    },
	    accessibility: {
	        point: {
	            valueSuffix: '%'
	        }
	    },
	    plotOptions: {
	        pie: {
	            allowPointSelect: true,
	            cursor: 'pointer',
	            dataLabels: {
	                enabled: false
	            },
	            showInLegend: true
	        }
	    },
	    series: [{
	        name: 'Dokumen',
	        colorByPoint: true,
	        data: <?=json_encode($series_data)?> 

	    }]
	});

	$('#page_breadcrumbs').html('<li><span>Dashboard</span></li>');

	setTimeout(()=>{
		$('svg > rect:first').attr('fill','rgba(255,255,255,0.7)');
	},2000);
});
</script> 
<style type="text/css">
	.page-content{
		background: transparent url({{ site_url }}pub/img/backround_balaidesa.jpg) top left no-repeat;
		background-size: cover;
	}
</style>