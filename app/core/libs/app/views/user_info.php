<div class="actions" style="
    position: absolute;
    margin: -38px 0 0 0;
    right: 0;
    margin-right: 40px;
    "><a onclick="kembalilah()" class="btn btn-info btn-sm"><i class="fa fa-times"></i> Tutup</a></div>
<script type="text/javascript">
    const kembalilah = function(){
    history.back()
}
</script>    
<div class="row">

    <div class="col-md-12">
        <!-- BEGIN PROFILE SIDEBAR -->
        <div class="profile-sidebar" style="width: 400px">
            <!-- PORTLET MAIN -->
            <div class="portlet light profile-sidebar-portlet ">
                <!-- SIDEBAR USERPIC -->
                <div class="profile-userpic">
                    <img src="<?=$account['photo_url']?>" class="img-responsive" alt="Foto <?=$am['nama_lengkap']?>"> </div>
                <!-- END SIDEBAR USERPIC -->
                <!-- SIDEBAR USER TITLE -->
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"> <?=$am['nama_lengkap']?></div>
                    <div class="profile-usertitle-job"> <?=$am['t'] == 0 ? 'Admin' : 'Marketing'?> </div>
                </div>
                <!-- END SIDEBAR USER TITLE -->
               
                
            </div>
            <!-- END PORTLET MAIN -->
            <!-- PORTLET MAIN -->
            <div class="portlet light ">
                <!-- STAT -->
                <div class="row list-separated profile-stat">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="uppercase profile-stat-title"> <?=$am['prospek']?> </div>
                        <div class="uppercase profile-stat-text"> Prospek </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="uppercase profile-stat-title"> <?=$am['survey']?> </div>
                        <div class="uppercase profile-stat-text"> Survey </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="uppercase profile-stat-title"> <?=$am['pelanggan']?> </div>
                        <div class="uppercase profile-stat-text"> Pelanggan </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <div class="uppercase profile-stat-title"> <?=$am['batal']?> </div>
                        <div class="uppercase profile-stat-text"> Batal </div>
                    </div>
                </div>
                <!-- END STAT -->
                <div style="display: none">
                     
                </div>
            </div>
            <!-- END PORTLET MAIN -->
        </div>
        <!-- END BEGIN PROFILE SIDEBAR -->
        <!-- BEGIN PROFILE CONTENT -->
        <div class="profile-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light ">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Profil Akun</span>
                            </div>
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_1" data-toggle="tab">Info Pribadi</a>
                                </li>
                                
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <!-- PERSONAL INFO TAB -->
                                <div class="tab-pane active" id="tab_1_1">
                                    <form role="form" action="#">
                                        <div class="form-group">
                                            <label class="control-label" style="font-weight: bold;line-height: 22px;">Nama Lengkap</label>
                                            <div class="form-control" style="border-radius: 9px !important;"><?=$am['nama_lengkap']?></div>
                          <!--               <div class="form-group">
                                            <label class="control-label" style="font-weight: bold;line-height: 22px;">NIP/NIK</label>
                                            <div class="form-control" style="border-radius: 9px !important;"><?=$am['nip_nik']?></div>
                                        <div class="form-group">
                                            <label class="control-label" style="font-weight: bold;line-height: 22px;">Alamat</label>
                                            <div class="form-control" style="border-radius: 9px !important;"><?=$am['alamat']?></div> -->
                                        <div class="form-group">
                                            <label class="control-label" style="font-weight: bold;line-height: 22px;">Email</label>
                                            <div class="form-control" style="border-radius: 9px !important;"><?=$account['email']?></div>
                                        <div class="form-group">
                                            <label class="control-label" style="font-weight: bold;line-height: 22px;">Nomor HP</label>
                                            <div class="form-control" style="border-radius: 9px !important;"><?=$account['nomor_hp']?></div>    
                                    </form>
                                </div>
                                <!-- END PERSONAL INFO TAB -->
                                <!-- CHANGE AVATAR TAB -->
                                 
                                <!-- END CHANGE AVATAR TAB -->
                                <!-- CHANGE PASSWORD TAB -->
                                 
                                <!-- END CHANGE PASSWORD TAB -->
                                <!-- PRIVACY SETTINGS TAB -->
                                
                                <!-- END PRIVACY SETTINGS TAB -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PROFILE CONTENT -->
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        let nama_marketing = '<?=$am['nama_lengkap']?>';
        let breadcrumbs = `
        <ul id="page_breadcrumbs" class="page-breadcrumb">
            <li>
                <a href="{{ site_url }}dashboard">
                    <span>Dashboard</span></a> <i class="fa fa-circle"></i>
                </li>       
            </li>
            <li>
                <a href="{{ site_url }}manajemen/user">
                    <span>Manajemen User</span></a> <i class="fa fa-circle"></i>
                
            </li>
            <li>
                <a href="{{ site_url }}manajemen/user/info">
                    <span>Detail</span></a> <i class="fa fa-circle"></i>
                
            </li>
            <li>
                <span>${nama_marketing}</span>
            </li>
        </ul>
        `;
        $('#page_breadcrumbs').replaceWith(breadcrumbs);
    });
</script>