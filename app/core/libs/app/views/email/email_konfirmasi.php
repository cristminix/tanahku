<!DOCTYPE html>
<html>
<head>
	<title>Verifikasi Email</title>

	<style type="text/css">

		.letter{
			width: 800px;
			margin: 0 auto;

			border: solid 1px #000;
			padding: 1em
		}
		.btn{
			text-transform: uppercase;
			background: blue;
			color: #fff;
			text-decoration: none;
			padding: .5em 1em;
			margin:1em auto;
			display: block;
			font-weight: bold;
			width: 300px;
			text-align: center;
		}
		.header{
			text-align: center;
			border-bottom: solid 1px #000;
		}
		.footer{
			border-top: solid 1px #000;
			text-align: center;
			padding: 1em 1em 0 1em;
		}
		a{
			text-decoration: none;
		}
	</style>
</head>
<body>
<div class="letter">
	<header class="header">
		<img src="<?=site_url()?>themes/metronic/assets/img/mail-header-logo.png" style="width:128px">
		<h3 style="font-size: 40px;margin-top: 1px;">PERUMDAM TKR</h3>
	</header>
	<div class="body">
		<p style="font-size: 130%">Hai <b>{{ nama_lengkap }},</b></p>
		<p>Selamat akun Anda sudah aktif, Anda bisa login ke aplikasi web dan aplikasi android Pendataan Potensi Sambungan Langganan (PPSL) Perumdam Tirta Kerta Raharja dengan menggunakan email dan sandi yang telah anda daftrarkan.</p>
		<p>Silahkan download dan install aplikasi android dengan klik tombol dibawah ini :</p>
		<a class="img img-responsive" href="{{ link_donwload_app }}"><img src="<?=site_url()?>pub/img/google-play-badge.png"></a>
		<p>Link login aplikasi web : {{ link_login }}</p>
		 
		<p>
			Salam
		</p>
		<p>
			<b>PERUMDAM TKR</b>
		</p>
	</div>
	<footer class="footer">
		&copy; 2020 PERUMDAM Tirta Kerta Raharja All rights reserved.
	</footer>
</div>
</body>
</html>