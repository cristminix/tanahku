<!DOCTYPE html>
<html>
<head>
	<title>Transaksi Kode {{ kode_pelanggan }} telah dibatalkan.</title>

	<style type="text/css">

		.letter{
			width: 800px;
			margin: 0 auto;

			border: solid 1px #000;
			padding: 1em
		}
		.btn{
			text-transform: uppercase;
			background: blue;
			color: #fff;
			text-decoration: none;
			padding: .5em 1em;
			margin:1em auto;
			display: block;
			font-weight: bold;
			width: 300px;
			text-align: center;
		}
		.header{
			text-align: center;
			border-bottom: solid 1px #000;
		}
		.footer{
			border-top: solid 1px #000;
			text-align: center;
			padding: 1em 1em 0 1em;
		}
		a{
			text-decoration: none;
		}
	</style>
</head>
<body>
<div class="letter">
	<header class="header">
		<img src="<?=site_url()?>themes/metronic/assets/img/mail-header-logo.png" style="width:128px">
		<h3 style="font-size: 40px;margin-top: 1px;">PERUMDAM TKR</h3>
	</header>
	<div class="body">
		<p style="font-size: 130%">Hai <b>{{ nama_lengkap }},</b></p>
		<p>Sayangya Transaksi Kode {{ kode_pelanggan }} telah dibatalkan.</p>
		<p>Berikut Detail Data : </p>
		<p>
			<table>
				<tbody>
					<tr>
						<td>Nama</td>
						<td>: {{ nama_pelanggan }}</td>
					</tr>
					<tr>
						<td>Kode</td>
						<td>: {{ kode_pelanggan }}</td>
					</tr>
					<tr>
						<td>Daya Listrik</td>
						<td>: {{ daya_listrik }} VA</td>
					</tr>
					<tr>
						<td>Cabang</td>
						<td>: {{ cabang }}</td>
					</tr>
					<tr>
						<td>Alamat</td>
						<td>: {{ alamat }}</td>
					</tr>
					<tr>
						<td>RT/RW / Nomor Rumah</td>
						<td>: {{ rt_rw_no_rumah }}</td>
					</tr>
					<tr>
						<td>Kelurahan / Kecamatan / Kabupaten</td>
						<td>: {{ kelurahan_kecamatan_kabupaten }}</td>
					</tr>
				</tbody>
			</table>
		</p> 
		<p>
			Salam
		</p>
		<p>
			<b>PERUMDAM TKR</b>
		</p>
	</div>
	<footer class="footer">
		&copy; 2020 PERUMDAM Tirta Kerta Raharja All rights reserved.
	</footer>
</div>
</body>
</html>