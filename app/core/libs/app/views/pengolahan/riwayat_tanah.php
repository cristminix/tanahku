 <script src="{{ theme_assets }}global/plugins/typeahead/handlebars.min.js" type="text/javascript"></script>
 <script src="{{ theme_assets }}global/plugins/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>
<h4>Riwayat Tanah <?=$group_title?></h4>
<h5>Silahkan Pilih Group Riwayat Tanah (Persil/Kls/Blok) :</h5>
<div id="select_kecamatan" style="padding: 0 0 1em 0">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
            <div class="col-md-8" style="padding: 0"> <input type="text" name="q" class="form-control" id="cari_aja" value="<?=$group_title?>"></div>    
            <div class="col-md-4" style="padding: 0">
                <?if(!empty($group_id)):?>
                <button onclick="clearGroup()" class="btn btn-warning"><i class="fa fa-eraser"></i></button>
                <?endif?>    
            </div>    
           
            
            </div>
        </div>
    
    </div>
</div>
<iframe src="" id="export_excel" style="display: none"></iframe> 
<div class="modal" tabindex="-1" id="detail_riwayat_tanah">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body" style="padding-top: 0">
                <div class="row">
                    <div class="col-md-12" style="background:#2b3643;padding:.5em  ">
                        <h4><a style="margin-right: 1em" data-dismiss="modal"><i class="fa fa-chevron-left"></i></a> <span style="color: #fff">Isi Data</span></h4>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-12">
                         <div id="lihat-riwayat_tanah" style="padding: 1em">
                            Memuat Detail 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<style type='text/css'>
body
{
   /* font-family: Arial;
    font-size: 14px;*/
}

a:hover
{
    text-decoration: underline;
}
th.action{
	width 200px !important;
}
span.select2{
	width: 100
}
table.table > thead > tr > th.text-center{
    vertical-align: top;
}
</style>
<?php echo $output; ?>
<script type="text/javascript">
    if('<?=$group_title?>' != ''){
        $('#page_breadcrumbs').append('<li><span><?=$group_title?></span></li>')
    }
    clearGroup = ()=>{
         clearSessionSearch(()=>{
            document.location.href = site_url()+`pengolahan/riwayat-tanah/`;
        });   
    };

    gc.FormDef = <?=json_encode($_SERVER['FORM_DEF'])?>;
    function displayDetail(event,el) {
        // const el = event.target;
        console.log(el.href);
        const tr = $(el).closest('tr');
        console.log(tr)
        const url = el.href;
        $.post(url,(res)=>{
            console.log(res);
            $("#detail_riwayat_tanah").unbind('show.bs.modal');
            $("#detail_riwayat_tanah").on('show.bs.modal', function(){
                $('#lihat-riwayat_tanah').html(res);
            });
            $("#detail_riwayat_tanah").modal("show");
        });
        event.preventDefault();
        return false;
    }
    $('button').click(()=>{
        ref = $('#riwayat_tanah_table').DataTable();
        ref.ajax.reload();
    })
    $(document).ready(function(){
        window.app = new Vue({
            el:'#grid',
            data:{
                filter:{
                },
                is_admin : <?=$is_admin?'true':'false'?>,
                nama_marketing:'',
            },
            mounted(){
                let self = this;
                this.$nextTick(function(){
                    $('a.export_btn').click(function(){
                        self.export();
                    });
                });
            },
            methods:{
                export(){
                    console.log('export');
                    let param = JSON.stringify(this.filter);
                    let url_prxy = site_url()+'export/riwayat_tanah/excel?group_id=<?=$group_id?>';
                    $('iframe#export_excel').prop('src',url_prxy);
                },
                reloadData(){
                    var e = $.Event( "keypress", { which: 13 } );
                    $('input[type=search]').trigger(e);
                    e = $.Event( "keydown", { which: 13 } );
                    $('input[type=search]').trigger(e);
                    e = $.Event( "keyup", { which: 13 } );
                    $('input[type=search]').trigger(e);   
                }
            }
        });
    });

gc.table = 'p_riwayat';    
</script>
<script type="text/javascript" src="<?=site_url()?>pub/accordion_form/script.js"></script>
<link rel="stylesheet" type="text/css" href="<?=site_url()?>pub/accordion_form/style.css">
<script type="text/javascript">

     var e = new Bloodhound({
                datumTokenizer: function(e) {
                    // console.log(e)
                    return Bloodhound.tokenizers.whitespace(e.name)
                },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                local: <?=json_encode($group_list_tmp)?>
            });
            e.initialize(), $("#cari_aja").typeahead(null, {
                displayKey: "name",
                hint: false,
                source: e.ttAdapter(),

            }).on('typeahead:selected', function (e, data) {
          
            var slug = slugify(data.name);
            clearSessionSearch(()=>{
                document.location.href = site_url()+`pengolahan/riwayat-tanah/${data.id}/${slug}`;
            });
            

        });
</script>