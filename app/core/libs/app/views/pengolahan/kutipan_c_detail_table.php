<div class="general" id="vue_<?=md5($parent_id)?>">
    <table class="table table-bordered table-list table-kutipan-c-detail">
        <thead>
            <tr>
               <th rowspan="2" style="vertical-align: middle;text-align: center;">No.</th>			
              <th colspan="4" class="text-center">SAWAH</th>
              <th colspan="4" class="text-center">DARAT</th>
            	<th rowspan="2" style="vertical-align: middle;text-align: center;">ACT</th>
            </tr>
            <tr>
              <th class="text-center">Persil</th>
              <th class="text-center">Klas</th>
              <th class="text-center">Luas</th>
              <th class="text-center">Sebab Tanggal Ubah</th>
              <th class="text-center">Persil</th>
              <th class="text-center">Klas</th>
              <th class="text-center">Luas</th>
              <th class="text-center">Sebab Tanggal Ubah</th>
            </tr>
          </thead>
        <tbody>
        <tr v-for="row,index in json" v-bind:id="row.id">
            <td  style="background:#94A0B2!important;color:#fff;vertical-align:middle;text-align:center;width:10px">{{index+1}}</td>
            <td v-bind:style="{width:c.width}" v-if="c.type != 'hidden'" v-for="c,k in colDefParsed">
                <input v-if="c.type=='text'||c.type=='number'" v-bind:okey="k" @click="onInputClick(index)" @change="onInputChanged(row,index)" @keyup="onInputKeyUp(row,index)" class="form-control" v-bind:type="c.type" v-bind:maxlength="c.length" v-model="row[k]"/>
                <textarea v-if="c.type=='textarea'" v-bind:okey="k" @click="onInputClick(index)" @change="onInputChanged(row,index)" @keyup="onInputKeyUp(row,index)" class="form-control" v-bind:type="c.type" v-bind:maxlength="c.length" v-model="row[k]"></textarea>
            </td>
            <td class="act" style="text-align:center">
                <a @click="saveRow(row,index)" v-if="isDirty(index)" class="btn btn-sm btn-success" href="javascript:;"><i class="fa fa-check"></i></a>
                <a @click="delRow(row,index)" v-if="!isDirty(index)" class="btn btn-sm btn-danger" href="javascript:;"><i class="fa fa-trash"></i></a>
                <a @click="cancelRow(row,index)" v-if="!row.id" class="btn btn-sm btn-warning" href="javascript:;"><i class="fa fa-close"></i></a>
            </td>
        </tr>
        <tr v-if="json.length==0">
            <td :colspan="columnCount">Tidak ada data.</td>
        </tr>
        <tr>
            <td style="text-align:right" :colspan="columnCount">
                <a @click="addRow()" class="btn btn-sm btn-info" href="javascript:;"><i class="fa fa-plus"></i> Tambah </a>
            </td>
        </tr>
        </tbody>
    </table>
</div> 

<style type="text/css">
	input[type='number'] {
    -moz-appearance:textfield;
}
input[okey=s_luas],
input[okey=d_luas]{
	text-align: center;
}
input[okey=s_persil],
input[okey=d_persil],
input[okey=d_kelas],
input[okey=s_kelas]{
	text-align: center !important;
}
table.table-kutipan-c-detail td{
	padding: 2px !important;
 }
 table.table-kutipan-c-detail td textarea,
 table.table-kutipan-c-detail td input{
 	padding: 2px;
 	font-size: 90%;
 	height: 34px;
 	border: none;

 }
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    -webkit-appearance: none;
}
#lihat-kutipan_c{
	padding: 0 !important;
    margin: 0px -15px;
}
</style>

<script type="text/javascript">
	toggleCls = (el,delCls,addCls)=>{
    $(el).removeClass(delCls);
    $(el).addClass(addCls);
}
	$('#detail_kutipan_c span.title').html('<b>NO. <?=$parent->no_c?> - <?=$parent->nama?></b>')
	const item = <?=json_encode($item)?>;
	const parent_id = '<?=$parent_id?>';
	const id = 'vue_' + MD5(parent_id);
	const selector =  `#${id}`;
	const columnDef = item.columnDef;
	const url = site_url() + item.url;
	const tableHeader = item.tableHeader;
	window[id] = new Vue({
        el : selector,
        data:{
            fk_value: parent_id,
            fk: 'parent_id',
            pk: 'id',
            fk_parent: 'id',
            columnDef:columnDef,
            colDefParsed: {},
            url: url,
            tableHeader: tableHeader,
            json:[],
            newColumn:{},
            columnCount: 2,
            dirtyRows:[],
            tmpJson:{}
        },
        methods:{
            addRow(){
                const newColumn = Object.assign({},this.newColumn);
                const index = this.json.push(newColumn) - 1;
                this.$nextTick(()=>{
                    $(selector).find('tbody tr:last').prev().find('input:first').focus();
                });
                this.getTmpJson(index);
                return index;
            },
            delRow(row,index){
                const el = event.target;
                const orig_cls = 'fa-trash';
                const loading_cls = 'fa-spin fa-spinner';
                const i = $(el).find('i.fa').get(0);
///
                swal({
                  title: "Konfirmasi",
                  text: 'Hapus baris ?',
                  type: "warning",
                  showCancelButton: true,
                  cancelButtonText:'Batal',
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Ya",
                  closeOnConfirm: true
                },
                ()=>{
                   const postData = {};
                    postData[this.pk] = this.json[index][this.pk];
                    if($(el).attr('loading') == 'true'){
                        return;
                    }
                    $(el).attr('loading','true');
                    toggleCls(i,orig_cls,loading_cls);

                    $.post(`${this.url}/${this.fk_value}?cmd=delete`,postData,(res)=>{
                        console.log(res);
                        if(res.success){
                            this.json.splice(index, 1);
                            this.unsetTmpJson(index);
                            this.unsetDirty(index);
                        }
                        $(el).attr('loading',false);
                        toggleCls(i,loading_cls,orig_cls);
                    },'json');
                }); 
//
                 
            },
            cancelRow(row,index){
                this.json.splice(index, 1);
                this.unsetTmpJson(index);
                this.unsetDirty(index);
            },
            saveRow(row,index){
                const el = event.target;
                const orig_cls = 'fa-check';
                const loading_cls = 'fa-spin fa-spinner';
                const i = $(el).find('i.fa').get(0);
                if($(el).attr('loading') == 'true'){
                    return;
                }
                $(el).attr('loading','true');
                toggleCls(i,orig_cls,loading_cls);

                const newRow    = Object.assign({},this.json[index]);
                const postData  = newRow;
                postData[this.fk] = this.fk_value;
                const oper = typeof postData[this.pk] != 'undefined' ? 'edit':'add';
                $.post(`${this.url}/${this.fk_value}?cmd=${oper}`,postData,(res)=>{
                    console.log(res);
                    if(res.success){
                        try{
                            if(res.data[this.pk]){
                                newRow[this.pk] = res.data[this.pk];
                            }
                        }catch(e){
                            console.log(e)
                        }
                        
                        
                        this.json[index] = newRow;
                        this.unsetDirty(index);
                        this.unsetTmpJson(index);
                        this.getTmpJson(index);
                    }
                    $(el).attr('loading',false);
                    toggleCls(i,loading_cls,orig_cls);
                },'json');
            },
            isDirty(index){
                return $.inArray(index,this.dirtyRows) != -1;
            },
            unsetDirty(index){
                const dirty_idx = $.inArray(index,this.dirtyRows);
                console.log(dirty_idx)
                if(dirty_idx != -1){
                    this.dirtyRows.splice(dirty_idx, 1);
                }
                console.log(this.dirtyRows)
            },
            unsetTmpJson(index){
                const tmpJsonKey = MD5(index);
                if(typeof this.tmpJson[tmpJsonKey] == 'object'){
                    this.tmpJson[tmpJsonKey] = null;
                }
            },
            compareRow(index){
                const tmpJson = this.getTmpJson(index);
                let isDirty = false;
                for( key in this.columnDef){
                    if(this.json[index][key] != tmpJson[key]){
                        isDirty = true;
                        break;
                    }
                };
                if(!isDirty){
                    this.unsetDirty(index);
                }else{
                    if(!this.isDirty(index)){
                        this.dirtyRows.push(index);
                    }
                    
                }
            },
            onInputClick(index){
                const tmpJson = this.getTmpJson(index);
                this.compareRow(index);
            },
            onInputChanged(row,index){ 
                this.compareRow(index);
            },
            getTmpJson(index){
                const tmpJsonKey = MD5(index.toString());
                if(typeof this.tmpJson[tmpJsonKey] != 'object'){
                    this.tmpJson[tmpJsonKey] = Object.assign({},this.json[index]);
                }
                return this.tmpJson[tmpJsonKey];
            },
            onInputKeyUp(row,index){ 
                this.compareRow(index);
            },
            getList(){
                this.parseColumnDef();
                $.get(`${this.url}/${this.fk_value}?cmd=list`,(res)=>{
                    // console.log(res);
                    this.json = res.data;
                },'json');
            },
            parseColumnDef(){
                let colDefParsed = {};
                let columnCount = 0;
                $.each(this.columnDef,(key,val)=>{
                    columnCount += 1;
                    // console.log(key,value);
                    const valueArr = val.split('|');
                    const value = {
                        type: valueArr[0],
                        length: valueArr[1],
                        label: valueArr[2],
                        width: typeof valueArr[3] != 'undefined'? `${valueArr[3]}px`:'auto'
                    }; 
                    colDefParsed[key] = value;

                });
                this.colDefParsed = colDefParsed;
                this.columnCount = columnCount+2;
            }
            
        },
        mounted(){
            this.getList();
        },
        watch:{
            json(_newVal,_oldVal){
                // this.json_str = JSON.stringify(_newVal);
            }
        }
    }) ;
</script>