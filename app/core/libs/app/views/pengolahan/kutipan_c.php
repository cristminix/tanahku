<div class="modal" tabindex="-1" id="modalForm">

</div>
<div class="tabbable tabbable-tabdrop" style="padding-top: 2px">
    <ul class="nav nav-tabs">
        
    </li>
        <li class="active">
            <a href="#tab1" data-toggle="tab" aria-expanded="false"><i class="fa fa-list"></i></a>
        </li>
        <li class="">
            <a href="#tab2" data-toggle="tab" aria-expanded="true"><i class="fa fa-table"></i></a>
        </li>
        
        
        
        
        
    </ul>
    <div class="tab-content ">
        <div class="tab-pane  active" id="tab1">
         <h4> Kutipan C</h4>
<iframe src="" id="export_excel" style="display: none"></iframe> 


<div class="modal" tabindex="-1" id="detail_kutipan_c">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            
            <div class="modal-body" style="padding-top: 0">
                <div class="row">
                    <div class="col-md-12" style="background:#2b3643;padding:.5em  ">
                        <h4><a style="margin-right: 1em" data-dismiss="modal"><i class="fa fa-chevron-left"></i></a> <span class="title" style="color: #fff">Detail Kutipan C</span></h4>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-12">
                         <div id="lihat-kutipan_c" style="padding: 1em">
                            Memuat Detail 
                        </div>
               
                    </div>
                     
                </div>
            </div>
            
        </div>
    </div>
</div>

<?php 
foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
 
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
 
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
 
 
<style type='text/css'>
body
{
   /* font-family: Arial;
    font-size: 14px;*/
}

a:hover
{
    text-decoration: underline;
}
th.action{
    width 200px !important;

}
span.select2{
    width: 100
}
table.table > thead > tr > th.text-center{
    vertical-align: top;
}
</style>
<?php echo $output; ?>


<script type="text/javascript">
    function displayDetail(event,el) {
        // const el = event.target;
        // console.log(el.href);

        const tr = $(el).closest('tr');
        console.log(tr)
        const url = el.href;
        $.post(url,(res)=>{
        
            $("#detail_kutipan_c").unbind('show.bs.modal');
            $("#detail_kutipan_c").on('show.bs.modal', function(){
                $('#lihat-kutipan_c').html(res);
            });
            $("#detail_kutipan_c").modal("show");
        });
        event.preventDefault();
        return false;
    }
    $('button').click(()=>{
        ref = $('#pengolahan_kutipan_c_table').DataTable();
        ref.ajax.reload();
    })
    $(document).ready(function(){
        window.app = new Vue({
            el:'#grid',
            data:{
                // dd_status_survey:{
                //     'Terlaksana' : 'Terlaksana',
                //     'Belum Terlaksana' : 'Belum Terlaksana',
                // },
                filter:{

                },
                is_admin : <?=$is_admin?'true':'false'?>,
                nama_marketing:'',
            },
            mounted(){
                // this.reloadData()
                let self = this;
                this.$nextTick(function(){
                    // if(!this.is_admin){
                    //     this.nama_marketing = '<?=$default_nama_marketing?>';
                    //     this.filter.id_user = '<?=$default_filter_user_id?>';
                    // }

                    $('a.export_btn').click(function(){
                        self.export();
                    });

                    // let chkCHG = function(){
                        
                    //     if(this.value.match(/^Status:/)){
                    //         // console.log(this.value);
                    //         if(this.value == 'Status:')
                    //         $('#pilih_status').val('').change();
                                
                    //     }else{
                    //         $('#pilih_status').val('');
                    //     }
                    // };
                    // $('input[type=search]').change(chkCHG);
                    // $('input[type=search]').keyup(chkCHG);
                    // $('input[type=search]').keydown(chkCHG);
                });
            },
            methods:{
                export(){
                    console.log('export');
                    let param = JSON.stringify(this.filter);
                    let url_prxy = site_url()+'export/kutipan_c/excel';
                    if(!$('.nav.nav-tabs .active i').hasClass('fa-list')){
                        url_prxy += '?parent_id=' + window.kutipan_c_parent_id;
                    }
                    $('iframe#export_excel').prop('src',url_prxy);
                  
                },
                reloadData(){
         

                    var e = $.Event( "keypress", { which: 13 } );
                    
                    $('input[type=search]').trigger(e);

                    e = $.Event( "keydown", { which: 13 } );
                    $('input[type=search]').trigger(e);
                    e = $.Event( "keyup", { which: 13 } );
                    $('input[type=search]').trigger(e);   
                }
            }
        });

    });
</script>
  
        </div>
        <div class="tab-pane" id="tab2">
            <h4>Daftar Kutipan C <span id="parent_title"></span></h4>
<h5>Silahkan Pilih NO. C</h5>
<div id="select_no_c" style="padding: 0 0 1em 0">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
            <div class="col-md-8" style="padding: 0"> <input type="text" name="q" class="form-control" id="cari_aja" value="<?=$group_title?>"></div>    
            <div class="col-md-4" style="padding: 0">
             
                <button v-if="hasParent" onclick="clearParent()" class="btn btn-warning"><i class="fa fa-eraser"></i></button>
                <button   onclick="refreshGrid()" class="btn btn-success"><i class="fa fa-refresh"></i></button>
               
            </div>    
           
            
            </div>
        </div>
    
    </div>
</div>
            <div class="customDTContainer">
 </div>
<div id="alert_cnt" style="padding: 2px;"></div>
<div id='list-report-error' class='report-div error report-list'></div>
<div id='list-report-success' class='report-div success report-list' <?php if ($success_message !== null) {
    ?>style="display:block"<?php
}?>><?php
if ($success_message !== null) {?>
    <p><?php echo $success_message; ?></p>
<?php }
?></div>
<div class="datatables-add-button">
    
<a v-if="hasParent" role="button" class="add_button add_button_grp btn btn-sm btn-circle green tooltips" href="<?php echo $add_url?>">
    <i class="icon wb-plus" aria-hidden="true"></i>  Tambah Data
</a>
<div v-if="hasParent" class="actions" style="float: right;">
    <a role="button" class="export_btn btn btn-sm btn-circle green tooltips" href="javascript:;">
    <i class="fa fa-file-excel-o" aria-hidden="true"></i>  Export Excel
</a>
</div>
</div>
<div style='height:10px;'></div>
<style type="text/css">
    button.refresh-data,button#successMsg {
    position: absolute;
    margin: -100000px;
    z-index: -1;
}
    .datatables-pager{
        display: block;
        /*width: 200px;
        float: left;*/
    }
    .dataTables_length{
        float: left;
        width: 200px;
        display: block;
    }
    #alert_cnt p{
        display: inline;
    }

</style>
<div class="dataTablesContainer">
            <table class="table table-kutipan-c table-bordered" id="table_kutipan_c_1">
                <thead>
                <tr>
                  <th style="width: 40px;vertical-align: middle;text-align: center;" rowspan="2">#</th>  
               
                  <th colspan="4" class="text-center">SAWAH</th>
                  <th colspan="4" class="text-center">DARAT</th>
                  <th style="width: 80px;vertical-align: middle;text-align: center;" rowspan="2" style="width: 80px">ACT</th>  

                </tr>

                <tr>
                  <th class="text-center" style="width: 40px">Persil</th>
                  <th class="text-center" style="width: 40px">Klas</th>
                  <th class="text-center" style="width: 40px">Luas</th>
                  <th style="text-align: center;">Sebab Tanggal Ubah</th>
                  <th class="text-center" style="width: 40px">Persil</th>
                  <th class="text-center" style="width: 40px">Klas</th>
                  <th class="text-center" style="width: 40px">Luas</th>
                  <th class="text-center">Sebab Tanggal Ubah</th>
                </tr>
                <tr>
                   
                    <td style="padding: 2px"><i class="fa fa-search" style="margin: .8em 0 0 0;"></i></td>
                    <td style="padding: 2px"><input v-model="s_persil" value="<?=$q_s_persil?>" class="form-control q_search" type="text" name="q_persil"></td>
                    <td style="padding: 2px"><input v-model="s_kelas" value="<?=$q_s_kelas?>" class="form-control q_search" type="text" name="q_kelas"></td>
                    <td style="padding: 2px"><input v-model="s_luas" value="<?=$q_s_luas?>" class="form-control q_search" type="text" name="q_no_c"></td>
                    <td style="padding: 2px"><input v-model="s_sbb_tgl_ubah" value="<?=$q_s_sbb_tgl_ubah?>" class="form-control q_search" type="text" name="q_s_sbb_tgl_ubah"></td>
                    <td style="padding: 2px"><input v-model="d_persil" value="<?=$q_d_persil?>" class="form-control q_search" type="text" name="d_persil"></td>
                    <td style="padding: 2px"><input v-model="d_kelas" value="<?=$q_d_kelas?>" class="form-control q_search" type="text" name="d_kelas"></td>
                    <td style="padding: 2px"><input v-model="d_luas" value="<?=$q_d_luas?>" class="form-control q_search" type="text" name="d_luas"></td>
                    <td style="padding: 2px"><input v-model="d_sbb_tgl_ubah" value="<?=$q_d_sbb_tgl_ubah?>" class="form-control q_search" type="text" name="q_d_sbb_tgl_ubah"></td>

                    <td style="padding: 2px">
                        <button class="btn btn-sm btn-default" onclick="clearSessionSearch()"><i class="fa fa-eraser"></i></button>
                    </td>
                </tr>
              </thead>
            </table>
        </div>
        <script type="text/javascript">
        $(document).ready(function(){
        window.kutipan_c_parent_id = '';    
        window.table_kutipan_c_1 = $('#table_kutipan_c_1').DataTable({ 
 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
             
            "ajax": {
                "url": "<?=site_url('pengolahan/kutipan_c/table/custom_grid_data')?>?parent_id=" + window.kutipan_c_parent_id,
                "type": "POST"
            },
 
             
            "columnDefs": [
            { 
                "targets": [ 0 ], 
                "orderable": false, 
            },
            ],
"bSort" : false,
 
        responsive: 0,
        
        "iDisplayLength": default_per_page,
  
        "oLanguage":{
            "sProcessing":   list_loading,
            "sLengthMenu":   show_entries_string,
            "sZeroRecords":  list_no_items,
            "sInfo":         displaying_paging_string,
            "sInfoEmpty":   list_zero_entries,
            "sInfoFiltered": filtered_from_string,
            "sSearch":       search_string+":",
            "oPaginate": {
                "sFirst":    '',
                "sPrevious": '<',
                "sNext":     '>',
                "sLast":     ''
            }
        },
        "bDestory": true,
        "bRetrieve": true,
        "fnDrawCallback": function() {
            $('.image-thumbnail').fancybox({
                'transitionIn'  :   'elastic',
                'transitionOut' :   'elastic',
                'speedIn'       :   600,
                'speedOut'      :   200,
                'overlayShow'   :   false
            });
            add_edit_button_listener();
            
        },
        "sDom": 'T<"clear"><"H"lfr>t<"F"ip>',
        "oTableTools": {
            "aButtons": aButtons,
            "sSwfPath": base_url+"public/assets/gc/themes/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
        }


            //-----------------------------
 
        });
        loadListenersForDatatables();
 
    });
        </script>
    </div>
</div>
</div>

<style type="text/css">
 
     table#table_kutipan_c_1   tr td:first-child btn{
        margin: 0;
     }
     table#table_kutipan_c_1   tr td:first-child {
        text-align: center;
        padding: 0;
        width: 70px;
     }
     
     
     table#table_kutipan_c_1   tr th:first-child + th  ,
     table#table_kutipan_c_1   tr td:first-child + td  {
 
        text-align: center;
     }
     table#table_kutipan_c_1   tr th:first-child + th + th  ,
     table#table_kutipan_c_1   tr td:first-child + td + td {
 
        text-align: center;
     }
     table#table_kutipan_c_1   tr th:first-child + th + th + th,
     table#table_kutipan_c_1   tr td:first-child + td + td + td{
     
        text-align: right;
     }
     table#table_kutipan_c_1   tr th:first-child + th + th + th + th,
     table#table_kutipan_c_1   tr td:first-child + td + td + td + td{
     
        text-align: left;
     }
     table#table_kutipan_c_1   tr th:first-child + th + th + th + th + th,
     table#table_kutipan_c_1   tr td:first-child + td + td + td + td + td{
     
        text-align: center;
     }
     table#table_kutipan_c_1   tr th:first-child + th + th + th + th + th + th,
     table#table_kutipan_c_1   tr td:first-child + td + td + td + td + td + td{
     
        text-align: center;
     }
     table#table_kutipan_c_1   tr th:first-child + th + th + th + th + th + th + th,
     table#table_kutipan_c_1   tr td:first-child + td + td + td + td + td + td + td{
     
        text-align: center;
     }
     table#table_kutipan_c_1   tr th:first-child + th + th + th + th + th + th + th + th,
     table#table_kutipan_c_1   tr td:first-child + td + td + td + td + td + td + td + td{
     
        text-align: left;
     }
     table#table_kutipan_c_1   tr th:first-child + th + th + th + th + th + th + th + th+th,
     table#table_kutipan_c_1   tr td:first-child + td + td + td + td + td + td + td + td+td{
     
        text-align: center;
     }
     
</style>
<script type="text/javascript">
    clearSessionSearch = (callback)=>{
        $.post(site_url()+`pengolahan/kutipan_c/set_session_search?clear=true`,{},(res)=>{
            if(res.success){
                if(typeof callback == 'function'){
                    callback();
                }else{
                    $('input.q_search').val('');
                    refreshGrid();
                }
                
            }
          },'json');
    }
    refreshGrid = ()=>{
        window.table_kutipan_c_1.ajax.url(window.table_kutipan_c_1.ajax.url()).load();
    };
    clearParent = ()=>{
        window.kutipan_c_parent_id = '';
        var url = site_url() + `pengolahan/kutipan_c/table/custom_grid_data?parent_id=${window.kutipan_c_parent_id}`;
        $('span#parent_title').text(``);
        window.table_kutipan_c_1.ajax.url(url).load();
        $('#cari_aja').val('')
        $('*[v-if=hasParent]').hide();
    }
    gotoParent = (parent_id)=>{
        window.kutipan_c_parent_id = parent_id;
        $.get(site_url()+`pengolahan/buku_c/get_row/${parent_id}`,(res)=>{
            
            var url = site_url() + `pengolahan/kutipan_c/table/custom_grid_data?parent_id=${window.kutipan_c_parent_id}`;
            $('span#parent_title').text(res.nama);
            $('#cari_aja').val(res.nama);
            window.table_kutipan_c_1.ajax.url(url).load();
            $('*[v-if=hasParent]').show();
            $('a.add_button_grp').attr('href',site_url()+`pengolahan/kutipan_c/table/add?parent_id=${window.kutipan_c_parent_id}`)
        },'json');
        
    }
    $(document).ready(function(){
        $('*[v-if=hasParent]').hide()
        
        $('button.refresh-data').click(()=>{
            refreshGrid();
        });
        var e = new Bloodhound({
                datumTokenizer: function(e) {
                    // console.log(e)
                    return Bloodhound.tokenizers.whitespace(e.name)
                },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                
            });
            e.initialize(), $("#cari_aja").typeahead(null, {
                displayKey: "nama",
                hint: false,
               source: function(query, syncResults, asyncResults) {
                $.get(site_url()+`pengolahan/buku_c/ac?q=` + query, function(data) {
                  asyncResults(data);
                },'json');
              },

            }).on('typeahead:selected', function (e, data) {
            

            window.kutipan_c_parent_id = data.id;
            var url = site_url() + `pengolahan/kutipan_c/table/custom_grid_data?parent_id=${window.kutipan_c_parent_id}`;
            $('span#parent_title').text(`${data.nama}`);
            window.table_kutipan_c_1.ajax.url(url).load();
            $('*[v-if=hasParent]').show();
            $('a.add_button_grp').attr('href',site_url()+`pengolahan/kutipan_c/table/add?parent_id=${window.kutipan_c_parent_id}`)

        });
        function k_delay(callback, ms) {
          var timer = 0;
          return function() {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
              callback.apply(context, args);
            }, ms || 0);
          };
        }
        $('input.q_search').each((index,el)=>{
            // console.log(a,b)
            $(el).keyup(k_delay(function (e) {
              var el = e.target;  
              var postData = {key:$(el).attr('name'),value:el.value};
              
              $.post(site_url()+`pengolahan/kutipan_c/set_session_search`,postData,(res)=>{
                if(res.success){
                    refreshGrid();
                }
              },'json');
            }, 500));
        });
    });
gc.FormDef = <?=json_encode($form_def)?>;
gc.table = 'p_kutipan_c_detail';    
</script>
<script type="text/javascript" src="<?=site_url()?>pub/accordion_form/script.js"></script>
<link rel="stylesheet" type="text/css" href="<?=site_url()?>pub/accordion_form/style.css">