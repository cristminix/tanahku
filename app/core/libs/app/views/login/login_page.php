 <!-- BEGIN LOGO -->
 <video class="videobg" loop autoplay>
     <source src="{{ site_url }}pub/img/Drone_10212.mp4" type="video/mp4">
     <source src="{{ site_url }}pub/img/Rice_Field_37662.mp4" type="video/mp4">
 </video>
 <div class="container" style=";background: transparent url({{ site_url }}pub/img/kades_trans.png) bottom left no-repeat;">
        <div class="logo" style="margin-top: 1em">
            <a href="javascript:;">
                <img src="{{ site_url }}pub/img/logo_in.png" style="height: 100px;" alt="" /> </a>
        </div>
        <div class="logo" style="margin-top: 0">
            <a href="javascript:;">
                <img src="{{ theme_assets }}pages/img/logo-big-white.png" style="height: auto;" alt="" /> </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content" style="margin-top: 0">

            <form class="login-form" action="{{ site_url }}login" id="login-form" method="post">
                <div class="form-title" style="text-align: center;">
                    <span class="form-title">SISTEM INFORMASI PERTANAHAN</span>
                    <span class="form-subtitle"  style="display: none">Please login.</span>
                </div>
                 <?if($login_failed):?>
                <div class="alert alert-danger">
                    <button class="close" data-close="alert"></button>
                    <span> <?=!empty($message)?$message:'Nama Pengguna atau Kata Sandi Salah.' ?></span>
                </div>
                <?endif?>
                <div class="form-group">
                    <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                    <label class="control-label visible-ie8 visible-ie9">Nama Pengguna</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Nama Pengguna" name="username" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Kata Sandi</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Kata Sandi" name="password" /> </div>
                <div class="form-actions">
                    <button type="submit" class="btn red btn-block uppercase">Login</button>
                </div>
                
                <div class="form-group" style="text-align: center;">
                    <span class="form-title">PEMERINTAH KABUPATEN BREBES</span>
                    <span class="form-title">KELURAHAN / DESA GRINTING KECAMATAN BULAKAMBA</span>
                </div>
            </form>
            <!-- END LOGIN FORM -->
        </div>
    </div>
<script type="text/javascript">
    $(document).ready(()=>{
        setTimeout(()=>{
             $('#login-form').unbind('submit');
    $('#login-form').submit(()=>{
        console.log('login')
        return true;
    })
            $('.videobg').get(0).play()
        },1000);
    
        $(window).resize(()=>{
            var h = $(window).height();
            $('.container').height(h+30)
        }).resize();
    });
   
</script>

<style type="text/css">
    body{
        overflow: hidden;
    }
    .form-title{
        text-shadow: 1px 1px 0px #222, 
               1px 0px 0px maroon; 
    }
    .videobg{
        position: fixed;
        z-index: -1;
    }
</style>