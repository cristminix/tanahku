<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="margin-left: 0">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN THEME PANEL -->
        
        <!-- END THEME PANEL -->
        
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Lupa Kata Sandi
        <small></small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <div class="note note-info">
            <p> Silahkan masukkan email anda. </p>
            <div class="form-group" style="padding: 1em">
                <input type="email" name="email" class="form-control input-medium">
            </div>
            <p> Klik tombol kirim email link reset kata sandi berikut jika anda belum menerima email dari kami. </p>
            <div style="padding: 1em">
                <button id="send_email_forget" class="btn btn-success" disabled><i class="fa fa-envelope"></i> KIRIM SAYA EMAIL LINK LUPA KATA SANDI </button>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('input[type=email]').focus();
        $('input[type=email]').change(function(){
            $('#send_email_forget').removeAttr('disabled')
        });
        $('#send_email_forget').click(function(){
            let postData = {
                email:$('input[type=email]').val()
            };
            App.startPageLoading(true);
            $.post('{{ site_url }}forget?resend_email=true',postData,function(r){
                $('.note.note-info').html('<div class="alert alert-success"><i class="fa fa-check"></i> '+r.message+'</div>');
                App.stopPageLoading();


            },'json');
        });
        
    });
</script>