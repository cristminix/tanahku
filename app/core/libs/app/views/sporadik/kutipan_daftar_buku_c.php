<h4>Kutipan Daftar Buku C</h4>
<iframe src="" id="export_excel" style="display: none"></iframe> 
<div class="modal" tabindex="-1" id="detail_kutipan_daftar_buku_c">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body" style="padding-top: 0">
                <div class="row">
                    <div class="col-md-12" style="background:#2b3643;padding:.5em  ">
                        <h4><a style="margin-right: 1em" data-dismiss="modal"><i class="fa fa-chevron-left"></i></a> <span style="color: #fff">Isi Kutipan</span></h4>
                    </div> 
                </div>
                <div class="row">
                    <div class="col-md-12">
                         <div id="lihat-kutipan_daftar_buku_c" style="padding: 1em">
                            Memuat Detail 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 
foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<style type='text/css'>
body
{
   /* font-family: Arial;
    font-size: 14px;*/
}

a:hover
{
    text-decoration: underline;
}
th.action{
	width 200px !important;
}
span.select2{
	width: 100
}
table.table > thead > tr > th.text-center{
    vertical-align: top;
}
table.table-list tbody textarea.form-control,
table.table-list tbody input.form-control
{
    padding: 0 !important;
    height: auto !important;
    font-size: 10px
}
input[type='number'] {
    -moz-appearance:textfield;
}

input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
    -webkit-appearance: none;
}
</style>
<?php echo $output; ?>
<script type="text/javascript">
    gc.FormDef = <?=json_encode($_SERVER['FORM_DEF'])?>;
    function displayDetail(event,el) {
        // const el = event.target;
        console.log(el.href);
        const tr = $(el).closest('tr');
        console.log(tr)
        const url = el.href;
        $.post(url,(res)=>{
            console.log(res);
            $("#detail_kutipan_daftar_buku_c").unbind('show.bs.modal');
            $("#detail_kutipan_daftar_buku_c").on('show.bs.modal', function(){
                $('#lihat-kutipan_daftar_buku_c').html(res);
            });
            $("#detail_kutipan_daftar_buku_c").modal("show");
        });
        event.preventDefault();
        return false;
    }
    $('button').click(()=>{
        ref = $('#kutipan_daftar_buku_c_table').DataTable();
        ref.ajax.reload();
    })
    $(document).ready(function(){
        window.app = new Vue({
            el:'#grid',
            data:{
                filter:{
                },
                is_admin : <?=$is_admin?'true':'false'?>,
                nama_marketing:'',
            },
            mounted(){
                let self = this;
                this.$nextTick(function(){
                    $('a.export_btn').click(function(){
                        self.export();
                    });
                });
            },
            methods:{
                export(){
                    console.log('export');
                    let param = JSON.stringify(this.filter);
                    let url_prxy = site_url()+'sporadik/kutipan_daftar_buku_c/export/excel?param='+btoa(param);
                    $('iframe#export_excel').prop('src',url_prxy);
                },
                reloadData(){
                    var e = $.Event( "keypress", { which: 13 } );
                    $('input[type=search]').trigger(e);
                    e = $.Event( "keydown", { which: 13 } );
                    $('input[type=search]').trigger(e);
                    e = $.Event( "keyup", { which: 13 } );
                    $('input[type=search]').trigger(e);   
                }
            }
        });
    });
gc.table = 'spo_kutipan_daftar_buku_c';    
</script>
<script type="text/javascript" src="<?=site_url()?>pub/accordion_form/script.js"></script>
<link rel="stylesheet" type="text/css" href="<?=site_url()?>pub/accordion_form/style.css">
