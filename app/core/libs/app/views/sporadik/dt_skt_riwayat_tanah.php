<div class="customDTContainer">
<script type="text/javascript">
    var dialog_forms = '1';
</script>
<div id="modalForm"></div>
<script type="text/javascript">
    var base_url = '{{ base_url }}';
    var subject = 'Surat Keterangan';
    var unique_hash = '<?= $unique_hash ?>';
    var displaying_paging_string = "Halaman _START_ - _END_ dari _TOTAL_ total";
    var filtered_from_string    = "(filtered from _MAX_ total entries)";
    var show_entries_string     = "Tampil _MENU_";
    var search_string           = "Cari";
    var list_no_items           = "Tidak ada item";
    var list_zero_entries       = "Ditampilkan 0 - 0 dari 0 item";
    var list_loading            = "Mohon Tunggu";
    var paging_first    = "Awal";
    var paging_previous = '<i class="icon wb-chevron-left-mini"></i>';
    var paging_next     = '<i class="icon wb-chevron-right-mini"></i>';
    var paging_last     = "Terakhir";
    var message_alert_delete = "Apakah anda yakin ingin menghapus data?";
    var default_per_page = 10;
    var unset_export = false;
    var unset_print = false;
    var export_text = 'Ekspor';
    var print_text = 'Cetak';
    var datatables_aaSorting = [[ 0, "asc" ]];
</script>
<div id="alert_cnt" style="padding: 2px;"></div>
<div id='list-report-error' class='report-div error report-list'></div>
<div id='list-report-success' class='report-div success report-list' <?php if ($success_message !== null) {
    ?>style="display:block"<?php
}?>><?php
if ($success_message !== null) {?>
    <p><?php echo $success_message; ?></p>
<?php }
?></div>
<div class="datatables-add-button">
<a role="button" class="add_button btn btn-sm btn-circle green tooltips" href="<?php echo $add_url?>">
    <i class="icon wb-plus" aria-hidden="true"></i>  Tambah Data
</a>
<div class="actions" style="float: right;display: none;">
    <a role="button" class="export_btn btn btn-sm btn-circle green tooltips" href="javascript:;">
    <i class="fa fa-file-excel-o" aria-hidden="true"></i>  Export Excel
</a>
</div>
</div>
<div style='height:10px;'></div>
<style type="text/css">
    button.refresh-data,button#successMsg {
    position: absolute;
    margin: -100000px;
    z-index: -1;
}
    .datatables-pager{
        display: block;
        /*width: 200px;
        float: left;*/
    }
    .dataTables_length{
        float: left;
        width: 200px;
        display: block;
    }
    #alert_cnt p{
        display: inline;
    }
</style>
<div class="dataTablesContainer">
   <table id="skt_riwayat_tanah_table" class="table table-bordered table-hover table-striped groceryCrudTable">
        <thead>
            <tr>
<!-- $crud->columns('id_pelanggan', 'tgl_rencana_survey','tgl_selesai_survey','keterangan','id_user','is_active','status'); -->
                <th class="no" field_name="no">#</th>
                <th class="" field_name="judul">Nomor Surat  - (TGL) PSL/NO.C/KLS</th>
                <th class="actions">Aksi</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
    <button data-url="{{ base_url }}sporadik/skt_riwayat_tanah/custom_grid_ajax" class="btn btn-primary refresh-data"><i class="icon wb-refresh"></i>Refresh</button>
<button style="opacity: 0" type="button" class="btn btn-primary" id="successMsg" data-plugin="alertify" data-type="log" data-delay="10000" data-log-message="Berhasil dengan sukses">Save Success</button>
<script type="text/javascript">
    $(document).ready(function(){
        $('.dataTables_length').parent().addClass('datatables-pager');
    });
</script>
<script type="text/javascript">
    var default_javascript_path = '{{ base_url }}public/assets/gc/js';
    var default_css_path = '{{ base_url }}public/assets/gc/css';
    var default_texteditor_path = '{{ base_url }}public/assets/gc/texteditor';
    var default_theme_path = '{{ base_url }}public/assets/gc/themes';
    var base_url = '{{ base_url }}';
</script>
<script type="text/javascript">
    var default_javascript_path = '{{ base_url }}public/assets/gc/js';
    var default_css_path = '{{ base_url }}public/assets/gc/css';
    var default_texteditor_path = '{{ base_url }}public/assets/gc/texteditor';
    var default_theme_path = '{{ base_url }}public/assets/gc/themes';
    var base_url = '{{ base_url }}';
</script>
<!-- <script type="text/javascript" src="{{ base_url }}public/assets/gc/js/jquery_plugins/jquery.noty.js"></script>
<script type="text/javascript" src="{{ base_url }}public/assets/gc/js/jquery_plugins/config/jquery.noty.config.js"></script><script type="text/javascript" src="{{ base_url }}public/assets/gc/js/common/lazyload-min.js"></script>
<script type="text/javascript" src="{{ base_url }}public/assets/gc/js/common/list.js"></script>
<script type="text/javascript" src="{{ base_url }}public/assets/gc/js/jquery_plugins/jquery.fancybox-1.3.4.js"></script>
<script type="text/javascript" src="{{ base_url }}public/assets/gc/js/jquery_plugins/jquery.easing-1.3.pack.js"></script>
<script type="text/javascript" src="{{ base_url }}public/assets/gc/themes/datatables/js/datatables.js"></script>  --> 
    </div>
<script type="text/javascript">
    var table;
    var DONT_INIT_DT = true;
    $(document).ready(function() {
        //datatables
        table = $('#skt_riwayat_tanah_table').DataTable({ 
            "processing": true, 
            "serverSide": true, 
            "order": [], 
            "ajax": {
                "url": "<?=site_url('sporadik/skt_riwayat_tanah/custom_grid_data')?>",
                "type": "POST"
            },
            "columnDefs": [
            { 
                "targets": [ 0 ], 
                "orderable": false, 
            },
            ],
            //----------------------------
// "sPaginationType": "full_numbers",
        responsive: 0,
            // fixedHeader: {
            //     header: !0,
            //     headerOffset: offsetTop
            // },
            // bPaginate: !1,
            // sDom: "t",
        // "bStateSave": use_storage,
        // "fnStateSave": function (oSettings, oData) {
        //     localStorage.setItem( 'DataTables_' + unique_hash, JSON.stringify(oData) );
        // },
        // "fnStateLoad": function (oSettings) {
        //     return JSON.parse( localStorage.getItem('DataTables_'+unique_hash) );
        // },
        "iDisplayLength": default_per_page,
        // "aaSorting": datatables_aaSorting,
        "oLanguage":{
            "sProcessing":   list_loading,
            "sLengthMenu":   show_entries_string,
            "sZeroRecords":  list_no_items,
            "sInfo":         displaying_paging_string,
            "sInfoEmpty":   list_zero_entries,
            "sInfoFiltered": filtered_from_string,
            "sSearch":       search_string+":",
            "oPaginate": {
                "sFirst":    '',
                "sPrevious": '<',
                "sNext":     '>',
                "sLast":     ''
            }
        },
        "bDestory": true,
        "bRetrieve": true,
        "fnDrawCallback": function() {
            $('.image-thumbnail').fancybox({
                'transitionIn'  :   'elastic',
                'transitionOut' :   'elastic',
                'speedIn'       :   600,
                'speedOut'      :   200,
                'overlayShow'   :   false
            });
            add_edit_button_listener();
        },
        "sDom": 'T<"clear"><"H"lfr>t<"F"ip>',
        "oTableTools": {
            "aButtons": aButtons,
            "sSwfPath": base_url+"public/assets/gc/themes/datatables/extras/TableTools/media/swf/copy_csv_xls_pdf.swf"
        }
            //-----------------------------
        });
        loadListenersForDatatables();
    });
</script>
 <style type="text/css">
     .dataTablesContainer table tr td:first-child + td + td{
        width: 200px;
     }
     .dataTablesContainer table tr td:first-child ,
     .dataTablesContainer table tr td:first-child + td + td,
     .dataTablesContainer table tr td:first-child + td + td + td + td{
        text-align: center;
     }
      button.refresh-data,button#successMsg {
    position: absolute;
    margin: -100000px;
    z-index: -1;
}
    .datatables-pager{
        display: block;
        /*width: 200px;
        float: left;*/
    }
    .dataTables_length{
        float: left;
        width: 200px;
        display: block;
    }
    #alert_cnt p{
        display: inline;
    }
 </style>
 </div>
