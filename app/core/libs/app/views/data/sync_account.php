
<script type="text/javascript">
	
	 

	// console.log(user_map);
	// console.log(paginate(ids,10,39));

	window.app_sync = new Vue({
		el:'#syncAccountModal',
		data:{
			per_page: 10,
			total_pages:0,
			ids: <?=json_encode($ids)?>,
			total_records: 0,
			account: <?=json_encode($account)?>,
			user_map: <?=json_encode($user_map)?>,
			p_min: 0,
			p_max :100,
			p_now : 0,
			page:1,
			process_complete: false,
			is_processing: false,
			pp_pages: 0,
			pp_records: 0
		},
		mounted(){
			this.total_records = this.ids.length;
			this.pp_records = this.total_records;
			this.process_complete = false;
			this.page = 1;
			this.$nextTick(function(){
				this.calculateTotalPages();
			});
		},
		methods:{
			paginate(array, page_size, page_number) {
			  // human-readable page numbers usually start with 1, so we reduce 1 in the first argument
			  return array.slice((page_number - 1) * page_size, page_number * page_size);
			},
			calculateTotalPages(){
				this.total_pages = Math.ceil(this.total_records/this.per_page);
				this.pp_pages = this.total_pages;
			},
			main_proc(){
				this.is_processing = true;
				let url_prxy = site_url()+'data/sync_account/do';
				let form_data = new FormData();
				let ids = this.paginate(this.ids,this.per_page,this.page);
				form_data.append('ids',ids);

				axios({
                    method:'post',
                    url: url_prxy,
                    data:form_data,
                    headers: {'Content-Type': 'multipart/form-data' },

                })
                .then((response) => {
                    let data = response.data;
                    
                    if(data.success === 1){
                    	this.page += 1;
                    	this.p_now = (this.page/this.total_pages) * 100;
                    	this.pp_pages -= 1;
                    	this.pp_records -= ids.length;

                    	if(this.page <= this.total_pages){
                    		this.main_proc();
                    	}else{
                    		this.process_complete = true;
                    		this.is_processing = false;
                    	}
                        // this.form.foto[`foto_${jenis}`] = '';
                    }else{
                        // swal(data.msg);
                    }
                })
                .catch((error) => {
                    try{swal(error.response.data);}catch(e){}
                    
                });
			},
			sub_proc(){

			}
		}
	});
</script>