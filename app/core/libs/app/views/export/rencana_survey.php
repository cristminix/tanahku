<table id="excel_report_table" border="1" style="border-collapse: collapse;border:solid 1px #ddd;">
	<thead>
		<tr>
			<td style="border:solid 1px #ddd;">No</td>
			<td>Nama Pelanggan</td>
			<td>Rencana Survey</td>
			<td>Selesai Survey</td>
			<td>Keterangan</td>
			<td>Marketing</td>
			<td>Status</td>
		</tr>
	</thead>
	<tbody>
		<?foreach($records as $index => $rec):?>
		<tr>
			<td><?=($index+1)?></td>
			<td><?=$rec->nama_pelanggan?></td>
			<td><?=$rec->tgl_rencana_survey?></td>
			<td><?=$rec->tgl_selesai_survey?></td>
			<td><?=$rec->keterangan?></td>
			<td><?=$rec->nama_marketing?></td>
			<td><?=$rec->status?></td>
		</tr>
		<?endforeach?>
	</tbody>
</table>