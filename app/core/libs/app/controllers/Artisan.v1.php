<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 

use Foundationphp\Exporter\Xml;
use Foundationphp\Exporter\WordXml;
use Foundationphp\Exporter\OpenDoc;
use Foundationphp\Exporter\MsWord;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;


require_once APPPATH . '/core/FCM.php';

class Artisan extends CI_Controller {
    public function __construct($value = '')
    {
    	
        parent::__construct();
    }
    public function index($value='')
    {
    }
    public function gen_var()
    {
        $codes = '';
        $dir = APP . '/modules/';
        foreach(glob($dir.'*_parsed.yml') as $f){
            $conf = Yaml::parse(file_get_contents($f));
            $conf_orig = Yaml::parse(file_get_contents(str_replace('_parsed', '', $f)) );
            $table = $conf['models']['table'];
            print_r($conf_orig);
            $page_title = str_replace('SPO ','',$conf_orig['config']['vars']['page_title']);

            $codes .= "'$table'=>'$page_title',\n";

        }
        echo $codes;
    }
    public function console($cmd = '', $a = '', $b = '', $c = '', $d = '', $e = '')
    {
        $method = str_replace(':', '_', $cmd);

        if (method_exists($this, $method)) {
            return $this->{$method}($a, $b, $c, $d, $e);
        }

        echo ('Unexistent command '."$cmd\n");
    }
    public function db_list_field($_0,$table)
    {
        $field_data = $this->db->field_data($table);
        $fields = [$table=>[]];

        foreach ($field_data as $field) {
            $fields[$table][] = $field->name;
        }
        $target_yaml = APP . '/form/'.$table.'.yml';
        file_put_contents($target_yaml,Yaml::dump($fields));
    }
    public function format_xml($file_name)
    {
        if(!file_exists($file_name)){
            die("FILE NOT FOUND : $sourcefile\n");
        }
        $xml_buffer = file_get_contents($file_name);
        $dom = new DOMDocument;
        $dom->preserveWhiteSpace = FALSE;
        $dom->loadXML($xml_buffer);
        $dom->formatOutput = TRUE;
        return $dom->saveXML();
        // file_put_contents($file_name, $xml_buffer);

    }
    public function word_extract($file_name)
    {
        $sourcefile = BASE . '/uploads/word/templates/'.$file_name.'.docx';
        if(!file_exists($sourcefile)){
            die("FILE NOT FOUND : $sourcefile\n");
        }
        $xslt = BASE . '/uploads/word/templates/output/'.$file_name.'.xslt';

        $zip = new ZipArchive();
        $zip->open($sourcefile);
        $content = $zip->getFromName('word/document.xml');
        if (file_put_contents($xslt, $content)) {
            echo 'Main content extracted'."\n";
        } else {
            echo 'Problem extracting main content'."\n";
        }

        $zip->close();
        return $xslt;
    }
    public function format_result_row($row)
    {

        $row['dt_surat_kuasa']   = tanggal_indo($row['dt_surat_kuasa']);
        $row['list_lampiran']    =  json_decode($row['list_lampiran']);
        $row['list_permohonan']  =  json_decode($row['list_permohonan']);

        foreach ($row['list_permohonan'] as $i => &$v) {
            $v = ($i+1) .'. '.$v ;
        }
        return $row;
    }
    public function generate_word($_0,$table)
    {
        $dir = BASE . '/uploads/word/templates/';
        $result = $this->db->where('id',1)
                       ->get($table);

        if($result->num_rows() > 0){
            try {
                $xml = new WordXml($result->result_array(), null, $options);
                $xml->set_has_children( 'list_lampiran');
                $xml->set_has_children( 'list_permohonan');
                $xml->set_callback_row([$this,'format_result_row'])
                    ->generate();
                $download = new MsWord($xml);
                $download->setDocTemplate($dir . 'output/'.$table.'_wordTemplate.docx');
                $download->setXsltSource($dir . 'output/'.$table.'_word.xslt');
                $download->setImageSource($dir . 'images');
                $download->create($table.'.docx');
            } catch (Exception $e) {
                $error = $e->getMessage();
                echo $error;
            }
        } 
    }
    public function extract_xslt($_0,$file_name)
    {
        $xslt = $this->word_extract($file_name);
        $xml_buffer = $this->format_xml($xslt);

        $opening_tag = '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">'."\n";
        $opening_tag .= '<xsl:template match="/">'."\n";
        $opening_tag .= '<xsl:value-of select=""/>'."\n";
        $closing_tag = '</xsl:template>'."\n".'</xsl:stylesheet>'."\n";

        $xml_buffer_arr = $this->splitNewLine($xml_buffer);
        $xml_buffer = "";

        foreach ($xml_buffer_arr as $line) {
            if(preg_match('/\<w\:document/', $line)){
                $xml_buffer .= $opening_tag ;
                $xml_buffer .= $line . "\n";

            }
            else if(preg_match('/\<w\:body/', $line)){
                $opening_loop = "\t".'<xsl:for-each select="root/row">';
                $xml_buffer .= $line . "\n";
                $xml_buffer .= $opening_loop."\n";
                
                
            }
            else if(preg_match('/\<\/w\:body/', $line)){
                $closing_loop = "\t".'</xsl:for-each>';
                
                $xml_buffer .= $closing_loop."\n";
                $xml_buffer .= $line . "\n";
                
            }else{
                $xml_buffer .= $line . "\n";
            }
            
        }
        $xml_buffer .= $closing_tag;
        file_put_contents($xslt, $xml_buffer);
        echo $xslt;
    }
  
    public function generate_odt($_0,$table)
    {
        $dir = BASE . '/uploads/odt/templates/';
        $result = $this->db->where('id',1)
                       ->get($table);

        if($result->num_rows() > 0){
            try {
                $xml = new WordXml($result->result_array(), null, $options);
                $xml->set_has_children( 'list_lampiran');
                $xml->set_has_children( 'list_permohonan');
                $xml->set_callback_row([$this,'format_result_row'])
                    ->generate();
                $download = new OpenDoc($xml);
                $download->setDocTemplate($dir . 'output/'.$table.'_wordTemplate.odt');
                $download->setXsltSource($dir . 'output/'.$table.'_word.xslt');
                $download->setImageSource($dir . 'images');
                $download->create($table.'.odt');
            } catch (Exception $e) {
                $error = $e->getMessage();
                echo $error;
            }
        } 
    }

    public function extract_xslt2($_0,$file_name)
    {
        $xslt = $this->odt_extract($file_name);
        $xml_buffer = $this->format_xml($xslt);

        $opening_tag = '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">'."\n";
        $opening_tag .= '<xsl:template match="/">'."\n";
        $opening_tag .= '<xsl:value-of select=""/>'."\n";
        $closing_tag = '</xsl:template>'."\n".'</xsl:stylesheet>'."\n";

        $xml_buffer_arr = $this->splitNewLine($xml_buffer);
        $xml_buffer = "";

        foreach ($xml_buffer_arr as $line) {
            if(preg_match('/\<office\:document\-content/', $line)){
                $xml_buffer .= $opening_tag ;
                $xml_buffer .= $line . "\n";

            }
            else if(preg_match('/\<office\:body/', $line)){
                $opening_loop = "\t".'<xsl:for-each select="root/row">';
                $xml_buffer .= $line . "\n";
                $xml_buffer .= $opening_loop."\n";
                
                
            }
            else if(preg_match('/\<\/office\:body/', $line)){
                $closing_loop = "\t".'</xsl:for-each>';
                
                $xml_buffer .= $closing_loop."\n";
                $xml_buffer .= $line . "\n";
                
            }else{
                $xml_buffer .= $line . "\n";
            }
            
        }
        $xml_buffer .= $closing_tag;
        file_put_contents($xslt, $xml_buffer);
        echo $xslt;
    }
    function apply_template_var($var,$key,$line,$_config,$_var){
        $obj_props = explode('.', $key);
        if(count($obj_props) == 2){
            $config_item = $obj_props[0];
            $config_item_key = $obj_props[1];
            if(isset($_config[$config_item])){
                if(isset($_config[$config_item][$config_item_key])){
                    return str_replace($var, $_config[$config_item][$config_item_key], $line);
                }
            }
        }else{
            if(isset($_var[$key])){
                return str_replace($var, $_var[$key], $line);
            }
        }
        return $line;
    }
    function splitNewLine($text) {
        $code=preg_replace('/\n$/','',preg_replace('/^\n/','',preg_replace('/[\r\n]+/',"\n",$text)));
        return explode("\n",$code);
    }

    public function odt_extract($file_name)
    {
        $sourcefile = BASE . '/uploads/odt/templates/'.$file_name.'.odt';
        if(!file_exists($sourcefile)){
            die("FILE NOT FOUND : $sourcefile\n");
        }
        $xslt = BASE . '/uploads/odt/templates/output/'.$file_name.'.xslt';


        $zip = new ZipArchive();
        $zip->open($sourcefile);
        $content = $zip->getFromName('content.xml');
        if (file_put_contents($xslt, $content)) {
            echo 'Main content extracted<br>';
        } else {
            echo 'Problem extracting main content';
        }

        for ($i = 0; $i < $zip->numFiles; $i++) {
            $filename = $zip->getNameIndex($i);
            if (pathinfo($filename, PATHINFO_DIRNAME) == 'Pictures') {
                if ($zip->deleteIndex($i)) {
                    echo 'Image deleted<br>';
                } else {
                    echo 'Problem deleting image<br>';
                }
            }
        }
        $zip->close();
        return $xslt;
    }
    public function parse_form()
    {
        $target_yaml = APP . '/form/spo_surat_kuasa.yml';
        $buffer = file_get_contents($target_yaml);
        $form_def = Yaml::parse($buffer);

        $target_json = APP . '/form/spo_surat_kuasa.json';
        file_put_contents($target_json,json_encode($form_def, JSON_PRETTY_PRINT));
        print_r($form_def);
    }

    public function generate_module($_me,$module_name)
    {
        $yml_path = APP . '/modules/'. $module_name.".yml";
        if(!file_exists($yml_path)){
            die("FILE NOT FOUND : $yml_path\n");
        }
        echo "READ : $yml_path \n";
        $module_config = Yaml::parse(file_get_contents($yml_path));
        $config = $module_config['config'];
        $vars   = $config['vars'];
        
        // print_r($var_keys);
        // print_r($config);
        // exit();
        // 1. PARSE vars
        $config_parsed = [
            'controllers' => [],
            'routes' => [],
            'views' => [],
            'models' => [],
            'forms' => []
        ];
        $var_keys = array_keys($vars); 
        $config_parsed_keys = array_keys($config_parsed);

        foreach ($config_parsed as $config_item => $config_parsed_value) {
            foreach ($config[$config_item] as $config_key => $config_value) {
                $config_key_new     = $config_key;
                $config_value_new   = $config_value;

                foreach ($vars as $name => $value) {
                    $config_key_new = str_replace('$'.$name, $value, $config_key_new);
                    $config_value_new = str_replace('$'.$name, $value, $config_value_new);
                }
                $config_parsed[$config_item][$config_key_new]=$config_value_new;
            }
        }
        $config_parsed_yml = APP.'/modules/'.$module_name.'_parsed.yml';
        file_put_contents($config_parsed_yml, Yaml::dump($config_parsed));
        echo "WRITE : $config_parsed_yml\n";
        // print_r($config_parsed);
        $ctl_tpl_path = APP . '/templates/ctl.txt';
        if(!file_exists($ctl_tpl_path)){
            die("FILE NOT FOUND : $ctl_tpl_path\n");
        }
        echo "GENERATE : $ctl_tpl_path\n"; 

        $buffer = file_get_contents($ctl_tpl_path);
        $buffer_arr = $this->splitNewLine($buffer);
        $buffer = '';

        foreach($buffer_arr as $line){
            $ck_line = trim($line);
            if(empty($ck_line)){
                continue;
            }
            // echo $line . "\n";
            if(preg_match_all("~\{\{\s*(.*?)\s*\}\}~", $line,$matches)){
                foreach($matches[0] as $index => $var){
                    $key_var = $matches[1][$index];
                    $line = $this->apply_template_var($var,$key_var,$line,$config_parsed,$vars);
                }
            }
            $buffer .= $line . "\n";

        }
        // echo $buffer;
        $target_ctl = APPPATH .'/controllers/'. $config_parsed['controllers']['ctl']; 

        echo "WRITE : $target_ctl\n";
        file_put_contents($target_ctl, $buffer);
        // mdl
        $mdl_tpl_path = APP . '/templates/mdl.txt';
        if(!file_exists($mdl_tpl_path)){
            die("FILE NOT FOUND : $mdl_tpl_path\n");
        }
        echo "GENERATE : $mdl_tpl_path\n"; 

        $buffer = file_get_contents($mdl_tpl_path);
        $buffer_arr = $this->splitNewLine($buffer);
        $buffer = '';

        foreach($buffer_arr as $line){
            $ck_line = trim($line);
            if(empty($ck_line)){
                continue;
            }
            // echo $line . "\n";
            if(preg_match_all("~\{\{\s*(.*?)\s*\}\}~", $line,$matches)){
                foreach($matches[0] as $index => $var){
                    $key_var = $matches[1][$index];
                    $line = $this->apply_template_var($var,$key_var,$line,$config_parsed,$vars);
                }
            }
            $buffer .= $line . "\n";

        }
        echo $buffer;
        $target_mdl = APPPATH .'/models/'. $config_parsed['models']['model']; 

        echo "WRITE : $target_mdl\n";
        file_put_contents($target_mdl, $buffer);

        // dt
        $dt_tpl_path = APP . '/templates/dt.txt';
        if(!file_exists($dt_tpl_path)){
            die("FILE NOT FOUND : $dt_tpl_path\n");
        }
        echo "GENERATE : $dt_tpl_path\n"; 

        $buffer = file_get_contents($dt_tpl_path);
        $buffer_arr = $this->splitNewLine($buffer);
        $buffer = '';

        foreach($buffer_arr as $line){
            $ck_line = trim($line);
            if(empty($ck_line)){
                continue;
            }
            // echo $line . "\n";
            if(preg_match_all("~\{\{\s*(.*?)\s*\}\}~", $line,$matches)){
                foreach($matches[0] as $index => $var){
                    $key_var = $matches[1][$index];
                    $line = $this->apply_template_var($var,$key_var,$line,$config_parsed,$vars);
                }
            }
            $buffer .= $line . "\n";

        }
        // echo $buffer;
        $target_dt = APPPATH .'/views/'. $config_parsed['views']['dir'].'/'.$config_parsed['views']['dt']; 

        echo "WRITE : $target_dt\n";
        file_put_contents($target_dt, $buffer);

        // index
        $index_tpl_path = APP . '/templates/index.txt';
        if(!file_exists($index_tpl_path)){
            die("FILE NOT FOUND : $index_tpl_path\n");
        }
        echo "GENERATE : $index_tpl_path\n"; 

        $buffer = file_get_contents($index_tpl_path);
        $buffer_arr = $this->splitNewLine($buffer);
        $buffer = '';

        foreach($buffer_arr as $line){
            $ck_line = trim($line);
            if(empty($ck_line)){
                continue;
            }
            // echo $line . "\n";
            if(preg_match_all("~\{\{\s*(.*?)\s*\}\}~", $line,$matches)){
                foreach($matches[0] as $index => $var){
                    $key_var = $matches[1][$index];
                    $line = $this->apply_template_var($var,$key_var,$line,$config_parsed,$vars);
                }
            }
            $buffer .= $line . "\n";

        }
        // echo $buffer;
        $target_index = APPPATH .'/views/'. $config_parsed['views']['dir'].'/'.$config_parsed['views']['index']; 

        echo "WRITE : $target_index\n";
        file_put_contents($target_index, $buffer);
    }
    
    public function import_riwayat()
    {
        
        $reader = new SpreadsheetReader(BASE.'/import/export_riwayat.xlsx');
        $yml_index = APP . '/import/index_export_riwayat.yml';

        $file_info = ['indexes' => []];
        foreach ($reader as $index =>  $row)
        {   
            $row_str = implode(' ', $row);
            $row_str = trim($row_str);

            if(preg_match('/psl/', $row_str)){
                $file_info['indexes'][] = $index;
            }
        }
        file_put_contents($yml_index, Yaml::dump($file_info));        
    }
    public function import_riwayat_data()
    {
        // $import_riwayat_t_info = [
        //     'riwayat_id' => 5,
        //     'order' => 4,
        //     'keterangan' => json_encode(['JB NYUWETIN EDI L=77','2019 MULYANI'])
        // ];
        // $this->db->insert('import_riwayat_t_info',$import_riwayat_t_info);
        // echo ;
        $reader = new SpreadsheetReader(BASE.'/import/export_riwayat.xlsx');
        $yml_index = APP . '/import/index_export_riwayat.yml';

        $file_info = Yaml::parse(file_get_contents($yml_index));
        $file_info['group_index'] = ['0'=>'PERSIL 1 D.IV'];
        $oper = '';
        $output_yml = '';
        $file_index = 0;
        $file_content = ['content'=>[]];
        foreach ($reader as $index =>  $row)
        {   

            $row_str = implode(' ', $row);
            $row_str = trim($row_str);
            $row_key = '';
            
            if(in_array($index, $file_info['indexes'])){
                if($oper == 'start_list'){

                    $oper= 'close_list';
                }else{
                    $oper = 'start_list';
                    $output_yml =  APP . '/import/data/'.$file_index.'.yml';
                    continue;
                }
            }

            if($oper == 'start_list'){
                if(preg_match('/persil/i', $row_str)){
                    $row_str = preg_replace('/\s+/', ' ', $row_str);
                    $row_str = preg_replace('/(PERSIL)(\d+)/', '$1 $2', $row_str);
                    $file_info['group_index'][$file_index+1] = $row_str;
                    // echo "$row_str\n";
                    // $row_key = $row_str;
                }else{

                    $file_content['content'][] = $row;
                }
                
            }

            if($oper == 'close_list'){
                
                file_put_contents($output_yml, Yaml::dump($file_content));
                // echo "WRITE: $output_yml\n";
                $file_index += 1;
                $oper= 'start_list';
                $file_content = ['content'=>[]];
                $output_yml =  APP . '/import/data/'.$file_index.'.yml';

            }
        }
        file_put_contents($yml_index, Yaml::dump($file_info));        
    }
    public function import_riwayat_group()
    {
        $yml_index = APP . '/import/index_export_riwayat.yml';

        $file_info = Yaml::parse(file_get_contents($yml_index));

        $riwayat_keys = ['persil','kelas','no_c','nama_lama','luas_lama','nama_baru','luas_baru','keterangan'];
        $riwayat_index= array_keys($riwayat_keys);

        // print_r($riwayat_index);

        // exit();
        for($fi=0;$fi<=172;$fi++) {

            // preg_match('/\d+/', basename($file), $matches);
            $file_index = $fi;
            $file =  APP . '/import/data/'.$file_index.'.yml';
            $data = Yaml::parse(file_get_contents($file));
            $group_name = '';
            $end = end($data['content']) ;
            if(!isset($file_info['group_index'][$file_index])){
                $group_name = 'PERSIL ' . $data['content'][0][0] . ' ' . $data['content'][0][1];
            }else{
                $group_name = $file_info['group_index'][$file_index];
            }
            // echo $file_index . ':'. $group_name ."\n";

            // 
            $p_group = [
                'name'  => $group_name,
                'order' => $file_index 
            ];
            echo "\nINSERT p_riwayat_group:".json_encode($p_group)."\n";
            $this->db->insert('p_riwayat_group',$p_group);
            $p_group_id = $this->db->insert_id();

            // continue;
            // break;
 
            foreach ($data['content'] as $p_order => $row) {
                if(is_array($row)){
                    $riwayat = [
                        'group_id' => $p_group_id,
                        'order' => $p_order
                    ];
                    $t_info = [];
                    foreach ($row as $i => $item) {
                        # code...

                        if($i <= 7){
                            $riwayat[$riwayat_keys[$i]] = $item;
                        }else{
                            // KETERANGAN
                            $item = trim($item);
                            if(!empty($item) && !is_null($item)){
                                $t_info[] = $item;
                            }
                            
                        }
                    }
                    echo ".";
                    $riwayat['kelas'] = str_replace(',', '.', $riwayat['kelas']);
                    $keterangan_arr = [];
                    if(!empty($riwayat['keterangan'])){
                        $keterangan_arr[] = $riwayat['keterangan'];
                    }
                    

                    foreach ($t_info as $num => $content) {
                       $keterangan_arr[] = $content;
                    }
                    
                    $riwayat['keterangan'] = json_encode($keterangan_arr);
                    $this->db->insert('p_riwayat',$riwayat);
                    $riwayat_id = $this->db->insert_id();
                }
                
            }

            // break;
        }
        
    }
    public function fix_riwayat_data()
    {
         foreach (glob(APP . '/import/data/*yml') as $key => $file) {
            preg_match('/\d+/', basename($file), $matches);
            $file_index = $matches[0];
            $data = Yaml::parse(file_get_contents($file));
            echo "Processing : $file\n";
            $length = count($data['content']) - 1;
            $empty_indexes=[];
            while($length > 0){
                $row_str = implode('',$data['content'][$length]);
                $row_str = trim($row_str);
                if(empty($row_str)){
                    $empty_indexes[] = $length;
                }else{
                    break;
                }
                $length -= 1;
            }

            while(count($empty_indexes)>1){
                $index = array_pop($empty_indexes);
                // echo "$index\n";
                unset($data['content'][$index]);
            }
            // foreach ($data['content'] as $p_order => $row) {

            // }
            // print_r($data['content']);
            // unlink($file.'_');
            file_put_contents($file, Yaml::dump($data));
            // break;
        }
    }

    public function import_kutipan_c()
    {
        
        $reader = new SpreadsheetReader(BASE.'/import/export_kutipan_c.xlsx');
        $yml_index = APP . '/import/index_export_kutipan_c.yml';

        $file_info = ['indexes' => []];
        $flag = '';
        $no_c = '';
        $last_no_c = '';
        $data = [];
        foreach ($reader as $index =>  $row)
        {   
            $row_str = implode(' ', $row);
            $row_str = trim($row_str);
          
            if(preg_match('/NO\.(\d+)/', $row[0],$match)){
                // $file_info['indexes'][] = $index;
                // print_r($match);
                $no_c = $match[1];
                if($flag == 'start_list_table'){
                    $flag = 'close_list';
                }else{
                    $flag = 'start_list';
                    $last_no_c = $no_c;
                    $data['content']=[];
                    continue;
                }
                
            }
             if(preg_match('/persil/', $row[0],$match)){
                $flag = 'start_list_table';
                continue;
             }

             if($flag == 'start_list_table'){
                $data['content'][] = $row;
             }
             if($flag == 'close_list'){
                echo $last_no_c . "\n";
                $output_yml = APP . '/import/data/'.$last_no_c.'.yml';
                file_put_contents($output_yml, Yaml::dump($data)); 
                $last_no_c = $no_c;
                $flag = 'start_list';
                $data['content']=[];
                    continue;
             }

        }
        // file_put_contents($yml_index, Yaml::dump($file_info));        
    }
    public function fix_kutipan_data()
    {
         foreach (glob(APP . '/import/data/*yml') as $key => $file) {
            preg_match('/\d+/', basename($file), $matches);
            $file_index = $matches[0];
            $data = Yaml::parse(file_get_contents($file));
            echo "Processing : $file\n";
            $length = count($data['content']) - 1;
            $empty_indexes=[];
            while($length > 0){
                $row_str = implode('',$data['content'][$length]);
                $row_str = trim($row_str);
                if(empty($row_str)){
                    $empty_indexes[] = $length;
                }else{
                    break;
                }
                $length -= 1;
            }

            while(count($empty_indexes)>0){
                $index = array_pop($empty_indexes);
                // echo "$index\n";
                unset($data['content'][$index]);
            }
            // foreach ($data['content'] as $p_order => $row) {

            // }
            // print_r($data['content']);
            // unlink($file.'_');
            file_put_contents($file, Yaml::dump($data));
            // break;
        }
    }

    public function import_kutipan_data()
    {
        for($i=1;$i<=52;$i++){
            $file = APP . '/import/data/'.$i.'.yml';
            $data = Yaml::parse(file_get_contents($file));
            $keys = ['s_persil',
            's_kelas',
            's_luas',
            's_sbb_tgl_ubah',
            'd_persil',
            'd_kelas',
            'd_luas',
            'd_sbb_tgl_ubah'];

            $row = [];
            $parent_id = $this->db->where('no_c',$i)->get('p_buku_c')->row()->no_c;

            if(!empty($parent_id)){
                foreach ($data['content'] as $order => $item) {
                    foreach ($keys as $index => $field) {
                        $row[$field] = $item[$index];
                    }
                    $row['order'] = $order+1;
                    $row['tgl_buat'] = date('Y-m-d H:i:s');
                    $row['tgl_edit'] = date('Y-m-d H:i:s');
                    $row['parent_id'] = $parent_id;
                    $row['user_id'] = 1;
                    echo "INSERT p_kutipan_c\n";
                    $this->db->insert('p_kutipan_c',$row);
                }
            }else{
                echo "SKIPPED : $i\n";
            }
            
            
            // print_r($data['content']);
        }
    }

     public function import_blok()
    {
        
        $reader = new SpreadsheetReader(BASE.'/import/daftar_blok_desagrinting.xlsx');
        $yml_index = APP . '/import/blok.yml';

        $file_info = ['indexes' => []];

        $blok = [];
        $persil_blok = [];
        foreach ($reader as $index =>  $row)
        {   
            // $row_str = implode(' ', $row);
            // $row_str = trim($row_str);

            // if(preg_match('/psl/', $row_str)){
            //     $file_info['indexes'][] = $index;
            // }
            // $row = trim($row);
            $col = 0;
            if($index==0){
                $blok = $row;

                foreach ($blok as $b) {
                    $this->db->insert('m_blok',['blok'=>$b]);
                }
                // print_r($blok);
                // exit();

                // 
            }else{
                foreach ($row as $col => $r) {
                    $persil = trim($r);
                    if(!empty($persil) && $persil != 'ancan'){
                        $this->db->insert('m_persil',[
                            'persil' => $persil,
                            'blok' => $blok[$col]
                        ]);
                    }
                     
                }
                
            }
            if($index==11){
                break;
            }
        }
        // file_put_contents($yml_index, Yaml::dump($file_info));        
    }

    public function update_blok()
    {
        $records = $this->db->where('blok IS NULL')
                            ->get('p_riwayat_group')
                            ->result();
        foreach ($records as $r) {
            $persil = $r->persil;

            $blok_persil = $this->db->where('persil',$persil)->get('m_persil')->row();
            if(!empty($blok_persil)){
                $this->db->where('id',$r->id)
                         ->update('p_riwayat_group',[
                            'blok' => $blok_persil->blok
                         ]);
            }
        }                    
    }
} 