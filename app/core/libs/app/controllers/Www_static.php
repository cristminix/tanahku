<?php


class Www_static extends CI_Controller
{

    public function assets($key,$ext)
    { 
        
        $config_key = 'assets_' . $key . '_' . $ext;
        $cache_path =  BASE ."/cache/assets/".$key.'_'.$ext;

        
        
        
        $expire = 50;

        if ($expire) {
            header("Pragma: public");
            header("Cache-Control: public");
            header('Expires: ' . gmdate('D, d M Y H:i:s', time()+$expire) . ' GMT');
        }

        if (! file_exists($cache_path) or (filemtime($cache_path) < filemtime($cache_path))) {
            require_once APPPATH . 'config/assets.php';
            $files = $config[$config_key];


            $buffer = combine_assets($files, $ext);
            // print_r($buffer);
            // die();
            file_put_contents($cache_path, $buffer);
        } elseif (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) &&
            (strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == filemtime($cache_path)) &&
                $expire ) {
            // Send 304 back to browser if file has not beeb changed
            header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($cache_path)).' GMT', true, 304);
            exit();
        }
        
        header("Content-Type: text/" . ($ext == 'css' ? 'css' : 'javascript'));
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($cache_path)) . ' GMT');
    
        echo file_get_contents($cache_path);
        exit();
    }
    public function js($filename)
    {
        // error_reporting(E_ALL);

        $tmu = "themes/metronic/assets/";
        
      
        
        $js_code = "var gc={settings:{loadDataOnly:false},setSetting:function(k,v){this.settings[k]=v;return this;},oTable:null,oTableArray:[],oTableMapping:[],instance:function(){return this.oTableArray.length?oTableArray[0]:null},exclude_js:['jquery-ui-1.10.3.custom.min.js']};var site_url=function(){return '".site_url()."'};var base_url=function(){return '".base_url()."'};var gbs=function(){return '".base_url()."';};var tmu=function(){return '".site_url($tmu)."';}";
        $cache_path =  BASE ."/cache/assets/".$filename;

        $expire = 30*60*60;

        if ($expire) {
            header("Pragma: public");
            header("Cache-Control: public");
            header('Expires: ' . gmdate('D, d M Y H:i:s', time()+$expire) . ' GMT');
        }

        if (! file_exists($cache_path) or (filemtime($cache_path) < filemtime($cache_path))) {
            file_put_contents($cache_path, $js_code);
        } elseif (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) &&
            (strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == filemtime($cache_path)) &&
                $expire ) {
            // Send 304 back to browser if file has not beeb changed
            header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($cache_path)).' GMT', true, 304);
            exit();
        }
        
        header('Content-Type: text/javascript');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($cache_path)) . ' GMT');
        echo file_get_contents($cache_path);
        exit();
    }
    public function getFotoPelanggan64()
    {
        $response=[
            'success' => false,
            'data' => '',
            'msg' => 'Unknown Error',
            'mime'=>'',
            'filename'=>''
        ];
        $id_foto = $this->input->post('id_foto');
        $key = $this->input->post('key');
        $foto = $this->db->where('id',$id_foto)->get('m_foto_pelanggan')->row();
        if(is_object($foto)){
            if(in_array($key, ['foto_ktp','foto_ktp_selfi','foto_kk','foto_rumah'])){
                if(!empty($foto->{$key})){
                    //
                    $image_path = BASE . '/uploads/pelanggan/' . $foto->{$key};
                    if(file_exists($image_path)){
                        $img_size = getimagesize($image_path);
                        $response['success'] = true;
                        $response['data'] = base64_encode(file_get_contents($image_path));
                        $response['mime'] = $img_size['mime'];
                        $response['msg'] = 'Success';
                        $response['filename'] = $foto->{$key};

                    }else{
                        $response['msg'] = 'File tidak ditemukan.' . "\n" . $image_path;
                    }
                }
            }else{
                $response['msg'] = 'Unknown key';
            }
        }else{
            $response['msg'] = 'Records not found '. "\n" . $this->db->last_query();
        }
        header('Content-Type: application/json');
        echo json_encode($response);
        exit();
    }
    public function _drawEmptyImage($width, $height)
    {
        $cache_dir = $this->config->item('image_cache_dir') . 'image_files/';
        $cache = $cache_dir . "empty-image-${width}x${height}.png";

        if (file_exists(FCPATH . $cache)) {
            redirect(site_url($cache));
            exit;
        }

        $im = imagecreatetruecolor($width, $height);
        $backgroundColor = imagecolorallocate($im, 200, 200, 200);
        imagefill($im, 0, 0, $backgroundColor);

        $box = new Box($im);
        $box->setFontFace(FCPATH .'www_static/assets/fonts/ProximaNovaBold/proximanova-bold-webfont.ttf');
        $box->setFontColor(new Color(255, 255, 255));
    
        $box->setFontSize($height/10);
        $box->setBox(0, 0, $width, $height);
        $box->setTextAlign('center', 'center');
        $box->draw("$width x $height");

        header("Content-type: image/png");
        imagepng($im, FCPATH.$cache);

        redirect(site_url($cache));
    }
    public function photo_64()
    {
        $encrypted_path = $this->input->post('encrypted_path');
        
        $image_path =  my_simple_crypt($encrypted_path, 'd');
        $file_not_found_path = FCPATH . '/resource/images/file_not_found.png';
        if(!file_exists($image_path)){
            $image_path = $file_not_found_path;
        }
        header('Content-Type: application/json');
        $img_size = getimagesize($image_path);
        $response = [
            'success' => true,
            'data' => base64_encode(file_get_contents($image_path)),
            'mime' => $img_size['mime']
        ];
        echo json_encode($response);
    }
    public function lampiran($filename64)
    {
        $this->load->helper('file');
        $filename = base64_decode($filename64);
        $path = BASE.'/uploads/lampiran/'.$filename;
        header('Content-Type: application/json');
        
        if(file_exists($path)){
            $buffer = file_get_contents($path);
            $buffer64 = base64_encode($buffer);
            $mime = get_mime_by_extension($path);
            echo json_encode(['success'=>true,'mime' => $mime, 'data'=>$buffer64]);
            exit;
        }
        echo json_encode(['success'=>false,'msg' => 'File tidak ditemukan' ]);
        exit;
    }
    public function thumb($encrypted_path = '', $width = 100, $height = 100, $base64 = '')
    {
        $no_image_path =  'public/assets/themes/images/no-image-2.png';
        $path =  my_simple_crypt($encrypted_path, 'd');
        // die($path);
        if (is_dir($path)) {
            $path = 'invalid';
        }
     
        if (!file_exists($path)) {
            return $this->_drawEmptyImage($width, $height);
            $path = $no_image_path;
        }

        

        $data = getimagesize($path);

        // print_r($data);

        $o_width  = $data[0];
        $o_height = $data[1];

        $eq    = $o_width / $width;
        $height = $o_height/$eq;

        // die();
        $_end = end(explode('.', $path));
        $ext = '.'.$_end;
        $file = (object)array();
        $file->width        = $data[0];
        $file->height       = $data[1];
        $file->filename     = basename($path);
        $file->extension    = $ext;
        $file->mimetype     = $data['mime'];

        $cache_dir = FCPATH.'cache/_thumbnails/';

        if (! is_dir($cache_dir)) {
            mkdir($cache_dir, 0777, true);
        }
        $image_thumb = $cache_dir . md5($file->filename) . "${width}x${height}" . $file->extension;
        $image_url = site_url().'cache/_thumbnails/'. md5($file->filename) . "${width}x${height}" . $file->extension;
        
        if (file_exists($image_thumb)) {

            if($base64=='true'){
                header('Content-Type: application/json');
                $img_size = getimagesize($image_thumb);
                $response = [
                    'success' => true,
                    'data' => base64_encode(file_get_contents($image_thumb)),
                    'mime' => $img_size['mime']
                ];
                echo json_encode($response);
                return;
            }

            redirect($image_url);
            exit();
        }
        $expire = 60;

        if ($expire) {
            header("Pragma: public");
            header("Cache-Control: public");
            header('Expires: ' . gmdate('D, d M Y H:i:s', time()+$expire) . ' GMT');
        }

        if (! file_exists($cache_path) or (filemtime($cache_path) < filemtime($cache_path))) {
            thumb_image($path, $image_thumb, $width, $height);
        } elseif (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) &&
            (strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == filemtime($image_thumb)) &&
                $expire ) {
            // Send 304 back to browser if file has not beeb changed
            header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($image_thumb)).' GMT', true, 304);
            exit();
        }
        
        header('Content-type: ' . $file->mimetype);
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($image_thumb)) . ' GMT');
        ob_clean();
        echo file_get_contents($image_thumb);
        exit();
    }
    public function files($type = '', $a = '', $b = '', $c = '', $d = '')
    {
        $avail_kontens = array('thumb','large','404');

        if (in_array($type, $avail_kontens)) {
            $method = str_replace('-', '_', $type);
            return $this->{$method}($a, $b, $c, $d);
        }
    }
    public function _show_404()
    {
        $file_no_image = FCPATH . 'pub/themes/images/no-image.png';
        header('Content-type: ' . 'image/png');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s', filemtime($file_no_image)) . ' GMT');
        echo file_get_contents($file_no_image);
        // echo file_exists($file_no_image);
        die();
    }
    
    
    public function large($encrypted_path = '', $width = null, $height = null, $mode = null)
    {
        return $this->thumb($encrypted_path, $width, $height, $mode);
    }
}
