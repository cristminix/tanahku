<?php  defined('BASEPATH') OR exit('No direct script access allowed');
use Foundationphp\Exporter\WordXml;
use Foundationphp\Exporter\OpenDoc;
use Foundationphp\Exporter\MsWord;
use Foundationphp\Exporter\Xml;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
class Kutipan_daftar_buku_c extends Theme_Controller {
	public $_page_title = 'Kutipan Daftar Buku C';
	public function __construct()
    {
        parent::__construct();
        // $this->load->model('m_api_simpeg');
    }
	///////////////////////////////////////////////////////////////////
	public function custom_grid_data()
    {
        $this->load->model('m_spo_kutipan_daftar_buku_c','model');
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $deletable = $field->jml_detail == 0;
            $action = "<td class=\"actions\"> 
               ".(0 ? "<a href=\"".site_url('sporadik/kutipan_daftar_buku_c/generate_odt/'.$field->id)."/".slugify($field->nama_pemilik)."\" class=\"btn btn-sm btn-icon btn-pure btn-info\" role=\"button\" onclick=\"displayDetail(event,this)\"> <i class=\"fa fa-eye\" aria-hidden=\"true\"></i> 
            </a> ":"")."
            <a href=\"".site_url('sporadik/kutipan_daftar_buku_c/index/edit/'.$field->id)."/".slugify($field->nama_pemilik)."\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-edit\" aria-hidden=\"true\"></i> 
            </a> "."
            <a href=\"".site_url('sporadik/kutipan_daftar_buku_c/generate_word/'.$field->id)."/".slugify($field->nama_pemilik)."\" class=\"btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-download\" aria-hidden=\"true\"></i> 
            </a> "."
            <a href=\"".site_url('sporadik/kutipan_daftar_buku_c/file/edit/'.$field->id)."/".slugify($field->nama_pemilik)."/file_upload\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-upload\" aria-hidden=\"true\"></i> 
            </a>".($deletable?"
                        <a onclick=\"javascript: return delete_row('".site_url('sporadik/kutipan_daftar_buku_c/index/delete/'.$field->id)."',-1,this)\" href=\"javascript:;\" class=\"gc-bt-delete delete_button btn btn-sm btn-icon btn-pure btn-danger\" role=\"button\"> <i class=\"fa fa-trash\" aria-hidden=\"true\"></i> 
                        </a>":"")."
            </td>";
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->nama_pemilik .' ' . sprintf(" - (%s) %s", tanggal_indo($field->dt_kutipan), $field->no_c ); 
            $row[] = $action; 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('sporadik/kutipan_daftar_buku_c/index/add')];
        // die($this->session->userdata($data_marketing_filter_status));
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';
        $this->load->view('sporadik/dt_kutipan_daftar_buku_c.php',$tdata);
    }
    private function _customGrid(){
        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('sporadik/kutipan_daftar_buku_c/index/add')];
        $data['output'] = $this->load->view('sporadik/dt_kutipan_daftar_buku_c.php',$tdata,true);
        $data['unique_hash'] = md5(date('YmdHis-Unit'));
        // $data['unor_list'] = $this->m_api_simpeg->get_unor();
        $data['is_admin']  = $this->cms_user_group()=='admin';
        $this->view('sporadik/kutipan_daftar_buku_c.php', $data );
    }
    public function list_kutipan_detail($fk_id)
    {
 
        $results   = [
            'success' => false,
            'data'=> []
        ];
 
        $cmd = $this->input->get('cmd');
        $post_keys = ["parent_id","s_persil","s_kelas","s_luas_ha","s_luas","s_pajak","s_pajak_usd","s_sbb_tgl_ubah","d_persil","d_kelas","d_pajak","d_pajak_usd","d_luas_ha","d_luas","d_sbb_tgl_ubah"];
        $post_data = [];

        foreach ($post_keys as $name) {
            $post_data[$name] = $this->input->post($name);
        }
        switch ($cmd) {
            case 'add':
                $this->session->set_userdata('kutipan_daftar_buku_c_tmp_fk',$fk_id);
                $post_data['is_tmp'] = 1;
                $results['success'] = true;
                $this->db->insert('spo_kutipan_daftar_buku_c_detail',$post_data);
                $results['data'] = $post_data;
                $results['data']['id'] = $this->db->insert_id();
                break;
            case 'edit':
                $post_data['is_tmp'] = 0;
                $results['success'] = true;
                $pk = $this->input->post('id');
                $this->db->where('id',$pk)->update('spo_kutipan_daftar_buku_c_detail',$post_data);
                $results['data'] = $post_data;
                $results['data']['id'] = $pk;
                break;
            case 'delete':
                $temporal = $this->input->get('temporal') == 'true';
                $results['success'] = true;
                if($temporal){
                    $this->db->where('parent_id',$fk_id)->where('is_tmp',1)->delete('spo_kutipan_daftar_buku_c_detail');
                    $this->session->unset_userdata('kutipan_daftar_buku_c_tmp_fk');

                }else{
                    $pk = $this->input->post('id');
                    $this->db->where('id',$pk)->delete('spo_kutipan_daftar_buku_c_detail');
                }
                
                break;    
            case 'list':
                $results['success'] = true;
                $results['data'] = $this->db->where('parent_id',$fk_id)
                                            ->get('spo_kutipan_daftar_buku_c_detail')
                                            ->result_array();
                $results['sql'] = $this->db->last_query();                            
                break;
        }
        echo json_encode($results);
    }
    /*public function ac_picker()
    {
        $param_64  = $this->input->get('param');
        $param_str = base64_decode($param_64);
        $param     = json_decode($param_str);
        // print_r($param);
        $query = $this->input->get('term');
        $results = $this->db->select('a.*,a.template value')
                            ->like('a.template',$query,'%%')
                            ->where('a.target',$param->target)
                            ->order_by('a.'.$param->order,'asc')
                            ->get($param->table.' a')
                            ->result();
        echo json_encode($results);
    }
    */
    public function index()
    {
        $target_yaml = APP . '/form/spo_kutipan_daftar_buku_c.yml';
        $buffer = file_get_contents($target_yaml);
        $_SERVER['FORM_DEF'] = Yaml::parse($buffer);
        $crud = $this->new_crud();
        $crud->set_subject('Kutipan');
        $state = $crud->getState();
        switch ($state) {
            case 'list':
                $args = func_get_args();
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            default:
                # code...
                break;
        }
        $crud->unset_jquery();
        // $crud->unset_export();
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_table('spo_kutipan_daftar_buku_c');
		$crud->set_model('m_spo_kutipan_daftar_buku_c');
		$crud->set_theme('datatables');
        $crud->field_type('tgl_buat','hidden');
        $crud->field_type('tgl_edit','hidden');
        $crud->field_type('user_id','hidden');
        $crud->field_type('file','hidden');
         
    	$id_user = $this->cms_user_id();
        $crud->display_as('nama_pemilik','Nama Pemilik');
        
        $crud->display_as('no_c','No. C');
        $crud->display_as('ttd_jabatan','Jabatan');
        $crud->display_as('ttd_nama','Nama'); 
        $crud->display_as('dt_kutipan','Tanggal Kutipan'); 
        $crud->callback_after_insert(array($this,'_update_detail')); 
		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	$id_user = $this->cms_user_id();
        $data = $crud->render();
		$data->is_admin  = $this->cms_user_group()=='admin';
		$this->view('sporadik/kutipan_daftar_buku_c.php',$data);
    }
    public function file($cmd='',$id='',$slug='',$mode='')
    {
        if (empty($cmd)) {
           exit('No direct script access allowed');
        }
        $crud = $this->new_crud();
        $crud->set_subject('File');
        $crud->set_table('spo_kutipan_daftar_buku_c');
        $crud->set_model('m_spo_kutipan_daftar_buku_c');
        $crud->set_theme('datatables');
        $crud->columns('file');
        $crud->fields('file');
        $upload_dir = 'word/spo_kutipan_daftar_buku_c';
        if(!is_dir(BASE.'/uploads/'.$upload_dir)){
            @mkdir(BASE.'/uploads/'.$upload_dir);
        }
        $crud->set_field_upload('file', $upload_dir);
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $id_user = $this->cms_user_id();
        $data = $crud->render();
        $data->is_admin  = $this->cms_user_group()=='admin';
        $this->view('sporadik/file_upload',$data);
    } 
    public function format_result_row($row)
    {
        $row['dt_kutipan'] = tanggal_indo($row['dt_kutipan']);
        $row['list_kutipan_detail']  =  $this->db->where('parent_id',$row['id'])
                                            ->get('spo_kutipan_daftar_buku_c_detail')
                                            ->result_array();
        foreach ($row['list_kutipan_detail'] as $index => &$kutipan) {
            $kutipan['nomor'] = ($index + 1) . '.';
        } 
        return $row;
    }
    public function generate_odt($id,$slug)
    {
        $table = 'spo_kutipan_daftar_buku_c';
        $dir = BASE . '/uploads/odt/templates/';
        $result = $this->db->where('id',$id)
                       ->get($table);
        if($result->num_rows() > 0){
            try {
                $xml = new WordXml($result->result_array(), null, $options);
                $xml->set_has_children( 'list_kutipan_detail');
                // $xml->set_has_children( 'list_permohonan');
                $xml->set_callback_row([$this,'format_result_row'])
                    ->generate();
                $download = new OpenDoc($xml);
                $download->setDocTemplate($dir . 'output/'.$table.'_wordTemplate.odt');
                $download->setXsltSource($dir . 'output/'.$table.'_word.xslt');
                $download->setImageSource($dir . 'images');
                $path = $download->create($table.'.odt',true);
                $path = explode('uploads', $path);
                $path = '../../../uploads'.$path[1];
                echo site_url().'pub/viewerjs/ViewerJS/#'.$path;
                // print_r($path);
            } catch (Exception $e) {
                $error = $e->getMessage();
                echo $error;
            }
        } 
    }
    public function generate_word($id,$slug="")
    {
        $table = 'spo_kutipan_daftar_buku_c';
        $dir = BASE . '/uploads/word/templates/';
        $result = $this->db->where('id',$id)
                       ->get($table);
        if($result->num_rows() > 0){
            try {
                $xml = new WordXml($result->result_array(), null, $options);
                $xml->set_has_children( 'list_kutipan_detail');
                // $xml->set_has_children( 'list_permohonan');
                $xml->set_callback_row([$this,'format_result_row'])
                    ->generate();
                $download = new MsWord($xml);
                $download->setDocTemplate($dir . 'output/'.$table.'_wordTemplate.docx');
                $download->setXsltSource($dir . 'output/'.$table.'_word.xslt');
                $download->setImageSource($dir . 'images');
                $download->create('Kutipan_Daftar_Buku_C_'.$id.'-'.date('YmdHis').'.docx');
            } catch (Exception $e) {
                $error = $e->getMessage();
                echo $error;
            }
        } 
    } 
     function _update_detail($post_data,$pk){
        $old_fk = $this->session->userdata('kutipan_daftar_buku_c_tmp_fk');
        $new_fk = $pk;
        $row = [
            'parent_id' => $new_fk,
            'is_tmp' => 0
        ];

        // print_r($row);
        $this->db->where('parent_id',$old_fk)->where('is_tmp',1)->update('spo_kutipan_daftar_buku_c_detail',$row);
        $this->session->unset_userdata('kutipan_daftar_buku_c_tmp_fk');
    }
     
    
}
