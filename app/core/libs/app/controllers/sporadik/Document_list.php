<?php
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
defined('BASEPATH') OR exit('No direct script access allowed');

class Document_list extends Theme_Controller {
	public $_page_title = 'Dokumen List';
	public function __construct()
    {
        parent::__construct();
        // $this->load->model('m_api_simpeg');
    }
	 
	 
	
	///////////////////////////////////////////////////////////////////
	public function custom_grid_data()
    {
        $this->load->model('m_document_list','model');
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $action = "<td class=\"actions\"> 
            ".($field->jml_detail > 0 ? "<a href=\"".site_url('sporadik/document_list/detail/'.$field->id)."/".slugify($field->nama)."\" class=\"btn btn-sm btn-icon btn-pure btn-info\" role=\"button\" onclick=\"displayDetail(event,this)\"> <i class=\"fa fa-eye\" aria-hidden=\"true\"></i> 
            </a> ":"")."
            <a href=\"".site_url('sporadik/document_list/index/edit/'.$field->id)."/".slugify($field->nama)."\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-edit\" aria-hidden=\"true\"></i> 
            </a> 
            </td>";


            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->judul; 

            $row[] = $action; 
              
             
 
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('sporadik/document_list/index/add')];
        // die($this->session->userdata($data_marketing_filter_status));
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';

        $this->load->view('sporadik/dt_document_list',$tdata);
    }
    private function _customGrid(){
        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('sporadik/document_list/index/add')];
       

        $data['output'] = $this->load->view('sporadik/dt_document_list',$tdata,true);

        $data['unique_hash'] = md5(date('YmdHis-Unit'));

        // $data['unor_list'] = $this->m_api_simpeg->get_unor();
        $data['is_admin']  = $this->cms_user_group()=='admin';


        $this->view('sporadik/document_list', $data );
    }
    public function index()
    {
        $crud = $this->new_crud();
        $crud->set_subject('Dokumen List');
        
        $state = $crud->getState();
        switch ($state) {
            case 'list':
                $args = func_get_args();
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            
            default:
                # code...
                break;
        }

        $crud->unset_jquery();
        // $crud->unset_export();
        
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_table('m_doc_list');
		$crud->set_model('m_document_list');
		$crud->set_theme('datatables');
    	$id_user = $this->cms_user_id();
		
		$crud->fields('judul');

		

		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	
    	$id_user = $this->cms_user_id();

	
		$data = $crud->render();
		$data->is_admin  = $this->cms_user_group()=='admin';
	       
		$this->view('sporadik/document_list',$data);
    }
     
}