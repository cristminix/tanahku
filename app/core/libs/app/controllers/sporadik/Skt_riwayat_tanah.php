<?php  defined('BASEPATH') OR exit('No direct script access allowed');
use Foundationphp\Exporter\WordXml;
use Foundationphp\Exporter\OpenDoc;
use Foundationphp\Exporter\MsWord;
use Foundationphp\Exporter\Xml;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;

class Skt_riwayat_tanah extends Theme_Controller {
	public $_page_title = 'Surat Keterangan Riwayat Tanah';
	public function __construct()
    {
        parent::__construct();
        // $this->load->model('m_api_simpeg');
    }
    public function format_result_row($row)
    {

        $row['dt_surat_keterangan'] = tanggal_indo($row['dt_surat_keterangan']);
        $row['list_riwayat']  =  $this->db->where('skt_riwayat_tanah_id',$row['id'])
                                            ->get('spo_m_riwayat_tanah')
                                            ->result_array();
        foreach ($row['list_riwayat'] as $index => &$riwayat) {
            $riwayat['nomor'] = ($index + 1) . '.';
        }                                    
        return $row;
    }
    public function generate_odt($id,$slug)
    {
        $table = 'spo_skt_riwayat_tanah';
        $dir = BASE . '/uploads/odt/templates/';
        $result = $this->db->where('id',$id)
                       ->get($table);

        if($result->num_rows() > 0){
            try {
                $xml = new WordXml($result->result_array(), null, $options);
                $xml->set_has_children( 'list_riwayat');
                // $xml->set_has_children( 'list_permohonan');
                $xml->set_callback_row([$this,'format_result_row'])
                    ->generate();
                   
                $download = new OpenDoc($xml);
                $download->setDocTemplate($dir . 'output/'.$table.'_wordTemplate.odt');
                $download->setXsltSource($dir . 'output/'.$table.'_word.xslt');
                $download->setImageSource($dir . 'images');
                $path = $download->create($table.'.odt',true);
                $path = explode('uploads', $path);
                $path = '../../../uploads'.$path[1];
                echo site_url().'pub/viewerjs/ViewerJS/#'.$path;
                // print_r($path);
            } catch (Exception $e) {
                $error = $e->getMessage();
                echo $error;
            }
        } 
    }
    public function generate_word($id,$slug="")
    {
        $table = 'spo_skt_riwayat_tanah';
        $dir = BASE . '/uploads/word/templates/';
        $result = $this->db->where('id',$id)
                       ->get($table);
        if($result->num_rows() > 0){
            try {

                $xml = new WordXml($result->result_array(), null, $options);
                $xml->set_has_children( 'list_riwayat');
                // $xml->set_has_children( 'list_permohonan');
                $xml->set_callback_row([$this,'format_result_row'])
                    ->generate();
                // print_r($xml);
                // die();     
                $download = new MsWord($xml);
                $download->setDocTemplate($dir . 'output/'.$table.'_wordTemplate.docx');
                $download->setXsltSource($dir . 'output/'.$table.'_word.xslt');
                $download->setImageSource($dir . 'images');
                $download->create('Surat_Keterangan_Riwayat_Tanah_'.$id.'-'.date('YmdHis').'.docx');
            } catch (Exception $e) {
                $error = $e->getMessage();
                echo $error;
            }
        } 
    }
	///////////////////////////////////////////////////////////////////
	public function custom_grid_data()
    {
        $this->load->model('m_spo_skt_riwayat_tanah','model');
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $row_id => $field) {
            $deletable = $field->jml_detail == 0;
            $action = "<td class=\"actions\"> 
            <a href=\"".site_url('sporadik/skt_riwayat_tanah/index/edit/'.$field->id)."/".slugify($field->nomor_surat)."/\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-edit\" aria-hidden=\"true\"></i> 
            </a> "."
            <a href=\"".site_url('sporadik/skt_riwayat_tanah/generate_word/'.$field->id)."/".slugify($field->nomor_surat)."\" class=\"btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-download\" aria-hidden=\"true\"></i> 
            </a> "."
            <a href=\"".site_url('sporadik/skt_riwayat_tanah/file/edit/'.$field->id)."/".slugify($field->nama_1)."/file_upload\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure ".(empty($field->file)?'btn-info':'btn-success')."\" role=\"button\"> <i class=\"fa fa-upload\" aria-hidden=\"true\"></i> 
            </a>".($deletable?"
                        <a onclick=\"javascript: return delete_row('".site_url('sporadik/skt_riwayat_tanah/index/delete/'.$field->id)."',-1,this)\" href=\"javascript:;\" class=\"gc-bt-delete delete_button btn btn-sm btn-icon btn-pure btn-danger\" role=\"button\"> <i class=\"fa fa-trash\" aria-hidden=\"true\"></i> 
                        </a>":"")."
            </td>";
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->nomor_surat . sprintf(" - %s/%s (%s) %s/%s/%s",$field->rt,$field->rw, tanggal_indo($field->dt_surat_keterangan),$field->persil,$field->no_c,$field->kelas); 
            $row[] = $action; 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('sporadik/skt_riwayat_tanah/index/add')];
        // die($this->session->userdata($data_marketing_filter_status));
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';
        $this->load->view('sporadik/dt_skt_riwayat_tanah.php',$tdata);
    }
    private function _customGrid(){
        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('sporadik/skt_riwayat_tanah/index/add')];
        $data['output'] = $this->load->view('sporadik/dt_skt_riwayat_tanah.php',$tdata,true);
        $data['unique_hash'] = md5(date('YmdHis-Unit'));
        // $data['unor_list'] = $this->m_api_simpeg->get_unor();
        $data['is_admin']  = $this->cms_user_group()=='admin';
        $this->view('sporadik/skt_riwayat_tanah.php', $data );
    }
    public function list_riwayat($fk_id)
    {
        $param_64  = $this->input->get('param');
        $param_str = base64_decode($param_64);
        $param     = json_decode($param_str);
        $results   = [
            'success' => false,
            'data'=> []
        ];
        // print_r($param);
        $cmd = $this->input->get('cmd');
        $post_keys = ['tahun','skt_riwayat_tanah_id','nama_pemilik','dasar_perolehan','no_c','persil','kelas','keterangan'];
        $post_data = [];

        foreach ($post_keys as $name) {
            $post_data[$name] = $this->input->post($name);
        }
        switch ($cmd) {
            case 'add':
                $this->session->set_userdata('spo_m_riwayat_tanah_tmp_fk',$fk_id);
                $post_data['is_tmp'] = 1;
                $results['success'] = true;
                $this->db->insert('spo_m_riwayat_tanah',$post_data);
                $results['data'] = $post_data;
                $results['data']['id'] = $this->db->insert_id();
                break;
            case 'edit':
                $post_data['is_tmp'] = 0;
                $results['success'] = true;
                $pk = $this->input->post('id');
                $this->db->where('id',$pk)->update('spo_m_riwayat_tanah',$post_data);
                $results['data'] = $post_data;
                $results['data']['id'] = $pk;
                break;
            case 'delete':
                $results['success'] = true;
                 $temporal = $this->input->get('temporal') == 'true';
                $results['success'] = true;
                if($temporal){
                    $this->db->where('skt_riwayat_tanah_id',$fk_id)->where('is_tmp',1)->delete('spo_m_riwayat_tanah');
                    $this->session->unset_userdata('spo_m_riwayat_tanah_tmp_fk');

                }else{
                    $pk = $this->input->post('id');
                    $this->db->where('id',$pk)->delete('spo_m_riwayat_tanah');
                }

                break;    
            case 'list':
                $results['success'] = true;
                $results['data'] = $this->db->where('skt_riwayat_tanah_id',$fk_id)
                                            ->get('spo_m_riwayat_tanah')
                                            ->result_array();
                $results['sql'] = $this->db->last_query();                            
                break;
        }
        echo json_encode($results);
    }
    
    public function index()
    {
        $target_yaml = APP . '/form/spo_skt_riwayat_tanah.yml';
        $buffer = file_get_contents($target_yaml);
        $_SERVER['FORM_DEF'] = Yaml::parse($buffer);
        $crud = $this->new_crud();
        $crud->set_subject('Surat Keterangan');
        $state = $crud->getState();
        switch ($state) {
            case 'list':
                $args = func_get_args();
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            default:
                # code...
                break;
        }
        $crud->unset_jquery();
        // $crud->unset_export();
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_table('spo_skt_riwayat_tanah');
		$crud->set_model('m_spo_skt_riwayat_tanah');
		$crud->set_theme('datatables');
        $crud->field_type('tgl_buat','hidden');
        $crud->field_type('tgl_edit','hidden');
        $crud->field_type('user_id','hidden');
        $crud->field_type('file','hidden');
         
    	$id_user = $this->cms_user_id();
    

        $crud->display_as('jenis_tanah','Jenis');
        $crud->display_as('luas_tanah','Luas');
        $crud->display_as('no_c','No. C');
        $crud->display_as('rt','RT');
        $crud->display_as('rw','RW');
      
        $crud->display_as('rt_t','RT');
        $crud->display_as('rw_t','RW');
        $crud->display_as('nomor_surat','Nomor');
        $crud->display_as('ttd_nama','Nama');
        $crud->display_as('ttd_jabatan','Jabatan');
        $crud->display_as('jalan_t','Jalan');
        $crud->display_as('nib_t','NIB');

        $crud->display_as('batas_timur','Batas Timur');
        $crud->display_as('batas_selatan','Batas Selatan');
        $crud->display_as('batas_utara','Batas Utara');
        $crud->display_as('batas_barat','Batas Barat');

        $crud->display_as('nama_pemilik_terakhir','Pemilik Terakhir');
        $crud->callback_after_insert(array($this,'_update_detail')); 

        $crud->display_as('dt_surat_keterangan','Tanggal Surat Keterangan');
		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	$id_user = $this->cms_user_id();
        $data = $crud->render();
		$data->is_admin  = $this->cms_user_group()=='admin';
		$this->view('sporadik/skt_riwayat_tanah.php',$data);
    }
    public function file($cmd='',$id='',$slug='',$mode='')
    {
        if (empty($cmd)) {
           exit('No direct script access allowed');
        }
        $crud = $this->new_crud();
        $crud->set_subject('File');
        $crud->set_table('spo_skt_riwayat_tanah');
        $crud->set_model('m_spo_skt_riwayat_tanah');
        $crud->set_theme('datatables');
        
        $crud->columns('file');
        $crud->fields('file');
        $upload_dir = 'word/spo_skt_riwayat_tanah';
        if(!is_dir(BASE.'/uploads/'.$upload_dir)){
            @mkdir(BASE.'/uploads/'.$upload_dir);
        }
        $crud->set_field_upload('file', $upload_dir);

        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $id_user = $this->cms_user_id();
        $data = $crud->render();
        $data->is_admin  = $this->cms_user_group()=='admin';

        
        $this->view('sporadik/file_upload',$data);
    }
    function _update_detail($post_data,$pk){
        $old_fk = $this->session->userdata('spo_m_riwayat_tanah_tmp_fk');
        $new_fk = $pk;
        $row = [
            'skt_riwayat_tanah_id' => $new_fk,
            'is_tmp' => 0
        ];

        // print_r($row);
        $this->db->where('skt_riwayat_tanah_id',$old_fk)->where('is_tmp',1)->update('spo_m_riwayat_tanah',$row);
        $this->session->unset_userdata('spo_m_riwayat_tanah_tmp_fk');
    }
}
