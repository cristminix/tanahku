<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 

use Foundationphp\Exporter\Xml;
use Foundationphp\Exporter\WordXml;
use Foundationphp\Exporter\OpenDoc;
use Foundationphp\Exporter\MsWord;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
use LucidFrameTest\Console\ConsoleTable;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Firebase\Messaging\WebPushConfig;

require_once APPPATH . '/core/FCM.php';

class Artisan extends CI_Controller {
    public function __construct($value = '')
    {
    	// if(!defined('CLI_APP')){
    	// 	die('access denied');
    	// }
    	// if(!isset($_SERVER['argv'])){
    	// 	die('access denied');
    		
    	// }
    	// if( count($_SERVER['argv']) < 2){
    	// 	die('access denied');
    	// }
        // print_r($_SERVER);
        parent::__construct();
    }
    public function index($value='')
    {
    	echo 'This is index';
    }
    public function console($cmd = '', $a = '', $b = '', $c = '', $d = '', $e = '')
    {
        $method = str_replace(':', '_', $cmd);

        if (method_exists($this, $method)) {
            return $this->{$method}($a, $b, $c, $d, $e);
        }

        echo ('Unexistent command '."$cmd\n");
    }
    public function update_acc($value='')
    {
    	$str = '1234';
    	$passwd = md5($this->config->item('encryption_key').$str.'manis-legi'.$str);
    	$this->db->update('account_adm',['passwd'=>$passwd]);
    }
    
   public function import_riwayat()
    {
        $import_riwayat_t_info = [
            'riwayat_id' => 5,
            'order' => 4,
            'keterangan' => json_encode(['JB NYUWETIN EDI L=77','2019 MULYANI'])
        ];
        $this->db->insert('import_riwayat_t_info',$import_riwayat_t_info);
        // echo ;
        // $reader = new SpreadsheetReader(BASE.'/import/export_riwayat.xlsx');
        // foreach ($reader as $row)
        // {   
        //     print_r($row);
        //      $import_35 = [
        //         // 'persil' => $row[0],
        //         // 'kelas' => $row[1],
        //         // 'no_c' => $row[2],
        //         // 'nama_lama' => $row[3],
        //         // 'luas' => $row[4],
        //         // 'nama_baru' => $row[5],
        //         // 'luas_baru' => $row[6]
        //     ];
          
        //     // if($this->_checkEmpty($import_35)){
        //     //     print_r($import_35);
        //     //     $this->db->insert('import_35',$import_35);
        //     // }
        // }        
    }
    public function format_xml($file_name)
    {
        if(!file_exists($file_name)){
            die("FILE NOT FOUND : $sourcefile\n");
        }
        $xml_buffer = file_get_contents($file_name);
        $dom = new DOMDocument;
        $dom->preserveWhiteSpace = FALSE;
        $dom->loadXML($xml_buffer);
        $dom->formatOutput = TRUE;
        return $dom->saveXML();
        // file_put_contents($file_name, $xml_buffer);

    }
    public function word_extract($file_name)
    {
        $sourcefile = BASE . '/uploads/word/templates/'.$file_name.'.docx';
        if(!file_exists($sourcefile)){
            die("FILE NOT FOUND : $sourcefile\n");
        }
        $xslt = BASE . '/uploads/word/templates/output/'.$file_name.'.xslt';

        $zip = new ZipArchive();
        $zip->open($sourcefile);
        $content = $zip->getFromName('word/document.xml');
        if (file_put_contents($xslt, $content)) {
            echo 'Main content extracted'."\n";
        } else {
            echo 'Problem extracting main content'."\n";
        }

        $zip->close();
        return $xslt;
    }
    public function format_result_row($row)
    {

        $row['dt_surat_kuasa']   = tanggal_indo($row['dt_surat_kuasa']);
        $row['list_lampiran']    =  json_decode($row['list_lampiran']);
        $row['list_permohonan']  =  json_decode($row['list_permohonan']);

        foreach ($row['list_permohonan'] as $i => &$v) {
            $v = ($i+1) .'. '.$v ;
        }
        return $row;
    }
    public function generate_word($_0,$table)
    {
        $dir = BASE . '/uploads/word/templates/';
        $result = $this->db->where('id',1)
                       ->get($table);

        if($result->num_rows() > 0){
            try {
                $xml = new WordXml($result->result_array(), null, $options);
                $xml->set_has_children( 'list_lampiran');
                $xml->set_has_children( 'list_permohonan');
                $xml->set_callback_row([$this,'format_result_row'])
                    ->generate();
                $download = new MsWord($xml);
                $download->setDocTemplate($dir . 'output/'.$table.'_wordTemplate.docx');
                $download->setXsltSource($dir . 'output/'.$table.'_word.xslt');
                $download->setImageSource($dir . 'images');
                $download->create($table.'.docx');
            } catch (Exception $e) {
                $error = $e->getMessage();
                echo $error;
            }
        } 
    }
    public function extract_xslt($_0,$file_name)
    {
        $xslt = $this->word_extract($file_name);
        $xml_buffer = $this->format_xml($xslt);

        $opening_tag = '<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">'."\n";
        $opening_tag .= '<xsl:template match="/">'."\n";
        $opening_tag .= '<xsl:value-of select=""/>'."\n";
        $closing_tag = '</xsl:template>'."\n".'</xsl:stylesheet>'."\n";

        $xml_buffer_arr = $this->splitNewLine($xml_buffer);
        $xml_buffer = "";

        foreach ($xml_buffer_arr as $line) {
            if(preg_match('/\<w\:document/', $line)){
                $xml_buffer .= $opening_tag ;
                $xml_buffer .= $line . "\n";

            }
            else if(preg_match('/\<w\:body/', $line)){
                $opening_loop = "\t".'<xsl:for-each select="root/row">';
                $xml_buffer .= $line . "\n";
                $xml_buffer .= $opening_loop."\n";
                
                
            }
            else if(preg_match('/\<\/w\:body/', $line)){
                $closing_loop = "\t".'</xsl:for-each>';
                
                $xml_buffer .= $closing_loop."\n";
                $xml_buffer .= $line . "\n";
                
            }else{
                $xml_buffer .= $line . "\n";
            }
            
        }
        $xml_buffer .= $closing_tag;
        file_put_contents($xslt, $xml_buffer);
        echo $xslt;
    }
    public function db_list_field($_0,$table)
    {
        $field_data = $this->db->field_data($table);
        $fields = [$table=>[]];

        foreach ($field_data as $field) {
            $fields[$table][] = $field->name;
        }
        $target_yaml = APP . '/form/'.$table.'.yml';
        file_put_contents($target_yaml,Yaml::dump($fields));
    }

    public function parse_form()
    {
        $target_yaml = APP . '/form/spo_surat_kuasa.yml';
        $buffer = file_get_contents($target_yaml);
        $form_def = Yaml::parse($buffer);

        $target_json = APP . '/form/spo_surat_kuasa.json';
        file_put_contents($target_json,json_encode($form_def, JSON_PRETTY_PRINT));
        print_r($form_def);
    }

    public function generate_module($_me,$module_name)
    {
        $yml_path = APP . '/modules/'. $module_name.".yml";
        if(!file_exists($yml_path)){
            die("FILE NOT FOUND : $yml_path\n");
        }
        echo "READ : $yml_path \n";
        $module_config = Yaml::parse(file_get_contents($yml_path));
        $config = $module_config['config'];
        $vars   = $config['vars'];
        
        // print_r($var_keys);
        // print_r($config);
        // exit();
        // 1. PARSE vars
        $config_parsed = [
            'controllers' => [],
            'routes' => [],
            'views' => [],
            'models' => [],
            'forms' => []
        ];
        $var_keys = array_keys($vars); 
        $config_parsed_keys = array_keys($config_parsed);

        foreach ($config_parsed as $config_item => $config_parsed_value) {
            foreach ($config[$config_item] as $config_key => $config_value) {
                $config_key_new     = $config_key;
                $config_value_new   = $config_value;

                foreach ($vars as $name => $value) {
                    $config_key_new = str_replace('$'.$name, $value, $config_key_new);
                    $config_value_new = str_replace('$'.$name, $value, $config_value_new);
                }
                $config_parsed[$config_item][$config_key_new]=$config_value_new;
            }
        }
        $config_parsed_yml = APP.'/modules/'.$module_name.'_parsed.yml';
        file_put_contents($config_parsed_yml, Yaml::dump($config_parsed));
        echo "WRITE : $config_parsed_yml\n";
        // print_r($config_parsed);
        $ctl_tpl_path = APP . '/templates/ctl.txt';
        if(!file_exists($ctl_tpl_path)){
            die("FILE NOT FOUND : $ctl_tpl_path\n");
        }
        echo "GENERATE : $ctl_tpl_path\n"; 

        $buffer = file_get_contents($ctl_tpl_path);
        $buffer_arr = $this->splitNewLine($buffer);
        $buffer = '';

        foreach($buffer_arr as $line){
            $ck_line = trim($line);
            if(empty($ck_line)){
                continue;
            }
            // echo $line . "\n";
            if(preg_match_all("~\{\{\s*(.*?)\s*\}\}~", $line,$matches)){
                foreach($matches[0] as $index => $var){
                    $key_var = $matches[1][$index];
                    $line = $this->apply_template_var($var,$key_var,$line,$config_parsed,$vars);
                }
            }
            $buffer .= $line . "\n";

        }
        // echo $buffer;
        $target_ctl = APPPATH .'/controllers/'. $config_parsed['controllers']['ctl']; 

        echo "WRITE : $target_ctl\n";
        file_put_contents($target_ctl, $buffer);
        // mdl
        $mdl_tpl_path = APP . '/templates/mdl.txt';
        if(!file_exists($mdl_tpl_path)){
            die("FILE NOT FOUND : $mdl_tpl_path\n");
        }
        echo "GENERATE : $mdl_tpl_path\n"; 

        $buffer = file_get_contents($mdl_tpl_path);
        $buffer_arr = $this->splitNewLine($buffer);
        $buffer = '';

        foreach($buffer_arr as $line){
            $ck_line = trim($line);
            if(empty($ck_line)){
                continue;
            }
            // echo $line . "\n";
            if(preg_match_all("~\{\{\s*(.*?)\s*\}\}~", $line,$matches)){
                foreach($matches[0] as $index => $var){
                    $key_var = $matches[1][$index];
                    $line = $this->apply_template_var($var,$key_var,$line,$config_parsed,$vars);
                }
            }
            $buffer .= $line . "\n";

        }
        echo $buffer;
        $target_mdl = APPPATH .'/models/'. $config_parsed['models']['model']; 

        echo "WRITE : $target_mdl\n";
        file_put_contents($target_mdl, $buffer);

        // dt
        $dt_tpl_path = APP . '/templates/dt.txt';
        if(!file_exists($dt_tpl_path)){
            die("FILE NOT FOUND : $dt_tpl_path\n");
        }
        echo "GENERATE : $dt_tpl_path\n"; 

        $buffer = file_get_contents($dt_tpl_path);
        $buffer_arr = $this->splitNewLine($buffer);
        $buffer = '';

        foreach($buffer_arr as $line){
            $ck_line = trim($line);
            if(empty($ck_line)){
                continue;
            }
            // echo $line . "\n";
            if(preg_match_all("~\{\{\s*(.*?)\s*\}\}~", $line,$matches)){
                foreach($matches[0] as $index => $var){
                    $key_var = $matches[1][$index];
                    $line = $this->apply_template_var($var,$key_var,$line,$config_parsed,$vars);
                }
            }
            $buffer .= $line . "\n";

        }
        // echo $buffer;
        $target_dt = APPPATH .'/views/'. $config_parsed['views']['dir'].'/'.$config_parsed['views']['dt']; 

        echo "WRITE : $target_dt\n";
        file_put_contents($target_dt, $buffer);

        // index
        $index_tpl_path = APP . '/templates/index.txt';
        if(!file_exists($index_tpl_path)){
            die("FILE NOT FOUND : $index_tpl_path\n");
        }
        echo "GENERATE : $index_tpl_path\n"; 

        $buffer = file_get_contents($index_tpl_path);
        $buffer_arr = $this->splitNewLine($buffer);
        $buffer = '';

        foreach($buffer_arr as $line){
            $ck_line = trim($line);
            if(empty($ck_line)){
                continue;
            }
            // echo $line . "\n";
            if(preg_match_all("~\{\{\s*(.*?)\s*\}\}~", $line,$matches)){
                foreach($matches[0] as $index => $var){
                    $key_var = $matches[1][$index];
                    $line = $this->apply_template_var($var,$key_var,$line,$config_parsed,$vars);
                }
            }
            $buffer .= $line . "\n";

        }
        // echo $buffer;
        $target_index = APPPATH .'/views/'. $config_parsed['views']['dir'].'/'.$config_parsed['views']['index']; 

        echo "WRITE : $target_index\n";
        file_put_contents($target_index, $buffer);
    }
    function apply_template_var($var,$key,$line,$_config,$_var){
        $obj_props = explode('.', $key);
        if(count($obj_props) == 2){
            $config_item = $obj_props[0];
            $config_item_key = $obj_props[1];
            if(isset($_config[$config_item])){
                if(isset($_config[$config_item][$config_item_key])){
                    return str_replace($var, $_config[$config_item][$config_item_key], $line);
                }
            }
        }else{
            if(isset($_var[$key])){
                return str_replace($var, $_var[$key], $line);
            }
        }
        return $line;
    }
    function splitNewLine($text) {
        $code=preg_replace('/\n$/','',preg_replace('/^\n/','',preg_replace('/[\r\n]+/',"\n",$text)));
        return explode("\n",$code);
    }
}













