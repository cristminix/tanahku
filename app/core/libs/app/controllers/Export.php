<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export extends Theme_Controller {
	public $_page_title = 'Export';
	public function buku_c()
	{
		error_reporting(0);
		

		$param = $this->input->get('param');
		$param = base64_decode($param);
		$param = json_decode($param);


		$nama_marketing = '';
		 	
		$records = $this->db->select("a.*")
							->from('p_buku_c a')
							->get()->result();

		$data = [
			'records' => $records
		] ;
		$template_file = BASE . '/uploads/excel/templates/buku_c.xlsx';
		$filename = 'Buku_C_' .date('YmdHis'). '.xlsx';
		$save_path = BASE . '/uploads/excel/'.$filename;

		$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($template_file);
		$worksheet = $spreadsheet->getActiveSheet();
		$row  = 2;
		$col  = 1 ;
		
		$styleArray = [
			'font' => [
		        'size' => 11,
		        'name' => 'Arial'
		    ],
		    'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		            'color' => ['rgb' => '000000'],
		        ],
		    ],
		];


		foreach ($records as $index => $rec) {
			$no = ($index + 1) .'.';
			$worksheet->getCell("A{$row}")->setValue( ''.$rec->no_c );
			$worksheet->getCell("B{$row}")->setValue($rec->nama); 
		

			$worksheet->getStyle("A{$row}:B{$row}")->applyFromArray($styleArray);
			$row += 1;
		}

		$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save($save_path);

	
	    header('Content-Description: File Transfer');
	    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	    header('Content-Disposition: attachment; filename='.basename($save_path));
	    header('Content-Transfer-Encoding: binary');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate');
	    header('Pragma: public');
	    header('Content-Length: ' . filesize($save_path));
	    echo file_get_contents($save_path);
	    unlink($save_path);
		
	}
	public function kutipan_c()
	{
		error_reporting(0);
		

		$parent_id = $this->input->get('parent_id');
	 


		
		$template_file = BASE . '/uploads/excel/templates/kutipan_c.xlsx';
		$filename = 'Kutipan_C_' .date('YmdHis'). '.xlsx';
		$save_path = BASE . '/uploads/excel/'.$filename;

		$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($template_file);
		$worksheet = $spreadsheet->getActiveSheet();
		$row  = 1;
		$col  = 1 ;
		if(!empty($parent_id)){
			$this->db->where('a.parent_id',$parent_id);
		}
		$buku_c = $this->db->select("b.nama,b.no_c,a.parent_id, COUNT(a.id) jml_detail")
                 ->join('p_buku_c b','b.id=a.parent_id')
                 ->group_by('b.id')
                 ->from('p_kutipan_c' . ' a')
                 ->get()
                 ->result_array();
        $styleArray = [
			'font' => [
		        'size' => 10,
		        'name' => 'Arial',
		        'bold' =>true
		    ] 
		];    
		$style = array(
			'font' => [
		        'size' => 10,
		        'name' => 'Arial',
		        'bold' =>true
		    ] ,
	        'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
	        ),
	        'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		            'color' => ['rgb' => '000000'],
		        ],
		    ],
	    );
	    $style3 = [
			'font' => [
		        'size' => 10,
		        'name' => 'Arial'
		    ],
		    'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		            'color' => ['rgb' => '000000'],
		        ],
		    ],
		    'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
	        ),
		];
		$style4 = [
			'font' => [
		        'size' => 10,
		        'name' => 'Arial'
		    ],
		    'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		            'color' => ['rgb' => '000000'],
		        ],
		    ],
		    'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
	            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
	        ),
		];
		$aleft = [
		    'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
	            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_BOTTOM,
	        ),
		];
		$aright= [
		    'alignment' => array(
	            'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_BOTTOM,
	        ),
		];
		$worksheet->getColumnDimension('E')->setWidth(18.71);     
		$worksheet->getColumnDimension('I')->setWidth(18.57);     
		$worksheet->getColumnDimension('D')->setWidth(10.29);     
		$worksheet->getColumnDimension('H')->setWidth(10.29);     
        
        foreach ($buku_c as $b) {
        	$worksheet->getCell("B{$row}")->setValue( 'NO.'.$b['no_c'] );
        	$worksheet->getCell("E{$row}")->setValue( $b['nama'] );
        	$worksheet->getRowDimension($row)->setRowHeight(25);
        	$worksheet->getStyle("B{$row}:E{$row}")->applyFromArray($styleArray);

        	$row += 1;
        	$worksheet->getRowDimension($row)->setRowHeight(25);
        	$row += 1;
        	$worksheet->mergeCells("B{$row}:E{$row}");
        	$worksheet->getCell("B{$row}")->setValue( 'SAWAH' );

        	$worksheet->getCell("F{$row}")->setValue( 'DARAT' );
        	$worksheet->mergeCells("F{$row}:I{$row}");
        	$worksheet->getStyle("B{$row}:I{$row}")->applyFromArray($style);		
        	$worksheet->getRowDimension($row)->setRowHeight(25);
        	$row += 1;
        	//persil	klas	Luas	Sebab & Tgl perubahan	persil	klas	Luas	Sebab & Tgl perubahan
			$worksheet->getCell("B{$row}")->setValue( 'Persil' );
			$worksheet->getCell("C{$row}")->setValue( 'Klas' );
			$worksheet->getCell("D{$row}")->setValue( 'Luas' );
			$worksheet->getCell("E{$row}")->setValue(  "Sebab & Tgl \nPerubahan"  );
			$worksheet->getStyle("E{$row}")->getAlignment()->setWrapText(true); 
			$worksheet->getCell("F{$row}")->setValue( 'Persil' );
			$worksheet->getCell("G{$row}")->setValue( 'Klas' );
			$worksheet->getCell("H{$row}")->setValue( 'Luas' );
			$worksheet->getCell("I{$row}")->setValue( "Sebab & Tgl \nPerubahan"  );
			$worksheet->getStyle("I{$row}")->getAlignment()->setWrapText(true); 

			$worksheet->getStyle("B{$row}:I{$row}")->applyFromArray($style3);
			$worksheet->getRowDimension($row)->setRowHeight(25);
			$o_row = $row+1;
			$worksheet->getStyle("B{$o_row}:I{$o_row}")->applyFromArray($style3);
			$worksheet->mergeCells("B{$row}:B{$o_row}");
			$worksheet->mergeCells("C{$row}:C{$o_row}");
			$worksheet->mergeCells("D{$row}:D{$o_row}");
			$worksheet->mergeCells("E{$row}:E{$o_row}");
			$worksheet->mergeCells("F{$row}:F{$o_row}");
			$worksheet->mergeCells("G{$row}:G{$o_row}");
			$worksheet->mergeCells("H{$row}:H{$o_row}");
			$worksheet->mergeCells("I{$row}:I{$o_row}");
			$row += 1;
			$row += 1;
			
        	$records = $this->db->where('parent_id',$b['parent_id'])
        						->order_by('id')
        						->get('p_kutipan_c')
        						->result();
         	// print_r($records);
         	// die();
         	foreach ($records as $item) {
         		$worksheet->getCell("B{$row}")->setValue( $item->s_persil );
         		$worksheet->getStyle("B{$row}")->applyFromArray($aright);

				$worksheet->getCell("C{$row}")->setValue( $item->s_kelas );
				$worksheet->getCell("D{$row}")->setValue( $item->s_luas );
				$worksheet->getStyle("D{$row}")->applyFromArray($aright);
				$worksheet->getCell("E{$row}")->setValue( $item->s_sbb_tgl_ubah );
				$worksheet->getStyle("E{$row}")->applyFromArray($aleft);

				$worksheet->getStyle("E{$row}")->getAlignment()->setWrapText(true); 
				$worksheet->getCell("F{$row}")->setValue( $item->d_persil );
				$worksheet->getStyle("F{$row}")->applyFromArray($aright);

				$worksheet->getCell("G{$row}")->setValue( $item->d_kelas );
				$worksheet->getCell("H{$row}")->setValue( $item->d_luas );
				$worksheet->getStyle("H{$row}")->applyFromArray($aright);

				$worksheet->getCell("I{$row}")->setValue( $item->d_sbb_tgl_ubah  );
				$worksheet->getStyle("I{$row}")->applyFromArray($aleft);
				$worksheet->getStyle("I{$row}")->getAlignment()->setWrapText(true); 
				$worksheet->getStyle("B{$row}:I{$row}")->applyFromArray($style3);

				$worksheet->getRowDimension($row)->setRowHeight(25);
				$row += 1;
 	        }
 	        
			$worksheet->getRowDimension($row)->setRowHeight(25);
 	        $row += 1;         
		}	

	

		$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save($save_path);

	
	    header('Content-Description: File Transfer');
	    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	    header('Content-Disposition: attachment; filename='.basename($save_path));
	    header('Content-Transfer-Encoding: binary');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate');
	    header('Pragma: public');
	    header('Content-Length: ' . filesize($save_path));
	    echo file_get_contents($save_path);
	    unlink($save_path);
		
	}
	public function riwayat_tanah()
	{
		error_reporting(0);
		

		$group_id = $this->input->get('group_id');
		if(empty($group_id)){
			die('invalid group_id');
		}
		$group = $this->db->where('id',$group_id)
						  ->get('p_riwayat_group')
						  ->row();

		if(empty($group)){
			die('invalid group_id');
		}

        if(empty($group->name) || $group->name == '-'){
            $group->name = 'PERSIL ' . $group->persil;
            if(!empty($group->kelas)){
                $group->name .= ' KELAS ' .$group->kelas;
            }
            if(!empty($group->blok)){
                $group->name .= ' BLOK ' .$group->blok;
            }
            // $this->db->where('id',$field->id)->update('p_riwayat_group',['name'=>$field->name]);
        }
     			  
		$this->load->model('m_riwayat_tanah');

		$records = $this->m_riwayat_tanah->get_datatables();

		$template_file = BASE . '/uploads/excel/templates/Riwayat_Tanah.xlsx';
		$filename = 'Riwayat_Tanah_'. $group->name .'_'.date('YmdHis'). '.xlsx';
		$save_path = BASE . '/uploads/excel/'.$filename;

		$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($template_file);
		$worksheet = $spreadsheet->getActiveSheet();
		$row  = 3;
		$col  = 1 ;
		
		$styleArray = [
			'font' => [
		        'size' => 10,
		        'name' => 'Arial'
		    ],
		    'borders' => [
		        'allBorders' => [
		            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
		            'color' => ['rgb' => '000000'],
		        ],
		    ],
		    'alignment' => array(
	            'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_TOP,
	        ),
		];

		$worksheet->getCell("A1")->setValue( $group->name );
		foreach ($records as $index => $rec) {
			$no = ($index + 1).'.';
			$worksheet->getCell("A{$row}")->setValue( ''.$no);
			$worksheet->getCell("B{$row}")->setValue($rec->no_c); 
			$worksheet->getCell("C{$row}")->setValue($rec->nama_lama); 
			$worksheet->getCell("D{$row}")->setValue($rec->luas_lama); 
			$worksheet->getCell("E{$row}")->setValue($rec->nama_baru); 
			$worksheet->getCell("F{$row}")->setValue($rec->luas_baru); 
			$worksheet->getCell("G{$row}")->setValue(implode("\n",json_decode($rec->keterangan))); 
			$worksheet->getStyle("G{$row}")->getAlignment()->setWrapText(true); 

			$worksheet->getStyle("A{$row}:G{$row}")->applyFromArray($styleArray);
			$row += 1;
		}

		$writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
		$writer->save($save_path);

	
	    header('Content-Description: File Transfer');
	    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	    header('Content-Disposition: attachment; filename='.basename($save_path));
	    header('Content-Transfer-Encoding: binary');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate');
	    header('Pragma: public');
	    header('Content-Length: ' . filesize($save_path));
	    echo file_get_contents($save_path);
	    unlink($save_path);
		
	}
	
}