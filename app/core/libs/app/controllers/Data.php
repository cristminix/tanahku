<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
 

class Data extends Theme_Controller {

	 
	public function ac_jabatan()
	{
		$param  = $this->input->get('param'); 
        $field = str_replace('ttd_', '', $param) ;
        $query = $this->input->get('term');
        $results = $this->db->select('a.*,a.'.$field.' value')
                            ->like('a.'.$field,$query,'%%')
                            ->get('m_pejabat a')
                            ->result();
        echo json_encode($results);
	}
	public function ac_persil($exclude_p_riwayat_group="")
    {
    	if($exclude_p_riwayat_group == 'true'){
    		$group_persil_tmp = $this->db->select('persil')->from('p_riwayat_group')->get()->result_array();
    		$group_persil = [];

    		foreach ($group_persil_tmp as $pr) {
    		 	if(preg_match('/\,/',$pr['persil'])){
    		 		$prr = explode(',', $pr['persil']);
    		 		foreach ($prr as $ii) {
    		 			$group_persil[] = $ii;
    		 		}
    		 	}else{
    		 		$group_persil[] = $pr['persil'];
    		 	}
    		} 

    		if(count($group_persil) > 0){
    			$this->db->where_not_in('persil',$group_persil);
    		}
    	}
    	$persil = $this->input->get('q');
        $result = $this->db->select("persil,blok")
                           ->like('persil',$persil,'%%')
                           ->get('m_persil')
                           ->result();
        foreach ($result as &$row) {
        	$row->nama = 'PERSIL ' . $row->persil;
        	if(!empty($row->blok)){
        		$row->nama .= ' BLOK ' . $row->blok;
        	}
        }       
        // echo $this->db->last_query();            
        echo json_encode($result);
    } 
	
}

