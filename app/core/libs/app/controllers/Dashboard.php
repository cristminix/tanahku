<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Theme_Controller {
	public $_page_title = 'Dashboard';
	
	public function index()
	{
		$table_keys = [
			'spo_kutipan_daftar_buku_c'=>'Kutipan Daftar Buku C',
			'spo_sba_kesaksian'=>'Berita Acara Kesaksian',
			'spo_skt_akses_jalan'=>'Surat Keterangan Akses Jalan',
			'spo_skt_kepemilikan_tanah'=>'Surat Keterangan Kepemilikan Tanah',
			'spo_skt_riwayat_tanah'=>'Surat Keterangan Riwayat Tanah',
			'spo_spp_sertifikat_tanah'=>'Surat Pernyataan Permohonan Sertifikat Tanah',
			'spo_spr_png_fisik_bidat'=>'Surat Pernyataan Penguasaan Fisik Bidang Tanah',
			'spo_spt_tidak_sengketa'=>'Surat Peryataan Tanah Tidak Sengketa',

		];

		$series_data = [
		];
		foreach ($table_keys as $table => $caption) {
			$cx = $this->db->select("COUNT(id) as cx")->get($table)->row()->cx + 0;		
			$series['y'] = $cx;
			$series['name'] = sprintf("%s(%d)",$caption,$cx);
			$series_data[] = $series;
		}

		$data = [
			'series_data' => $series_data
		];
		$this->view('dashboard/dashboard',$data);
	}

	public function set_toggle_session_menu($state)
	{
		$this->session->set_userdata('toggle_session_menu',$state);
	}
}
