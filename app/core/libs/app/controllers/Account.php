<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends Theme_Controller {

	
	public function index()
	{
		$this->view('dashboard');
	}
	public function _unique_nomor_hp_for_edit($nomor_hp,$user_id)
	{
		$am = $this->db->where('user_id',$user_id)->get('account_map')->row();
		$account_id = $am->parent_id;
		$group_id = $am->t;

		if(!is_object($am)){
			return false;
		}
		$valid_nomor_hp_unique = false;
		 
        if($group_id == 0){

            $this->db->where('user_id !=', $account_id);
        }
        $valid_nomor_hp_unique = $this->db->select("COUNT(*) _count")
                                      ->where('value',$nomor_hp)
                                      ->where('key','nomor_hp')
                                      ->get('user_info')
                                      
                                      ->row()->_count == 0;
            
        

        return $valid_nomor_hp_unique;
	}

	public function _unique_email_for_edit($email,$user_id)
	{
		$am = $this->db->where('user_id',$user_id)->get('account_map')->row();
		$account_id = $am->parent_id;
		$group_id = $am->t;

		if(!is_object($am)){
			return false;
		}
		$valid_email_unique = false;
		$log = '';
		// STEP 1
        // PEGAWAI
        if($group_id == 2){
        	$this->db->where('id !=',$account_id);
        }    
        $valid_email_unique = $this->db->select("COUNT(id) _count")
                                          ->where('email',$email)
                                          ->get('account')
                                          ->row()->_count == 0;
                                        
        // die($valid_email_unique);
        // STEP 2
        if($valid_email_unique){
            // check in account_register
            // NON
            if($group_id == 1){
                $this->db->where('id !=', $account_id);
            }
            $valid_email_unique = $this->db->select("COUNT(id) _count")
                                          ->where('email',$email)
                                          ->get('account_register')
                                          ->row()->_count == 0;
  			
        }

        // STEP 3
        if($valid_email_unique){
            // check in user_info
            // NON
            if($group_id == 0){

                $this->db->where('id !=', $account_id);
            }
            $valid_email_unique = $this->db->select("COUNT(id) _count")
                                          ->where('email',$email)
                                          ->get('account_adm')
                                          ->row()->_count == 0;
            
        }
        // die($valid_email_unique);
        return $valid_email_unique;
	}
	public function changePassword()
	{
		$this->form_validation->set_rules('user_id','User ID','required');
        $this->form_validation->set_rules('old_password','Kata Sandi Lama','required');
        $this->form_validation->set_rules('new_password','Kata Sandi Baru','required|min_length[4]');
        $this->form_validation->set_rules('repeat_new_password','Ulang Kata Sandi Baru','required|min_length[4]');
                
        $user_id = $this->input->post('user_id');
        $old_passwd = $this->input->post('old_password');
        $new_passwd = $this->input->post('new_password');
        $repeat_new_passwd = $this->input->post('repeat_new_password');
        
        $msg = '';
        $msg_repeat_password_not_same = '';

        

        $success = false;
        $am = $this->db->where('user_id',$user_id)->get('account_map')->row();
        if($am->t == '2'){
            $encrypted_passwd = md5(md5('$notimetodie$').$old_passwd);
        }else{
            $encrypted_passwd =  md5($this->config->item('encryption_key').$old_passwd.'manis-legi'.$old_passwd);
        }
        

        if($this->form_validation->run()){
            $uppercase = preg_match('@[A-Z]@', $new_passwd);
            $lowercase = preg_match('@[a-z]@', $new_passwd);
            $number    = preg_match('@[0-9]@', $new_passwd);
            
            $valid_passwd = true;
            $valid_repeat_passwd = false;
            
            $valid_old_passwd = $am->passwd === $encrypted_passwd;
            if(!$valid_old_passwd){
                $msg .= 'Kata Sandi Lama salah' ."\n";
            }
            // if(!($uppercase&&$lowercase && $number)) {
            //   $msg .= 'Kata Sandi Baru tidak valid' . "\n";
            // }else{
            //     $valid_passwd = true;
            // }
            if($new_passwd !== $repeat_new_passwd){
              $msg .= 'Ulang Kata Sandi Baru tidak Sama'."\n";
            }else{
                $valid_repeat_passwd = true;
            }

            if($valid_passwd && $valid_repeat_passwd && $valid_old_passwd){
                $this->load->model('m_account_v2');
                $success = $this->m_account_v2->change_password($user_id, $encrypted_passwd, $new_passwd,$am);
            }
        }else{
            $validation_errors = validation_errors();
            $msg = str_replace(['<p>','</p>'],['',""],$validation_errors);
                        
        }
        
        
        $response = ['success'=>$success, 'message'=>$msg];

        echo json_encode($response);
	}
	public function updateProfile()
	{

		$this->load->library('form_validation');
		$this->form_validation->set_rules('user_id','User ID','required|numeric');
		$this->form_validation->set_rules('email','Email','valid_email');
		$this->form_validation->set_rules('nomor_hp','Nomor HP','numeric');
		// $this->form_validation->set_rules('nama_lengkap','Nama Lengkap','numeric');

		$response = [
			'success' => false,
			'message' => ''
		];

		$user_id = $this->input->post('user_id');
		$nama = $this->input->post('nama');
		$email = $this->input->post('email');
		$nomor_hp = $this->input->post('nomor_hp');
		
		if($this->form_validation->run()){
			$unique_email = true;
			$unique_nomor_hp = true;

			if(isset($email)){
				$unique_email = $this->_unique_email_for_edit($email,$user_id);
			}
			if(isset($nomor_hp)){
				$unique_nomor_hp = $this->_unique_nomor_hp_for_edit($nomor_hp,$user_id);
			}

			if($unique_email && $unique_nomor_hp){
				$am = $this->db->where('id',$user_id)->get('user_map')->row();
				if(is_object($am)){
					$account_id = $am->user_id;
					$group_id = $am->t;

					switch ($group_id) {
						case '1': // non
				
						$data = [
							
						];
						if(!empty($email)){
							$data['email'] = $email;
						}
						if(!empty($nama)){
							$data['nama_lengkap'] = $nama;
						}
						if(!empty($nomor_hp)){
							$data['nomor_hp'] = $nomor_hp;
						}

						$this->db->where('id',$account_id)->update('account_register',$data);
						break; 
					case '2': // PEGAWAI
						// $data = [];
						
						
						// if(!empty($email)){
						// 	$data['email'] = $email;
						// 	$this->db->where('id',$account_id)->update('account',$data);
						// }

						 
						// $account = $this->db->where('id',$account_id)->get('account')->row();
						// $this->m_api_simpeg->update_profile($account->id_pegawai,$nomor_hp);
						
						break;
					case '0': // adm
						$data = [];
						
						
						if(!empty($email)){
							$data['email'] = $email;
						}
						if(!empty($nama)){
							$data['nama_lengkap'] = $nama;
						}
						

						$this->db->where('id',$account_id)->update('account_adm',$data);

						if(!empty($nomor_hp)){

							$user_info = $this->db->where('user_id',$user_id)
												  ->where('key','nomor_hp')
												  ->get('user_info')
												  ->row();
							if(!empty($user_info)){
								$this->db->where('user_id',$user_id)
										 ->where('key','nomor_hp')
										 ->update('user_info',['value'=>$nomor_hp]);
							}else{
								$this->db->insert('user_info',[
									'key' => 'nomor_hp',
									'value' => $nomor_hp,
									'user_id' => $user_id
								]);	
							}
							 
						}
						break;  
						
					}
					$response['success'] = true;
				}
			}
			if(!$unique_email){
				$response['message'] .= 'Email sudah digunakan' ."<br/>";
			}
			if(!$unique_nomor_hp){
				$response['message'] .= 'Nomor HP sudah digunakan' . "<br/>";
			}

		}else{
			$response['message'] = validation_errors();
		}

		echo json_encode($response);

	}
	public function change_avatar()
	{
		// print_r($_FILES);
		$owner = $this->input->post('user_id');
		$group_id = $this->input->post('group_id');

		// if(empty($group_id)){
		// 	die();
		// }
		// if( $group_id == 2){
		// 	die();
		// }

		$config['upload_path'] = BASE.'/uploads/avatar/';
		$config['allowed_types'] = 'jpg|jpeg|png|JPG|JPEG|PNG';
    
		$this->load->library('upload', $config);
    
    	$field_name = "file";
        
		if ( ! $this->upload->do_upload($field_name))
		{
			$msg =  $this->upload->display_errors() ;
			echo json_encode(['success'=>false,'message'=>$msg]);
			return false;
		}

		$uploaded_file =(object) $this->upload->data();
		$uploaded_file->success = true;

		$row = $this->db->where('owner',$owner)
							->where('rules','photo_profile')
							->get('uploads')->row();

		if(is_object($row)){
			@unlink(BASE .'/'. $row->file_path);
			$this->db->where('owner',$owner)
					 ->where('rules','photo_profile')
					 ->delete('uploads');
		}
		$upload = [
			'rules' => 'photo_profile',
			'desc'=>'photo_profile',
			'owner' => $owner,
			'user_id' => $owner,
			'file_name' => $uploaded_file->file_name,
			'file_type' => $uploaded_file->file_type,
			'file_ext' => $uploaded_file->file_ext,
			'is_image' => 1,
			'file_size' => $uploaded_file->file_size,
			'image_width'=>$uploaded_file->image_width,
			'image_height'=>$uploaded_file->image_height,
			'creation_date'=> date('Y-m-d H:i:s'),
			'file_path' => 'uploads/avatar/'.$uploaded_file->file_name
		];
		$this->db->insert('uploads',$upload);

		$upload['id'] = $this->db->insert_id();

		echo json_encode($uploaded_file);
	}
	public function login($validate=false)
	{
		if($validate=='validate' && $this->input->get('mType') == 'json'){

			//
			header("Content/Type:application/json");
			$response=new stdClass();
			$response->msg = '#json only for validate';
			echo json_encode($response);
			exit();
		}
		// $this->log->write_log2('login',$this->input->ip_address().' : Mengakses halaman login');

		if(is_object($this->__session_data)){
			return $this->_login_success();
		}
		if($this->input->post()){ //detect input post only
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username','Username/Email  ','required');
			$this->form_validation->set_rules('password','Kata Sandi  ','required');
		
		 	if(APP_YML['app_enable_google_captcha']){
				$this->form_validation->set_rules('g-recaptcha-response','Rechaptcha','required');
            }

			if($this->form_validation->run() != false){

			 	if(APP_YML['app_enable_google_captcha']){

					if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
					{
					    $secret = APP_YML['app_google_captcha_secret'];
					    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
					    $responseData = json_decode($verifyResponse);
					    if($responseData->success)
					    {
					        $succMsg = 'Your login request have submitted successfully.';

					    }
					    else
					    {
					        $errMsg = 'Robot verification failed, please try again.';
					        die($errMsg);
					    }
					}else{
						die('Robot verification failed, please try again.');
					}
				}

				$username = $this->input->post('username');
				$password = $this->input->post('password');

				if($account = $this->_proces_login($username,$password)){
					if($account->is_active == 1){
						return $this->_login_success($account);
					}else{
						return $this->_login_failed('Akun tidak aktif');

					}
				}else{
					return $this->_login_failed();
				}
			}else{
				return $this->_login_validation_error();
			}
	 
		}
		$this->__site_layout = 'login_layout';
		
		// $captcha = $this->_create_captcha();
		// $this->session->set_userdata('captcha_word', strtolower($captcha['word']) );

		$this->view('login/login_page',['login_failed'=>false,'validation_error'=>'','captcha_image'=>'']);
	}
	public function _login_validation_error()
	{
		// $this->log->write_log2('login',$this->input->ip_address().' : Login validasi error');

		$data=[
			'validation_error' => validation_errors(),
			'login_failed' => true
		];
		$this->__site_layout = 'login_layout';
		$this->view('login/login_page',$data);
	}
	public function _login_failed($message='')
	{
		// $this->log->write_log2('login',$this->input->ip_address().' : Login salah memasukkan data');

		$data=[
			'login_failed' => true,
			'message' => $message
		];
		$this->__site_layout = 'login_layout';
		$this->view('login/login_page',$data);
	}
	public function _login_success($account='')
	{
		

		if(!empty($account)){
			$this->__session_data = $account;
			$this->session->set_userdata('account',$account);

			// $this->log->write_log2('login',$this->input->ip_address().' : Login berhasil , data=',json_encode($account));

		}

		redirect('/dashboard');
	}
	public function logout($value='')
	{
		// echo 'This is logout page';
		$this->session->unset_userdata('account');

		// $this->log->write_log2('logout',$this->input->ip_address().' : Logout berhasil ');

		redirect('login');
	}
	public function forget($value='')
	{
		 
		if($this->input->get('change_password') == 'true'  ){
    		$id = $this->input->post('user_id');
    		$t = $this->input->post('t');
    		$parent_id = $this->input->post('parent_id');
    		$password = $this->_encrypt_password($this->input->post('password'));
    		$reg_account = $this->session->userdata('reg_account');
    		// $data = $this->input->get('data');
    		// $data = json_decode(base64_decode($data));
    		// echo $data;
    		if(is_array($reg_account)){
    			if($id == $reg_account['user_id']){
    				$data = [
						'passwd' => $password
					];
					if($t == 0){
						$table_name = 'account_adm';
					}else if($t == 1){
						$table_name = 'account_register';

					}else if($t == 2){
						$table_name = 'account';

					}

					$this->db->where('id',$parent_id)->update($table_name,$data);
					$this->session->unset_userdata('reg_account');
					die('<div class="alert alert-success"><i class="fa fa-check"></i> Kata sandi berhasil diubah</div>');
					// $this->__site_layout = 'single_layout';
					// return $this->view('login/forget_form',['change_password_success'=>true]);
    			}
    		}
    		die('Link Expired !');
    	}
		if($this->input->get('form') == 'true'  ){
    		$id = $this->input->get('id');
    		$reg_account = $this->session->userdata('reg_account');
    		$data = $this->input->get('data');
    		$data = json_decode(base64_decode($data));
    		// echo $data;
    		if(is_array($reg_account)){
    			if($data->user_id == $reg_account['user_id']){
    				$data = [
						'page_title' => 'Reset Kata Sandi',
						'user_id' => $data->user_id,
						'email'=>$data->email,
						't' => $data->t,
						'parent_id' => $data->parent_id
					];
					$this->__site_layout = 'single_layout';
					return $this->view('login/forget_form',$data);
    			}
    		}
    		die('Link Expired !');
    	}
		if($this->input->get('resend_email') == 'true'){
    		$this->_forget_send_email();
    		die();
    	}
		$data = [
			'page_title' => 'Lupa Kata Sandi'
		];
		$this->__site_layout = 'single_layout';
		$this->view('login/forget_page',$data);
	}
	public function forget_v2_post()
	{

		$this->load->library('form_validation');
		$this->form_validation->set_rules('owner','Owner','required');
		$this->form_validation->set_rules('email','Email','required|valid_email');
		$this->form_validation->set_rules('user_id','User ID','required');
		$this->form_validation->set_rules('parent_id','Account ID','required');
		$this->form_validation->set_rules('passwd','Kata Sandi','required|min_length[6]');
		$this->form_validation->set_rules('repeat_passwd','Ulangi Kata Sandi','required|min_length[6]|matches[passwd]');

		$owner = $this->input->post('owner');
		$email = $this->input->post('email');
		$user_id = $this->input->post('user_id');
		$account_id = $this->input->post('parent_id');
		$passwd = $this->input->post('passwd');

		$msg = 'Maaf kami tidak dapat meneruskan permintaan Anda';
		$success = false;
		$log = false;
		$mail_cache = $this->db->where('value',$owner)
							   ->where('key','forget_password')
							   ->where('user_id',$user_id)
							   ->get('caches')->row();
		if($this->form_validation->run()){
			
			if(!empty($mail_cache)){
				$valid_interval =  strtotime($mail_cache->valid_until) - time();
				if($valid_interval>0){ 
				$uppercase = preg_match('@[A-Z]@', $passwd);
	            $lowercase = preg_match('@[a-z]@', $passwd);
	            $number    = preg_match('@[0-9]@', $passwd);

	            if(!($uppercase && $lowercase && $number)) {
	              $msg = 'Kata Sandi Baru tidak valid' . "<br/>";
	            }else{
	            	$am = $this->db->where('user_id',$user_id)->get('account_map')->row();
	            	$group_id = $am->t;
	            	
	            	$table_maps = [
						'0' => 'account_adm',
						'1' => 'account_register',
						'2' => 'account'
					];


					$this->load->model('m_account_v2');
					$this->m_account_v2->change_password($user_id,$am->passwd, $passwd,$am);

					
					$success = true;
	            }
	        	}
			}else{
				// $msg = ''
			}						   
		}else{
			$msg = validation_errors();
			// $msg = str_replace(['<p>','</p>'],['',""],$validation_errors);
		}
		

		$this->db->where('value',$owner)
							   ->where('key','forget_password')
							   ->where('user_id',$user_id)
							   ->delete('caches');
		$response =[
			'mail_cache' => $mail_cache,
			'msg' => $msg,
			'success' => $success,
			'log' => $log
		];
		echo json_encode($response);
	}
	public function forget_v2()
	{
		$owner = $this->input->get('owner');
		$mail_cache = $this->db->where('value',$owner)
							   ->where('key','forget_password')
							   ->get('caches')->row();
		$msg = 'Maaf kami tidak dapat meneruskan permintaan Anda';

		$success = false;

		if(!empty($mail_cache)){
			$valid_interval =  strtotime($mail_cache->valid_until) - time();
			if($valid_interval > 0){
				$am = $this->db->where('user_id',$mail_cache->user_id)
							   ->get('account_map')
							   ->row();
				if(!empty($am)){
					 		 	 
					
					$new_owner = md5(microtime());
					$this->db->where('value',$owner)
							 ->where('key','forget_password')
							 ->update('caches',['value'=>$new_owner]);
					$data = [
						'page_title' => 'Ubah Kata Sandi',
						'user_id' => $am->user_id,
						'email'=>$am->email,
						't' => $am->t,
						'parent_id' => $am->parent_id,
						'owner' => $new_owner
					];
					$this->__site_layout = 'single_layout';
					// $this->_page_title = 'Ubah Kata Sandi'; 
					return $this->view('login/forget_form_v2',$data);
				}else{
					$msg = 'Peta user tidak sah';
				}				   
				
			}else{
				$msg = 'Maaf Link sudah kadaluarsa';
			}
			$title = $msg;
		}
		$this->__site_layout = 'info_layout';	
		$this->view('account/forget_info',['success'=>$success,'msg'=>$msg,'owner'=>$owner,'title'=>$title]);	

		
	}
	 
	function _proces_login($username,$password){
		$passwd = $this->_encrypt_password($password);
        // echo $password . "\n";
        $password_simpeg = $this->_encrypt_password($password,true);

        $is_nip =  $this->db->select("id,nip")
        		 ->where('nip',$username)
        		 ->get('account')
        		 ->row();
       	$login = [];

       	if(is_object($is_nip)){
       		$login = $this->db->where('nip_nik',$username)
                 ->where('passwd', $password_simpeg)
                 ->where('is_active',1)
                 ->where('t',2)
                 ->where('parent_id',$is_nip->id)
                 ->get('account_map')
                 ->row();
       	}else{
       		$login = $this->db->group_start()->where('username',$username)
                 ->or_where('email',$username)
                 ->group_end()
                 ->group_start()
                 ->where('passwd', $passwd)
                 ->or_where('passwd', $password_simpeg)
                 ->group_end()
                 ->where('is_active',1)
                 ->get('account_map')

                 ->row();	
       	}		 
        
        

		if(!empty($login)){
			if($login->role == 'Admin'){
				$login->rules = 'admin';
			}else if($login->role == 'Pegawai'){
				$login->rules = 'pegawai';
			}else if($login->role == 'Non Pegawai'){
				$login->rules = 'non';
			}
		}
		// die($this->db->last_query());
		return $login;

	}
	function _encrypt_password($str,$simpeg=false){
		if($simpeg){
        	return md5(md5('$notimetodie$').$str);
		}
		return md5($this->config->item('encryption_key').$str.'manis-legi'.$str);
	}

	function _send_email($from_address, $from_name, $to_address, $subject, $message)
    {

  

        $this->load->library('email');
        //send email to user
        // $config['useragent']      = (string) $this->cms_get_config('cms_email_useragent');
        $config['protocol']       = 'smtp';
        // $config['mailpath']       = (string) $this->cms_get_config('cms_email_mailpath');
        $config['smtp_host']      = SMTP_YML['smtp_host'];
        $config['smtp_user']      = SMTP_YML['smtp_user'];
        $config['smtp_pass']      = SMTP_YML['smtp_pass'];
        $config['smtp_port']      = SMTP_YML['smtp_port'];
        // $config['smtp_timeout']   = 30;//(integer) $this->cms_get_config('cms_email_smtp_timeout');
        // $config['wordwrap']       = (boolean) $this->cms_get_config('cms_email_wordwrap');
        // $config['wrapchars']      = (integer) $this->cms_get_config('cms_email_wrapchars');
        $config['mailtype']       = 'html';
        $config['charset']        = 'iso-8859-1';
        $config['validate']       = FALSE;
        // $config['priority']       = (integer) $this->cms_get_config('cms_email_priority');
        $config['crlf']           = "\r\n";
        $config['newline']        = "\r\n";
        $config['smtp_crypto']        = "ssl";
        // $config['bcc_batch_mode'] = (boolean) $this->cms_get_config('cms_email_bcc_batch_mode');
        // $config['bcc_batch_size'] = (integer) $this->cms_get_config('cms_email_bcc_batch_size');
       
        // print_r($config);
        // die($message);
        $this->email->initialize($config);
        // $this->email->set_header("Reply-To","no-reply@perumdamtkr.com");
        $this->email->from($from_address, $from_name);
        $this->email->to($to_address);
        $this->email->subject($subject);
        $this->email->message($message);

        $success = $this->email->send();
       
        if(!$success){
        	log_message('error', $this->email->print_debugger());
        }
        return $success;
    }

    public function myaccount($value='')
    {
    	$foto = '';
    	// $this->load->model('m_api_simpeg_v2');
    	$am = $this->db->where('id',$this->cms_user_id())->get('user_map')->row_array();
    
		$account = $this->db->where('id',$am['user_id'])->get('account_adm')->row_array();
		$row = $this->db->where('owner',$am['id'])
					->where('rules','photo_profile')
					->get('uploads')->row();
		// print_r($row);
		// die();			
		if(is_object($row)){
			$foto_path = BASE . '/' . $row->file_path;
			if(file_exists($foto_path)){
				$foto = site_url($row->file_path);
			}
		}	
		$user_info = $this->db->select('value')
								  ->where('user_id',$am['user_id'])
								  ->where('key','nomor_hp')
								  ->get('user_info')
								  ->row();
	
		if(is_object($user_info)){
			$account['nomor_hp'] = $user_info->value;
		}
    	

    	$data = [
    		'am'=> $am,
    		'account' => $account,
    		'foto' => empty($foto)?site_url().'themes/metronic/assets/pages/media/profile/profile_user.png' :$foto
    	];
    	$this->view('my_user_info',$data);
    }

    public function register($value='')
    {
    	if($this->input->get('unique_validation') == 'true'){
    		$nik 	  = $this->input->post('nik');
    		$email 	  = $this->input->post('email');
    		$nomor_hp = $this->input->post('nomor_hp');

    		$pass_email = $this->_unique_email($email);
    		$pass_nik = $this->db->where('nik',$nik)->get('account_register')->num_rows() == 0;
    		$pass_nomor_hp = $this->_unique_nomor_hp($nomor_hp);

    		$response = [
    			'success' => $pass_email   && $pass_nik  && $pass_nomor_hp ,
    			'message' => (!$pass_email    ? 'Email Sudah Digunakan' . "\n":'').
    						 (!$pass_nik   ? 'NIK Sudah Digunakan' . "\n":'').
    						 (!$pass_nomor_hp  ? 'Nomor HP Sudah Digunakan' . "\n":'')
    		];
    		die(json_encode($response));
    	}
    	if($this->input->get('resend_email') == 'true'){
    		$owner = $this->input->get('owner');
    		$data_64 = $this->input->get('data');
    		$json = base64_decode($data_64);
    		$obj = json_decode($json);
    		if(is_object($obj)){
    			$data = (array)$obj;
    			$this->_register_send_email($owner, $data);
    			echo "Resend Email Berhasil";
    		}
    		
    		die();
    	}
    	 
		if($this->input->post()){ //detect input post only
			$this->load->library('form_validation');
			$this->form_validation->set_rules('nama_lengkap','Nama Lengkap','required');
			$this->form_validation->set_rules('email','Alamat Email','required');
			$this->form_validation->set_rules('nik','NIK','required');
			$this->form_validation->set_rules('nomor_hp','Nomor HP','required');
			$this->form_validation->set_rules('password','Kata Sandi','required');
			$this->form_validation->set_rules('repeat_password','Ulangi Kata Sandi','required');
            
            if(APP_YML['app_enable_google_captcha']){
				$this->form_validation->set_rules('g-recaptcha-response','Rechaptcha','required');
            }


			if($this->form_validation->run() != false){

            	if(APP_YML['app_enable_google_captcha']){

					if(isset($_POST['g-recaptcha-response']) && !empty($_POST['g-recaptcha-response']))
					{
					    $secret = APP_YML['app_google_captcha_secret'];
					    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
					    $responseData = json_decode($verifyResponse);
					    if($responseData->success)
					    {
					        $succMsg = 'Your contact request have submitted successfully.';

					    }
					    else
					    {
					        $errMsg = 'Robot verification failed, please try again.';
					        die($errMsg);
					    }
					}else{
						$errMsg = 'Robot verification failed, please try again.';
					        die($errMsg);
					}
				}	

				$nama_lengkap = $this->input->post('nama_lengkap');
				$password = $this->input->post('password');
				$email = $this->input->post('email');
				$nik = $this->input->post('nik');
				$repeat_password = $this->input->post('repeat_password');
				$nomor_hp = $this->input->post('nomor_hp');

				$new_account = [
					'nama_lengkap' => $nama_lengkap,
					'email' => $email,
					'nik' => $nik,
					'password' => $password,
					'nomor_hp'=>$nomor_hp,
					'username'=>slugify($nama_lengkap).md5(microtime())
				];

				if($register = $this->_proces_register($new_account,$msg)){
					return $this->_register_success($register);
				}else{
					return $this->_register_failed($msg);
				}
			}else{
				return $this->_register_validation_error();
			}
	 
		}
	 
		$this->__site_layout = 'register_layout';
		$this->view('register/register_page',['register_failed'=>false,'validation_error'=>'']);
    }
    function _proces_register( $new_account, &$msg)
    {
		$owner = md5(microtime());
    	 

		$config['upload_path'] = BASE.'/uploads/';
		$config['allowed_types'] = 'jpg|jpeg|png|JPG|JPEG|PNG';
    
		$this->load->library('upload', $config);
    
    	$field_name = "nik_file";
        
		if ( ! $this->upload->do_upload($field_name))
		{
			$msg =  $this->upload->display_errors() ;
			return false;
		}
    	$field_name2 = "ktp_selfi_file";

		$uploaded_file = $this->upload->data();

		if ( ! $this->upload->do_upload($field_name2))
		{
			$msg =  $this->upload->display_errors() ;
			@unlink(BASE . '/uploads/'.$uploaded_file['file_name']);
			return false;
		}
		$uploaded_file2 = $this->upload->data();
		

		$upload = [
			'rules' => 'register',
			'desc'=>'KTP',
			'owner' => $owner,
			'file_name' => $uploaded_file['file_name'],
			'file_type' => $uploaded_file['file_type'],
			'file_ext' => $uploaded_file['file_ext'],
			'is_image' => 1,
			'file_size' => $uploaded_file['file_size'],
			'image_width'=>$uploaded_file['image_width'],
			'image_height'=>$uploaded_file['image_height'],
			'creation_date'=> date('Y-m-d H:i:s'),
			'file_path' => 'uploads/'.$uploaded_file['file_name']
		];
		$this->db->insert('uploads',$upload);
		$ktp_id = $this->db->insert_id();

		$upload2 = [
			'rules' => 'register',
			'desc'=>'KTP Selfi',
			'owner' => $owner,
			'file_name' => $uploaded_file2['file_name'],
			'file_type' => $uploaded_file2['file_type'],
			'file_ext' => $uploaded_file2['file_ext'],
			'is_image' => 1,
			'file_size' => $uploaded_file2['file_size'],
			'image_width'=>$uploaded_file2['image_width'],
			'image_height'=>$uploaded_file2['image_height'],
			'creation_date'=> date('Y-m-d H:i:s'),
			'file_path' => 'uploads/'.$uploaded_file2['file_name']
		];
		$this->db->insert('uploads',$upload2);
		$ktp_selfi_id = $this->db->insert_id();

		$files = [
			'ktp'=> $ktp_id,
			'ktp_selfi' => $ktp_selfi_id
		];

		// $this->db->where('email',$new_account['email'])->where('is_active',0)->delete('account_register');

		$account_register = [
			'nik' => $new_account['nik'],
			'passwd' => $this->_encrypt_password($new_account['password']),
			'old_passwd' => $this->_encrypt_password($new_account['password']),
			'email'=>$new_account['email'],
			'nama_lengkap'=>$new_account['nama_lengkap'],
			'reg_date'=>date('Y-m-d H:i:s'),
			'is_active' => 0,
			'nomor_hp' => $new_account['nomor_hp'],
			'username' => $new_account['username'],
			'is_email_confirmed'=>0,
			'files'=> json_encode($files)
		];

		$this->db->insert('account_register',$account_register);
		$account_id = $this->db->insert_id();

		// create user_map
        $user_map = [
            'user_id' => $account_id,
            't' => '1'
        ];

        $this->db->insert('user_map',$user_map);
        $user_id = $this->db->insert_id();    

        // update uploads
		$this->db->where('owner',$owner)->update('uploads',['user_id'=>$user_id]);

        // send email
        // mail cache
        $current_date = date('Y-m-d H:i:s');
        $stop_date = date('Y-m-d H:i:s', strtotime( "$current_date + 1 day" )); 

        $mail_cache = [
            'user_id' => $user_id,
            'key' => 'mail_confirm',
            'valid_until' => $stop_date,
            'value' => $owner
        ];

        $this->db->insert('caches',$mail_cache);
		$this->_register_send_email($account_register, $owner);


		return [$account_register, $owner];

	}
	function _register_validation_error( ){
		 // echo 'Register:Validation Err';
		 // echo validation_errors();
		$this->__site_layout = 'register_layout';
		$this->view('register/register_page',['register_failed'=>false,'validation_error'=>validation_errors()]);

	}
	function _register_failed( $msg ){
		$this->__site_layout = 'register_layout';
		$this->view('register/register_page',['register_failed'=>true,'msg'=>$msg]);

	}
	function _register_success($register){
		$data = [
	 		'nama_lengkap' => $register[0]['nama_lengkap'],
	 		'email' => $register[0]['email'],
	 	];
		redirect('register/success?ref=register&owner='.$register[1].'&data='.base64_encode(json_encode($data)));
	}
	function _register_send_email($account_register, $owner){
		
		$message = $this->load->view('email/email_verifikasi',[],true);
	 	
        $this->_custom_keywords = [
        	'nama_lengkap' =>  $account_register['nama_lengkap'],
        	'link_verifikasi'=> site_url('confirm_mail_v2?owner=').$owner
        ];
        $message = $this->cms_parse_keyword($message);

		$this->_send_email(SMTP_YML['smtp_user'], 'PPSL PERUMDAM TKR', $account_register['email'], 'Verifikasi Email Pendaftaran Baru PPSL PERUMDAM TKR', $message);
	}
	function _forget_send_email(){
		

		$message = $this->load->view('email/email_forget',[],true);
		$email = $this->input->post('email');
		$response = [
			'success' => false,
			'message' => 'Tidak Ada Akun Terkait dengan alamat Email ini',
			'email'=>$email
		];

		$reg_account = $this->db->where('email',$email)->get('account_map')->row_array();
		
		// $owner = 'owner';
		$this->session->set_userdata('reg_account',$reg_account);

		if(is_array($reg_account)){
			$data = ['user_id'=>$reg_account['user_id'],'email'=>$email];
			$link_reset_password = site_url() . 'forget?form=true&user_group=non&data='.base64_encode(json_encode($data));
			$this->_custom_keywords = [
	        	'nama_lengkap' =>  $reg_account['nama_lengkap'],
	        	'link_reset_password'=>  $link_reset_password
	        	
	        ];
	        $message = $this->cms_parse_keyword($message);

			$this->_send_email(SMTP_YML['smtp_user'], 'PPSL PERUMDAM TKR', $email, 'Permintaan Reset Password PPSL PERUMDAM TKR', $message);
			$response['success'] = true;
			$response['message'] = 'Email berhasil dikirim';
		}

		echo json_encode($response); 
       
	}
	public function register_success()
	{
		$owner = $this->input->get('owner');
		$data = $this->input->get('data');
		$this->__site_layout = 'register_success_layout';
		$this->view('register/sukses',['register_failed'=>false,'validation_error'=>'','owner'=>$owner,'data'=>$data]);
	}
	public function confirm_mail()
	{
		$get_owner = $this->input->get('owner');
		$owner = $this->session->userdata('upload_owner_register');

		if($owner == $get_owner){
			// echo 'Valid Activation Code : Activation Succes ';
		}
		$upload = $this->db->where('owner',$get_owner)->get('uploads')->row();
		// print_r($upload);
		if(is_object($upload)){
			$reg_id = $upload->user_id;
			// echo $reg_id;
			$this->db->where('id',$reg_id)->update('account_register',['is_email_confirmed'=>1]);

			// redirect('login?ref=aktifasi');

			$this->__site_layout = 'confirm_mail_success_layout';
			$this->view('register/confirm_mail',['register_failed'=>false,'validation_error'=>'','owner'=>$owner]);
	
		}else{
			die('Maaf link sudah Kadaluarsa' );
		}
	}
	public function confirm_mail_v2()
	{
		$owner = $this->input->get('owner');
		$mail_cache = $this->db->where('value',$owner)
							   ->where('key','mail_confirm')
							   ->get('caches')->row();
		$msg = 'Maaf kami tidak dapat meneruskan permintaan Anda';

		$success = false;

		if(!empty($mail_cache)){
			$valid_interval =  strtotime($mail_cache->valid_until) - time();
			if($valid_interval > 0){
				$am = $this->db->where('user_id',$mail_cache->user_id)
							   ->get('account_map')
							   ->row();
				if(!empty($am)){
					$this->db->where('id',$am->parent_id)
						 	 ->update('account_register',['is_email_confirmed'=>1]);
					$success = true;		 	 
					$msg = 'Terima kasih sudah melakukan Konfirmasi Email';
					
				}else{
					$msg = 'Peta user tidak sah';
				}				   
				
			}else{
				$msg = 'Maaf Link sudah kadaluarsa';
			}

			if($valid_interval <= 0){
				$this->db->where('value',$owner)
							 ->where('key','mail_confirm')
							 ->delete('caches');
			}
			$title = $msg;
		}
		$this->__site_layout = 'info_layout';	
		$this->view('register/confirm_mail',['success'=>$success,'msg'=>$msg,'owner'=>$owner,'title'=>$title]);					 
	}
	public function _unique_nomor_hp($nomor_hp)
    {
        $valid_nomor_hp_unique = false;
        $this->load->model('m_api_simpeg_v2');
        // STEP 1
        // PEGAWAI
     
        $r = $this->m_api_simpeg_v2->check_nomor_hp('', $nomor_hp);

        $valid_nomor_hp_unique = $r->success;
      
        // STEP 2
        if($valid_nomor_hp_unique){
            // check in account_register
            // NON

            $valid_nomor_hp_unique = $this->db->select("COUNT(id) _count")
                                          ->where('nomor_hp',$nomor_hp)
                                          ->get('account_register')
                                          ->row()->_count == 0;
  
        }
        // STEP 3
        if($valid_nomor_hp_unique){

            $valid_nomor_hp_unique = $this->db->select("COUNT(*) _count")
                                          ->where('value',$nomor_hp)
                                          ->where('key','nomor_hp')
                                          ->get('user_info')
                                          
                                          ->row()->_count == 0;
            
        }
        return $valid_nomor_hp_unique;
    }

    public function _unique_email($email)
    {
        $valid_email_unique = false;
      
        // STEP 1
        // PEGAWAI
         
        $valid_email_unique = $this->db->select("COUNT(id) _count")
                                          ->where('email',$email)
                                          ->get('account')
                                          ->row()->_count == 0;
                                        

        // STEP 2
        if($valid_email_unique){
           
            $valid_email_unique = $this->db->select("COUNT(id) _count")
                                          ->where('email',$email)
                                          ->get('account_register')
                                          ->row()->_count == 0; 
        }

        // STEP 3
        if($valid_email_unique){

            $valid_email_unique = $this->db->select("COUNT(id) _count")
                                          ->where('email',$email)
                                          ->get('account_adm')
                                          ->row()->_count == 0;
            
        }
        return $valid_email_unique;
    }
	 
}
