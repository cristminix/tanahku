<?php

class Manajemen extends Theme_Controller
{
    public $_page_title = 'Manajemen';
	
    public function __construct()
    {
        parent::__construct();
        // $this->load->model('m_api_simpeg');
    }
    public function _form($action='')
    {
        if($action == 'activate'){
            $id = $this->input->post('id');
            $jenis = $this->input->post('jenis');
            $table = [
                'non' => 'account_register'
            ];
            if($jenis == 'non'){
                // if($this->db->where('id',$id)->get($table[$jenis])->num_rows() == 1){
                //     die('Tidak bisa menghapus admin , karena tinggal satu pengguna');
                // }
                $message = $this->load->view('email/email_active',[],true);
                $email = $this->input->post('email');
                $response = [
                    'success' => false,
                    'message' => 'Tidak Ada Akun Terkait dengan alamat Email ini',
                    'email'=>$email
                ];
                $reg_account = $this->db->where('email',$email)->get('account_register')->row_array();
                
                $owner = 'owner';

                if(is_array($reg_account)){
                    $this->_custom_keywords = [
                        'nama_lengkap' =>  $reg_account['nama_lengkap'],
                        'link_login'=> site_url('login?ref=email_active').$owner,
                        'link_donwload_apk' => APP_YML['app_link_downlod_mobile'],
                    ];
                    $message = $this->cms_parse_keyword($message);

                    send_email(SMTP_YML['smtp_user'], 'PPSL PERUMDAM TKR', $email, 'Selamat Akun PPSL PERUMDAM TKR Anda Sudah aktif', $message);
                    $response['success'] = true;
                    $response['message'] = 'Email berhasil dikirim';
                }

                
                $data = [
                    'is_active' => true,
                    'is_verified' => true,
                ];
                $this->db->where('id',$id)->update($table[$jenis],$data);

                // echo 'success'; 
            }
            
            echo 'success';
            die();
        }
        if($action == 'delete'){
            $id = $this->input->post('id');
            $jenis = $this->input->post('jenis');
            $table = [
                'non' => 'account_register',
                'admin' => 'account_adm',
                'pegawai' => 'account',
            ];
            if($jenis == 'admin'){
                if($this->db->where('id',$id)->get($table[$jenis])->num_rows() == 1){
                    die('Tidak bisa menghapus admin , karena tinggal satu pengguna');
                }
            }
            $this->db->where('id',$id)->delete($table[$jenis]);
            echo 'success';
            die();
        }
        if($action == 'edit'){
            $id = $this->input->post('id');
            $nik = $this->input->post('nik');
            $nama_lengkap = $this->input->post('nama_lengkap');
            $email = $this->input->post('email');
            $is_active = $this->input->post('is_active');
            $is_verified = $this->input->post('is_verified');
            $jenis = $this->input->post('jenis');

            if($jenis == 'non'){
                $data = [
                    'nik' => $nik,
                    'nama_lengkap'=>$nama_lengkap,
                    'email'=>$email,
                    'is_active' => $is_active,
                    'is_verified' => $is_verified
                ];

                $this->db->where('id',$id)->update('account_register',$data);
            }
            

            echo 'success';
            die();
        }
    }
	function user_old($form='',$action='')
	{
        $this->_page_title = 'Manajemen User';

        if($form == 'form'){
            return $this->_form($action);
        }
        // $this->log->write_log2('LEVEL_IS_EQUAL', ['uri'=>'manajemen/user']);
        $app_data = [ 
            'pagination' => [
                'page' => $this->input->post('hal') ?? 1,
                'per_page'   => $this->input->post('batas') ?? 10
            ],
            'search_query' => $this->input->post('search_query'),
            'sub_title' => 'Manajemen Pengguna' ,
            'jenis_pengguna'  => ['admin'=>'Admin','pegawai'=>'Pegawai','non'=>'Non Pegawai'],
            'status_pengguna' => ['is_aktif'=>'Aktif','is_verified'=>'Verified','inative'=>'Inactive']
        ];
        $pensiun_detail = $this->load->view('manajemen/user_detail',$app_data,true);
        $pensiun_form = $this->load->view('manajemen/user_form',$app_data,true);

        $this->view('manajemen/user', [
        	'app_data'=>$app_data,
        	'detail'=>$pensiun_detail,
        	'form'=>$pensiun_form
        ]);
		 
	}
    public function user($sub='',$sub2='',$sub3='')
    {
        if(method_exists($this, $sub)){
            return $this->{$sub}($sub2,$sub3);
        }
        // $this->load->model('m_api_simpeg');

        // $this->m_api_simpeg->sync_account();
        
        $this->_page_title = 'Manajemen User';
        $data = [
            // 'unor_list' => $this->m_api_simpeg->get_unor()
        ];
        $this->view('manajemen_user',$data);
    }
    
    public function data($sub='',$sub2='',$sub3='')
    {
        if(method_exists($this, $sub)){
            return $this->{$sub}($sub2,$sub3);
        }
    }
    public function fetch($value='')
    {
        $this->load->model('m_user');

        $payload = extract_payloads();
        $filter = (array)$payload;

        $total_rec = $this->m_user->get_count($payload->search_query,$filter);

        $batas         = $payload->per_page;

        if(empty($batas)){
            $batas = 10;
        }
        $total_pages   = ceil($total_rec / $batas);
        $hal           = $payload->page ?? $total_pages;
        if(empty($hal)){
            $hal = $total_pages;
        }
        $mulai         = ($hal - 1) * $batas;
        if($mulai<0){
            $mulai = 0;
        } 
        $logs = $this->m_user->get_list($payload->search_query,$mulai,$batas,$filter);
        $data = [ 
            'users' => $logs,
            'success' => 1,
            'total_pages' => $total_pages,
            'total_rec' => intval($total_rec),
            'page'=>$hal
        ];
        echo json_encode($data);
    }
    public function setActive()
    {
        $payload = extract_payloads();
        $response=[
            'success'=>0,
            'msg'=>''
        ];
        $am = $this->db->where('user_id',$payload->user_id)->get('account_map')->row();
        if(is_object($am)){
            $new_state = 0;
            if( $am->is_active == 1){
                $new_state = -1;
            }else if( $am->is_active == 0){
                $new_state = 1;
            }else if( $am->is_active == -1){
                $new_state = 0;
            }   
            // echo $new_state;

            if($am->role == 'Admin'){
                if($new_state != 1){
                    // check admin count
                    $cx_admin = $this->db->where('user_id !=',$payload->user_id)
                                         ->where('role','Admin')
                                         ->get('account_map')
                                         ->num_rows();
                    if($cx_admin == 0){
                        $response['msg'] = 'Tidak dapat mem-banned Admin';
                    }                     

                }
            }else{
                $table_name = '';
                switch ($am->t) {
                    case 0:
                        $table_name = 'account_adm';
                    break;
                    case 1:
                        $table_name = 'account_register';
                    break;
                    case 2:
                        $table_name = 'account';
                    break;
                }
                if($table_name == 'account_register' && $new_state == 1){
                    $this->send_email_active($am);
                }
                $data = ['is_active' => $new_state];
                $this->db->where('id',$am->parent_id)->update($table_name,$data);
                $response['success']    = 1;
                // $response['table_name'] = $table_name;
                $am->is_active          = $new_state;
                $response['user']       = $am;
                // $response['sql']        = $this->db->last_query();
            }
        }

        echo json_encode($response);
    }
    public function send_email_active($am)
    {
        # code...
    }
    public function info($user_id)
    {
        

        
        $am = $this->db->where('user_id',$user_id)->get('account_map')->row_array();
        $table_maps = [
            '0' => 'account_adm',
            '1' => 'account_register',
            '2' => 'account'
         ];
        $table_name = $table_maps[$am['t']];
        $account = $this->db->where('id',$am['parent_id'])->get($table_name)->row();

        $foto = '';
        if(!isset($account->nomor_hp)){
            $account->nomor_hp='';
        }
        if(!isset($account->email)){
            $account->email='';
        }
        if($account->email == '0'){
            $account->email = '';
        }
        $this->load->model('m_api_simpeg_v2');
        // print_r($am)
        switch ($am['t']) {
            case '0':
                $user_info = $this->db->select('value')
                                          ->where('user_id',$user_id)
                                          ->where('key','nomor_hp')
                                          ->get('user_info')
                                          ->row();
            
                if(!empty($user_info)){
                    $account->nomor_hp = $user_info->value;
                }

                $upload = $this->db->where('user_id',$user_id)
                                   ->where('rules','photo_profile')
                                   ->where('owner',$user_id)
                                   ->get('uploads')
                                   ->row();
                if(!empty($upload)){
                    $account->photo_url = site_url() . $upload->file_path;
                    
                }
                break;
            case '1':
                
                $upload = $this->db->where('user_id',$user_id)
                                   ->where('rules','photo_profile')
                                   ->where('owner',$user_id)
                                   ->get('uploads')
                                   ->row();
                if(!empty($upload)){
                    $account->photo_url = site_url() . $upload->file_path;
                    
                }
                break;
            case '2':
                $pegawai = $this->m_api_simpeg_v2->get_profile($account->id_pegawai);
               
                $account->photo_url = $pegawai->thumb_url;
                $account->nomor_hp = $pegawai->nomor_hp;
                break;
            
        }

        $data = [
            'am' => $am,
            'account' => (array)$account,
            'foto' => empty($foto)?site_url().'themes/metronic/assets/pages/media/profile/profile_user.png' :$foto
        ];
        $this->view('user_info',$data);
    }
}
