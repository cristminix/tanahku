<?php  defined('BASEPATH') OR exit('No direct script access allowed');
use Foundationphp\Exporter\WordXml;
use Foundationphp\Exporter\OpenDoc;
use Foundationphp\Exporter\MsWord;
use Foundationphp\Exporter\Xml;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
class List_permohonan extends Theme_Controller {
	public $_page_title = 'Daftar Permohonan';
	public function __construct()
    {
        parent::__construct();
        // $this->load->model('m_api_simpeg');
    }
	///////////////////////////////////////////////////////////////////
	public function custom_grid_data()
    {
        $this->load->model('m_list_permohonan','model');
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $action = "<td class=\"actions\"> 
                
            <a href=\"".site_url('master/list_permohonan/index/edit/'.$field->id)."/".slugify($field->nama_1)."\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-edit\" aria-hidden=\"true\"></i> 
            </a>   
            <a onclick=\"javascript: return delete_row('".site_url('master/list_permohonan/index/delete/'.$field->id)."',-1,this)\" href=\"javascript:;\" class=\"gc-bt-delete delete_button btn btn-sm btn-icon btn-pure btn-danger\" role=\"button\"> <i class=\"fa fa-trash\" aria-hidden=\"true\"></i> 
            </a>
            </td>";
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $field->template; 
            $row[] = $action; 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('master/list_permohonan/index/add')];
        // die($this->session->userdata($data_marketing_filter_status));
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';
        $this->load->view('master/dt_list_permohonan.php',$tdata);
    }
    private function _customGrid(){
        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('master/list_permohonan/index/add')];
        $data['output'] = $this->load->view('master/dt_list_permohonan.php',$tdata,true);
        $data['unique_hash'] = md5(date('YmdHis-Unit'));
        // $data['unor_list'] = $this->m_api_simpeg->get_unor();
        $data['is_admin']  = $this->cms_user_group()=='admin';
        $this->view('master/list_permohonan.php', $data );
    }
    /*public function ac_picker()
    {
        $param_64  = $this->input->get('param');
        $param_str = base64_decode($param_64);
        $param     = json_decode($param_str);
        // print_r($param);
        $query = $this->input->get('term');
        $results = $this->db->select('a.*,a.template value')
                            ->like('a.template',$query,'%%')
                            ->where('a.target',$param->target)
                            ->order_by('a.'.$param->order,'asc')
                            ->get($param->table.' a')
                            ->result();
        echo json_encode($results);
    }
    */
    public function index()
    {
        $target_yaml = APP . '/form/spo_m_permohonan.yml';
        $buffer = file_get_contents($target_yaml);
        $_SERVER['FORM_DEF'] = Yaml::parse($buffer);
        $crud = $this->new_crud();
        $crud->set_subject('Permohonan');
        $state = $crud->getState();
        switch ($state) {
            case 'list':
                $args = func_get_args();
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            default:
                # code...
                break;
        }
        $crud->unset_jquery();
        // $crud->unset_export();
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_table('spo_m_permohonan');
		$crud->set_model('m_list_permohonan');
		$crud->set_theme('datatables');
        $crud->set_rules('template','Nama Permohonan','trim|required');
        $crud->set_rules('target','Target','trim|required');
       
    	$id_user = $this->cms_user_id();
        $crud->display_as('name','Nama');
        $crud->display_as('template','Nama Permohonan');
        $crud->field_type('name','hidden');
        $crud->field_type('target','dropdown',['spo_surat_kuasa'=>'Surat Kuasa']);
		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	$id_user = $this->cms_user_id();
        $data = $crud->render();
		$data->is_admin  = $this->cms_user_group()=='admin';
		$this->view('master/list_permohonan.php',$data);
    }
    public function file($cmd='',$id='',$slug='',$mode='')
    {
        if (empty($cmd)) {
           exit('No direct script access allowed');
        }
        $crud = $this->new_crud();
        $crud->set_subject('File');
        $crud->set_table('spo_m_permohonan');
        $crud->set_model('m_list_permohonan');
        $crud->set_theme('datatables');
        $crud->columns('file');
        $crud->fields('file');
        $upload_dir = 'word/spo_m_permohonan';
        if(!is_dir(BASE.'/uploads/'.$upload_dir)){
            @mkdir(BASE.'/uploads/'.$upload_dir);
        }
        $crud->set_field_upload('file', $upload_dir);
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $id_user = $this->cms_user_id();
        $data = $crud->render();
        $data->is_admin  = $this->cms_user_group()=='admin';
        $this->view('master/file_upload',$data);
    } 
    public function format_result_row($row)
    {
        // $row['dt_surat_kuasa'] = tanggal_indo($row['dt_surat_kuasa']);
        // $row['dasar_tgl_surat_kuasa'] = tanggal_indo($row['dasar_tgl_surat_kuasa']);
        // ;
        // $row['list_lampiran']  =  json_decode($row['list_lampiran']);
        // $row['list_permohonan']  =  json_decode($row['list_permohonan']);
        // foreach ($row['list_permohonan'] as $i => &$v) {
        //     $v = ($i+1) .'. '.$v ;
        // }
        return $row;
    }
    public function generate_odt($id,$slug)
    {
        $table = 'spo_m_permohonan';
        $dir = BASE . '/uploads/odt/templates/';
        $result = $this->db->where('id',$id)
                       ->get($table);
        if($result->num_rows() > 0){
            try {
                $xml = new WordXml($result->result_array(), null, $options);
                // $xml->set_has_children( 'list_lampiran');
                // $xml->set_has_children( 'list_permohonan');
                $xml->set_callback_row([$this,'format_result_row'])
                    ->generate();
                $download = new OpenDoc($xml);
                $download->setDocTemplate($dir . 'output/'.$table.'_wordTemplate.odt');
                $download->setXsltSource($dir . 'output/'.$table.'_word.xslt');
                $download->setImageSource($dir . 'images');
                $path = $download->create($table.'.odt',true);
                $path = explode('uploads', $path);
                $path = '../../../uploads'.$path[1];
                echo site_url().'pub/viewerjs/ViewerJS/#'.$path;
                // print_r($path);
            } catch (Exception $e) {
                $error = $e->getMessage();
                echo $error;
            }
        } 
    }
    public function generate_word($id,$slug="")
    {
        $table = 'spo_m_permohonan';
        $dir = BASE . '/uploads/word/templates/';
        $result = $this->db->where('id',$id)
                       ->get($table);
        if($result->num_rows() > 0){
            try {
                $xml = new WordXml($result->result_array(), null, $options);
                // $xml->set_has_children( 'list_lampiran');
                // $xml->set_has_children( 'list_permohonan');
                $xml->set_callback_row([$this,'format_result_row'])
                    ->generate();
                $download = new MsWord($xml);
                $download->setDocTemplate($dir . 'output/'.$table.'_wordTemplate.docx');
                $download->setXsltSource($dir . 'output/'.$table.'_word.xslt');
                $download->setImageSource($dir . 'images');
                $download->create($table.'.docx');
            } catch (Exception $e) {
                $error = $e->getMessage();
                echo $error;
            }
        } 
    } 
}
