<?php

/**
 * 
 */
class Preview extends CI_Controller
{
	public $_page_title = 'Preview';

	public function __construct()
	{
		parent::__construct();
	}
	function email_verifikasi()
	{
		$data = file_get_contents(BASE . '/themes/metronic/assets/img/mail-header-logo.png');
		$logo_64_data = 'data:image/png;filename=foto.png;base64,'.base64_encode($data);

		$data = [
			'logo_64' => $logo_64_data
		];
		$this->load->view('email_verifikasi',$data);
	}
	function email_konfirmasi()
	{
		$data = file_get_contents(BASE . '/themes/metronic/assets/img/logo.png');
		$logo_64_data = 'data:image/png,base64,'.base64_encode($data);

		$data = [
			'logo_64' => $logo_64_data
		];
		$this->load->view('email_konfirmasi',$data);
	}
}