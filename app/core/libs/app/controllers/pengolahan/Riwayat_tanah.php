<?php  defined('BASEPATH') OR exit('No direct script access allowed');
use Foundationphp\Exporter\WordXml;
use Foundationphp\Exporter\OpenDoc;
use Foundationphp\Exporter\MsWord;
use Foundationphp\Exporter\Xml;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
class Riwayat_tanah extends Theme_Controller {
	public $_page_title = 'Riwayat Tanah';
	public function __construct()
    {
        parent::__construct();
        // $this->load->model('m_api_simpeg');
    }
    function set_session_search(){
        if(!empty($this->input->get('clear'))){
             $this->session->unset_userdata('session_search');
        }else{
            $key    = $this->input->post('key');
            $value  = $this->input->post('value');

            $key    = str_replace('q_', 'a.', $key);
            $session_search = $this->session->userdata('session_search');
            if(!is_array($session_search)){
                $session_search = [];
            }
            $session_search[$key] = $value;
            $this->session->set_userdata('session_search',$session_search);
        }
        
        header('Content-Type:application/json');
        echo json_encode(['success'=>true,'data'=>$session_search]);
    }
	///////////////////////////////////////////////////////////////////
	public function custom_grid_data()
    {
        $group_id = $this->input->get('group_id');
        $this->load->model('m_riwayat_tanah','model');
        $list = $this->model->get_datatables();
        $mmo = $this->model->get_min_max_order($group_id);
        // die($this->db->last_query());
        $data = array();
        $no = $_POST['start'];
        $maxRec = count($list) -1;
        foreach ($list as $row_index => $field) {
            $file_exist = !empty($field->file);
            $action = "<td class=\"actions\"> "
            .(1? "<a href=\"".site_url('pengolahan/riwayat_tanah/file/edit/'.$field->id)."/".slugify($field->name)."/file_upload\" class=\"btn edit_button btn-sm btn-icon btn-pure ".($file_exist?'btn-success':'btn-info')."\" role=\"button\" > <i class=\"fa fa-paperclip\" aria-hidden=\"true\"></i> 
            </a> ":"").
            "
            <a href=\"".site_url('pengolahan/riwayat_tanah/index/'.$group_id.'/edit/'.$field->id)."/".slugify($field->nama_lama)."\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-edit\" aria-hidden=\"true\"></i> 
            </a>  
            <a onclick=\"javascript: return delete_row('".site_url('pengolahan/riwayat_tanah/index/delete/'.$field->id)."',-1,this)\" href=\"javascript:;\" class=\"gc-bt-delete delete_button btn btn-sm btn-icon btn-pure btn-danger\" role=\"button\"> <i class=\"fa fa-trash\" aria-hidden=\"true\"></i> 
            </a>
            </td>";
            if(empty($group_id)){
                $action = "<a onclick='gotoGroup(".$field->group_id.")' url=\"".site_url('pengolahan/riwayat_tanah/index/').$field->group_id."\" href=\"javascript:;\" class=\"btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-list\" aria-hidden=\"true\"></i> 
            </a>  ";
            }
            $no++;
            $row = array();
            if(!empty($group_id)){
                $btn_up_disabled = $row_index == 0 ? ' disabled ' : '';
                $btn_down_disabled = $row_index == $maxRec ? ' disabled ' : '';
                
                $field_order = $field->order += 0;

                $up_orderable = $field_order > $mmo->_min;
                $down_orderable = $field_order < $mmo->_max;

                $tmp_o_up = $field_order - 1;
                $tmp_o_down = $field_order + 1;

                $up_order_fn = $up_orderable ? sprintf('onclick="moveUp(%d,%d,%d,%d)"',$field->id,$field_order,$tmp_o_up,$mmo->_max) : '';
                $down_order_fn = $down_orderable ? sprintf('onclick="moveDown(%d,%d,%d,%d)"',$field->id,$field_order,$tmp_o_down,$mmo->_max) : '';

                $row[] = '<button '.$up_order_fn.' '.$btn_up_disabled.' class="btn btn-xs btn-success"><i class="fa fa-chevron-up"></i></button><button '.$down_order_fn.' '.$btn_down_disabled.' class="btn btn-xs btn-warning"><i class="fa fa-chevron-down"></i></button>';
            }else{
                $row[] = $no;
            }
            
            $row[] = $field->persil;
            $row[] = $field->kelas;
            $row[] = $field->no_c;
            $row[] = $field->nama_lama;
            $row[] = $field->luas_lama;
            $row[] = $field->nama_baru;
            $row[] = $field->luas_baru;
            $row[] = implode('<br/>',json_decode($field->keterangan));
            $row[] = $action; 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('pengolahan/riwayat_tanah/index/add')];
        // die($this->session->userdata($data_marketing_filter_status));
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';
        $this->load->view('pengolahan/dt_riwayat_tanah.php',$tdata);
    }
    private function _customGrid($group_id="",$slug=""){
        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('pengolahan/riwayat_tanah/index/'.$group_id.'/add'),'group_id'=>$group_id];
        $session_search = $this->session->userdata('session_search');

        if(is_array($session_search)){
           
            foreach ($session_search as $key => $value) {
                $value = trim($value);
                if(!empty($value)){
                    $key = str_replace('a.', 'q_', $key);
                    $tdata[$key] = $value;
                }
            }
        }
        $data['output'] = $this->load->view('pengolahan/dt_riwayat_tanah.php',$tdata,true);
        $data['unique_hash'] = md5(date('YmdHis-Unit'));
        // $data['unor_list'] = $this->m_api_simpeg->get_unor();
        $data['is_admin']  = $this->cms_user_group()=='admin';
        $group_list_tmp = $this->db->select('a.*')
                               ->from('p_riwayat_group a')
                               ->order_by('a.order','asc')
                               ->get()->result();
        $group_list = [];
        $group_list_by_name = [];

        foreach ($group_list_tmp as $grp) {
            if(empty($grp->name) || $grp->name == '-'){
                $grp->name = 'PERSIL ' . $grp->persil;
                if(!empty($grp->kelas)){
                    $grp->name .= ' KELAS ' .$grp->kelas;
                }
                if(!empty($grp->blok)){
                    $grp->name .= ' BLOK ' .$grp->blok;
                }
            } 
            $group_list[$grp->id]= $grp->name;
            $group_list_by_name[$grp->name]=$grp->id;
        }   
        $data['group_list_tmp'] =  $group_list_tmp;
        $data['group_list_by_name'] =  $group_list_by_name;
        $data['group_title'] = $group_list[$group_id];
        $data['group_list'] =  $group_list;

        
      
        $this->view('pengolahan/riwayat_tanah.php', $data );
    }
    public function keterangan_picker()
    {
        // $param_64  = $this->input->get('param');
        // $param_str = base64_decode($param_64);
        // $param     = json_decode($param_str);
        // // print_r($param);
        // $query = $this->input->get('term');
        // $results = $this->db->select('a.*,a.template value')
        //                     ->like('a.template',$query,'%%')
        //                     ->where('a.target',$param->target)
        //                     ->order_by('a.'.$param->order,'asc')
        //                     ->get($param->table.' a')
        //                     ->result();
        // echo json_encode($results);
    }
    
    public function index($group_id="",$slug="")
    {
        $target_yaml = APP . '/form/p_riwayat.yml';
        $buffer = file_get_contents($target_yaml);
        $_SERVER['FORM_DEF'] = Yaml::parse($buffer);
        $crud = $this->new_crud();
        $crud->set_subject('Data');
        $state = $crud->getState();

        // die($state);
        switch ($state) {
            case 'list':
                $args = func_get_args();
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            default:
                # code...
                break;
        }
        if(!empty($group_id)){
            $crud->where('group_id',$group_id);
        }
        $crud->unset_jquery();
        // $crud->unset_export();
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_table('p_riwayat');
		$crud->set_model('m_riwayat_tanah');
		$crud->set_theme('datatables');
        // if(!empty($group_id)){
            $crud->field_type('group_id','hidden' );
        // }
        
        // $crud->set_relation('group_id','p_riwayat_group','name');
        $crud->field_type('order','hidden');
        $crud->fields('persil','kelas','no_c','nama_lama','luas_lama','nama_baru','luas_baru','keterangan','group_id','order');
        $crud->unset_texteditor('keterangan');
     
    	$id_user = $this->cms_user_id();
        $crud->display_as('no_c','No. C');
        $crud->display_as('nama_lama','Nama Lama');
        $crud->display_as('nama_baru','Nama Baru');
        $crud->display_as('luas_baru','Luas Baru');
        $crud->display_as('luas_lama','Luas Lama');
        $crud->display_as('order','Urutan ke');
        $crud->display_as('group_id','Group Riwayat');
        
        $crud->set_rules('no_c','Nomor C','trim|required|numeric');

		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	$id_user = $this->cms_user_id();
         $crud->callback_before_insert(array($this,'set_group_id'));
        $data = $crud->render();
		$data->is_admin  = $this->cms_user_group()=='admin';

        $group_list_tmp = $this->db->select('a.*')
                               ->from('p_riwayat_group a')
                               ->order_by('a.order','asc')
                               ->get()->result();
        $group_list = [];
        $group_list_by_name = [];

        foreach ($group_list_tmp as $grp) {
            if(empty($grp->name) || $grp->name == '-'){
                $grp->name = 'PERSIL ' . $grp->persil;
                if(!empty($grp->kelas)){
                    $grp->name .= ' KELAS ' .$grp->kelas;
                }
                if(!empty($grp->blok)){
                    $grp->name .= ' BLOK ' .$grp->blok;
                }
            } 
            $group_list[$grp->id]= $grp->name;
            $group_list_by_name[$grp->name]=$grp->id;
        }   
       
        $data->group_list =  $group_list;
        $data->group_list_by_name =  $group_list_by_name;
        $data->group_list_tmp =  $group_list_tmp;

        $session_search = $this->session->userdata('session_search');

         
		$this->view('pengolahan/riwayat_tanah.php',$data);
    }
    function set_group_id($post_array){
        $this->load->model('m_riwayat_tanah','model');
        $group_id =  $this->uri->segment(4);
        if(!is_numeric($group_id) || empty($group_id)){
            exit();
        }
        $post_array['group_id'] = $group_id;
        $no = $this->model->get_count_by_group($post_array['group_id']);
        if($no != 0){
            $no -= 1;
        }
        $this->_check_valid_order_n($no,$post_array['group_id']);
        $post_array['order'] = $no;
        
        return $post_array;
    }
    
    public function move($id,$a,$b,$direction)
    {
        $group_id = $this->input->get('group_id');
        $sql = "
            UPDATE p_riwayat SET `order`=(CASE `order` WHEN $a THEN $b ELSE $a END) 
            WHERE (`order`=$a OR `order`= $b ) AND group_id=$group_id";

        // die($sql);    
        if (is_numeric($a) && is_numeric($b)) {
            $this->_check_valid_order($a,$b,$group_id);
            $result = $this->db->query($sql);
        }

        header('Content-Type:application/json');
        echo json_encode(['success'=>true,'data'=>[$id,$order,$tmpOrder,$direction]]);
    }

    function _check_valid_order($a,$b,$group_id){
        $record_exists = $this->db->where('a.group_id',$group_id)
                 ->where('a.order',$a)
                 ->or_where('a.order',$b)
                 ->get('p_riwayat a')
                 ->num_rows() == 2;
        if(!$record_exists){
            $record_orders = $this->db->select('a.id')
                                      ->where('a.group_id',$group_id)
                                      ->order_by('a.order','asc')
                                      ->get('p_riwayat a')->result_array();
            foreach ($record_orders as $new_order => $row) {
                $this->db->where('id',$row['id'])
                         ->update('p_riwayat',['order'=>$new_order]);
            }                          
        }         
    }

    function _check_valid_order_n($a,$group_id){
        $record_exists = $this->db->where('a.group_id',$group_id)
                 ->where('a.order',$a)
                 ->get('p_riwayat a')
                 ->num_rows() == 1;
        if(!$record_exists){
            $record_orders = $this->db->select('a.id')
                                      ->where('a.group_id',$group_id)
                                      ->order_by('a.order','asc')
                                      ->get('p_riwayat a')->result_array();
            foreach ($record_orders as $new_order => $row) {
                $this->db->where('id',$row['id'])
                         ->update('p_riwayat',['order'=>$new_order]);
            }                          
        } 
        return $record_exists;        
    }
    
    public function file($cmd='',$id='',$slug='',$mode='')
    {
        if (empty($cmd)) {
           exit('No direct script access allowed');
        }
        $crud = $this->new_crud();
        $crud->set_subject('File');
        $crud->set_model('m_riwayat_tanah');
        $crud->set_table('p_riwayat');
        $crud->set_theme('datatables');
        $crud->columns('file');
        $crud->fields('file');
        $upload_dir = 'p_riwayat';
        if(!is_dir(BASE.'/uploads/'.$upload_dir)){
            @mkdir(BASE.'/uploads/'.$upload_dir);
        }
        $crud->set_field_upload('file', $upload_dir);
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $id_user = $this->cms_user_id();
        $data = $crud->render();
        $data->is_admin  = $this->cms_user_group()=='admin';
        $this->view('pengolahan/file_upload',$data);
    } 
}
