<?php
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
defined('BASEPATH') OR exit('No direct script access allowed');

class Kutipan_c extends Theme_Controller {
	public $_page_title = 'Kutipan C';
	public function __construct()
    {
        parent::__construct();
        // $this->load->model('m_api_simpeg');
    }
	 
	 
	
	///////////////////////////////////////////////////////////////////
	public function custom_grid_data()
    {
        $this->load->model('m_kutipan_c' );
        $list = $this->m_kutipan_c->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $action = "<td class=\"actions\"> 
            ".  "<a href=\"".site_url('pengolahan/kutipan_c/detail/'.$field->parent_id)."/".slugify($field->nama)."\" class=\"btn btn-sm btn-icon btn-pure btn-info\" role=\"button\" onclick=\"displayDetail(event,this)\"> <i class=\"fa fa-list\" aria-hidden=\"true\"></i> 
            </a> " ."
             
            </td>";


            $no++;
            $row = array();
            $row[] = $no;
            
            $row[] =  $field->no_c ; 
            $row[] = $field->nama; 

            $row[] = $action; 
              
             
 
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->m_kutipan_c->count_all(),
            "recordsFiltered" => $this->m_kutipan_c->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('pengolahan/kutipan_c/index/add')];
        // die($this->session->userdata($data_marketing_filter_status));
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';

        $this->load->view('pengolahan/dt_kutipan_c',$tdata);
    }
    private function _customGrid(){
        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('pengolahan/kutipan_c/index/add')];
       

        $data['output'] = $this->load->view('pengolahan/dt_kutipan_c',$tdata,true);

        $data['unique_hash'] = md5(date('YmdHis-Unit'));

        // $data['unor_list'] = $this->m_api_simpeg->get_unor();
        $data['is_admin']  = $this->cms_user_group()=='admin';
        $data['form_def'] = Yaml::parse(file_get_contents(APP.'/form/p_kutipan_c_detail.yml')); 
        $session_search = $this->session->userdata('session_search_kutipan_c');
        if(is_array($session_search)){
            foreach ($session_search as $key => $value) {
                $ky = str_replace('a.', 'q_', $key);
                $data[$ky] = $value;
            }
        }
        $this->view('pengolahan/kutipan_c', $data );
    }
    public function index()
    {
        $crud = $this->new_crud();
        $crud->set_subject('Kutipan C');
        
        $state = $crud->getState();
        switch ($state) {
            case 'list':
                $args = func_get_args();
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            
            default:
                # code...
                break;
        }

        $crud->unset_jquery();
        
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_table('p_kutipan_c');
		$crud->set_model('m_kutipan_c');
		$crud->set_theme('datatables');
    	$id_user = $this->cms_user_id();
		
        $crud->columns('nama','no_c','jml_detail','aksi');
        

		$crud->fields('parent_id');
        // $crud->set_relation('parent_id','p_buku_c','no_c');
		if($this->cms_user_group() != 'admin'){
    		// $crud->where('account_view.user_id',$id_user);
    	}
    	// $crud->callback_column('is_active',array($this,'cbStatus'));
    	// $crud->callback_column('aksi',array($this,'cbAksi'));

    	// $crud->display_as('nip_nik','NIP/NIK');
        $dd = [];
        $dd_tmp = $this->db->order_by('id','asc')->get('p_buku_c')->result_array();
        
        foreach ($dd_tmp as $row) {
            $dd[$row['id']]=$row['no_c'] .' - ' . $row['nama'];
        }
        $crud->field_type('parent_id','dropdown',$dd);
        $crud->display_as('no_c','Nomor C');
        $crud->display_as('parent_id','Nomor C');
    	$crud->display_as('jml_detail','Jumlah Detail');

		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	
    	$id_user = $this->cms_user_id();

	
		$data = $crud->render();
		$data->is_admin  = $this->cms_user_group()=='admin';
		// $data->filter_date = $this->input->get('date');
        // $data->unor_list = $this->m_api->get_unor();
	    $data->form_def = Yaml::parse(file_get_contents(APP.'/form/p_kutipan_c_detail.yml'));   
		$this->view('pengolahan/kutipan_c',$data);
    }
    
    public function detail($parent_id="")
    {
        $parent = $this->db->where('id',$parent_id)->get('p_buku_c')->row();
        $item = Yaml::parse(file_get_contents(APP.'/form/p_kutipan_c_detail.yml'));
        $item = $item['item'];
        // echo json_encode(['data'=>$details,'sql'=>$this->db->last_query()]);
        $this->load->view('pengolahan/kutipan_c_detail_table',['item'=>$item, 'parent_id'=>$parent_id,'parent'=>$parent]);                    
    }
    public function list_kutipan_detail($fk_id)
    {
 
        $results   = [
            'success' => false,
            'data'=> []
        ];
 
        $cmd = $this->input->get('cmd');
        $post_keys = ["parent_id","s_persil","s_kelas" ,"s_luas", "s_sbb_tgl_ubah","d_persil","d_kelas" ,"d_luas","d_sbb_tgl_ubah"];
        $post_data = [];

        foreach ($post_keys as $name) {
            $post_data[$name] = $this->input->post($name);
        }
        switch ($cmd) {
            case 'add':
                $results['success'] = true;
                $this->load->model('m_kutipan_c_detail');
                $no = $this->m_kutipan_c_detail->get_count_by_group($post_data['parent_id']);
                if($no != 0){
                    $no -= 1;
                }
                $this->_check_valid_order_n($no,$parent_id);
                $post_data['order'] = $no;

                $this->db->insert('p_kutipan_c',$post_data);
                $results['data'] = $post_data;
                $results['data']['id'] = $this->db->insert_id();
                break;
            case 'edit':
                $results['success'] = true;
                $pk = $this->input->post('id');
                $this->db->where('id',$pk)->update('p_kutipan_c',$post_data);
                $results['data'] = $post_data;
                $results['data']['id'] = $pk;
                break;
            case 'delete':
                $results['success'] = true;
                $pk = $this->input->post('id');
                $this->db->where('id',$pk)->delete('p_kutipan_c');
                break;    
            case 'list':
                $results['success'] = true;
                $results['data'] = $this->db->where('parent_id',$fk_id)
                                            ->order_by('order','asc')
                                            ->get('p_kutipan_c')
                                            ->result_array();
                $results['sql'] = $this->db->last_query();                            
                break;
        }
        echo json_encode($results);
    }
    public function table($cmd="")
    {
        switch ($cmd) {
            case 'custom_grid_data':
                $parent_id = $this->input->get('parent_id');
                $this->load->model('m_kutipan_c_detail' );
                $list = $this->m_kutipan_c_detail->get_datatables();
                $data = array();
                $no = $_POST['start'];
                $maxRec = count($list) -1;
                $mmo = $this->m_kutipan_c_detail->get_min_max_order($parent_id);
                //
        
        
        // die($this->db->last_query());
        $data = array();
        $no = $_POST['start'];
        
                //

                foreach ($list as $field) {
                    $action = "<td class=\"actions\"> 
                     <a href=\"".site_url('pengolahan/kutipan_c/table/edit/'.$field->id)."/".slugify($field->s_persil)."\" class=\"btn btn-sm gc-bt-edit edit_button  btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-edit\" aria-hidden=\"true\"></i> 
                    </a> 
                    <a onclick=\"javascript: return delete_row('".site_url('pengolahan/kutipan_c/table/delete/'.$field->id)."',-1,this)\" href=\"javascript:;\" class=\"gc-bt-delete delete_button btn btn-sm btn-icon btn-pure btn-danger\" role=\"button\"> <i class=\"fa fa-trash\" aria-hidden=\"true\"></i> 
                     </a> 
                    </td>";

                    if(empty($parent_id)){
                        $action = "<a onclick='gotoParent(".$field->parent_id.",".$field->nama.")' url=\"".site_url('pengolahan/kutipan_c/table/').$field->parent_id."\" href=\"javascript:;\" class=\"btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-list\" aria-hidden=\"true\"></i> 
                         </a>  ";
                    }
                    $no++;
                    $row = array();
               
                    if(!empty('')){
                        $btn_up_disabled = $row_index == 0 ? ' disabled ' : '';
                        $btn_down_disabled = $row_index == $maxRec ? ' disabled ' : '';
                        
                        $field_order = $field->order += 0;

                        $up_orderable = $field_order > $mmo->_min;
                        $down_orderable = $field_order < $mmo->_max;

                        $tmp_o_up = $field_order - 1;
                        $tmp_o_down = $field_order + 1;

                        $up_order_fn = $up_orderable ? sprintf('onclick="moveUp(%d,%d,%d,%d)"',$field->id,$field_order,$tmp_o_up,$mmo->_max) : '';
                        $down_order_fn = $down_orderable ? sprintf('onclick="moveDown(%d,%d,%d,%d)"',$field->id,$field_order,$tmp_o_down,$mmo->_max) : '';

                        $row[] = '<button '.$up_order_fn.' '.$btn_up_disabled.' class="btn btn-xs btn-success"><i class="fa fa-chevron-up"></i></button><button '.$down_order_fn.' '.$btn_down_disabled.' class="btn btn-xs btn-warning"><i class="fa fa-chevron-down"></i></button>';
                    }else{
                        $row[] = $no;
                    }
                    
                    $row[] = $field->s_persil ; 
                    $row[] = $field->s_kelas; 
                    $row[] = $field->s_luas; 
                    $row[] = $field->s_sbb_tgl_ubah; 

                    $row[] = $field->d_persil ; 
                    $row[] = $field->d_kelas; 
                    $row[] = $field->d_luas; 
                    $row[] = $field->d_sbb_tgl_ubah; 

                    $row[] = $action; 
                      
                     
         
                    $data[] = $row;
                }
         
                $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->m_kutipan_c_detail->count_all(),
                    "recordsFiltered" => $this->m_kutipan_c_detail->count_filtered(),
                    "data" => $data,
                );
                //output dalam format JSON
                echo json_encode($output);
                exit();
                break;
            
            default:
                # code...
                break;
        }

        $crud = $this->new_crud();
        $crud->set_subject('Kutipan C');
        
        $state = $crud->getState();
         
        $crud->unset_jquery();
        
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_table('p_kutipan_c');
        $crud->set_model('m_kutipan_c_detail');
        $crud->set_theme('datatables');
        $id_user = $this->cms_user_id();
        
        $crud->fields('s_persil','s_kelas','s_luas','s_sbb_tgl_ubah','d_persil','d_kelas','d_luas','d_sbb_tgl_ubah','parent_id','order');
        $crud->field_type('parent_id','hidden');
        $crud->field_type('order','hidden');
        $crud->display_as('s_persil','Persil');
        $crud->display_as('s_kelas','Kelas');
        $crud->display_as('s_luas','Luas');
        $crud->display_as('s_sbb_tgl_ubah','Sebab Tanggal Perubahan');

        $crud->display_as('d_persil','Persil');
        $crud->display_as('d_kelas','Kelas');
        $crud->display_as('d_luas','Luas');
        $crud->display_as('d_sbb_tgl_ubah','Sebab Tanggal Perubahan');
        $crud->callback_before_insert(array($this,'set_parent_id'));
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        
        $id_user = $this->cms_user_id();

    
        $data = $crud->render();
        $data->is_admin  = $this->cms_user_group()=='admin';
        // $data->filter_date = $this->input->get('date');
        // $data->unor_list = $this->m_api->get_unor();
           
        $this->view('pengolahan/kutipan_c',$data);
    }
    function set_parent_id($post_array){
        $this->load->model('m_kutipan_c_detail' );
        $parent_id =  $this->input->get('parent_id');
        if(!is_numeric($parent_id) || empty($parent_id)){
            echo 'invalid_parent_id';
            exit();
        }
        $post_array['parent_id'] = $parent_id;
        $no = $this->m_kutipan_c_detail->get_count_by_group($parent_id);
        if($no != 0){
            $no -= 1;
        }
        $this->_check_valid_order_n($no,$parent_id);
        $post_array['order'] = $no;
        
        // print_r($post_array);
        // die();
        return $post_array;
    }
    function set_session_search(){
        if(!empty($this->input->get('clear'))){
             $this->session->unset_userdata('session_search_kutipan_c');
        }else{
            $key    = $this->input->post('key');
            $value  = $this->input->post('value');

            $key    = str_replace('q_', 'a.', $key);
            $session_search = $this->session->userdata('session_search');
            if(!is_array($session_search)){
                $session_search = [];
            }
            $session_search[$key] = $value;
            $this->session->set_userdata('session_search_kutipan_c',$session_search);
        }
        
        header('Content-Type:application/json');
        echo json_encode(['success'=>true,'data'=>$session_search]);
    }
    public function move($id,$a,$b,$direction)
    {
        $parent_id = $this->input->get('parent_id');
        $sql = "
            UPDATE p_riwayat SET `order`=(CASE `order` WHEN $a THEN $b ELSE $a END) 
            WHERE (`order`=$a OR `order`= $b ) AND parent_id=$parent_id";

        // die($sql);    
        if (is_numeric($a) && is_numeric($b)) {
            $this->_check_valid_order($a,$b,$parent_id);
            $result = $this->db->query($sql);
        }

        header('Content-Type:application/json');
        echo json_encode(['success'=>true,'data'=>[$id,$order,$tmpOrder,$direction]]);
    }

    function _check_valid_order($a,$b,$parent_id){
        $record_exists = $this->db->where('a.parent_id',$parent_id)
                 ->where('a.order',$a)
                 ->or_where('a.order',$b)
                 ->get('p_kutipan_c a')
                 ->num_rows() == 2;
        if(!$record_exists){
            $record_orders = $this->db->select('a.id')
                                      ->where('a.parent_id',$parent_id)
                                      ->order_by('a.order','asc')
                                      ->get('p_kutipan_c a')->result_array();
            foreach ($record_orders as $new_order => $row) {
                $this->db->where('id',$row['id'])
                         ->update('p_kutipan_c',['order'=>$new_order]);
            }                          
        }         
    }

    function _check_valid_order_n($a,$parent_id){
        $record_exists = $this->db->where('a.parent_id',$parent_id)
                 ->where('a.order',$a)
                 ->get('p_kutipan_c a')
                 ->num_rows() == 1;
        if(!$record_exists){
            $record_orders = $this->db->select('a.id')
                                      ->where('a.parent_id',$parent_id)
                                      ->order_by('a.order','asc')
                                      ->get('p_kutipan_c a')->result_array();
            foreach ($record_orders as $new_order => $row) {
                $this->db->where('id',$row['id'])
                         ->update('p_kutipan_c',['order'=>$new_order]);
            }                          
        } 
        return $record_exists;        
    }
}