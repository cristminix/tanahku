<?php
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
defined('BASEPATH') OR exit('No direct script access allowed');

class Buku_c extends Theme_Controller {
	public $_page_title = 'Buku C';
	public function __construct()
    {
        parent::__construct();
        // $this->load->model('m_api_simpeg');
    }
	 
	 
	
	///////////////////////////////////////////////////////////////////
	public function custom_grid_data()
    {
        $this->load->model('m_buku_c','model');
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $action = "<td class=\"actions\"> <a href=\"".site_url('pengolahan/buku_c/index/edit/'.$field->id)."/".slugify($field->nama)."\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-edit\" aria-hidden=\"true\"></i> </a> </td>";


            $no++;
            $row = array();
            $row[] = $no;
           
            $row[] =  $field->no_c; 
             $row[] = $field->nama; 
             $row[] = $action; 
              
             
 
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('pengolahan/buku_c/index/add')];
        // die($this->session->userdata($data_marketing_filter_status));
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';

        $this->load->view('pengolahan/dt_buku_c',$tdata);
    }
    private function _customGrid(){
        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('pengolahan/buku_c/index/add')];
       

        $data['output'] = $this->load->view('pengolahan/dt_buku_c',$tdata,true);

        $data['unique_hash'] = md5(date('YmdHis-Unit'));

        // $data['unor_list'] = $this->m_api_simpeg->get_unor();
        $data['is_admin']  = $this->cms_user_group()=='admin';


        $this->view('pengolahan/buku_c', $data );
    }
    public function index()
    {
        $crud = $this->new_crud();
        $crud->set_subject('Buku C');
        
        $state = $crud->getState();
        switch ($state) {
            case 'list':
                $args = func_get_args();
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            
            default:
                # code...
                break;
        }

        $crud->unset_jquery();
        
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->set_table('p_buku_c');
		$crud->set_model('m_buku_c');
		$crud->set_theme('datatables');
        $crud->set_rules('nama','Nama','trim|required');
        $crud->set_rules('no_c','Nomor C','trim|required|numeric|callback_noc_check');
    	$id_user = $this->cms_user_id();
		
        $crud->columns('nama','no_c','aksi');
		$crud->fields('nama','no_c');

		if($this->cms_user_group() != 'admin'){
    		// $crud->where('account_view.user_id',$id_user);
    	}
    	// $crud->callback_column('is_active',array($this,'cbStatus'));
    	// $crud->callback_column('aksi',array($this,'cbAksi'));

    	// $crud->display_as('nip_nik','NIP/NIK');
    	$crud->display_as('no_c','Nomor C');

		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	
    	$id_user = $this->cms_user_id();

	
		$data = $crud->render();
		$data->is_admin  = $this->cms_user_group()=='admin';
		// $data->filter_date = $this->input->get('date');
        // $data->unor_list = $this->m_api->get_unor();
	       
		$this->view('pengolahan/buku_c',$data);
    }
    public function noc_check($str)
    {
        $pk = $this->uri->segment(5);
        if(!empty($pk) && is_numeric($pk)){
            $no_c_old = $this->db->where("id",$pk)->get('p_buku_c')->row()->no_c;
            $this->db->where("no_c !=",$no_c_old);
        }
        $num_row = $this->db->where('no_c',$str)->get('p_buku_c')->num_rows();
        if ($num_row >= 1){
            $this->form_validation->set_message('noc_check', 'Nomor C sudah ada');
            return false;
        }
        else{
            return true;
        }
    }
    public function ac()
    {
        $q = $this->input->get('q');
        $q_match = false;
        if(preg_match('/no\.(\d+)\ \-\ (.*)/i', $q, $match)){
            $this->db->where('no_c',$match[1])
                     ->or_like('nama',$match[2],'%%');
            $q_match = true;
        }
        else if(preg_match('/no\.(\d+)/i', $q, $match)){
            $this->db->like('no_c',$match[1],'%%');
            $q_match = true;
        }else{
            $this->db->like('no_c',$q)->or_like('nama',$q,'%%');
        }
        $result = $this->db->select("id,CONCAT('NO.',no_c,' - ',nama) nama")
                           ->get('p_buku_c')
                           ->result_array();

        echo json_encode($result);
    }
    public function get_row($id)
    {
        $result = $this->db->select("id,CONCAT('NO.',no_c,' - ',nama) nama")
                           ->where('id',$id)->get('p_buku_c')
                           ->row();

        echo json_encode($result);
    }
}