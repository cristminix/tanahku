<?php  defined('BASEPATH') OR exit('No direct script access allowed');
use Foundationphp\Exporter\WordXml;
use Foundationphp\Exporter\OpenDoc;
use Foundationphp\Exporter\MsWord;
use Foundationphp\Exporter\Xml;
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;
class Riwayat_tanah_group extends Theme_Controller {
	public $_page_title = 'Group Riwayat Tanah';
	public function __construct()
    {
        parent::__construct();
        // $this->load->model('m_api_simpeg');
    }
	///////////////////////////////////////////////////////////////////
    public function get_by_id($id)
    {
        $field = $this->db->where('id',$id)->get('p_riwayat_group')->row();
        if(!empty($field)){
            if(empty($field->name) || $field->name == '-'){
                $field->name = 'PERSIL ' . $field->persil;
                if(!empty($field->kelas)){
                    $field->name .= ' KELAS ' .$field->kelas;
                }
                if(!empty($field->blok)){
                    $field->name .= ' BLOK ' .$field->blok;
                }
                // $this->db->where('id',$field->id)->update('p_riwayat_group',['name'=>$field->name]);
            }
        }
        if($this->input->get('clear_session') == 'true'){
            // $this->session->unset_userdata('session_search');
        }
        header('Content-Type:application/json');

        echo json_encode(['success'=>true,'data'=>$field]);
    }
	public function custom_grid_data()
    {
        $this->load->model('m_riwayat_tanah_group','model');
        $list = $this->model->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $file_exist = !empty($field->file);
            $action = "<td class=\"actions\"> 
               ".(1? "<a href=\"".site_url('pengolahan/riwayat-tanah-group/file/edit/'.$field->id)."/".slugify($field->name)."/file_upload\" class=\"btn edit_button btn-sm btn-icon btn-pure ".($file_exist?'btn-success':'btn-info')."\" role=\"button\" > <i class=\"fa fa-file-image-o\" aria-hidden=\"true\"></i> 
            </a> ":"")."<td class=\"actions\"> 
               ".($field->jml_detail>0 ? "<a href=\"".site_url('pengolahan/riwayat-tanah/'.$field->id)."/".slugify($field->name)."\" class=\"btn btn-sm btn-icon btn-pure btn-info\" role=\"button\" > <i class=\"fa fa-list\" aria-hidden=\"true\"></i> 
            </a> ":"")."
            <a href=\"".site_url('pengolahan/riwayat_tanah_group/index/edit/'.$field->id)."/".slugify($field->name)."\" class=\"gc-bt-edit edit_button btn btn-sm btn-icon btn-pure btn-info\" role=\"button\"> <i class=\"fa fa-edit\" aria-hidden=\"true\"></i> 
            </a> ".($field->jml_detail==0?"
            <a onclick=\"javascript: return delete_row('".site_url('pengolahan/riwayat_tanah_group/index/delete/'.$field->id)."',-1,this)\" href=\"javascript:;\" class=\"gc-bt-delete delete_button btn btn-sm btn-icon btn-pure btn-danger\" role=\"button\"> <i class=\"fa fa-trash\" aria-hidden=\"true\"></i> 
            </a>":"")."
            </td>";
            $no++;
            $row = array();
            $row[] = $no;
            if(empty($field->name) || $field->name == '-'){
                $field->name = 'PERSIL ' . $field->persil;
                if(!empty($field->kelas)){
                    $field->name .= ' KELAS ' .$field->kelas;
                }
                if(!empty($field->blok)){
                    $field->name .= ' BLOK ' .$field->blok;
                }
                // $this->db->where('id',$field->id)->update('p_riwayat_group',['name'=>$field->name]);
            } 
            $row[] = sprintf("<a class='info' href='%s'>%s</a>",site_url().'pengolahan/riwayat-tanah/'.$field->id.'/'.slugify($field->name),$field->name); 
            $img_url = site_url().'uploads/p_riwayat_group/'.$field->file.'?ref='.slugify($field->name);
            $row[] = ($file_exist)?sprintf("<a onclick='showPicture()' class='info' href='%s'><img class='thumbnail img-sm' src='%s' alt='%s'/></a>",$img_url,$img_url,$field->name):''; 
            $row[] = $action; 
            $data[] = $row;
        }
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->model->count_all(),
            "recordsFiltered" => $this->model->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
    }
    public function custom_grid_ajax()
    {
        $tdata = ['add_url' => site_url('pengolahan/riwayat_tanah_group/index/add')];
        // die($this->session->userdata($data_marketing_filter_status));
        $tdata['is_admin'] = $this->cms_user_group() == 'admin';
        $this->load->view('pengolahan/dt_riwayat_tanah_group.php',$tdata);
    }
    private function _customGrid(){
        $dump = file_get_contents(APP . '/config/gc.yml');
        $data = Yaml::parse($dump);
        $tdata = ['add_url' => site_url('pengolahan/riwayat_tanah_group/index/add')];
        $data['output'] = $this->load->view('pengolahan/dt_riwayat_tanah_group.php',$tdata,true);
        $data['unique_hash'] = md5(date('YmdHis-Unit'));
        // $data['unor_list'] = $this->m_api_simpeg->get_unor();
        $data['is_admin']  = $this->cms_user_group()=='admin';
        $this->view('pengolahan/riwayat_tanah_group.php', $data );
    }
    public function file($cmd='',$id='',$slug='',$mode='')
    {
        if (empty($cmd)) {
           exit('No direct script access allowed');
        }
        $crud = $this->new_crud();
        $crud->set_subject('File');
        $crud->set_model('m_riwayat_tanah_group');
        $crud->set_table('p_riwayat_group');
        $crud->set_theme('datatables');
        $crud->columns('file');
        $crud->fields('file');
        $upload_dir = 'p_riwayat_group';
        if(!is_dir(BASE.'/uploads/'.$upload_dir)){
            @mkdir(BASE.'/uploads/'.$upload_dir);
        }
        $crud->set_field_upload('file', $upload_dir);
        $state = $crud->getState();
        $state_info = $crud->getStateInfo();
        $id_user = $this->cms_user_id();
        $data = $crud->render();
        $data->is_admin  = $this->cms_user_group()=='admin';
        $this->view('pengolahan/file_upload',$data);
    } 
    public function index()
    {
        $target_yaml = APP . '/form/p_riwayat_group.yml';
        $buffer = file_get_contents($target_yaml);
        $_SERVER['FORM_DEF'] = Yaml::parse($buffer);
        $crud = $this->new_crud();
        $crud->set_subject('Data');
        $state = $crud->getState();
        switch ($state) {
            case 'list':
                $args = func_get_args();
                return call_user_func_array([$this,'_customGrid'], $args);
                break;
            default:
                # code...
                break;
        }
        $crud->unset_jquery();
        $crud->fields('persil','kelas','blok','name','order');
        if (! $this->input->is_ajax_request()) {
            $crud->set_theme_config(['crud_paging' => true ]);
        }
        $crud->field_type('order','hidden');
        $crud->field_type('name','hidden');
        
        $crud->set_table('p_riwayat_group');
        $crud->order_by('order','asc');
		$crud->set_model('m_riwayat_tanah_group');
		$crud->set_theme('datatables');
        $crud->set_rules('persil','Persil','trim|required');
        $crud->set_rules('kelas','Kelas','trim|required');
        $crud->set_rules('blok','Blok','trim|required');
        // $crud->set_rules('blok','Blok','trim|required');
        
    	$id_user = $this->cms_user_id();
        $crud->display_as('name','Group Title'); 
        $crud->display_as('order','Urutan ke'); 
		$state = $crud->getState();
    	$state_info = $crud->getStateInfo();
    	$id_user = $this->cms_user_id();
        $data = $crud->render();
		$data->is_admin  = $this->cms_user_group()=='admin';
		$this->view('pengolahan/riwayat_tanah_group.php',$data);
    }
     
    
      
}
