<?php

use GuzzleHttp\Client;

class FCM{

    protected $endpoint;
    protected $topic;
    protected $data;
    protected $notification;

    public function __construct()
    {
        $this->endpoint = "https://fcm.googleapis.com/fcm/send";
    }

     

    public function data(array $data=[]){

        $this->data = $data;
        $data['sound'] = 'default';

    }
    
    public function notification(array $notification = [])
    {
        $this->notification = $notification;
        $this->notification['click_action'] = site_url('notifikasi');
        return $this;
    }
    public function send($token){

        $server_key = 'AAAAxHiFgaQ:APA91bFELT6dZ6tjjZHs5DhRNUoCbJGVu9-OMD4cC-uKNl4JYTYyJUhp3J-hA5XM0QAUbFkHCOjSNno3PPpiZGqn5dPKeTwLvnXvxHBogoIj6jTydXeIKyaXr4UZpzjt_4LnbYyvkLRP';

        $headers = [
            'Authorization' => 'key='.$server_key,
            'Content-Type'  => 'application/json',
        ];
        $fields = [
            'data' => $this->data,
            'notification' => $this->notification,
            'to'=>$token,

        ];

        $fields = json_encode ( $fields ,JSON_PRETTY_PRINT);
        // echo $fields;
        
        $client = new Client();

        try{
            $request = $client->post($this->endpoint,[
                'headers' => $headers,
                "body" => $fields
            ]);
            $response = $request->getBody();
            return  $response;
        }
        catch (Exception $e){
            return  $e->getMessage();
        }

    }

}