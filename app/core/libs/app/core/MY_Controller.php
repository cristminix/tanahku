<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;

class Theme_Controller extends CI_Controller
{
    public $_page_title = 'SITAN';
	public $_custom_keywords=[];
	public $_module_name='';
	public $_theme = 'metronic';
	public $_homepage_url='';
    // public $_layout = 'full';
    public $__gc_conf = [];
    public $__site_layout = 'full';
    protected $__session_data = '';

    public function get_session_data($key)
    {
        if(is_object($this->__session_data)){
            return $this->__session_data->{$key};

        }
    }
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('form');
        $this->load->helper('string');
        // $this->load->helper('cms_helper');
        $this->load->library('form_validation');
        // $this->form_validation->CI =& $this;

        $this->load->library('Extended_Grocery_CRUD');
        $this->load->library('template');

        // $this->m_cms = new m_cms();

        $this->__session_data = $this->session->userdata('account');
        // define('SESSION_DATA', !is_object($this->__session_data) ? []:$this->__session_data);
        // $this->log->write_log2( $this->uri->segment(1) , $this->cms_user_id() );

        $this->log->write_log2();
        $conf = Yaml::parse(file_get_contents(APP.'config/public_sites.yml'));
        $public_sites = $conf['public_sites'];

        if(!is_object($this->__session_data) && !in_array($this->uri->segment(1),$public_sites )){
            redirect('login');
        } 

        $this->load->library('acl');
        $this->acl->start();
	}

	/**
     * @author goFrendiAsgard
     * @return Grocery_CRUD
     * @desc   return Grocery_CRUD
     */
    public function new_crud($conf=[])
    {
        $db_driver = $this->db->platform();
        $model_name = 'grocery_crud_model_'.$db_driver;
        $model_alias = 'm'.substr(md5(rand()), 0, rand(4, 15));

        $this->load->library('Extended_Grocery_CRUD');
        $crud = new Extended_Grocery_CRUD();
        if (file_exists(APPPATH.'/models/'.$model_name.'.php')) {
            $this->load->model('Grocery_CRUD_Model','grocery_crud_model');
            $this->load->model('Grocery_CRUD_Generic_Model','grocery_crud_generic_model');



            $this->load->model($model_name, $model_alias);
            $crud->basic_model = $this->{$model_alias};
        }
        $crud->set_theme('datatables');
        // resolve HMVC set rule callback problem
        $crud->form_validation = $this->form_validation;
        $crud->__gc_conf = $conf;
        // print_r($conf['gc']['validation']);
        // print_r($_FILES);
        if(is_array($conf['gc'])){
            if(is_array($conf['gc']['display_as'])){
                foreach ($conf['gc']['display_as'] as $field => $caption) {
                    $crud->display_as($field,$caption);
                }
            }
            if(is_array($conf['gc']['validation'])){
                foreach ($conf['gc']['validation'] as $field => $rules) {
                    $crud->set_rules($field,$conf['gc']['display_as'][$field],$rules);
                }
            }
            if(is_array($conf['gc']['columns'])){
                $crud->columns($conf['gc']['columns']);
            }
            if(is_array($conf['gc']['fields'])){
                $crud->fields($conf['gc']['fields']);
            }
            if(is_string($conf['gc']['table'])){
                $crud->set_table($conf['gc']['table']);
            }
            if(is_string($conf['gc']['model'])){
                $crud->set_model($conf['gc']['model']);
            }
            if(is_string($conf['gc']['subject'])){
                $crud->set_subject($conf['gc']['subject']);
            }

            if(is_array($conf['gc']['behaviour'])){
                if(is_bool($conf['gc']['behaviour']['add_callback_user_and_datestamp']) ){
                    if($conf['gc']['behaviour']['add_callback_user_and_datestamp']){
                        $crud->callback_before_insert(array($this,'_set_tgl_entry'));
                        $crud->callback_before_update(array($this,'_set_tgl_update'));
                    }
                }
            }
            if(is_array($conf['gc']['field_type'])){
                foreach ($conf['gc']['field_type'] as $field => $d) {
                    if(is_array($d)){
                        if($d['type'] == 'upload'){
                            $upload_dir = $conf['gc']['table'].'_'.$field;
                            if(!is_dir(BASE.'/uploads/'.$upload_dir)){
                                @mkdir(BASE.'/uploads/'.$upload_dir);
                            }
                            $crud->set_field_upload($field, $upload_dir);
                        }
                        if($d['type'] == 'multi_upload'){
                            $upload_dir = $conf['gc']['table'].'_'.$field;
                            if(!is_dir(BASE.'/uploads/'.$upload_dir)){
                                @mkdir(BASE.'/uploads/'.$upload_dir);
                            }
                            // $crud->set_field_multi_upload($field, $upload_dir);
                        }

                    }
                    if(is_string($d)){
                        $crud->field_type($field,$d);
                    }
                    
                }
            }

        }
        return $crud;
    }
     function _set_tgl_entry($post_array){
        $dt = date('Y-m-d H:i:s');
        $post_array['dt_created'] = $dt;
        $post_array['dt_edit'] = $dt;
        $post_array['user_id'] = $this->cms_user_id();
        return $post_array;
    }
     function _set_tgl_update($post_array){
        $dt = date('Y-m-d H:i:s');
        $post_array['dt_edit'] = $dt;
        
        return $post_array;
    } 
    /**
     * @author  goFrendiAsgard
     * @param   string view_url
     * @param   string data
     * @param   string navigation_name
     * @param   array config
     * @param   bool return_as_string
     * @return  string or null
     * @desc    replace $this->load->view. This method will also load header, menu etc except there is _only_content parameter via GET or POST
     */
    protected function view($view_url, $data = null, $config = [], $return_as_string = false)
    {
        
        $result   = null;
        $view_url = $this->cms_parse_keyword($view_url);
        
        /**
         * PREPARE PARAMETERS *********************************************************************************************
         */
       
        /**
         * PREPARE PARAMETERS *********************************************************************************************
         */

        // this method can be called as $this->view('view_path', $data, true);
        // or $this->view('view_path', $data, $navigation_name, true);
        if ( count($config) == 0) {
            $return_as_string = 'default';
            $navigation_name  = null;
            $config           = null;
        } elseif (is_bool($config)) {
            $return_as_string = $config;
            $config           = null;
        }

        if (!isset($return_as_string)) {
            $return_as_string = false;
        }
        if (!isset($config)) {
            $config = array();
        }

        $privilege_required = isset($config['privileges']) ? $config['privileges'] : array();
        $custom_theme       = isset($config['theme']) ? $config['theme'] : null;
        $custom_layout      = isset($config['layout']) ? $config['layout'] : null;
        $custom_title       = isset($config['title']) ? $config['title'] : null;
        $custom_metadata    = isset($config['metadata']) ? $config['metadata'] : array();
        $custom_partial     = isset($config['partials']) ? $config['partials'] : null;
        $custom_keyword     = isset($config['keyword']) ? $config['keyword'] : null;
        $custom_description = isset($config['description'])? $config['description'] : null;
        $custom_author      = isset($config['author'])? $config['author'] : null;
        $only_content       = isset($config['only_content']) ? $config['only_content'] : null;
        $always_allow       = isset($config['always_allow']) ? $config['always_allow'] : false;
        $layout_suffix      = isset($config['layout_suffix']) ? $config['layout_suffix'] : '';


         

        /**
         * SHOW THE PAGE IF IT IS ACCESSIBLE  *****************************************************************************
         */

        // GET THE THEME, TITLE & ONLY_CONTENT FROM DATABASE
        $theme              = '';
        $title              = '';
        $keyword            = '';
        $default_theme      = null;
        $default_layout     = $this->__site_layout;
        $page_title         = null;
        $page_keyword       = null;
        $page_description   = null;
        $page_author        = null;

        // echo $navigation_name_provided;

        

 
        // ASSIGN THEME
        if (isset($custom_theme) && $custom_theme !== null && $custom_theme != '') {
            $theme = $custom_theme;
        } elseif (isset($default_theme) && $default_theme != null && $default_theme != '') {
            $themes     = $this->cms_get_theme_list();
            $theme_path = array();
            foreach ($themes as $theme) {
                $theme_path[] = $theme['path'];
            }
            if (in_array($default_theme, $theme_path)) {
                $theme = $default_theme;
            }
        } else {
            $theme = 'metronic';
        }

        // ASSIGN TITLE
        $title = '';
        if (isset($custom_title) && $custom_title !== null && $custom_title != '') {
            $title = $custom_title;
        } elseif (isset($page_title) && $page_title !== null && $page_title != '') {
            $title = $page_title;
        } else {
            $title = $this->_page_title;
        }

        // ASSIGN KEYWORD
        if (isset($custom_keyword) && $custom_keyword != null && $custom_keyword != '') {
            $keyword = $custom_keyword;
        } elseif (isset($page_keyword) && $page_keyword !== null && $page_keyword != '') {
            $keyword = $page_keyword;
            if ($custom_keyword != '') {
                $keyword .= ', ' . $custom_keyword;
            }
        } else {
            $keyword = '';
        }

        // ASSIGN DESCRIPTION
        if (isset($custom_description) && $custom_description != null && $custom_description != '') {
            $description = $custom_description;
        } elseif (isset($page_description) && $page_description !== null && $page_description != '') {
            $description = $page_description;
            if ($custom_description != '') {
                $description .= ', ' . $custom_description;
            }
        } else {
            $description = '';
        }

        // ASSIGN AUTHOR
        if (isset($custom_author) && $custom_author != null && $custom_author != '') {
            $author = $custom_author;
        } else {
             
                $author = 'Putra Budiman';
             
        }


        // GET THE LAYOUT
        if (isset($custom_layout)) {
            $layout = $custom_layout;
        } elseif (isset($default_layout) && $default_layout != '') {
            $layout = $default_layout;
        } else {
            $this->load->library('user_agent');
            $layout = $this->agent->is_mobile() ? 'mobile' : 'default';
        }


            // if ($layout == 'mobile' && false) {
                $layout = $this->__site_layout;
            // } else {
                $theme = $this->_theme;
                 
            // }
        

        

        // IT'S SHOW TIME
       
        if ($this->input->is_ajax_request()) {
            $result = $this->load->view($view_url, $data, true);
        } else {
            // set theme, layout and title
            $this->template->title($title);
            $this->template->set_theme($theme);
            $this->template->set_layout($layout);

            // echo $layout;
            // die();

            // set keyword metadata
            if ($keyword != '') {
                $keyword_metadata = '<meta name="keyword" content="' . $keyword . '">';
                $this->template->append_metadata($keyword_metadata);
            }
            // set description metadata
            if ($description != '') {
                $description_metadata = '<meta name="description" content="' . $description . '">';
                $this->template->append_metadata($description_metadata);
            }
            // set author metadata
            if ($author != '') {
                $author_metadata = '<meta name="author" content="' . $author . '">';
                $this->template->append_metadata($author_metadata);
            }
            // add IE compatibility
            $this->template->append_metadata('<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">');
            // add width
            $this->template->append_metadata('<meta name="viewport" content="width=device-width, initial-scale=1.0">');
            
            // always use grocerycrud's jquery for maximum compatibility
            // $jquery_path = base_url('pub/gc/js/jquery-1.10.2.min.js');
            // $this->template->append_metadata('<script type="text/javascript" src="' . $jquery_path . '"></script>');

            /*
            // google analytic
            $analytic_property_id = $this->cms_get_config('cms_google_analytic_property_id');
            if (trim($analytic_property_id) != '') {
                // create analytic code
                $analytic_code  = '<script type="text/javascript"> ';
                $analytic_code .= 'var _gaq = _gaq || []; ';
                $analytic_code .= '_gaq.push([\'_setAccount\', \'' . $analytic_property_id . '\']); ';
                $analytic_code .= '_gaq.push([\'_trackPageview\']); ';
                $analytic_code .= '(function() { ';
                $analytic_code .= 'var ga = document.createElement(\'script\'); ga.type = \'text/javascript\'; ga.async = true; ';
                $analytic_code .= 'ga.src = (\'https:\' == document.location.protocol ? \'https://ssl\' : \'http://www\') + \'.google-analytics.com/ga.js\'; ';
                $analytic_code .= 'var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(ga, s); ';
                $analytic_code .= '})(); ';
                $analytic_code .= '</script>';
                // add to the template
                $this->template->append_metadata($analytic_code);
            }

			*/
            // config metadata
            foreach ($custom_metadata as $metadata) {
                $this->template->append_metadata($metadata);
            }


            $this->load->helper('directory');
            $partial_path = BASE . '/themes/' . $theme . '/views/partials/' . $layout . '/';
            if (is_dir($partial_path)) {
            // die($partial_path);

                $partials = directory_map($partial_path, 1);
                foreach ($partials as $partial) {
                    // if is directory or is not php, then ignore it
                    if (is_dir($partial)) {
                        continue;
                    }
                    $partial_extension = pathinfo($partial_path . $partial, PATHINFO_EXTENSION);
                    if (strtoupper($partial_extension) != 'PHP') {
                        continue;
                    }

                    // add partial to template
                    $partial_name = pathinfo($partial_path . $partial, PATHINFO_FILENAME);
                    if (isset($custom_partial[$partial_name])) {
                        $this->template->inject_partial($partial_name, $custom_partial[$partial_name]);
                    } else {
                        $this->template->set_partial($partial_name, 'partials/' . $layout . '/' . $partial, $data);
                    }
                }
            }

            $result = $this->template->build($view_url, $data, true);

        }

        // parse keyword
        // die($result);
        $result = $this->cms_parse_keyword($result);
		
        // parse widgets used_theme & navigation_path
        // $result = $this->__cms_parse_widget_theme_path($result, $theme, $layout, $navigation_name);
        
        if (false) {
        	// die($result);
            return $result;
        } else {
            $this->cms_show_html($result);
        }
    }

    
    /**
     * @author goFrendiAsgard
     * @param  string value
     * @return string
     * @desc   parse keyword like {{ site_url  }} , {{ base_url }} , {{ user_name }} , {{ language }}
     */
    public function cms_parse_keyword($value)
    {
        // echo ($value);
        $value = $this->cms_escape_template($value);
        
        if (strpos($value, '{{ ') !== false) {
            $pattern     = array();
            $replacement = array();
            
            $this->cms_parse_custom_keywords($pattern, $replacement);
            // user_name
            $pattern[]     = "/\{\{ user_id \}\}/si";
            $replacement[] = $this->cms_user_id();
            // //user_group
            $pattern[]     = "/\{\{ user_group \}\}/si";
            $replacement[] = $this->cms_user_group();
            // // user_name
            // $pattern[]     = "/\{\{ user_avatar \}\}/si";
            // $replacement[] = $this->cms_user_avatar();

            // // user_name
            $pattern[]     = "/\{\{ user_name \}\}/si";
            $replacement[] = $this->cms_user_name();
    
            // // user_real_name
            $pattern[]     = "/\{\{ display_name \}\}/si";
            $replacement[] = $this->cms_display_name();
    
            // // user_email
            // $pattern[]     = "/\{\{ user_email \}\}/si";
            // $replacement[] = $this->cms_user_email();

            // // hotstname
            $pattern[]     = "/\{\{ hostname \}\}/si";
            $replacement[] = $_SERVER['HTTP_HOST'];

            // print_r($_SERVER);
            
            $pattern[]     = "/\{\{ homepage \}\}/si";
            $replacement[] = site_url($this->_homepage_url);

            // site_url
            $site_url = site_url();
            if ($site_url[strlen($site_url) - 1] != '/') {
                $site_url .= '/';
            }
            $pattern[]     = '/\{\{ site_url \}\}/si';
            $replacement[] = $site_url;
    
            // base_url
            $base_url = base_url();
            if ($base_url[strlen($base_url) - 1] != '/') {
                $base_url .= '/';
            }
            $pattern[]     = '/\{\{ base_url \}\}/si';
            $replacement[] = $base_url;

            $pattern[]     = '/\{\{ theme_assets \}\}/si';
            $replacement[] = site_url('themes/'.$this->_theme.'/assets/');
    
            // module_path & module_name
            $module_path = $this->cms_module_path();
            $module_name = $this->cms_module_name($module_path);
            
            $module_site_url = site_url('app/modules/'.$module_path);
            $module_base_url = base_url('app/modules/'.$module_path);

            if ($module_site_url[strlen($module_site_url) - 1] != '/') {
                $module_site_url .= '/';
            }
            if ($module_base_url[strlen($module_base_url) - 1] != '/') {
                $module_base_url .= '/';
            }
            $pattern[]     = '/\{\{ module_path \}\}/si';
            $replacement[] = $module_path;
            $pattern[]     = '/\{\{ module_site_url \}\}/si';
            $replacement[] = $module_site_url;
            $pattern[]     = '/\{\{ module_base_url \}\}/si';
            $replacement[] = $module_base_url;
            $pattern[]     = '/\{\{ module_name \}\}/si';
            $replacement[] = $module_name;
    
            // // language
            // $pattern[]     = '/\{\{ language \}\}/si';
            // $replacement[] = $this->cms_language();
           
            // execute regex
            $value = preg_replace($pattern, $replacement, $value);
        }
        
       
        

        return $value;
    }
    public function cms_parse_custom_keywords(&$pattern, &$replacement)
    {
        foreach ($this->_custom_keywords as $key => $value) {
            $pattern[]     = "/\{\{ $key \}\}/si";
            $replacement[] = $value;
        }
    }
    /**
     * @author  goFrendiAsgard
     * @param   mixed variable
     * @param   int options
     * @desc    show variable in json encoded form
     */
    protected function cms_show_json($variable, $options = 0)
    {
        $result = '';
        // php 5.3.0 accepts 2 parameters, while lower version only accepts 1 parameter
        if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
            $result = json_encode($variable, $options);
        } else {
            $result = json_encode($variable);
        }
        // show the json
        $this->output->set_content_type('application/json')->set_output($result);
    }

    /**
     * @author  goFrendiAsgard
     * @param   mixed variable
     * @desc    show variable for debugging purpose
     */
    protected function cms_show_variable($variable)
    {
        $data = array(
            'cms_content' => '<pre>' . print_r($variable, true) . '</pre>'
        );
        $this->load->view('static_page', $data);
    }

    /**
     * @author  goFrendiAsgard
     * @param   string html
     * @desc    you are encouraged to use this instead of echo $html
     */
    protected function cms_show_html($html)
    {
        $data = array(
            'cms_content' => $html
        );
        $this->load->view('static_page', $data);
    }
    /**
     * @author goFrendiAsgard
     * @param  string expression
     * @return string
     * @desc return a "save" pattern which is not replace anything inside HTML tag, and
     * anything between <textarea></textarea> and <option></option>
     */
    public function cms_escape_template($str)
    {
        $pattern   = array();
        $pattern[] = '/(<textarea[^<>]*>)(.*?)(<\/textarea>)/si';
        $pattern[] = '/(value *= *")(.*?)(")/si';
        $pattern[] = "/(value *= *')(.*?)(')/si";

        $str = preg_replace_callback($pattern, array(
            $this,
            '__cms_preg_replace_callback_escape_template'
        ), $str);

        return $str;
    }
    /**
     * @author goFrendiAsgard
     * @param  array arr
     * @return string
     * @desc replace every &#123; and &#125; in $arr[1] into '{{' and '}}';
     */
    private function __cms_preg_replace_callback_escape_template($arr)
    {
        $to_be_replaced = array(
            '{{ ',
            ' }}'
        );
        $to_replace     = array(
            '&#123;&#123; ',
            ' &#125;&#125;'
        );
        return $arr[1] . str_replace($to_be_replaced, $to_replace, $arr[2]) . $arr[3];
    }
    /**
     * @author  goFrendiAsgard
     * @param   string module_name
     * @return  string
     * @desc    get module_path (folder name) of specified module_name (name space)
     */
    public function cms_module_path($module_name = null)
    {
        if (!isset($module_name)) {
            $module = $this->router->fetch_module();
            return $module;
        } else {
            $main_module = $this->config->item('main_module');
            if (!empty($main_module[$module_name])) {
                return $main_module[$module_name]['module_path'];
            } else {
                return '';
            }
        }
        return '';
    }
    /**
     * @author  goFrendiAsgard
     * @param   string module_path
     * @return  string
     * @desc    get module_name (name space) of specified module_path (folder name)
     */
    public function cms_module_name($module_path = null)
    {
        if (!isset($module_path) || is_null($module_path)) {
            $module_path = $this->cms_module_path();
        }

       
        return $this->_module_name;
    }

    public function cms_user_id()
    {
        if(is_object($this->__session_data)){
            return $this->__session_data->id_user;
        }
        
        return '-1';
    }
    public function cms_user_name()
    {
        if(is_object($this->__session_data)){
            return $this->__session_data->username;
        }

        return 'nobody';
        
    }
    public function cms_user_group()
    {
        if(is_object($this->__session_data)){
            return $this->__session_data->rules;
        }
        return 'nobody';
        
    }
    public function cms_display_name( )
    {
        if(is_object($this->__session_data)){
            return $this->__session_data->name;
        }
        return 'nobody';
    }
}