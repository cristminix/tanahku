<?php
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\Yaml\Exception\ParseException;

define('BASE', realpath(dirname(__FILE__).'/../../..')).'/';
define('APP',BASE.'/app/');

$ymls = [
	'config' => [
		'app','database','vars' ,'routes','smtp','assets','log'
	],
	'content' => [
		'menu' ,'account', 'breadcrumb','notification','controllers'
	]
];

foreach ($ymls as $dir => $items) {
	foreach ($items as $item) {
		$yml = file_get_contents(APP.'/'.$dir.'/'.$item.'.yml');
		$yml = Yaml::parse($yml);

		define(strtoupper($item).'_YML', $yml);
	}
}

