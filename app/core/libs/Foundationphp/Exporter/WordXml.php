<?php
namespace Foundationphp\Exporter;

 
class WordXml extends Xml
{
	protected $suppress = [];
	protected $callback_row = null;
	protected $hasChildren = [];

	public function __construct(
	    $dbResult,
	    $filename = null,
	    $options = array(
		    'local'       => false,
	        'download'    => false,
	        'suppress'    => null,
	        'rootname'    => 'root',
	        'rowname'     => 'row',
	        'stripNsplit' => null
	    )
	) {
		parent::__construct($dbResult, $filename, $options);
		if (isset($options['download'])) {
		    $this->download = $options['download'];
		}
		if (isset($options['rootname'])) {
		    $this->isValidName($options['rootname']);
		    $this->rootname = $options['rootname'];
		}
		if (isset($options['rowname'])) {
			$this->isValidName($options['rowname']);
			$this->rowname = $options['rowname'];
		}
		if (isset($options['stripNsplit'])) {
		    $fields = explode(',', $options['stripNsplit']);
		    foreach ($fields as $field) {
		    	$this->hasChildren[] = trim($field);
		    }
		}
		
	}
	protected function setResultType($result)
	{
		if(!is_array($result)){
			$type = get_class($result);
			if ($type == 'mysqli_result') {
				$this->resultType = 'mysqli';
			} elseif ($type == 'PDOStatement') {
				$this->resultType = 'pdo';
			} else {
				throw new \Exception ('Database result must be either mysqli_result or PDOStatement.');
			}
		}else{
			$this->resultType = 'codeigniter';
		}
		
		$this->dbResult = $result;
	}
	protected function getRow()
	{
		if(!is_null($this->callback_row)  ){
			$row = $this->dbResult[0];
			foreach ($row as $key => $value) {
				$value = trim($value);
				if(empty($value) && !preg_match('/image|gambar/', $key)){
					$row[$key] = ' ..... ';
				}
				else{
					$row[$key] = $value;
				}
			}
			return call_user_func_array($this->callback_row, [$row]);
		}
	}
	public function set_suppress($key)
	{
		if(!in_array($key, $this->suppress)){
			$this->suppress[] = $key;
		}
	}
	public function set_has_children($key)
	{
		if(!in_array($key, $this->hasChildren)){
			$this->hasChildren[] = $key;
		}
	}
	public function set_callback_row($callback_row)
	{
		$this->callback_row = $callback_row;
		return $this;
	}
	public function generate()
	{
		$w = new \XmlWriter();
		$w->openMemory();
		$w->setIndent(true);
		$w->setIndentString("    ");
		$w->startDocument('1.0', 'utf-8');
		$w->startElement($this->rootname);
		$row = $this->getRow();

		$keys = array_keys($row);
		foreach ($keys as $key) {
		    $this->isValidName($key);
		}
		$w->startElement($this->rowname);
		foreach ($row as $key => $value) {
		    if ($this->suppress && in_array($key, $this->suppress)) {
		        continue;
		    }
			if ($this->hasChildren && in_array($key, $this->hasChildren)) {
				$stripped = $value;
				$w->startElement($key);
				foreach ($stripped as $para) {
					if(is_array($para)){
						$w->startElement('p');
						foreach ($para as $sub_key => $sub_value) {
							$w->writeElement($sub_key, $sub_value);
						}
						$w->endElement();
					}else{
						$w->writeElement('p', $para);
					}
					
				}
				$w->endElement();
			} else {
			    $w->writeElement($key, $value);
			}
		} 
		$w->endElement();
		$w->endElement();
		$w->endDocument();
		$this->xml = $w->outputMemory();
		// write to file
		if (isset($this->filename) && $this->local) {
		    $success = file_put_contents($this->filename, $this->xml);
		    return $success;
		} elseif (isset($this->filename) && $this->download) {
    		$this->outputHeaders();
    		file_put_contents('php://output', $this->xml);
    		exit;
        }
	}
}