class SimpleReactFileUpload extends React.Component {

  constructor(props) {
    super(props);
    this.state ={
      file:null,
      percentCompleted:0,
      user_id : null,
      photoUrl : '',
      uploading : false
    }
    this.onFormSubmit = this.onFormSubmit.bind(this)
    this.onChange = this.onChange.bind(this)
    this.fileUpload = this.fileUpload.bind(this)

    
  }
  componentWillMount(){
    this.setState({
      user_id : this.props.user_id,
      photoUrl : this.props.foto,
      group_id : this.props.group_id,
    })
  } 
  onFormSubmit(e){
    try{
    e.preventDefault() // Stop form submit

    }catch(e){

    }
    this.fileUpload(this.state.file).then((response)=>{
      const filename = response.data.file_name;
      const photoUrl = site_url() + `uploads/avatar/${filename}`;
      this.setState({photoUrl});
      console.log(response.data);
    })
  }
 
  onChange(e) {
    this.setState({file:e.target.files[0],percentCompleted:0});

    setTimeout(()=>{
      // const changeAvaBtn = document.getElementById('changeAvaBtn');
      // changeAvaBtn.click();
      this.onFormSubmit(event);
    },250);
    
  }
  fileUpload(file){
    const url = site_url() + 'account/change_avatar';
    const formData = new FormData();
    formData.append('file',file);
    formData.append('user_id',this.state.user_id);
    formData.append('group_id',this.state.group_id);
    const config = {
        headers: {
            'content-type': 'multipart/form-data'
        },
        onUploadProgress: (progressEvent) => {
          var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total);
          let uploading = percentCompleted < 100;
          this.setState({percentCompleted:percentCompleted,uploading:uploading});
        }
    }
    return  axios.post(url, formData,config)
  }
  onChangeAvatar(){
    // const fileInput = document.getElementById('fileInput');
    // fileInput.click();
  }
  render() {
    const {percentCompleted, photoUrl,uploading,group_id} = this.state; 
    // const photoUrl = this
    return (
      <div>
      <img src={photoUrl} class="img-responsive" alt="avatar"/> 
      <a href="javascript:;" id="changeAvaBtn" title="Ganti Foto" onClick={this.onChangeAvatar}  style={{display:group_id==2?'none':'block'}}><i class="fa fa-pencil"></i></a>
      <form onSubmit={this.onFormSubmit}>
        <input id="fileInput" type="file" onChange={this.onChange} style={{display:group_id==2?'none':'block'}}/>
        <div id="percentage" style={{display:uploading?'block':'none'}}>{percentCompleted} %</div>
        <button type="submit" id="uploadBtn" style={{display:'none'}}>Upload</button>
      </form>
      </div>
   )
  }
}
