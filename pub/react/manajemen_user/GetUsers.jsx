const LEFT_PAGE = 'LEFT';
const RIGHT_PAGE = 'RIGHT';

/**
 * Helper method for creating a range of numbers
 * range(1, 5) => [1, 2, 3, 4, 5]
 */
const range = (from, to, step = 1) => {
  let i = from;
  const range = [];

  while (i <= to) {
    range.push(i);
    i += step;
  }

  return range;
}

class GetUsers extends React.Component{
    static contextType = AppContext;
    constructor(props) {
        super(props);
        const { totalRecords = null, pageLimit = 10, pageNeighbours = 0 ,totalPages} = props;

        this.pageLimit = typeof pageLimit === 'number' ? pageLimit : 10;
        this.totalRecords = typeof totalRecords === 'number' ? totalRecords : 0;

        // pageNeighbours can be: 0, 1 or 2
        this.pageNeighbours = typeof pageNeighbours === 'number'
          ? Math.max(0, Math.min(pageNeighbours, 2))
          : 0;

        this.totalPages = Math.ceil(this.totalRecords / this.pageLimit);

        this.state = { currentPage: 1 ,id_unor:'',status_marketing:''};
        // console.log( pagination)
    }
    componentDidMount(){
        // this.context.get_users();
        this.gotoPage(1);
    }
    componentDidUpdate(){
      
    }
    gotoPage = page => {
        // console.log(page)
        // this.context.page = page;
        // console.log(this.state);
        this.context.get_users(page);

    const { onPageChanged = f => f } = this.props;

    const currentPage = Math.max(0, Math.min(page, this.totalPages));

    const paginationData = {
      currentPage,
      totalPages: this.totalPages,
      pageLimit: this.pageLimit,
      totalRecords: this.totalRecords
    };

    this.setState({ currentPage }, () => onPageChanged(paginationData));
  }

  handleClick = page => evt => {
    evt.preventDefault();
    this.gotoPage(page);
  }

  handleMoveLeft = evt => {
    evt.preventDefault();
    this.gotoPage(this.state.currentPage - (this.pageNeighbours * 2) - 1);
  }

  handleMoveRight = evt => {
    evt.preventDefault();
    this.gotoPage(this.state.currentPage + (this.pageNeighbours * 2) + 1);
  }

  /**
   * Let's say we have 10 pages and we set pageNeighbours to 2
   * Given that the current page is 6
   * The pagination control will look like the following:
   *
   * (1) < {4 5} [6] {7 8} > (10)
   *
   * (x) => terminal pages: first and last page(always visible)
   * [x] => represents current page
   * {...x} => represents page neighbours
   */
  fetchPageNumbers = () => {

    const totalPages = this.totalPages;
    const currentPage = this.state.currentPage;
    const pageNeighbours = 10;
    this.row_number = 0;

    /**
     * totalNumbers: the total page numbers to show on the control
     * totalBlocks: totalNumbers + 2 to cover for the left(<) and right(>) controls
     */
    const totalNumbers = (this.pageNeighbours * 2) + 3;
    const totalBlocks = totalNumbers + 2;

    if (totalPages > totalBlocks) {

      const startPage = Math.max(2, currentPage - pageNeighbours);
      const endPage = Math.min(totalPages - 1, currentPage + pageNeighbours);

      let pages = range(startPage, endPage);

      /**
       * hasLeftSpill: has hidden pages to the left
       * hasRightSpill: has hidden pages to the right
       * spillOffset: number of hidden pages either to the left or to the right
       */
      const hasLeftSpill = startPage > 2;
      const hasRightSpill = (totalPages - endPage) > 1;
      const spillOffset = totalNumbers - (pages.length + 1);

      switch (true) {
        // handle: (1) < {5 6} [7] {8 9} (10)
        case (hasLeftSpill && !hasRightSpill): {
          const extraPages = range(startPage - spillOffset, startPage - 1);
          pages = [LEFT_PAGE, ...extraPages, ...pages];
          break;
        }

        // handle: (1) {2 3} [4] {5 6} > (10)
        case (!hasLeftSpill && hasRightSpill): {
          const extraPages = range(endPage + 1, endPage + spillOffset);
          pages = [...pages, ...extraPages, RIGHT_PAGE];
          break;
        }

        // handle: (1) < {4 5} [6] {7 8} > (10)
        case (hasLeftSpill && hasRightSpill):
        default: {
          pages = [LEFT_PAGE, ...pages, RIGHT_PAGE];
          break;
        }
      }

      return [1, ...pages, totalPages];

    }

    return range(1, totalPages);

  }
    formatDate(str){
        let _str = str.split(' ');
        try{
            let dt = _str[0].split('-');
            if(dt.length==3){
                return dt[2]+'-'+dt[1]+'-'+dt[0] + ' '+_str[1];
            }
        }catch(e){
            return '00-00-0000';
        }
    }
    // handleUpdate = (id) => {
    //     this.context.handleUpdate(id,this.name.value,this.email.value);
    // }
    setActive(user_id){
      this.context.setActive(user_id);
      // this.gotoPage(1)
    }
    
    unorChanged = (evt) => {
      this.setState({id_unor : evt.target.value});
      this.context.setUnor(evt.target.value);

      // this.gotoPage(1);
      // this.fetchPageNumbers();
    }
    statusMarketingChanged = (evt) =>{
      this.setState({status_marketing : evt.target.value});
      this.context.setStatusMarketing(evt.target.value);
      // this.gotoPage(1);
      // this.fetchPageNumbers();

    }
    render(){
        let allUsers;
        let filters;
        let mainData;
        let pagination;
        let m_jenis = {
          pegawai:'Pegawai',
          non:'Non Pegawai'
        }
        let m_role={
          marketing:'Marketing',
          admin:'Admin'
        }
        this.pageLimit = this.context.per_page;
        this.totalRecords = this.context.total_rec;
        this.totalPages= this.context.total_pages;
        this.currentPage = this.context.page;
        let unor_list = get_unor();
        filters = (<div className="row"></div>);

        allUsers = this.context.all_users.map(({user_id,row_number,nama_lengkap,role,roles, number,jenis, nip_nik,is_active,is_verified, isEditing}) => {
         
            return (
                <tr key={user_id}>
                    <td style={{textAlign:'center'}}>{row_number}</td>
                    <td>{nama_lengkap}</td>
                    <td>{nip_nik}</td>
                    <td>{role}</td>
                    <td>{is_active=='1'?'Aktif':'Tidak Aktif'}</td>
                    <td>{roles}</td>
                    
                </tr>
            );
        });
        const { currentPage } = this.state;
        const pages = this.fetchPageNumbers();
        pagination=(<div style={{textAlign:'center'}}>
        <nav aria-label="Countries Pagination">
          <ul className="pagination">
            { pages.map((page, index) => {

              if (page === LEFT_PAGE) return (
                <li key={index} className="page-item">
                  <a className="page-link" href="#" aria-label="Previous" onClick={this.handleMoveLeft}>
                    <span aria-hidden="true">&laquo;</span>
                    <span className="sr-only">Previous</span>
                  </a>
                </li>
              );

              if (page === RIGHT_PAGE) return (
                <li key={index} className="page-item">
                  <a className="page-link" href="#" aria-label="Next" onClick={this.handleMoveRight}>
                    <span aria-hidden="true">&raquo;</span>
                    <span className="sr-only">Next</span>
                  </a>
                </li>
              );

              return (
                <li key={index} className={`page-item${ currentPage === page ? ' active' : ''}`}>
                  <a className="page-link" href="#" onClick={ this.handleClick(page) }>{ page }</a>
                </li>
              );

            }) }

          </ul>
        </nav>
      </div>);
        if(this.context.all_users.length > 0){
            mainData = (
                <table className="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th style={{textAlign:'center'}}>NIP/NIK</th>
                            <th>Pegawai</th>
                            <th>Status</th>
                            <th>Role</th>
                      
                        </tr>
                    </thead>
                    <tbody>
                        {allUsers}
                    </tbody>
                </table>
            );
        }
        else{
            mainData = (
                <div className="alert alert-light" role="alert">
                    <h4 className="alert-heading">tidak ada data!</h4>
                    <hr/>
                </div>
            );
        }
        return(
            <div>
            {filters}
            {mainData}
            {pagination}

            </div>
        );
    }

}

const AllUsers = GetUsers;