const Axios = axios;
class Actions extends React.Component{
    state = {
        users:[],
        // pagination:{
        per_page : 10,
        page: 1,
        total_pages:0,
        total_rec:0,
        // },
        search_query:'',
        id_unor:'',
        status_marketing:''
    };
    componentDidUpdate(){
    }
    // FETCH USERS FROM DATABASE
    fetchUsers = (page,id_unor,status_marketing) => {
        App.startPageLoading({animate:1});
        let payload = {
            // per_page : this.state.per_page,
            page : page,
            // search_query: this.state.search_query,
            id_unor:  this.state.id_unor,
            status_marketing:  this.state.status_marketing
        };
        // console.log(this.state);
        Axios.post(site_url()+'manajemen/user/data/fetch',payload)
        .then(({data}) => {
            // console.log(data.success)
            if(data.success === 1){
                this.setState({
                    users:data.users,
                    // pagination:{
                        per_page:data.per_page,
                        page:data.page,
                        total_pages: data.total_pages,
                        total_rec:data.total_rec,
                    // }
                });
            }
            App.stopPageLoading();
        })
        .catch(error => {
            // console.log(error);
            App.stopPageLoading();

        })
    };
    setStatusMarketing = (status)=>{
      // this.status_marketing = status;
      this.setState({status_marketing:status},()=>{
        this.fetchUsers(1);
      });
      // console.log(status);;
      // 
    };
    setUnor = (id_unor) =>{
      this.setState({id_unor:id_unor},()=>{
        this.fetchUsers(1);
      });

  
    };
    // SET ACTIVE FROM DATABASE
    setActive = (user_id) => {
        App.startPageLoading({animate:1});

        Axios.post(site_url()+'manajemen/user/setActive',{
            user_id: user_id
        })
        .then(({data}) => {
            // console.log(data)
            if(data.success === 1){
                let users = this.state.users.map(user => {
                    if(user.user_id === user_id){
                        // user.isEditing = true;
                        data.user.row_number = user.row_number;
                        return data.user;
                    }
                    // user.isEditing = false;
                    return user;
                });

                this.setState({
                    users
                });

                
            }else{
                swal(data.msg);
            }
            App.stopPageLoading();

        })
        .catch(error => {
            App.stopPageLoading();
            // console.log(error);
        })
    };

    //  // ON EDIT MODE
    //  editMode = (id) => {
    //     let users = this.state.users.map(user => {
    //         if(user.id === id){
    //             user.isEditing = true;
    //             return user;
    //         }
    //         user.isEditing = false;
    //         return user;
    //     });

    //     this.setState({
    //         users
    //     });
    // }

    // //CANCEL EDIT MODE
    // cancelEdit = (id) => {
    //     let users = this.state.users.map(user => {
    //         if(user.id === id){
    //             user.isEditing = false;
    //             return user;
    //         }
    //         return user
            
    //     });
    //     this.setState({
    //         users
    //     });
    // }

    // // UPDATE USER
    // handleUpdate = (id,user_name,user_email) => {
    //     Axios.post(site_url()+'log_user/data/update',
    //     {
    //         id:id,
    //         user_name:user_name,
    //         user_email:user_email
    //     })
    //     .then(({data}) => {
    //         if(data.success === 1){
    //             let users = this.state.users.map(user => {
    //                 if(user.id === id){
    //                     user.user_name = user_name;
    //                     user.user_email = user_email;
    //                     user.isEditing = false;
    //                     return user;
    //                 }
    //                 return user; 
    //             });
    //             this.setState({
    //                 users
    //             });
    //         }
    //         else{
    //             swal(data.msg);
    //         }
    //     })
    //     .catch(error => {
    //         console.log(error);
    //     });
    // }


    // DELETE LOG
    handleDelete = (id) => {
        let deleteUser = this.state.users.filter(log => {
            return log.id !== id;
        });
        
        Axios.post(site_url()+'manajemen/user/data/delete',{
            id:id
        })
        .then(({data}) => {
            if(data.success === 1){
                this.setState({
                    users:deleteUser
                });
            }
            else{
                swal(data.msg);
            }
        })
        .catch(error => {
            // console.log(error);
        });
    }

    // INSERT USER
    // insertUser = (event,user_name,user_email) => {
    //     event.preventDefault();
    //     event.persist();
    //     Axios.post(site_url()+'log_user/data/insert',{
    //         user_name:user_name,
    //         user_email:user_email
    //     })
    //     .then(function ({data}) {
    //         if(data.success === 1){
    //             this.setState({
    //                 users:[
    //                     {"id":data.id,"user_name":user_name,"user_email":user_email},
    //                     ...this.state.users
    //                 ]
    //             });
    //             event.target.reset();
    //         }
    //         else{
    //             swal(data.msg);
    //         }
    //     }.bind(this))
    //     .catch(function (error) {
    //         console.log(error);
    //     });
    // }
}

