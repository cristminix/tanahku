const Axios = axios;
class Actions extends React.Component{
    state = {
        logs:[],
        // pagination:{
            per_page : 10,
            page: 1,
            total_pages:0,
            total_rec:0,
        // },
        search_query:''
    }

    // FETCH USERS FROM DATABASE
    fetchLogs = (page,per_page,search_query) => {
        Axios.post(site_url()+'log_user/data/fetch',{
            per_page : per_page,
            page : page,
            search_query: search_query
        })
        .then(({data}) => {
            // console.log(data.success)
            if(data.success === 1){
                this.setState({
                    logs:data.logs,
                    // pagination:{
                        per_page:data.per_page,
                        page:data.page,
                        total_pages: data.total_pages,
                        total_rec:data.total_rec,
                    // }
                });

                
            }
        })
        .catch(error => {
            console.log(error);
        })
    }

    //  // ON EDIT MODE
    //  editMode = (id) => {
    //     let users = this.state.logs.map(user => {
    //         if(user.id === id){
    //             user.isEditing = true;
    //             return user;
    //         }
    //         user.isEditing = false;
    //         return user;
    //     });

    //     this.setState({
    //         users
    //     });
    // }

    // //CANCEL EDIT MODE
    // cancelEdit = (id) => {
    //     let users = this.state.logs.map(user => {
    //         if(user.id === id){
    //             user.isEditing = false;
    //             return user;
    //         }
    //         return user
            
    //     });
    //     this.setState({
    //         users
    //     });
    // }

    // // UPDATE USER
    // handleUpdate = (id,user_name,user_email) => {
    //     Axios.post(site_url()+'log_user/data/update',
    //     {
    //         id:id,
    //         user_name:user_name,
    //         user_email:user_email
    //     })
    //     .then(({data}) => {
    //         if(data.success === 1){
    //             let users = this.state.logs.map(user => {
    //                 if(user.id === id){
    //                     user.user_name = user_name;
    //                     user.user_email = user_email;
    //                     user.isEditing = false;
    //                     return user;
    //                 }
    //                 return user; 
    //             });
    //             this.setState({
    //                 users
    //             });
    //         }
    //         else{
    //             swal(data.msg);
    //         }
    //     })
    //     .catch(error => {
    //         console.log(error);
    //     });
    // }


    // DELETE LOG
    handleDelete = (id) => {
        let deleteLog = this.state.logs.filter(log => {
            return log.id !== id;
        });
        
        Axios.post(site_url()+'log_user/data/delete',{
            id:id
        })
        .then(({data}) => {
            if(data.success === 1){
                this.setState({
                    logs:deleteLog
                });
            }
            else{
                swal(data.msg);
            }
        })
        .catch(error => {
            console.log(error);
        });
    }

    // INSERT USER
    // insertUser = (event,user_name,user_email) => {
    //     event.preventDefault();
    //     event.persist();
    //     Axios.post(site_url()+'log_user/data/insert',{
    //         user_name:user_name,
    //         user_email:user_email
    //     })
    //     .then(function ({data}) {
    //         if(data.success === 1){
    //             this.setState({
    //                 users:[
    //                     {"id":data.id,"user_name":user_name,"user_email":user_email},
    //                     ...this.state.logs
    //                 ]
    //             });
    //             event.target.reset();
    //         }
    //         else{
    //             swal(data.msg);
    //         }
    //     }.bind(this))
    //     .catch(function (error) {
    //         console.log(error);
    //     });
    // }
}

