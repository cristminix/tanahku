$(document).ready(function(){
	$('#syc_account_btn').on('click',function(){
		App.startPageLoading({animate:!0});
	    $('.modal-content > .script').load(site_url()+'data/sync_account',function(){
	        $('#syncAccountModal').modal({backdrop: 'static', keyboard: false,show:true});
	        App.stopPageLoading();
	    });
	});	
});