$(document).ready(function() {
    app.vm = new Vue({
        el: '#app',
        data: {
            gmaps:{
                map: null,
                markers: [],
                infoWindow: null
            },
            is_mobile: app.data.is_mobile,
            sub_title: app.data.sub_title,
            config: {
                filter_open: false
            },
            filter_data: {
               daya_listrik: app.data.dd_daya_listrik,
               status_pelanggan : app.data.dd_status_pelanggan,
               cabang : app.data.dd_cabang,
               kabupaten: app.data.dd_kabupaten,
               
            },
            dd_kecamatan:[],
            dd_kelurahan:[],
            pagination: app.data.pagination,
            pagination_per_pages: [5,10, 25, 50, 100],
            filter: {

                search_query: app.data.search_query,
                id_cab:'',
                status_pelanggan:'',
                id_user:'',
                id_daya:'',
                tanggal_awal:'',
                tanggal_akhir:'',
            },
            tmp_hash:null,
            grid_data: [],
            pager: {
                input_page: 1,
                current_page:1,
                page_count:1,
                page_to_display:[],
                is_first_page:false,
                has_next:false,
                has_prev:false,
                prev_page:0,
                next_page:0,
                max_page_to_display:5,
                index:0
            },
            state:{
            	grid_shown:true,
            	detail_shown:false,
                form_shown:false
            },
            detail:null,
            form:{mode:'',is_valid:false,foto:{foto_ktp:'',foto_ktp_selfi:'',foto_kk:'',foto_rumah:''}},
            new_detail:null,
            dl:{
                enable:false,
                url: app.site_url(`appbkpp/cetak/pegawai_pensiun/excel`),
                src:''
            },
            is_admin:app.is_admin,
            nama_marketing:'',
            yes_no : {'0':'No','1':'Yes'}
        },
        mounted:function(){
            let self = this;
            this.$nextTick(function(){
                $('a.export_btn').click(function(){
                        self.export();
                    });
                if(!this.is_admin){
                    this.nama_marketing = app.default_nama_marketing;
                    this.filter.id_user = app.default_filter_user_id;
                }
                $('input[name=tanggal_awal],input[name=tanggal_akhir]').datepicker({
                    format: 'dd-mm-yyyy',    
                    viewformat: 'dd/mm/yyyy',
                });
                $('input[name=tanggal_awal]').on('change',function(e){
                    self.filter.tanggal_awal = self.mysqlDate( e.target.value );
                    self.onFilterChanged();             
                });
                $('input[name=tanggal_akhir]').on('change',function(e){
                    self.filter.tanggal_akhir = self.mysqlDate(e.target.value);
                    self.onFilterChanged();             

                });;

            });
        	
            if(app.form_test){
                setTimeout(function(){
                    self.formEdit(self.grid_data[0]);
                },1500);
            }
            if(typeof app.set_filter_status != undefined){
                if($.inArray(app.set_filter_status,['prospek','survey','pelanggan','batal']) != -1){
                    self.filter.status_pelanggan = app.set_filter_status;
                } 
            }
            setTimeout(function(){
                self.gridPaging('');
                
            },250);
        },
        watch: {

        },
        methods: {
            export(){
                console.log('export');
                let param = JSON.stringify(this.filter);
                let url_prxy = site_url()+'export/data_transaksi/excel?param='+btoa(param);
                $('iframe#export_excel').prop('src',url_prxy);
              
            },
            getStatusPelangganText(item){
                
                    let status_pelanggan = item.status_pelanggan || 'PROSPEK';
                    return status_pelanggan.toUpperCase();
            },
            goBack: function() {

                document.location.href = app.link.pegawai_aktif;
            },
            toggleFilter: function() {
                this.config.filter_open = !this.config.filter_open;
            },
            onFilterChanged: function() {
                this.goPaging('');
            },
            onPaginationChanged: function() {
                this.goPaging('');

            },
            onGridSearch: function() {
                this.goPaging('');
            },
            goPaging: function(page) {
                 
                this.pagination.page = page;
                this.gridPaging(page);
            },
            inputPagingBlur: function(event) {
                const el = event.target;
                if (el.value == '') {
                    el.value = '1';
                }
            },
            inputPagingFocus: function(event) {
                const el = event.target;
                if (el.value == '1') {
                    el.value = '';
                }
            },
            gridPaging: function(page) {
                App.startPageLoading({animate:!0});
                var form_data = new FormData();
				for ( var key in this.filter ) {
				    form_data.append(key, this.filter[key] );
				}
				form_data.append('page',page);
				form_data.append('per_page',this.pagination.per_page);
                axios({
                	method:'post',
                	url:app.site_url('data/pelanggan/json'),
                	data:form_data,
                	headers: {'Content-Type': 'multipart/form-data' },

                })
			    .then((response) => {
			        let data = response.data;
                    this.buildPager(data);
			        this.grid_data = data.records;
                    App.stopPageLoading();
			        
			    })
			    .catch((error) => {
                    try{swal(error.response.data);}catch(e){}
                    App.stopPageLoading();
			        
			    });
            },
            buildPager(data){
                 
            	this.pager.page_count = parseInt(data.total_pages);
				this.pager.current_page = parseInt(data.current_page);
				this.pagination.page = parseInt(data.current_page);
				this.pager.input_page = parseInt(data.current_page);
				this.pager.page_to_display=[];

                
                if(this.pager.current_page > 1){
                    this.pager.index = (this.pager.current_page -1) * this.pagination.per_page;
                }else{
                    this.pager.index = 0;
                }
				this.pager.max_page_to_display = parseInt(this.pager.max_page_to_display);
				// this.pager.current_page = parseInt(this.pager.current_page);
				if((this.pager.current_page + (this.pager.max_page_to_display-1) >= data.total_pages)&&data.total_pages>this.pager.max_page_to_display){
					//                11             >   11-5=6    
					let i = 1;
					let last = data.total_pages;//11
					while(i<=this.pager.max_page_to_display){
						this.pager.page_to_display.push(last--);
						i++;
					}   
					 
					this.pager.page_to_display.reverse();
				}else{

					if(data.total_pages > 1 ){
					    let brk = 1;
						for (var i = (this.pager.current_page == data.total_pages?1:this.pager.current_page); i < (this.pager.current_page+this.pager.max_page_to_display); i++) {
							this.pager.page_to_display.push(i);
							if(brk == data.total_pages){
                                break;
                            }
                            brk++;
						}
					}else{
						this.pager.page_to_display.push(1);
					}
				}

				if(this.pager.current_page == data.total_pages){
					this.pager.has_next = false;
				}else{
					this.pager.has_next = true;
					this.pager.next_page = this.pager.current_page + 1;
				}

				if(this.pager.current_page == 1){
					this.pager.has_prev = false;
				}else{
					this.pager.has_prev = true;
					this.pager.prev_page = this.pager.current_page - 1;
				}
            },
          
            getRowNumber:function(index){
            	let number = 0;
            	let page = parseInt(this.pagination.page)+0;
            	let per_page = parseInt(this.pagination.per_page);

            	if(page === 1){
            		number = index+1;
            	}else{
            				// 1       +  (10 * 1)	
            		number = (index+1) + (per_page*(page-1));
            	}
            	return number;
            }, 
            backToGrid:function(isForm){
                if(typeof isForm != 'undefined'){
                    if(isForm == true){
                        this.state.form_shown = false;
                        this.new_detail = null;
                    }
                }
            	this.state.grid_shown = true;
            	this.state.detail_shown = false;
            },
            
            applyJsForm(){
                let self = this;
                this.$nextTick(function(){
                    ///////////////////////////////////////////////////////////////////
                    var bloodhoundSuggestions = new Bloodhound({
                      datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                      queryTokenizer: Bloodhound.tokenizers.whitespace,
                      remote: {
                        wildcard: '%QUERY',
                        url: site_url()+'data/ac_marketing?term=%QUERY',
                        transform: function(argument) {
                                return argument
                            }
                      }
                    });

                    $('input[name=nama_marketing]').typeahead(null, {
                        name:'Marketing',
                        display: function(item) {   
                            return item.value;
                        },
                        limit: 5,
                        templates: {
                            suggestion: function(item) {
                                return '<div>'+ item.value +'</div>';
                            }
                        },
                        source: bloodhoundSuggestions.ttAdapter()
                    }).on('typeahead:selected', function (e, datum) {
                        app.vm.form.id_user = datum.id;
                        app.vm.form.nama_marketing = datum.value;

                    }).on('keyup',function(){
                        if(this.value.length == 0){
                            app.vm.form.nama_marketing = '';
                            app.vm.form.id_user = '';
                        }
                    });
                    //////////////////////////////////////////////////////////////////
                    $('input[name=nama]').focus();
                    $('#form_pelanggan').validator().on('submit', function (e) {
                          let form = e.target;
                          if (e.isDefaultPrevented()) {
                            // handle the invalid form...
                            console.log("validation failed");
                          }else{
                            self.formSubmit(self.form);
                          }
                        
                            e.preventDefault();
                          
                          return false;

                    });
                    $('select[name=id_kab]').on('change',function(e){
                        self.updateFormDd('kec',self.form.id_kab);
                    }).trigger('change');
                    $('select[name=id_kec]').on('change',function(e){
                        self.updateFormDd('kel',self.form.id_kec);
                    }).trigger('change');


                    //////////////////////////////////////////////////////////////////////////
                    // UNIQUE VALIDATION SERVER
                    // kode_pelanggan,nik,no_kk,no_hp,email
                    //////////////////////////////////////////////////////////////////////////
                    let __validate = function(el){
                        let url_prxy = site_url() + 'transaksi/validation/' + self.form.mode;
                        var form_data = new FormData();
                    
                        form_data.append('field',el.name);
                        form_data.append('value',el.value);
                        if(self.form.mode == 'edit'){
                            form_data.append('id_pelanggan',self.form.id_pelanggan);
                        }
                        axios({
                            method:'post',
                            url: url_prxy,
                            data:form_data,
                            headers: {'Content-Type': 'multipart/form-data' },

                        })
                        .then((response) => {
                            let data = response.data;
                       
                            if(data.success){
                                // this.form.foto[`foto_${jenis}`] = '';
                            }else{
                                snackbar(data.message,4000,true);
                                let name = el.name;
                                $(`input[name=${name}]`).closest('.form-group').addClass('has-error');
                                $(`input[name=${name}]`).focus();
                                // 
                            }
                        })
                        .catch((error) => {
                            try{swal(error.response.data);}catch(e){}
                            
                        });
                    }
                    $('input[name=kode_pelanggan]').change(function(e){
                        __validate(e.target);
                    });
                    $('input[name=nik]').change(function(e){
                        __validate(e.target);
                    });
                    $('input[name=nik]').change(function(e){
                        __validate(e.target);
                    });
                    $('input[name=no_kk]').change(function(e){
                        __validate(e.target);
                    });
                    $('input[name=no_hp]').change(function(e){
                        __validate(e.target);
                    });
                    $('input[name=email]').change(function(e){
                        __validate(e.target);
                    });
                    //////////////////////////////////////////////////////////////////////////

                });
            },
            formEdit(item){
                this.detail = item;
                this.state.form_shown = true;
                this.state.grid_shown = false;
                this.form = {mode:'edit',submited:false};
                this.form.id_pelanggan = item.id_pelanggan;
                this.form.kode_pelanggan = item.kode_pelanggan;
                this.form.email = item.email;
                this.form.nama  = item.nama ;
                this.form.nik = item.nik;
                this.form.no_kk = item.no_kk;
                this.form.alamat = item.alamat;
                this.form.rt = item.rt;
                this.form.rw = item.rw;
                this.form.id_kec = item.id_kec;
                this.form.id_kel = item.id_kel;
                this.form.id_kab = item.id_kab;
                this.form.id_daya = item.id_daya;
                this.form.id_cab = item.id_cab;
                this.form.id_prov = item.id_prov;
                this.form.no_rumah = item.no_rumah;
                this.form.no_hp = item.no_hp;
                this.form.koordinat = item.koordinat;
                this.form.status_pelanggan = item.status_pelanggan||''; 
                this.form.foto = item.foto;
                this.form.nama_marketing = item.am_nama_lengkap;

                // this.form.jenis = item.jenis;

                this.applyJsForm();
                this.form.is_valid = this.validateForm();

            },
            formDelete(item){
                // this.detail = item;
                // this.state.detail_shown = true;
                // this.state.grid_shown = false;
                // this.detail.mode = 'hapus';
            },
            formAdd(){
                this.tmp_hash = this.generateTmpHash();
                

                /////////////////////////////////////////////////////////////////////////
                /// DUMMY TEST
               let new_data = {}; 
               new_data.kode_pelanggan = '';
               new_data.email = '';
               new_data.nama  = '' ;
               new_data.nik =  '';
               new_data.no_kk = '';
               new_data.alamat = '';
               new_data.rt = '';
               new_data.rw = '';
               new_data.id_kec = '';
               new_data.id_kel = '';
               new_data.id_kab = '3603';
               new_data.id_cab = '';
               new_data.id_daya = '';
               new_data.id_prov = '36';
 


               new_data.no_rumah = '';
               new_data.no_hp = '';
               new_data.koordinat = '';
               new_data.status_pelanggan = ''; 
               new_data.foto = {foto_ktp:'',foto_ktp_selfi:'',foto_kk:'',foto_rumah:''};

               this.detail = new_data;
 
                this.state.form_shown = true;
                this.state.grid_shown = false;
                this.form = {mode:'add',submited:false};

                this.form.id_pelanggan = new_data.id_pelanggan;
                this.form.kode_pelanggan = new_data.kode_pelanggan;
                this.form.email = new_data.email;
                this.form.nama  = new_data.nama ;
                this.form.nik = new_data.nik;
                this.form.no_kk = new_data.no_kk;
                this.form.alamat = new_data.alamat;
                this.form.rt = new_data.rt;
                this.form.rw = new_data.rw;
                this.form.id_kec = new_data.id_kec;
                this.form.id_kel = new_data.id_kel;
                this.form.id_kab = new_data.id_kab;
                this.form.id_cab = new_data.id_cab;
                this.form.id_daya = new_data.id_daya;
                this.form.no_rumah = new_data.no_rumah;
                this.form.no_hp = new_data.no_hp;
                this.form.koordinat = new_data.koordinat;
                this.form.status_pelanggan = new_data.status_pelanggan||''; 
                this.form.foto = new_data.foto;

                 

               

                this.applyJsForm();
                this.form.is_valid = this.validateForm();

            },
            generateTmpHash(){
                // let dt  = app.date;
                // let str = `${dt}_${user_id}_${unique}`;

                // return btoa(str);
                return uuidv4();
            },
            validateForm(){
                let isValid = true;

                // let a = this.form.id_pegawai !== "" ; 
             
                // let b = this.form.tanggal_sk.match(/^(\d{1,2})-(\d{1,2})-(\d{4})$/) ; 
                // let c = this.form.tanggal_pensiun.match(/^(\d{1,2})-(\d{1,2})-(\d{4})$/) ;
           
                // let d = this.form.jenis_pensiun.length > 1 ;
                // let e = this.form.no_sk.length > 1;
                // isValid = a && d && e;
                this.form.is_valid = isValid;
            },
            formSubmit(form){
                this.validateForm();
                if(!this.form.is_valid){
                    swal('Mohon Lengkapi data anda');
                    return;
                }
                if(this.form.submited === false){
                    this.form.submited = true;
                    let messages = {
                        'edit' : 'Sedang Mengupdate data',
                        'activate' : 'Sedang Mengirim Email',
                        'delete' : 'Sedang Menghapus data',

                    };
                    App.startPageLoading({animate:!0});

                    var form_data = new FormData();
                    if(this.form.mode == 'edit'  ){
                        form_data.append('id_pelanggan',this.form.id_pelanggan);
                    }
                    if(form.mode == 'add'){
                        form_data.append('tmp_hash',this.tmp_hash);
                        form_data.append('id_user',this.form.id_user);
                    }
                    
                    form_data.append('nik',this.form.nik);
                    form_data.append('no_kk',this.form.no_kk);
                    // form_data.append('kode_pelanggan',this.form.kode_pelanggan);
                    form_data.append('nama',this.form.nama);
                    form_data.append('email',this.form.email);
                    form_data.append('alamat',this.form.alamat);
                    form_data.append('rt',this.form.rt);
                    form_data.append('rw',this.form.rw);
                    form_data.append('id_kab',this.form.id_kab);
                    form_data.append('id_kec',this.form.id_kec);
                    form_data.append('id_kel',this.form.id_kel);
                    form_data.append('id_cab',this.form.id_cab);
                    form_data.append('id_data',this.form.id_daya);
                    form_data.append('no_rumah',this.form.no_rumah);
                    form_data.append('no_hp',this.form.no_hp);
                    form_data.append('koordinat',this.form.koordinat);
                    form_data.append('id_cab',this.form.id_cab);
                    form_data.append('id_daya',this.form.id_daya);
                    form_data.append('status_pelanggan',this.form.status_pelanggan);
                    axios({
                        method:'post',
                        url:app.site_url('transaksi/form/'+this.form.mode),
                        data:form_data,
                        headers: {'Content-Type': 'multipart/form-data' },

                    })
                    .then((response) => {
                        App.stopPageLoading();
                        let data = response.data;
                       
                            if(data=='success'){
                                snackbar('Success !');
                                this.backToGrid(true); 
                                this.gridPaging('');
                            }
                            else{
                                snackbar('GAGAL menyimpan data : '+data,4000,true);
                            }
                           
                        this.form.submited = false;
                    })
                     .catch((error) => {
                        App.stopPageLoading();
                        swal(error.response.data);
                        this.form.submited = false;
                    });

                        
                }
            },
            deleteFoto(jenis,form,tmp_hash,is_new){
                let url_prxy = app.site_url('transaksi/delete_foto');

                if(typeof tmp_hash != 'undefined'){
                    url_prxy += `/${tmp_hash}`;
                }
                if(typeof is_new != 'undefined'){
                    url_prxy += `/${is_new}`;
                }
                if(confirm('Apakah Anda yakin ingin Menghapus foto ' + jenis.toUpperCase()) ){
                    var form_data = new FormData();
                    
                    form_data.append('jenis',jenis);
                    form_data.append('id_pelanggan',form.id_pelanggan);
                    axios({
                        method:'post',
                        url: url_prxy,
                        data:form_data,
                        headers: {'Content-Type': 'multipart/form-data' },

                    })
                    .then((response) => {
                        let data = response.data;
                        
                        if(data.status){
                            this.form.foto[`foto_${jenis}`] = '';
                        }else{
                            swal(data.msg);
                        }
                    })
                    .catch((error) => {
                        try{swal(error.response.data);}catch(e){}
                        
                    });
                }
                return false;
            },
            uploadFoto(jenis,form,tmp_hash,is_new){
                let self = this;
                let url_prxy = app.site_url('transaksi/upload_foto');

                if(typeof tmp_hash != 'undefined'){
                    url_prxy += `/${tmp_hash}`;
                }
                if(typeof is_new != 'undefined'){
                    url_prxy += `/${is_new}`;
                }
                // //////////////////////////////////////////////////////
                $(`input[name=file_foto_${jenis}]`).unbind('change');
                $(`input[name=file_foto_${jenis}]`).on('change',function(e){
                    let file = $(e.target)[0].files[0];
                    if(typeof file == 'object'){
                        if(file.type.match(/image/)){

                            App.startPageLoading({animate:!0});
                            let form_data = new FormData();

                            if(is_new){
                                form_data.append('tmp_hash',tmp_hash);
                                form_data.append('is_new',1);

                            }else{
                                form_data.append('id_pelanggan',form.id_pelanggan);

                            }
                            
                            form_data.append(`file_foto_${jenis}`,file);
                            form_data.append('jenis',jenis);
                            // 
                            axios({
                                method:'post',
                                url: url_prxy,
                                data:form_data,
                                headers: {'Content-Type':  "multipart/form-data; charset=utf-8; boundary=" + Math.random().toString().substr(2)  },

                            })
                            .then((response) => {
                                let data = response.data;
                              
                                if(data.status){
                                    self.form.foto[`foto_${jenis}`] = data.file_name;
                                }else{
                                    swal(data.msg);
                                }
                                App.stopPageLoading();
                            })
                            .catch((error) => {
                                App.stopPageLoading();

                                try{swal(error.response.data);}catch(e){}
                                
                            });
                        }else{
                            swal('Anda hanya diperbolehkan mengupload file gambar');
                            $(e.target).val('');
                        }
                    }
                });
                $(`input[name=file_foto_${jenis}]`).trigger('click');

                ///////////////////////////////////////////////////////////
            },
            formatDate(str){
                try{
                    let dt = str.split('-');
                    if(dt.length==3){
                        return dt[2]+'-'+dt[1]+'-'+dt[0];
                    }
                }catch(e){
                    return '00-00-0000';
                }
            },
            mysqlDate(str){
                try{
                    let dt = str.split('-');
                    if(dt.length==3){
                        return dt[2]+'-'+dt[1]+'-'+dt[0];
                    }
                }catch(e){
                    return '0000-00-00';
                }
            },
            checkInvalidDate(str){
                if(str == '01-01-1970' || str == '00-00-0000' || str == '30-11--0001'){
                    return '-';
                }else{
                    return str;
                }
            },
            printExcel(){

                // this.dl.enable = false;
                // this.dl.src = app.site_url('docs/blank.txt');
                // this.$nextTick(()=>{
                //     let param64 = $.extend({},this.filter,this.pagination);
                //      param64 = btoa(JSON.stringify(param64));

                //     setTimeout(()=>{
                //         this.dl.enable = true;
                //         this.dl.src = this.dl.url + '?param='+param64;

                //     },500);
                // });
                
            },
            updateFormDd(jenis,parent_id){
                App.startPageLoading({animate:!0});

                let form_data = new FormData();

                let url_prxy = site_url() + `transaksi/dd_lokasi/${jenis}/${parent_id}`;
                // form_data.append('jenis',jenis);
                // 
                let self = this;
                axios({
                    method:'post',
                    url: url_prxy,
                    data:form_data,
                    headers: {'Content-Type':  "multipart/form-data; charset=utf-8; boundary=" + Math.random().toString().substr(2)  },

                })
                .then((response) => {
                    let data = response.data;
                    let key = {'kec':'kecamatan','kel':'kelurahan'}[jenis];
                    self[`dd_${key}`] = data;

                    setTimeout(function(){

                        if(jenis=='kec'){
                            let p = $(`select[name=id_${jenis}]`).closest('.form-group');
                                p.removeClass('has-error has-danger').addClass('has-success');
                                p.find('.help-block').html('').removeClass('.with-errors');    
                        }
                        
                    },100);

                    App.stopPageLoading();
                })
                .catch((error) => {
                    App.stopPageLoading();

                    try{swal(error.response.data);}catch(e){}
                    
                });
            },
            pickMaps(form){
             
                let lat = -6.16027;
                let lon = 106.62957;
             
                let self = this;

                try{
                    let coords = form.koordinat.split(',');
                    if(coords.length == 2){
                        lat = parseFloat(coords[0]);
                        lon = parseFloat(coords[1]);
                    }
                }catch(e){

                }
                document.getElementById("lat").value = lat;
                document.getElementById("lon").value = lon;

                $("#gmaps_modal").unbind('show.bs.modal');
                $("#gmaps_modal").on('show.bs.modal', function(){
                    if(!self.is_mobile){
                        setTimeout(function(){
                            let p_h = $(window).height();
                            let h_h = $('#gmap_header').height(); 
                            let g_h = p_h - h_h -25 ;
                            $('#gmap').height(g_h);
                        },1000);
                    }
                    
                    var initialLatlng = new google.maps.LatLng(lat,lon);
                    var myOptions = {
                        zoom:10,
                        center: initialLatlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    }

                    self.gmaps.map = new google.maps.Map(document.getElementById("gmap"), myOptions);
                    // marker refers to a global variable
                    self.gmaps.markers = new google.maps.Marker({
                        position: initialLatlng,
                        map: self.gmaps.map
                    });
                    self.gmaps.infoWindow = new google.maps.InfoWindow;
                    
                    var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';

                    google.maps.event.addListener(self.gmaps.map, "click", function(event) {
                        // get lat/lon of click
                        var clickLat = event.latLng.lat();
                        var clickLon = event.latLng.lng();

                        // show in input box
                        let _lat = clickLat.toFixed(5);
                        let _lon = clickLon.toFixed(5);

                        document.getElementById("lat").value = _lat;
                        document.getElementById("lon").value = _lon;

                        let koordinat = `${_lat},${_lon}`;
                        self.form.koordinat = koordinat;
                        $('input[name=koordinat]').val(koordinat);
                        var icon = {
                              url: 'https://maps.gstatic.com/mapfiles/place_api/icons/geocode-71.png',
                              size: new google.maps.Size(71, 71),
                              origin: new google.maps.Point(0, 0),
                              anchor: new google.maps.Point(17, 34),
                              scaledSize: new google.maps.Size(25, 25)
                            };
                        for (var i = 0; i < self.gmaps.markers.length; i++) {
                          self.gmaps.markers[i].setMap(null);
                        }
                        var pos = new google.maps.LatLng(clickLat,clickLon);
                        var marker = new google.maps.Marker({
                            position: pos,
                            map: self.gmaps.map,
                            icon:icon
                        });
                        self.gmaps.markers.push(marker);
                        self.gmaps.infoWindow.setPosition(pos);
                        self.gmaps.infoWindow.setContent('Gunakan Lokasi Ini');
                        self.gmaps.infoWindow.open(self.gmaps.map);

                    });
                    ///////////////////////////////////////

                    ////////////////////////////////////////////
                        self.gmaps.markers = [];

                        // Create the search box and link it to the UI element.
                        var input = document.getElementById('pac-input');
                        var searchBox = new google.maps.places.SearchBox(input);
                        self.gmaps.map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                        // Bias the SearchBox results towards current map's viewport.
                        self.gmaps.map.addListener('bounds_changed', function() {
                          searchBox.setBounds(self.gmaps.map.getBounds());
                        });
                        
                        // Listen for the event fired when the user selects a prediction and retrieve
                        // more details for that place.
                        searchBox.addListener('places_changed', function() {
                        
                          var places = searchBox.getPlaces();

                          if (places.length == 0) {
                            return;
                          }

                          // Clear out the old markers.
                          self.gmaps.markers.forEach(function(marker) {
                            marker.setMap(null);
                          });
                          self.gmaps.markers = [];

                          // For each place, get the icon, name and location.
                          var bounds = new google.maps.LatLngBounds();
                          places.forEach(function(place) {
                            if (!place.geometry) {
                              console.log("Returned place contains no geometry");
                              return;
                            }
                            var icon = {
                              url: place.icon,
                              size: new google.maps.Size(71, 71),
                              origin: new google.maps.Point(0, 0),
                              anchor: new google.maps.Point(17, 34),
                              scaledSize: new google.maps.Size(25, 25)
                            };

                            // place.geometry.location
                            let _lat = place.geometry.location.lat().toFixed(5);
                            let _lon = place.geometry.location.lng().toFixed(5);

                            document.getElementById("lat").value = _lat;
                            document.getElementById("lon").value = _lon;

                            let koordinat = `${_lat},${_lon}`;
                            self.form.koordinat = koordinat;
                            $('input[name=koordinat]').val(koordinat);
                            
                            // Create a marker for each place.
                            self.gmaps.markers.push(new google.maps.Marker({
                              map: self.gmaps.map,
                              icon: icon,
                              title: place.name,
                              position: place.geometry.location
                            }));
                            
                            self.gmaps.infoWindow.setPosition(place.geometry.location);
                            self.gmaps.infoWindow.setContent('Gunakan Lokasi Ini');
                            self.gmaps.infoWindow.open(self.gmaps.map);

                            if (place.geometry.viewport) {
                              // Only geocodes have viewport.
                              bounds.union(place.geometry.viewport);
                            } else {
                              bounds.extend(place.geometry.location);
                            }
                          });
                          self.gmaps.map.fitBounds(bounds);
                        });
                    // ///////////////////////////////////////
                });
                $("#gmaps_modal").modal("show");

            },
            goToMyLoc(){
                let self = this;
                // Try HTML5 geolocation.
                if (navigator.geolocation) {
                  navigator.geolocation.getCurrentPosition(function(position) {
                    var pos = {
                      lat: position.coords.latitude,
                      lng: position.coords.longitude
                    };

                    self.gmaps.infoWindow.setPosition(pos);
                    self.gmaps.infoWindow.setContent('Lokasi Anda');
                    self.gmaps.infoWindow.open(self.gmaps.map);

                    self.gmaps.map.setCenter(pos);
                    self.gmaps.map.setZoom(15);

                  }, function() {
                    handleLocationError(true, self.gmaps.infoWindow, self.gmaps.map.getCenter());
                  // swal(' Browser doesnt support Geolocation')

                  });
                } else {
                  // swal(' Browser doesnt support Geolocation')
                  handleLocationError(false, this.gmaps.infoWindow, this.gmaps.map.getCenter());
                }
            },
            saveLocation(){
                let _lat = document.getElementById("lat").value ;
                let _lon = document.getElementById("lon").value ;

                let koordinat = `${_lat},${_lon}`;
                this.form.koordinat = koordinat;
                $('input[name=koordinat]').val(koordinat);
            },
            lihatFoto(jenis,item){
                
                let key = 'foto_' + jenis;
                if(item.foto[key] != ''){
                    let foto_url = site_url() + 'uploads/pelanggan/' + item.foto[key];
                    let foto = `<img class="img-responsive" style="width:100%" src="${foto_url}"/>`;
                    $('#lihat-foto').html(foto);
                    $('#foto_modal_ro').modal("show");
                }else{
                    snackbar('Belum ada foto !',4000,true);
                } 

            },
            lihatPeta(item){

                let lat = -6.16027;
                let lon = 106.62957;
                let self = this;

                try{
                    let coords = item.koordinat.split(',');
                    if(coords.length == 2){
                        lat = parseFloat(coords[0]);
                        lon = parseFloat(coords[1]);
                    }
                }catch(e){
                    snackbar('Belum ada peta !',4000,true);
                    return;
                }
                
                this.gmaps.infoWindow = new google.maps.InfoWindow;
                $("#gmaps_modal_ro").unbind('show.bs.modal');
                $("#gmaps_modal_ro").on('show.bs.modal', function(){
                    
                    // $('#gmap_ro').height()    
                    if(!self.is_mobile){
                        setTimeout(function(){
                            let p_h = $(window).height();
                            let h_h = $('#gmap_header_ro').height(); 
                            let g_h = p_h - h_h -25 ;
                            $('#gmap_ro').height(g_h);
                        },1000);
                    }

                    document.getElementById("lat_v").value = lat;
                    document.getElementById("lon_v").value = lon;

                    var initialLatlng = new google.maps.LatLng(lat,lon);
                    var myOptions = {
                        zoom:10,
                        center: initialLatlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    self.gmaps.map = new google.maps.Map(document.getElementById("gmap_ro"), myOptions);
                    // marker refers to a global variable
                    self.gmaps.markers = new google.maps.Marker({
                        position: initialLatlng,
                        map: self.gmaps.map
                    });

                });
                $("#gmaps_modal_ro").modal("show");

            },
            formDeleteAction(detail){
                
                
                let form_data = new FormData();

                form_data.append('id_pelanggan',detail.id_pelanggan);

                App.startPageLoading({animate:!0});

                axios({
                    method:'post',
                    url:app.site_url('transaksi/form/'+detail.mode),
                    data:form_data,
                    headers: {'Content-Type': 'multipart/form-data' },

                })
                .then((response) => {
                    App.stopPageLoading();
                    let data = response.data;
                     
                        if(data=='success'){
                            snackbar('Success !');
                            this.backToGrid(true); 
                            this.gridPaging('');
                        }
                        else{
                            snackbar(data);
                        }
                   
                })
                 .catch((error) => {
                    App.stopPageLoading();
                    swal(error.response.data);
                });
            },
            formBatal:function(item){

                let self = this;
                let bb = bootbox.prompt({ 
                    required: true,
                    size: "big",
                    title: "Batalkan, tuliskan alasan?",
                    inputType:'textarea',
                    callback: function(result){ 
                        console.log(result);
                        if(result != null){

                            App.startPageLoading({animate:!0});

                            let form_data = new FormData();
                            let url_prxy = site_url() + `transaksi/setBatal/${item.id_pelanggan}/${item.nama}`;
                            form_data.append('alasan',result);
                            // 
                            axios({
                                method:'post',
                                url: url_prxy,
                                data:form_data,
                                headers: {'Content-Type':  "multipart/form-data; charset=utf-8; boundary=" + Math.random().toString().substr(2)  },
                            })
                            .then((response) => { 
                                // this.customModal('INFO',response.data);
                                if(response.data.success == 1){
                                    self.goPaging(self.pager.current_page);
                                }
                                
                                
                                App.stopPageLoading();
                            })
                            .catch((error) => {
                                App.stopPageLoading();
                                try{swal(error.response.data);}catch(e){}
                            });    
                
                        }else if (result === '') {
                            bb.find('.bootbox-input').addClass('input-validation-error');
                            return false;
                        }
                    }
                });
               
            },
            formCheck:function(item){
                let self = this;
                
                if(confirm('Jadikan Pelanggan ?')){
                    App.startPageLoading({animate:!0});

                    let form_data = new FormData();
                    let url_prxy = site_url() + `transaksi/setPelanggan/${item.id_pelanggan}/${item.nama}`;
                    // form_data.append('jenis',jenis);
                    // 
                    let self = this;
                    axios({
                        method:'post',
                        url: url_prxy,
                        data:form_data,
                        headers: {'Content-Type':  "multipart/form-data; charset=utf-8; boundary=" + Math.random().toString().substr(2)  },
                    })
                    .then((response) => { 
                        // this.customModal('INFO',response.data);
                        this.gridPaging(this.pager.current_page);
                        
                        App.stopPageLoading();
                    })
                    .catch((error) => {
                        App.stopPageLoading();
                        try{swal(error.response.data);}catch(e){}
                    });    
                }
            },
            formHistory:function(item){
                App.startPageLoading({animate:!0});

                let form_data = new FormData();
                let url_prxy = site_url() + `transaksi/history/${item.id_pelanggan}/${item.nama}`;
                // form_data.append('jenis',jenis);
                // 
                let self = this;
                axios({
                    method:'post',
                    url: url_prxy,
                    data:form_data,
                    headers: {'Content-Type':  "multipart/form-data; charset=utf-8; boundary=" + Math.random().toString().substr(2)  },
                })
                .then((response) => { 
                    this.customModal('HISTORY',response.data);
                    App.stopPageLoading();
                })
                .catch((error) => {
                    App.stopPageLoading();
                    try{swal(error.response.data);}catch(e){}
                });
            },
            customModal(title,content){
                $('#custom_modal .title').html(title);
                $('#custom_modal .content').html(content);
                $('#custom_modal').modal("show");
            }
        }
    });
});