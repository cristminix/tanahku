<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<office:document-content xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0" xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0" xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0" xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0" xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0" xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0" xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0" xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0" xmlns:math="http://www.w3.org/1998/Math/MathML" xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0" xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0" xmlns:ooo="http://openoffice.org/2004/office" xmlns:ooow="http://openoffice.org/2004/writer" xmlns:oooc="http://openoffice.org/2004/calc" xmlns:dom="http://www.w3.org/2001/xml-events" xmlns:xforms="http://www.w3.org/2002/xforms" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:rpt="http://openoffice.org/2005/report" xmlns:of="urn:oasis:names:tc:opendocument:xmlns:of:1.2" xmlns:xhtml="http://www.w3.org/1999/xhtml" xmlns:grddl="http://www.w3.org/2003/g/data-view#" xmlns:tableooo="http://openoffice.org/2009/table" xmlns:textooo="http://openoffice.org/2013/office" xmlns:field="urn:openoffice:names:experimental:ooo-ms-interop:xmlns:field:1.0" office:version="1.2">
  <office:scripts/>
  <office:font-face-decls>
    <style:font-face style:name="Lucida Sans1" svg:font-family="'Lucida Sans'" style:font-family-generic="swiss"/>
    <style:font-face style:name="Arial1" svg:font-family="Arial" style:font-family-generic="roman" style:font-pitch="variable"/>
    <style:font-face style:name="Tahoma" svg:font-family="Tahoma" style:font-family-generic="roman" style:font-pitch="variable"/>
    <style:font-face style:name="Times New Roman" svg:font-family="'Times New Roman'" style:font-family-generic="roman" style:font-pitch="variable"/>
    <style:font-face style:name="Arial" svg:font-family="Arial" style:font-family-generic="swiss" style:font-pitch="variable"/>
    <style:font-face style:name="Arial2" svg:font-family="Arial" style:font-family-generic="system" style:font-pitch="variable"/>
    <style:font-face style:name="Lucida Sans" svg:font-family="'Lucida Sans'" style:font-family-generic="system" style:font-pitch="variable"/>
    <style:font-face style:name="Microsoft YaHei" svg:font-family="'Microsoft YaHei'" style:font-family-generic="system" style:font-pitch="variable"/>
    <style:font-face style:name="Tahoma1" svg:font-family="Tahoma" style:font-family-generic="system" style:font-pitch="variable"/>
    <style:font-face style:name="Times New Roman1" svg:font-family="'Times New Roman'" style:font-family-generic="system" style:font-pitch="variable"/>
  </office:font-face-decls>
  <office:automatic-styles>
    <style:style style:name="Table1" style:family="table">
      <style:table-properties style:width="6.3847in" fo:margin-left="-0.075in" fo:margin-top="0in" fo:margin-bottom="0in" table:align="left" style:writing-mode="lr-tb"/>
    </style:style>
    <style:style style:name="Table1.A" style:family="table-column">
      <style:table-column-properties style:column-width="1.5125in"/>
    </style:style>
    <style:style style:name="Table1.B" style:family="table-column">
      <style:table-column-properties style:column-width="1.9368in"/>
    </style:style>
    <style:style style:name="Table1.C" style:family="table-column">
      <style:table-column-properties style:column-width="2.9354in"/>
    </style:style>
    <style:style style:name="Table1.1" style:family="table-row">
      <style:table-row-properties style:keep-together="true" fo:keep-together="auto"/>
    </style:style>
    <style:style style:name="Table1.A1" style:family="table-cell">
      <style:table-cell-properties fo:padding-left="0.075in" fo:padding-right="0.075in" fo:padding-top="0in" fo:padding-bottom="0in" fo:border="none"/>
    </style:style>
    <style:style style:name="Table2" style:family="table">
      <style:table-properties style:width="7.0104in" fo:margin-left="0.175in" fo:margin-right="-0.0792in" fo:margin-top="0in" fo:margin-bottom="0in" table:align="margins" style:writing-mode="lr-tb"/>
    </style:style>
    <style:style style:name="Table2.A" style:family="table-column">
      <style:table-column-properties style:column-width="2.3854in" style:rel-column-width="3435*"/>
    </style:style>
    <style:style style:name="Table2.B" style:family="table-column">
      <style:table-column-properties style:column-width="2.2292in" style:rel-column-width="3210*"/>
    </style:style>
    <style:style style:name="Table2.C" style:family="table-column">
      <style:table-column-properties style:column-width="2.3958in" style:rel-column-width="3450*"/>
    </style:style>
    <style:style style:name="Table2.1" style:family="table-row">
      <style:table-row-properties style:min-row-height="1.1875in" style:keep-together="true" fo:keep-together="auto"/>
    </style:style>
    <style:style style:name="Table2.A1" style:family="table-cell">
      <style:table-cell-properties fo:padding-left="0.075in" fo:padding-right="0.075in" fo:padding-top="0in" fo:padding-bottom="0in" fo:border="none"/>
    </style:style>
    <style:style style:name="Table2.C2" style:family="table-cell">
      <style:table-cell-properties fo:padding="0in" fo:border="none"/>
    </style:style>
    <style:style style:name="P1" style:family="paragraph" style:parent-style-name="Standard">
      <style:paragraph-properties>
        <style:tab-stops>
          <style:tab-stop style:position="0.3752in"/>
          <style:tab-stop style:position="1.25in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="P2" style:family="paragraph" style:parent-style-name="Standard">
      <style:paragraph-properties>
        <style:tab-stops>
          <style:tab-stop style:position="3in"/>
          <style:tab-stop style:position="4.4791in"/>
        </style:tab-stops>
      </style:paragraph-properties>
    </style:style>
    <style:style style:name="P3" style:family="paragraph" style:parent-style-name="Standard">
      <style:paragraph-properties>
        <style:tab-stops>
          <style:tab-stop style:position="0in"/>
          <style:tab-stop style:position="1.25in"/>
        </style:tab-stops>
      </style:paragraph-properties>
    </style:style>
    <style:style style:name="P4" style:family="paragraph" style:parent-style-name="Standard">
      <style:paragraph-properties fo:margin-left="3in" fo:margin-right="0in" fo:text-indent="-3in" style:auto-text-indent="false">
        <style:tab-stops>
          <style:tab-stop style:position="3in"/>
          <style:tab-stop style:position="4.4791in"/>
        </style:tab-stops>
      </style:paragraph-properties>
    </style:style>
    <style:style style:name="P5" style:family="paragraph" style:parent-style-name="Standard">
      <style:paragraph-properties fo:margin-left="3in" fo:margin-right="0in" fo:text-indent="-3in" style:auto-text-indent="false">
        <style:tab-stops>
          <style:tab-stop style:position="3in"/>
          <style:tab-stop style:position="4.4791in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="P6" style:family="paragraph" style:parent-style-name="Standard">
      <style:paragraph-properties fo:margin-left="0.3752in" fo:margin-right="0in" fo:text-indent="-0.3752in" style:auto-text-indent="false">
        <style:tab-stops>
          <style:tab-stop style:position="0.3752in"/>
          <style:tab-stop style:position="1.25in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="P7" style:family="paragraph" style:parent-style-name="Standard">
      <style:paragraph-properties fo:margin-left="0.3752in" fo:margin-right="0in" fo:text-indent="0in" style:auto-text-indent="false">
        <style:tab-stops>
          <style:tab-stop style:position="0.3752in"/>
          <style:tab-stop style:position="0.5909in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="P8" style:family="paragraph" style:parent-style-name="Standard">
      <style:paragraph-properties fo:margin-left="0.25in" fo:margin-right="0in" fo:text-indent="0in" style:auto-text-indent="false">
        <style:tab-stops>
          <style:tab-stop style:position="0.3752in"/>
          <style:tab-stop style:position="1.25in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="P9" style:family="paragraph" style:parent-style-name="Standard">
      <style:paragraph-properties fo:margin-left="0.25in" fo:margin-right="0in" fo:text-indent="0in" style:auto-text-indent="false">
        <style:tab-stops>
          <style:tab-stop style:position="0.3752in"/>
          <style:tab-stop style:position="1.25in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" fo:font-style="italic" style:font-size-asian="11pt" style:font-style-asian="italic" style:font-name-complex="Arial2" style:font-size-complex="11pt" style:font-style-complex="italic"/>
    </style:style>
    <style:style style:name="P10" style:family="paragraph" style:parent-style-name="Standard" style:master-page-name="Standard">
      <style:paragraph-properties fo:margin-left="3in" fo:margin-right="0in" fo:text-align="end" style:justify-single-word="false" fo:text-indent="-3in" style:auto-text-indent="false" style:page-number="auto">
        <style:tab-stops>
          <style:tab-stop style:position="3in"/>
          <style:tab-stop style:position="4.4791in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" fo:font-style="italic" style:font-size-asian="11pt" style:font-style-asian="italic" style:font-name-complex="Arial2" style:font-size-complex="11pt" style:font-style-complex="italic"/>
    </style:style>
    <style:style style:name="P11" style:family="paragraph" style:parent-style-name="Standard">
      <style:paragraph-properties fo:margin-left="3in" fo:margin-right="0in" fo:text-indent="-3in" style:auto-text-indent="false">
        <style:tab-stops>
          <style:tab-stop style:position="3in"/>
          <style:tab-stop style:position="4.4791in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="P12" style:family="paragraph" style:parent-style-name="Standard">
      <style:paragraph-properties fo:margin-left="3in" fo:margin-right="0in" fo:text-indent="-3in" style:auto-text-indent="false">
        <style:tab-stops>
          <style:tab-stop style:position="3in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="P13" style:family="paragraph" style:parent-style-name="Standard">
      <style:paragraph-properties fo:margin-left="0in" fo:margin-right="0in" fo:text-indent="0.3752in" style:auto-text-indent="false">
        <style:tab-stops>
          <style:tab-stop style:position="3in"/>
          <style:tab-stop style:position="4.4791in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="P14" style:family="paragraph" style:parent-style-name="Standard">
      <style:paragraph-properties>
        <style:tab-stops>
          <style:tab-stop style:position="3in"/>
          <style:tab-stop style:position="4.4791in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="P15" style:family="paragraph" style:parent-style-name="Standard" style:list-style-name="WWNum13">
      <style:paragraph-properties>
        <style:tab-stops>
          <style:tab-stop style:position="0.3752in"/>
          <style:tab-stop style:position="0.3937in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="P16" style:family="paragraph" style:parent-style-name="Standard" style:list-style-name="WWNum13">
      <style:paragraph-properties>
        <style:tab-stops>
          <style:tab-stop style:position="0.3752in"/>
          <style:tab-stop style:position="0.5909in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="P17" style:family="paragraph" style:parent-style-name="Standard">
      <style:paragraph-properties>
        <style:tab-stops>
          <style:tab-stop style:position="0.3752in"/>
          <style:tab-stop style:position="1.25in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="P18" style:family="paragraph" style:parent-style-name="Standard" style:list-style-name="WWNum14">
      <style:paragraph-properties>
        <style:tab-stops>
          <style:tab-stop style:position="0.3752in"/>
          <style:tab-stop style:position="1.25in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="P19" style:family="paragraph" style:parent-style-name="Standard">
      <style:paragraph-properties fo:text-align="center" style:justify-single-word="false">
        <style:tab-stops>
          <style:tab-stop style:position="0.3752in"/>
          <style:tab-stop style:position="1.25in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="P20" style:family="paragraph" style:parent-style-name="Standard">
      <style:paragraph-properties fo:text-align="start" style:justify-single-word="false">
        <style:tab-stops>
          <style:tab-stop style:position="0.3752in"/>
          <style:tab-stop style:position="1.25in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="P21" style:family="paragraph" style:parent-style-name="Standard">
      <style:paragraph-properties fo:margin-left="0.3752in" fo:margin-right="0in" fo:text-indent="-0.3752in" style:auto-text-indent="false">
        <style:tab-stops>
          <style:tab-stop style:position="0.3752in"/>
          <style:tab-stop style:position="1.25in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="P22" style:family="paragraph" style:parent-style-name="Standard">
      <style:paragraph-properties fo:margin-left="0.3752in" fo:margin-right="0in" fo:text-indent="-0.3752in" style:auto-text-indent="false">
        <style:tab-stops>
          <style:tab-stop style:position="0.3752in"/>
          <style:tab-stop style:position="1.5in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="P23" style:family="paragraph" style:parent-style-name="Standard">
      <style:paragraph-properties fo:margin-left="0.3752in" fo:margin-right="0in" fo:text-indent="-0.3752in" style:auto-text-indent="false">
        <style:tab-stops>
          <style:tab-stop style:position="0.3752in"/>
          <style:tab-stop style:position="1.1252in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="P24" style:family="paragraph" style:parent-style-name="Standard" style:list-style-name="WWNum13">
      <style:paragraph-properties fo:margin-left="0.5909in" fo:margin-right="0in" fo:text-indent="-0.2161in" style:auto-text-indent="false">
        <style:tab-stops>
          <style:tab-stop style:position="0.4925in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="P25" style:family="paragraph" style:parent-style-name="Standard" style:list-style-name="WWNum14">
      <style:paragraph-properties fo:margin-left="0.4925in" fo:margin-right="0in" fo:text-indent="-0.2425in" style:auto-text-indent="false">
        <style:tab-stops>
          <style:tab-stop style:position="1.25in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="P26" style:family="paragraph" style:parent-style-name="Standard">
      <style:paragraph-properties fo:margin-left="0.05in" fo:margin-right="0in" fo:text-align="center" style:justify-single-word="false" fo:text-indent="0in" style:auto-text-indent="false">
        <style:tab-stops>
          <style:tab-stop style:position="0.3752in"/>
          <style:tab-stop style:position="1.25in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="8pt" fo:font-style="italic" style:font-size-asian="8pt" style:font-style-asian="italic" style:font-name-complex="Arial2" style:font-size-complex="8pt" style:font-style-complex="italic"/>
    </style:style>
    <style:style style:name="P27" style:family="paragraph" style:parent-style-name="Standard">
      <style:paragraph-properties fo:margin-left="0.25in" fo:margin-right="0in" fo:text-indent="0in" style:auto-text-indent="false">
        <style:tab-stops>
          <style:tab-stop style:position="0.3752in"/>
          <style:tab-stop style:position="1.25in"/>
        </style:tab-stops>
      </style:paragraph-properties>
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="T1" style:family="text">
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="T2" style:family="text">
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" fo:font-weight="bold" style:font-size-asian="11pt" style:font-weight-asian="bold" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="T3" style:family="text">
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" fo:font-weight="bold" style:font-size-asian="11pt" style:font-weight-asian="bold" style:font-name-complex="Arial2" style:font-size-complex="11pt" style:font-weight-complex="bold"/>
    </style:style>
    <style:style style:name="T4" style:family="text">
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:text-underline-style="solid" style:text-underline-width="auto" style:text-underline-color="font-color" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
    <style:style style:name="T5" style:family="text">
      <style:text-properties style:font-name="Arial1" fo:font-size="11pt" style:text-underline-style="solid" style:text-underline-width="auto" style:text-underline-color="font-color" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt" style:font-weight-complex="bold"/>
    </style:style>
    <style:style style:name="T6" style:family="text">
      <style:text-properties style:text-position="super 58%" style:font-name="Arial1" fo:font-size="11pt" style:font-size-asian="11pt" style:font-name-complex="Arial2" style:font-size-complex="11pt"/>
    </style:style>
  </office:automatic-styles>
  <office:body>
	<xsl:for-each select="root/row">
    <office:text text:use-soft-page-breaks="true">
      <text:sequence-decls>
        <text:sequence-decl text:display-outline-level="0" text:name="Illustration"/>
        <text:sequence-decl text:display-outline-level="0" text:name="Table"/>
        <text:sequence-decl text:display-outline-level="0" text:name="Text"/>
        <text:sequence-decl text:display-outline-level="0" text:name="Drawing"/>
      </text:sequence-decls>
      <text:p text:style-name="P10">Lampiran- 1</text:p>
      <text:p text:style-name="P5"><text:tab/><text:tab/>Kepada </text:p>
      <text:p text:style-name="P5"/>
      <text:p text:style-name="P12"><text:tab/><text:tab/><text:tab/>Yth.<text:tab/>Kepala Kantor Pertanahan</text:p>
      <text:p text:style-name="P4">
        <text:span text:style-name="T1"><text:tab/><text:tab/>Kabupaten </text:span>
        <text:span text:style-name="T3">Brebes</text:span>
      </text:p>
      <text:p text:style-name="P5"><text:tab/><text:tab/><text:tab/>di .</text:p>
      <text:p text:style-name="P4">
        <text:span text:style-name="T1">
          <text:tab/>
          <text:tab/>
          <text:tab/>
          <text:tab/>
        </text:span>
        <text:span text:style-name="T5">BREBES</text:span>
      </text:p>
      <text:p text:style-name="P5">
        <text:tab/>
      </text:p>
      <text:p text:style-name="P5"/>
      <text:p text:style-name="P5">Dengan hormat,</text:p>
      <text:p text:style-name="P5"/>
      <text:p text:style-name="P5">Yang bertanda tangan di bawah ini :</text:p>
      <text:p text:style-name="P5"/>
      <table:table table:name="Table1" table:style-name="Table1">
        <table:table-column table:style-name="Table1.A"/>
        <table:table-column table:style-name="Table1.B"/>
        <table:table-column table:style-name="Table1.C"/>
        <table:table-row table:style-name="Table1.1">
          <table:table-cell table:style-name="Table1.A1" office:value-type="string">
            <text:p text:style-name="P13">Nama</text:p>
            <text:p text:style-name="P13">Umur</text:p>
            <text:p text:style-name="P13">Pekerjaan</text:p>
            <text:p text:style-name="P13">Nomor KTP</text:p>
          </table:table-cell>
          <table:table-cell table:style-name="Table1.A1" office:value-type="string">
            <text:p text:style-name="P14">: 1.<xsl:value-of select="nama_1"/></text:p>
            <text:p text:style-name="P2">
              <text:span text:style-name="T1">: </text:span>
              <text:span text:style-name="T2"><xsl:value-of select="umur_1"/></text:span>
              <text:span text:style-name="T1"> Th</text:span>
            </text:p>
            <text:p text:style-name="P14">: <xsl:value-of select="pekerjaan_1"/></text:p>
            <text:p text:style-name="P14">: <xsl:value-of select="nomor_ktp_1"/></text:p>
          </table:table-cell>
          <table:table-cell table:style-name="Table1.A1" office:value-type="string">
            <text:p text:style-name="P14">: 2.<xsl:value-of select="nama_2"/></text:p>
            <text:p text:style-name="P2">
              <text:span text:style-name="T1">: </text:span>
              <text:span text:style-name="T2"><xsl:value-of select="umur_2"/></text:span>
              <text:span text:style-name="T1"> Th</text:span>
            </text:p>
            <text:p text:style-name="P14">: <xsl:value-of select="pekerjaan_2"/></text:p>
            <text:p text:style-name="P14">: <xsl:value-of select="nomor_ktp_2"/></text:p>
          </table:table-cell>
        </table:table-row>
        <table:table-row table:style-name="Table1.1">
          <table:table-cell table:style-name="Table1.A1" office:value-type="string">
            <text:p text:style-name="P13">Alamat</text:p>
          </table:table-cell>
          <table:table-cell table:style-name="Table1.A1" table:number-columns-spanned="2" office:value-type="string">
            <text:p text:style-name="P14">: <xsl:value-of select="alamat"/> , RT <xsl:value-of select="rt"/> RW <xsl:value-of select="rw"/>, Kecamatan Bulakamba Kabupaten Brebes</text:p>
          </table:table-cell>
          <table:covered-table-cell/>
        </table:table-row>
      </table:table>
      <text:p text:style-name="P6"/>
      <text:p text:style-name="P6">Dalam hal ini bertindak untuk dan atas nama diri sendiri/selaku kuasa dari :</text:p>
      <text:p text:style-name="P6"/>
      <text:p text:style-name="P22"><text:tab/>Nama<text:tab/>: <xsl:value-of select="nama_kuasa"/></text:p>
      <text:p text:style-name="P23"><text:tab/>Umur<text:tab/><text:tab/>: <xsl:value-of select="umur_kuasa"/></text:p>
      <text:p text:style-name="P23"><text:tab/>Pekerjaan<text:tab/><text:tab/>: <xsl:value-of select="pekerjaan_kuasa"/></text:p>
      <text:p text:style-name="P23"><text:tab/>Nomor KTP<text:tab/>: <xsl:value-of select="nomor_ktp_kuasa"/> <text:s/></text:p>
      <text:p text:style-name="P6"><text:tab/>Alamat<text:tab/><text:tab/>: <xsl:value-of select="alamat_kuasa"/></text:p>
      <text:p text:style-name="P6"/>
      <text:p text:style-name="P6">Berdasarkan surat kuasa Nomor <xsl:value-of select="nomor_surat_kuasa"/> tanggal <xsl:value-of select="tgl_surat_kuasa"/></text:p>
      <text:p text:style-name="P6"/>
      <text:p text:style-name="P6">Dengan ini mengajukan permohonan :</text:p>
      <text:p text:style-name="P6"/>
      <xsl:for-each select="list_permohonan/p">
      <text:p text:style-name="P6"><text:tab/><xsl:value-of select="."/></text:p>
      </xsl:for-each>
      <text:p text:style-name="P6"/>
      <text:p text:style-name="P3">
        <text:span text:style-name="T1">Atas sebidang tanah <xsl:value-of select="jenis_tanah"/> C. No <text:s/><xsl:value-of select="no_c"/> Persil <xsl:value-of select="persil"/> Klas <xsl:value-of select="kelas"/> <text:s/>Luas </text:span>
        <text:span text:style-name="T4">+</text:span>
        <text:span text:style-name="T1"><xsl:value-of select="luas_tanah"/> M</text:span>
        <text:span text:style-name="T6">2 <text:s/></text:span>
        <text:span text:style-name="T1">Yang terletak di <xsl:value-of select="letak_tanah"/> Desa Grinting Kacamatan Bulakamba Kabupaten Brebes<text:tab/><text:tab/><text:tab/></text:span>
      </text:p>
      <text:p text:style-name="P6">Untuk melengkapi permohonan dimaksud, bersama ini kami lampirkan :</text:p>
      <text:p text:style-name="P6"/>
      <text:list xml:id="list1686005304136295957" text:style-name="WWNum13">
        <xsl:for-each select="list_lampiran/p">
          <text:list-item>
          <text:p text:style-name="P15"><xsl:value-of select="."/></text:p>
        </text:list-item>
        </xsl:for-each>  
      </text:list>
      <text:p text:style-name="P7"/>
      <text:p text:style-name="P1">Demikian permohonan kami, dan kami menjamin :</text:p>
      <text:p text:style-name="P1"/>
      <text:list xml:id="list5602669125325196100" text:style-name="WWNum14">
        <text:list-item>
          <text:p text:style-name="P18">Bahwa bidang tanah tersebut benar – benar saya kuasai secara fisik dan tidak dalam sengketa.</text:p>
        </text:list-item>
        <text:list-item>
          <text:p text:style-name="P25">Bahwa semua lampiran tersebut benar adanya, apabila terdapat ketidak benaran yang berakibat Perdata dan atau Pidana menjadi tanggung jawab saya dan permohonan tersebut batal dengan sendirinya.</text:p>
        </text:list-item>
      </text:list>
      <text:p text:style-name="P8"/>
      <table:table table:name="Table2" table:style-name="Table2">
        <table:table-column table:style-name="Table2.A"/>
        <table:table-column table:style-name="Table2.B"/>
        <table:table-column table:style-name="Table2.C"/>
        <table:table-row table:style-name="Table2.1">
          <table:table-cell table:style-name="Table2.A1" office:value-type="string">
            <text:p text:style-name="P1"/>
          </table:table-cell>
          <table:table-cell table:style-name="Table2.A1" table:number-columns-spanned="2" office:value-type="string">
            <text:p text:style-name="P19">Grinting, <xsl:value-of select="dt_surat_kuasa"/></text:p>
            <text:p text:style-name="P19">Hormat kami,</text:p>
            <text:p text:style-name="P8"/>
            <text:p text:style-name="P8"/>
            <text:p text:style-name="P26">Materai Rp 6.000</text:p>
            <text:p text:style-name="P1"/>
          </table:table-cell>
          <table:covered-table-cell/>
        </table:table-row>
        <table:table-row table:style-name="Table2.1">
          <table:table-cell table:style-name="Table2.A1" office:value-type="string">
            <text:p text:style-name="P1"/>
          </table:table-cell>
          <table:table-cell table:style-name="Table2.A1" office:value-type="string">
            <text:p text:style-name="P20">1.<xsl:value-of select="nama_1"/></text:p>
          </table:table-cell>
          <table:table-cell table:style-name="Table2.C2" office:value-type="string">
            <text:p text:style-name="P20">2.<xsl:value-of select="nama_2"/></text:p>
          </table:table-cell>
        </table:table-row>
      </table:table>
      <text:p text:style-name="P8"/>
      <text:p text:style-name="P8">
        <text:tab/>
        <text:tab/>
        <text:tab/>
        <text:tab/>
        <text:tab/>
        <text:tab/>
        <text:tab/>
        <text:tab/>
        <text:tab/>
      </text:p>
      <text:p text:style-name="P9"/>
      <text:p text:style-name="P8"/>
      <text:p text:style-name="P8">
        <text:soft-page-break/>
        <text:tab/>
        <text:tab/>
        <text:tab/>
        <text:tab/>
        <text:tab/>
        <text:tab/>
        <text:tab/>
        <text:s text:c="9"/>
        <text:tab/>
      </text:p>
      <text:p text:style-name="Standard"/>
    </office:text>
	</xsl:for-each>
  </office:body>
</office:document-content>
</xsl:template>
</xsl:stylesheet>
