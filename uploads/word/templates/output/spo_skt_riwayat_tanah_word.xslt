<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<w:document xmlns:ve="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml">
  <w:body>
	<xsl:for-each select="root/row">
    <w:p w:rsidR="006F0EBA" w:rsidRPr="00420C0A" w:rsidRDefault="006F0EBA" w:rsidP="006F0EBA">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="28"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="28"/>
        </w:rPr>
        <w:t>SURAT KETERANGAN</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="006F0EBA" w:rsidRPr="00420C0A" w:rsidRDefault="006F0EBA" w:rsidP="006F0EBA">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve">Nomor : <xsl:value-of select="nomor_surat"/></w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t></w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"></w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="006F0EBA" w:rsidRPr="00420C0A" w:rsidRDefault="006F0EBA" w:rsidP="006F0EBA">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="28"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="006F0EBA" w:rsidRPr="00420C0A" w:rsidRDefault="006F0EBA" w:rsidP="006F0EBA">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="28"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="006F0EBA" w:rsidRPr="00420C0A" w:rsidRDefault="006F0EBA" w:rsidP="006F0EBA">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="540"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
        <w:t>Kepala</w:t>
      </w:r>
      <w:r w:rsidR="004A20C6">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Desa</w:t>
      </w:r>
      <w:r w:rsidR="004A20C6">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t>Grinting</w:t>
      </w:r>
      <w:r w:rsidR="004A20C6">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Kecamatan</w:t>
      </w:r>
      <w:r w:rsidR="004A20C6">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t>Bulakamba</w:t>
      </w:r>
      <w:r w:rsidR="004A20C6">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Kabupaten</w:t>
      </w:r>
      <w:r w:rsidR="004A20C6">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t>Brebes</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="006F0EBA" w:rsidRDefault="006F0EBA" w:rsidP="006F0EBA">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Dengan</w:t>
      </w:r>
      <w:r w:rsidR="004A20C6">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>ini</w:t>
      </w:r>
      <w:r w:rsidR="004A20C6">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>menerangkan</w:t>
      </w:r>
      <w:r w:rsidR="004A20C6">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>bahwa</w:t>
      </w:r>
      <w:r w:rsidR="004A20C6">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>sebidang</w:t>
      </w:r>
      <w:r w:rsidR="004A20C6">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>tanah</w:t>
      </w:r>
      <w:r w:rsidR="004A20C6">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>terletak di:</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00C544FB" w:rsidRPr="00420C0A" w:rsidRDefault="00C544FB" w:rsidP="006F0EBA">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblStyle w:val="TableGrid"/>
        <w:tblW w:w="0" w:type="auto"/>
        <w:tblBorders>
          <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="2898"/>
        <w:gridCol w:w="7380"/>
      </w:tblGrid>
      <w:tr w:rsidR="00C544FB" w:rsidTr="00062B80">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2898" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:firstLine="540"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Jalan</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:firstLine="540"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>RT/RW</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:firstLine="540"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Desa/Kelurahan</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:firstLine="540"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Kecamatan</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:firstLine="540"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>NIB</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:firstLine="540"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Status Tanah</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:firstLine="540"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="7380" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="jalan_t"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="rt_t"/>/<xsl:value-of select="rw_t"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:bCs/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve">: </w:t>
            </w:r>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:bCs/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Grinting</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:bCs/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve">: </w:t>
            </w:r>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:bCs/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Bulakamba</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve">: </w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t><xsl:value-of select="nib_t"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00C544FB" w:rsidRPr="00FA22A6" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="1800"/>
              </w:tabs>
              <w:ind w:left="72" w:hanging="72"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve">: </w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"><xsl:value-of select="jenis_tanah"/> </w:t>
            </w:r>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve">C. No </w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"><xsl:value-of select="no_c"/> </w:t>
            </w:r>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve">Persil </w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"><xsl:value-of select="persil"/> </w:t>
            </w:r>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Klas</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"> <xsl:value-of select="kelas"/>  </w:t>
            </w:r>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Luas</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
                <w:u w:val="single"/>
              </w:rPr>
              <w:t>+</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"><xsl:value-of select="luas_tanah"/> </w:t>
            </w:r>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>M</w:t>
            </w:r>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
                <w:vertAlign w:val="superscript"/>
              </w:rPr>
              <w:t xml:space="preserve">2 </w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Yang terletak di</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"> <xsl:value-of select="letak_tanah"/> </w:t>
            </w:r>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Desa</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Grinting ,Kacamatan</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Bulakamba</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Kabupaten</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Brebes</w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
      <w:tr w:rsidR="00C544FB" w:rsidTr="00062B80">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2898" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00C544FB" w:rsidRPr="00BA6AF5" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:firstLine="540"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Dipergunakan</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>untuk</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="7380" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve">:  </w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t><xsl:value-of select="keperluan"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
    </w:tbl>
    <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00C544FB">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="3780"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
    </w:p>
    <w:p w:rsidR="006F0EBA" w:rsidRDefault="00C544FB" w:rsidP="00C544FB">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="3780"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Batas</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> – </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>batas</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>tanah</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00C544FB" w:rsidRPr="00420C0A" w:rsidRDefault="00C544FB" w:rsidP="00C544FB">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="3780"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblStyle w:val="TableGrid"/>
        <w:tblW w:w="0" w:type="auto"/>
        <w:tblBorders>
          <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLayout w:type="fixed"/>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="2898"/>
        <w:gridCol w:w="2610"/>
        <w:gridCol w:w="1754"/>
        <w:gridCol w:w="3016"/>
      </w:tblGrid>
      <w:tr w:rsidR="00C544FB" w:rsidTr="00062B80">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2898" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:firstLine="540"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Sebelah Utara</w:t>
            </w:r>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:tab/>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2610" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00C544FB" w:rsidRPr="00FA22A6" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="1800"/>
              </w:tabs>
              <w:ind w:left="72" w:hanging="72"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>:</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"> <xsl:value-of select="batas_utara"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1754" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00C544FB" w:rsidRPr="00FA22A6" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="1800"/>
              </w:tabs>
              <w:ind w:left="72" w:hanging="72"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Sebelah</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Timur</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:tab/>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="3016" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00C544FB" w:rsidRPr="00FA22A6" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="1800"/>
              </w:tabs>
              <w:ind w:left="72" w:hanging="72"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="batas_timur"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
      <w:tr w:rsidR="00C544FB" w:rsidTr="00062B80">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2898" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00C544FB" w:rsidRPr="00BA6AF5" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:firstLine="540"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Sebelah Selatan</w:t>
            </w:r>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:tab/>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2610" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve">: </w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t><xsl:value-of select="batas_selatan"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1754" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve">Sebelah Barat  </w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="3016" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="batas_barat"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
    </w:tbl>
    <w:p w:rsidR="00C544FB" w:rsidRDefault="006F0EBA" w:rsidP="00C544FB">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
      </w:r>
    </w:p>
    <w:p w:rsidR="006F0EBA" w:rsidRPr="00420C0A" w:rsidRDefault="006F0EBA" w:rsidP="00C544FB">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="90"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Yang tercatat</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>dalam</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Buku</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>leter C Desa</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Nomor</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"><xsl:value-of select="jenis_tanah"/> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve">C. No </w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"><xsl:value-of select="no_c"/> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve">Persil </w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="persil"/></w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Klas</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"><xsl:value-of select="kelas"/> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Luas</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
          <w:u w:val="single"/>
        </w:rPr>
        <w:t>+</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="luas_tanah"/></w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>M</w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
          <w:vertAlign w:val="superscript"/>
        </w:rPr>
        <w:t xml:space="preserve">2 </w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Yang terletak di</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="letak_tanah"/></w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Desa</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> Grinting</w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>,</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Kacamatan</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Bulakamba</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Kabupaten</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Brebes</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="006F0EBA" w:rsidRPr="00420C0A" w:rsidRDefault="00C544FB" w:rsidP="006F0EBA">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidR="006F0EBA" w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>dan</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="006F0EBA" w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>dipergunakan</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="006F0EBA" w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>untuk</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> Pekarangan</w:t>
      </w:r>
      <w:r w:rsidR="006F0EBA" w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>.</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="006F0EBA" w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>adalah</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="006F0EBA" w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>sebagai</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="006F0EBA" w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>berikut :</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="006F0EBA" w:rsidRPr="00420C0A" w:rsidRDefault="006F0EBA" w:rsidP="006F0EBA">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="006F0EBA" w:rsidRDefault="006F0EBA" w:rsidP="00C544FB">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="630"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="630" w:hanging="270"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>1. T</w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>anah tersebut</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>adalah</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>tanah</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>yasan yang telah</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>dipungut</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>hasil</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>bumi</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>turun</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>temurun</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:br/>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>sejak</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>sebelum</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>tanggal 24 September  1960</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="006F0EBA" w:rsidRDefault="006F0EBA" w:rsidP="00C544FB">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="630"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="630" w:hanging="270"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>2. Tanah tersebut</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>belum</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>tidak</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>dalam</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>sengketa, baik</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>sengketa</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> pemilikan</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>,serta</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>tidak</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>dalam</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>sitaan</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="006F0EBA" w:rsidRDefault="006F0EBA" w:rsidP="00C544FB">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="630"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="630" w:hanging="270"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>3. Tanah tersebut</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>belum</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>pernah</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>didaftarkan di Kantor Pertanahan</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>/</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>belum</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>ada</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>tanda</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>b</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>u</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>kti</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Hak</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>berupa</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>sertifikat</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="006F0EBA" w:rsidRDefault="006F0EBA" w:rsidP="00C544FB">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="630"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="630" w:hanging="270"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>4. Tanah tersebut</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>terakhir</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>dimilki</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve">oleh </w:t>
      </w:r>
      <w:r w:rsidR="007616AF">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t><xsl:value-of select="nama_pemilik_terakhir"/></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="006F0EBA" w:rsidRDefault="006F0EBA" w:rsidP="006F0EBA">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="360"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="006F0EBA" w:rsidRDefault="006F0EBA" w:rsidP="006F0EBA">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="360"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Dengan</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>riwayat</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>tanah</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>sebagai</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>berikut :</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="006F0EBA">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="360"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblW w:w="0" w:type="auto"/>
        <w:tblInd w:w="360" w:type="dxa"/>
        <w:tblBorders>
          <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
          <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
          <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
          <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
          <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
          <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="579"/>
        <w:gridCol w:w="1610"/>
        <w:gridCol w:w="1897"/>
        <w:gridCol w:w="1806"/>
        <w:gridCol w:w="1035"/>
        <w:gridCol w:w="1070"/>
        <w:gridCol w:w="1041"/>
        <w:gridCol w:w="1051"/>
      </w:tblGrid>
      <w:tr w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidTr="007616AF">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="587" w:type="dxa"/>
            <w:vMerge w:val="restart"/>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidRDefault="006F0EBA" w:rsidP="007616AF">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>No</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1690" w:type="dxa"/>
            <w:vMerge w:val="restart"/>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidRDefault="006F0EBA" w:rsidP="007616AF">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Tahun</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1910" w:type="dxa"/>
            <w:vMerge w:val="restart"/>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidRDefault="006F0EBA" w:rsidP="007616AF">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Dimiliki/Dikuasai</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1311" w:type="dxa"/>
            <w:vMerge w:val="restart"/>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidRDefault="006F0EBA" w:rsidP="007616AF">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Dasar Perolehan</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="3196" w:type="dxa"/>
            <w:gridSpan w:val="3"/>
          </w:tcPr>
          <w:p w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidRDefault="006F0EBA" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Terbit</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1062" w:type="dxa"/>
            <w:vMerge w:val="restart"/>
            <w:vAlign w:val="center"/>
          </w:tcPr>
          <w:p w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidRDefault="006F0EBA" w:rsidP="007616AF">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t>KET</w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
      <w:tr w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidTr="004A6DAB">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="587" w:type="dxa"/>
            <w:vMerge/>
          </w:tcPr>
          <w:p w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidRDefault="006F0EBA" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1690" w:type="dxa"/>
            <w:vMerge/>
          </w:tcPr>
          <w:p w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidRDefault="006F0EBA" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1910" w:type="dxa"/>
            <w:vMerge/>
          </w:tcPr>
          <w:p w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidRDefault="006F0EBA" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1311" w:type="dxa"/>
            <w:vMerge/>
          </w:tcPr>
          <w:p w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidRDefault="006F0EBA" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1063" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidRDefault="006F0EBA" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>No.C</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1070" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidRDefault="006F0EBA" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>PERSIL</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1063" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidRDefault="006F0EBA" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>KLAS</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1062" w:type="dxa"/>
            <w:vMerge/>
          </w:tcPr>
          <w:p w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidRDefault="006F0EBA" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
          </w:p>
        </w:tc>
      </w:tr>
      <xsl:for-each select="list_riwayat/p">
      <w:tr w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidTr="004A6DAB">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="587" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidRDefault="006F0EBA" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t><xsl:value-of select="nomor"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1690" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidRDefault="006F0EBA" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t><xsl:value-of select="tahun"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1910" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidRDefault="006F0EBA" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t xml:space="preserve"><xsl:value-of select="nama_pemilik"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1311" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidRDefault="006F0EBA" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t><xsl:value-of select="dasar_perolehan"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1063" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidRDefault="006F0EBA" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t><xsl:value-of select="no_c"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1070" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidRDefault="006F0EBA" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t><xsl:value-of select="persil"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1063" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidRDefault="006F0EBA" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t><xsl:value-of select="kelas"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1062" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidRDefault="006F0EBA" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t><xsl:value-of select="keterangan"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
      </xsl:for-each>
    </w:tbl>
    <w:p w:rsidR="006F0EBA" w:rsidRPr="00BA6AF5" w:rsidRDefault="006F0EBA" w:rsidP="006F0EBA">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="360"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="006F0EBA" w:rsidRPr="00420C0A" w:rsidRDefault="006F0EBA" w:rsidP="006F0EBA">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="006F0EBA" w:rsidRDefault="006F0EBA" w:rsidP="00C544FB">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="142"/>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="142"/>
        <w:jc w:val="both"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Demikian</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Surat</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Keterangan</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>ini kami buat</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>dengan</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>sebenar</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> – </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>benarnya</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>dengan</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>penuh</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>tanggung</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>jawab</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>dan</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>apa</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>bila</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>ternyata</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>pernyataan</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>ini</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>tidak</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>benar, kami bersedia</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>d</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>i</w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>tuntut</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>dihadapan</w:t>
      </w:r>
      <w:r w:rsidR="00C544FB">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> pihak yang </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>berwenang</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00C544FB">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="142"/>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="142"/>
        <w:jc w:val="both"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00C544FB">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="142"/>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="142"/>
        <w:jc w:val="both"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblStyle w:val="TableGrid"/>
        <w:tblW w:w="0" w:type="auto"/>
        <w:tblInd w:w="142" w:type="dxa"/>
        <w:tblBorders>
          <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="5143"/>
        <w:gridCol w:w="5164"/>
      </w:tblGrid>
      <w:tr w:rsidR="00C544FB" w:rsidTr="00C544FB">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="5224" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00C544FB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="both"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="5225" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00C544FB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>Grinting,</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t xml:space="preserve">  </w:t>
            </w:r>
            <w:r w:rsidR="007616AF">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t><xsl:value-of select="dt_surat_keterangan"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="007616AF" w:rsidP="00C544FB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t><xsl:value-of select="ttd_jabatan"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00C544FB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00C544FB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00C544FB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="00C544FB" w:rsidP="00C544FB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00C544FB" w:rsidRDefault="007616AF" w:rsidP="00C544FB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:bCs/>
              </w:rPr>
              <w:t><xsl:value-of select="ttd_nama"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
    </w:tbl>
    <w:p w:rsidR="00C544FB" w:rsidRPr="00BA6AF5" w:rsidRDefault="00C544FB" w:rsidP="00C544FB">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="142"/>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="142"/>
        <w:jc w:val="both"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="006F0EBA" w:rsidRPr="00420C0A" w:rsidRDefault="006F0EBA" w:rsidP="006F0EBA">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    
    <w:sectPr w:rsidR="00DF50A9" w:rsidRPr="00D956EA" w:rsidSect="00DF50A9">
      <w:pgSz w:w="12240" w:h="20160" w:code="5"/>
      <w:pgMar w:top="899" w:right="927" w:bottom="1079" w:left="1080" w:header="720" w:footer="720" w:gutter="0"/>
      <w:cols w:space="720"/>
      <w:docGrid w:linePitch="360"/>
    </w:sectPr>
	</xsl:for-each>
  </w:body>
</w:document>
</xsl:template>
</xsl:stylesheet>
