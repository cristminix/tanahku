<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<w:document xmlns:ve="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml">
  <w:body>
	<xsl:for-each select="root/row">
    <w:p w:rsidR="007B4099" w:rsidRPr="00420C0A" w:rsidRDefault="007B4099" w:rsidP="007B4099">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="28"/>
          <w:u w:val="single"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="28"/>
          <w:u w:val="single"/>
        </w:rPr>
        <w:t>SURAT PERNYATAAN</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="28"/>
          <w:u w:val="single"/>
        </w:rPr>
        <w:t xml:space="preserve"> TANAH TIDAK BERSENGKETA</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="007B4099" w:rsidRPr="00420C0A" w:rsidRDefault="007B4099" w:rsidP="007B4099">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="007B4099" w:rsidRPr="00420C0A" w:rsidRDefault="007B4099" w:rsidP="007B4099">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
      </w:r>
    </w:p>
    <w:p w:rsidR="007B4099" w:rsidRPr="00420C0A" w:rsidRDefault="007B4099" w:rsidP="007B4099">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="28"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="007B4099" w:rsidRDefault="007B4099" w:rsidP="007B4099">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="540"/>
          <w:tab w:val="left" w:pos="720"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve">Yang </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>bertanda</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>tangan</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>dibawah</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>ini :</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="007C3B1B" w:rsidRPr="007C3B1B" w:rsidRDefault="007C3B1B" w:rsidP="007B4099">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="540"/>
          <w:tab w:val="left" w:pos="720"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblStyle w:val="TableGrid"/>
        <w:tblW w:w="0" w:type="auto"/>
        <w:tblInd w:w="108" w:type="dxa"/>
        <w:tblBorders>
          <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="1440"/>
        <w:gridCol w:w="8901"/>
      </w:tblGrid>
      <w:tr w:rsidR="007C3B1B" w:rsidTr="007C3B1B">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1440" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="007C3B1B" w:rsidRDefault="007C3B1B" w:rsidP="007B4099">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>Nama</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="007C3B1B" w:rsidRDefault="007C3B1B" w:rsidP="007B4099">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>Umur</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="007C3B1B" w:rsidRDefault="007C3B1B" w:rsidP="007B4099">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>Pekerjaan</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="007C3B1B" w:rsidRDefault="007C3B1B" w:rsidP="007B4099">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>Alamat</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="8901" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="007C3B1B" w:rsidRDefault="007C3B1B" w:rsidP="007B4099">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:tab/>
              <w:t>: <xsl:value-of select="nama"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="007C3B1B" w:rsidRDefault="007C3B1B" w:rsidP="007B4099">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:tab/>
              <w:t>: <xsl:value-of select="umur"/> Th</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="007C3B1B" w:rsidRDefault="007C3B1B" w:rsidP="007C3B1B">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:tab/>
              <w:t>: <xsl:value-of select="pekerjaan"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="007C3B1B" w:rsidRDefault="007C3B1B" w:rsidP="007C3B1B">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:tab/>
              <w:t>: <xsl:value-of select="alamat"/> RT <xsl:value-of select="rt"/> RW <xsl:value-of select="rw"/>, Kecamatan Bulakamba Kabupaten Brebes</w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
    </w:tbl>
    <w:p w:rsidR="007B4099" w:rsidRDefault="007B4099" w:rsidP="007B4099">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="2160"/>
        </w:tabs>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="007B4099" w:rsidRPr="00BA6AF5" w:rsidRDefault="007B4099" w:rsidP="007B4099">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="2160"/>
        </w:tabs>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Dengan</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>ini</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>menyatakan</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>sebenarnya</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>hal</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> – </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>hal</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>sebagai</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>berikut</w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> :</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="007B4099" w:rsidRPr="00BA6AF5" w:rsidRDefault="007B4099" w:rsidP="007B4099">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:spacing w:line="360" w:lineRule="auto"/>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="007B4099" w:rsidRPr="007C3B1B" w:rsidRDefault="007B4099" w:rsidP="007B4099">
      <w:pPr>
        <w:numPr>
          <w:ilvl w:val="0"/>
          <w:numId w:val="25"/>
        </w:numPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:spacing w:line="360" w:lineRule="auto"/>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Bahwa</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B" w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>sebidang</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B" w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>tanah</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B" w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve">No.C /SPOP/SPPT*) </w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B" w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve">No .C </w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B" w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
        </w:rPr>
        <w:t><xsl:value-of select="no_c"/></w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B" w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> Persil </w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B" w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
        </w:rPr>
        <w:t><xsl:value-of select="persil"/></w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B" w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Klas</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B" w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B" w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t><xsl:value-of select="kelas"/></w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B" w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:bCs/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Luas</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B" w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B" w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
        </w:rPr>
        <w:t><xsl:value-of select="luas_tanah"/></w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> M</w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:vertAlign w:val="superscript"/>
        </w:rPr>
        <w:t>2</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B" w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> yang kami peroleh</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B" w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>berdasarkan</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B" w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> A</w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>kta/</w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:strike/>
        </w:rPr>
        <w:t>Segel?Kwitansi</w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> *)</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B" w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:br/>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Tanggal</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B" w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B" w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t xml:space="preserve"><xsl:value-of select="tgl_akta"/> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Nomor</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B" w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve">  </w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B" w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
        </w:rPr>
        <w:t><xsl:value-of select="nomor_akta"/></w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
        </w:rPr>
        <w:t xml:space="preserve">. </w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
        </w:rPr>
        <w:br/>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve">Terletak di </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Desa</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:bCs/>
        </w:rPr>
        <w:t>Grinting</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:bCs/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Kecamatan</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:bCs/>
        </w:rPr>
        <w:t>Bulakamba</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:bCs/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Kabupaten</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:bCs/>
        </w:rPr>
        <w:t>Brebes</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:bCs/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:bCs/>
        </w:rPr>
        <w:br/>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>dengan</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Batas- batas :</w:t>
      </w:r>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblStyle w:val="TableGrid"/>
        <w:tblpPr w:leftFromText="180" w:rightFromText="180" w:vertAnchor="text" w:horzAnchor="page" w:tblpX="1624" w:tblpY="157"/>
        <w:tblW w:w="10098" w:type="dxa"/>
        <w:tblBorders>
          <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLayout w:type="fixed"/>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="2178"/>
        <w:gridCol w:w="2430"/>
        <w:gridCol w:w="1890"/>
        <w:gridCol w:w="3600"/>
      </w:tblGrid>
      <w:tr w:rsidR="007C3B1B" w:rsidRPr="00F52E9C" w:rsidTr="007C3B1B">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2178" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="007C3B1B" w:rsidRPr="00F52E9C" w:rsidRDefault="007C3B1B" w:rsidP="007C3B1B">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00F52E9C">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>Sebelah Utara</w:t>
            </w:r>
            <w:r w:rsidRPr="00F52E9C">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:tab/>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2430" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="007C3B1B" w:rsidRPr="00F52E9C" w:rsidRDefault="007C3B1B" w:rsidP="007C3B1B">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="1800"/>
              </w:tabs>
              <w:ind w:left="72" w:hanging="72"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00F52E9C">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="batas_utara"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1890" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="007C3B1B" w:rsidRPr="00F52E9C" w:rsidRDefault="007C3B1B" w:rsidP="007C3B1B">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="1800"/>
              </w:tabs>
              <w:ind w:left="72" w:hanging="72"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00F52E9C">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>Sebelah Timur</w:t>
            </w:r>
            <w:r w:rsidRPr="00F52E9C">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:tab/>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="3600" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="007C3B1B" w:rsidRPr="00F52E9C" w:rsidRDefault="007C3B1B" w:rsidP="007C3B1B">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="1800"/>
              </w:tabs>
              <w:ind w:left="72" w:hanging="72"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00F52E9C">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="batas_timur"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
      <w:tr w:rsidR="007C3B1B" w:rsidRPr="00F52E9C" w:rsidTr="007C3B1B">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2178" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="007C3B1B" w:rsidRPr="00F52E9C" w:rsidRDefault="007C3B1B" w:rsidP="007C3B1B">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00F52E9C">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>Sebelah Selatan</w:t>
            </w:r>
            <w:r w:rsidRPr="00F52E9C">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:tab/>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2430" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="007C3B1B" w:rsidRPr="00F52E9C" w:rsidRDefault="007C3B1B" w:rsidP="007C3B1B">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00F52E9C">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="batas_selatan"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1890" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="007C3B1B" w:rsidRPr="00F52E9C" w:rsidRDefault="007C3B1B" w:rsidP="007C3B1B">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00F52E9C">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t xml:space="preserve">Sebelah Barat  </w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="3600" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="007C3B1B" w:rsidRPr="00F52E9C" w:rsidRDefault="007C3B1B" w:rsidP="007C3B1B">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00F52E9C">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="batas_barat"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
    </w:tbl>
    <w:p w:rsidR="007C3B1B" w:rsidRDefault="007C3B1B" w:rsidP="007C3B1B">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="007C3B1B" w:rsidRDefault="007B4099" w:rsidP="007C3B1B">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
      </w:r>
    </w:p>
    <w:p w:rsidR="007B4099" w:rsidRPr="007C3B1B" w:rsidRDefault="007B4099" w:rsidP="007C3B1B">
      <w:pPr>
        <w:pStyle w:val="ListParagraph"/>
        <w:numPr>
          <w:ilvl w:val="0"/>
          <w:numId w:val="25"/>
        </w:numPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Bahwa</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>fisik</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>tanah</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>ini</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>secara</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>nyata kami kuasai</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>dan kami pergunakan sendiri</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="007B4099" w:rsidRPr="007C3B1B" w:rsidRDefault="007B4099" w:rsidP="007B4099">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="007B4099" w:rsidRPr="007C3B1B" w:rsidRDefault="007B4099" w:rsidP="007B4099">
      <w:pPr>
        <w:numPr>
          <w:ilvl w:val="0"/>
          <w:numId w:val="25"/>
        </w:numPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="360" w:lineRule="auto"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Bahwa</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>tanah</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>ini</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>tidak</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
        </w:rPr>
        <w:t>dalam sengketa</w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> (baik</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>mengenai</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>kepemilikan</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Maupun</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>batas</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>tanah)</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="007B4099" w:rsidRPr="007C3B1B" w:rsidRDefault="007B4099" w:rsidP="007B4099">
      <w:pPr>
        <w:numPr>
          <w:ilvl w:val="0"/>
          <w:numId w:val="25"/>
        </w:numPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="360" w:lineRule="auto"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Bahwa</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>tanah</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>ini</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>tidak</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>dijaminkan</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>hutang</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>kepada</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>siapapun</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>juga.</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="007B4099" w:rsidRPr="007C3B1B" w:rsidRDefault="007B4099" w:rsidP="007B4099">
      <w:pPr>
        <w:numPr>
          <w:ilvl w:val="0"/>
          <w:numId w:val="25"/>
        </w:numPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="360" w:lineRule="auto"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Bahwa</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>tanah</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>ini</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>bukan</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>merupakan</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>harta</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>warisan yang belum</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>terbagi</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="007B4099" w:rsidRPr="007C3B1B" w:rsidRDefault="007B4099" w:rsidP="007B4099">
      <w:pPr>
        <w:numPr>
          <w:ilvl w:val="0"/>
          <w:numId w:val="25"/>
        </w:numPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="360" w:lineRule="auto"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Bahwa</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>apabila kami memalsukan</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>isi</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Surat</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Pernyataan</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>ini kami bersedia</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>dituntut</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>dimuka hakim baik</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>secara</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>pribadi</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>maupun</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>secara</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>perdata, karena</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>memberikan</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>keterangan</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>palsu</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="007C3B1B" w:rsidRDefault="007C3B1B" w:rsidP="007B4099">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="360" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="007B4099" w:rsidRPr="007C3B1B" w:rsidRDefault="007B4099" w:rsidP="007B4099">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="360" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Demikian</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Surat</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Pernyataan</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>ini kami buat</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>dengan</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>sebenarnya</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>untuk</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>dipergunakan</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>sebagai</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="007B4099" w:rsidRPr="007C3B1B" w:rsidRDefault="007B4099" w:rsidP="007B4099">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="360" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>mana</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>lampiran</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>permohonan kami memperoleh</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>sertifikat</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>hak atas</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>tanah</w:t>
      </w:r>
      <w:r w:rsidR="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>.</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="007B4099" w:rsidRPr="007C3B1B" w:rsidRDefault="007B4099" w:rsidP="007B4099">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="360" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="007B4099" w:rsidRDefault="007B4099" w:rsidP="00645AE6">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="360" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="right"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve">Grinting, </w:t>
      </w:r>
      <w:r w:rsidR="00645AE6">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>T</w:t>
      </w:r>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve">anggal </w:t>
      </w:r>
      <w:r w:rsidR="00645AE6">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t><xsl:value-of select="dt_surat_pernyataan"/></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00A214AB" w:rsidRDefault="00A214AB" w:rsidP="00645AE6">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="360" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="right"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblStyle w:val="TableGrid"/>
        <w:tblW w:w="0" w:type="auto"/>
        <w:tblInd w:w="108" w:type="dxa"/>
        <w:tblBorders>
          <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="4680"/>
        <w:gridCol w:w="5661"/>
      </w:tblGrid>
      <w:tr w:rsidR="00645AE6" w:rsidTr="00A214AB">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="4680" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00645AE6" w:rsidRDefault="00645AE6" w:rsidP="00A214AB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="360" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="007C3B1B">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>Saksi I</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00A214AB" w:rsidRDefault="00A214AB" w:rsidP="00A214AB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="360" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00645AE6" w:rsidRDefault="00645AE6" w:rsidP="00645AE6">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="360" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>( <xsl:value-of select="nama_saksi_1"/></w:t>
            </w:r>
            <w:r w:rsidRPr="007C3B1B">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t xml:space="preserve"> )</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00645AE6" w:rsidRDefault="00645AE6" w:rsidP="00645AE6">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="360" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00645AE6" w:rsidRDefault="00645AE6" w:rsidP="00A214AB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="360" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="007C3B1B">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>Saksi II</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00A214AB" w:rsidRDefault="00A214AB" w:rsidP="00A214AB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="360" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00645AE6" w:rsidRDefault="00645AE6" w:rsidP="00645AE6">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="360" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="007C3B1B">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>(</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t xml:space="preserve"> <xsl:value-of select="nama_saksi_2"/> </w:t>
            </w:r>
            <w:r w:rsidRPr="007C3B1B">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>)</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="5661" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00645AE6" w:rsidRDefault="00645AE6" w:rsidP="00A214AB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="360" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="007C3B1B">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>Pemohon</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00A214AB" w:rsidRPr="00A214AB" w:rsidRDefault="00A214AB" w:rsidP="00A214AB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="360" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00645AE6" w:rsidRDefault="00645AE6" w:rsidP="00645AE6">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="360" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:i/>
                <w:iCs/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="007C3B1B">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:i/>
                <w:iCs/>
              </w:rPr>
              <w:t>Materai</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:i/>
                <w:iCs/>
              </w:rPr>
              <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r w:rsidRPr="007C3B1B">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:i/>
                <w:iCs/>
              </w:rPr>
              <w:t>Rp 6.000</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00645AE6" w:rsidRDefault="00645AE6" w:rsidP="00A214AB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="360" w:lineRule="auto"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:i/>
                <w:iCs/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00A214AB" w:rsidRDefault="00A214AB" w:rsidP="00A214AB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="360" w:lineRule="auto"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00645AE6" w:rsidRDefault="00645AE6" w:rsidP="00645AE6">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="360" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00645AE6" w:rsidRDefault="00645AE6" w:rsidP="00645AE6">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="360" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t><xsl:value-of select="nama"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
    </w:tbl>
    <w:p w:rsidR="007B4099" w:rsidRPr="007C3B1B" w:rsidRDefault="007B4099" w:rsidP="007B4099">
      <w:pPr>
        <w:spacing w:line="360" w:lineRule="auto"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="007B4099" w:rsidRPr="007C3B1B" w:rsidRDefault="007B4099" w:rsidP="007B4099">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="007C3B1B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Mengetahui</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="007B4099" w:rsidRPr="007C3B1B" w:rsidRDefault="00A214AB" w:rsidP="007B4099">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t><xsl:value-of select="ttd_jabatan"/></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="007B4099" w:rsidRPr="007C3B1B" w:rsidRDefault="007B4099" w:rsidP="007B4099">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="007B4099" w:rsidRPr="007C3B1B" w:rsidRDefault="007B4099" w:rsidP="007B4099">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="007B4099" w:rsidRPr="007C3B1B" w:rsidRDefault="007B4099" w:rsidP="007B4099">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="007B4099" w:rsidRPr="007C3B1B" w:rsidRDefault="007B4099" w:rsidP="007B4099">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00DF50A9" w:rsidRPr="00A214AB" w:rsidRDefault="00A214AB" w:rsidP="00A214AB">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
      </w:pPr>
      <w:bookmarkStart w:id="0" w:name="_GoBack"/>
      <w:bookmarkEnd w:id="0"/>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t><xsl:value-of select="ttd_nama"/></w:t>
      </w:r>
    </w:p>
    <w:sectPr w:rsidR="00DF50A9" w:rsidRPr="00A214AB" w:rsidSect="00DF50A9">
      <w:pgSz w:w="12240" w:h="20160" w:code="5"/>
      <w:pgMar w:top="899" w:right="927" w:bottom="1079" w:left="1080" w:header="720" w:footer="720" w:gutter="0"/>
      <w:cols w:space="720"/>
      <w:docGrid w:linePitch="360"/>
    </w:sectPr>
	</xsl:for-each>
  </w:body>
</w:document>
</xsl:template>
</xsl:stylesheet>
