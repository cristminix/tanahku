<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<w:document xmlns:ve="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml">
  <w:body>
	<xsl:for-each select="root/row">
    <w:p w:rsidR="005B29E1" w:rsidRPr="00420C0A" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="28"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="28"/>
        </w:rPr>
        <w:t>SURAT KETERANGAN</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="28"/>
        </w:rPr>
        <w:t xml:space="preserve"> AKSES JALAN</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRPr="00420C0A" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve">Nomor : </w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t><xsl:value-of select="nomor_surat"/></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRPr="00420C0A" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="28"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRPr="00420C0A" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="28"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRPr="00420C0A" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="540"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
        <w:t>Kepala</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Desa</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t>Grinting</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Kecamatan</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t>Bulakamba</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Kabupaten</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t>Brebes</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Dengan</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>ini</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>menerangkan</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>bahwa</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>sebidang</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>tanah</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>milik</w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>:</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRPr="00420C0A" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRPr="008E241E" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Nama</w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
        <w:t>:</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
        </w:rPr>
        <w:t><xsl:value-of select="nama"/></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRPr="00420C0A" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Alamat</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve">: </w:t>
      </w:r>
      <w:r w:rsidRPr="008E241E">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t>RT</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t>. <xsl:value-of select="rt"/></w:t>
      </w:r>
      <w:r w:rsidRPr="008E241E">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
        </w:rPr>
        <w:t xml:space="preserve">  RW. </w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
        </w:rPr>
        <w:t><xsl:value-of select="rw"/></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRPr="00420C0A" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Desa/Kelurahan</w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
        <w:t xml:space="preserve">: </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t>Grinting</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRPr="00420C0A" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Kecamatan</w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
        <w:t xml:space="preserve">: </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t>Bulakamba</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRPr="00420C0A" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Kabupaten</w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
        <w:t xml:space="preserve">: </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t>Brebes</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRPr="00420C0A" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRPr="00420C0A" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Tanah tersebut</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>te</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>r</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>letak di Desa</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Grinting</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Persil</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t xml:space="preserve"><xsl:value-of select="persil"/> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Klas</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t><xsl:value-of select="kelas"/></w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Luas</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:u w:val="single"/>
        </w:rPr>
        <w:t>+</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t><xsl:value-of select="luas_tanah"/></w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> M</w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:vertAlign w:val="superscript"/>
        </w:rPr>
        <w:t>2</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>dan</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>dipergunakan</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>untuk</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> <xsl:value-of select="keperluan"/></w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve">   .</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRPr="00420C0A" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Bahwa Tanah tersebut</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>benar</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> – </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>benar</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>memiliki</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="008E241E">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
        </w:rPr>
        <w:t>akses</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="008E241E">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
        </w:rPr>
        <w:t>jalan</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>yaitu</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
        </w:rPr>
        <w:t><xsl:value-of select="nama_jalan"/></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Adapun</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>gambar</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>situasi</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>tanah</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Sbb:</w:t>
      </w:r>
    </w:p>
    
    <xsl:if test="gambar_denah=''">
    <w:p w:rsidR="005B29E1" w:rsidRDefault="00756042" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:noProof/>
        </w:rPr>
        <w:pict>
          <v:shapetype id="_x0000_t32" coordsize="21600,21600" o:spt="32" o:oned="t" path="m,l21600,21600e" filled="f">
            <v:path arrowok="t" fillok="f" o:connecttype="none"/>
            <o:lock v:ext="edit" shapetype="t"/>
          </v:shapetype>
          <v:shape id="AutoShape 82" o:spid="_x0000_s1026" type="#_x0000_t32" style="position:absolute;left:0;text-align:left;margin-left:140.25pt;margin-top:11.9pt;width:153pt;height:.05pt;z-index:251660288;visibility:visible" o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF&#10;90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA&#10;0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD&#10;OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893&#10;SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y&#10;JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl&#10;bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR&#10;JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY&#10;22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i&#10;OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA&#10;IQBx3wtmIgIAAD8EAAAOAAAAZHJzL2Uyb0RvYy54bWysU8GO2jAQvVfqP1i+s0kgUIgIq1UCvWy7&#10;SLv9AGM7xKpjW7YhoKr/3rEJaGkvVdUcnLE98+bNzPPy8dRJdOTWCa1KnD2kGHFFNRNqX+Jvb5vR&#10;HCPniWJEasVLfOYOP64+flj2puBj3WrJuEUAolzRmxK33psiSRxteUfcgzZcwWWjbUc8bO0+YZb0&#10;gN7JZJyms6TXlhmrKXcOTuvLJV5F/Kbh1L80jeMeyRIDNx9XG9ddWJPVkhR7S0wr6ECD/AOLjggF&#10;SW9QNfEEHaz4A6oT1GqnG/9AdZfophGUxxqgmiz9rZrXlhgea4HmOHNrk/t/sPTrcWuRYCWeTzBS&#10;pIMZPR28jqnRfBwa1BtXgF+ltjaUSE/q1Txr+t0hpauWqD2P3m9nA8FZiEjuQsLGGUiz679oBj4E&#10;EsRunRrbBUjoAzrFoZxvQ+EnjygcZot8kqUwOwp3s8k04pPiGmqs85+57lAwSuy8JWLf+korBcPX&#10;NouJyPHZ+UCMFNeAkFfpjZAyakAq1Jd4MR1PY4DTUrBwGdyc3e8qadGRBBXFb2Bx52b1QbEI1nLC&#10;1oPtiZAXG5JLFfCgNKAzWBeZ/Fiki/V8Pc9H+Xi2HuVpXY+eNlU+mm2yT9N6UldVnf0M1LK8aAVj&#10;XAV2V8lm+d9JYng8F7HdRHtrQ3KPHvsFZK//SDrONozzIoydZuetvc4cVBqdhxcVnsH7Pdjv3/3q&#10;FwAAAP//AwBQSwMEFAAGAAgAAAAhAL4vdUfdAAAACQEAAA8AAABkcnMvZG93bnJldi54bWxMj0FP&#10;wzAMhe9I/IfISFwQS1bUqStNpwmJA0e2SVyzxrTdGqdq0rXs1+Od4GY/Pz1/r9jMrhMXHELrScNy&#10;oUAgVd62VGs47N+fMxAhGrKm84QafjDApry/K0xu/USfeNnFWnAIhdxoaGLscylD1aAzYeF7JL59&#10;+8GZyOtQSzuYicNdJxOlVtKZlvhDY3p8a7A670anAcOYLtV27erDx3V6+kqup6nfa/34MG9fQUSc&#10;458ZbviMDiUzHf1INohOQ5KplK08vHAFNqTZioXjTViDLAv5v0H5CwAA//8DAFBLAQItABQABgAI&#10;AAAAIQC2gziS/gAAAOEBAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10ueG1sUEsB&#10;Ai0AFAAGAAgAAAAhADj9If/WAAAAlAEAAAsAAAAAAAAAAAAAAAAALwEAAF9yZWxzLy5yZWxzUEsB&#10;Ai0AFAAGAAgAAAAhAHHfC2YiAgAAPwQAAA4AAAAAAAAAAAAAAAAALgIAAGRycy9lMm9Eb2MueG1s&#10;UEsBAi0AFAAGAAgAAAAhAL4vdUfdAAAACQEAAA8AAAAAAAAAAAAAAAAAfAQAAGRycy9kb3ducmV2&#10;LnhtbFBLBQYAAAAABAAEAPMAAACGBQAAAAA=&#10;"/>
        </w:pict>
      </w:r>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRPr="00996C38" w:rsidRDefault="00756042" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:i/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:i/>
          <w:noProof/>
        </w:rPr>
        <w:pict>
          <v:rect id="Rectangle 81" o:spid="_x0000_s1029" style="position:absolute;left:0;text-align:left;margin-left:165.75pt;margin-top:14.75pt;width:114.75pt;height:176.25pt;z-index:251659264;visibility:visible" o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF&#10;90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA&#10;0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD&#10;OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893&#10;SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y&#10;JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl&#10;bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR&#10;JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY&#10;22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i&#10;OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA&#10;IQDEsVLQHQIAAD8EAAAOAAAAZHJzL2Uyb0RvYy54bWysU9uO0zAQfUfiHyy/07Rpy3ajpqtVlyKk&#10;BVYsfIDrOImF7TFjt+ny9Ttx2lIu4gHhB8vjGR+fOTOzvDlYw/YKgwZX8slozJlyEirtmpJ/+bx5&#10;teAsROEqYcCpkj+pwG9WL18sO1+oHFowlUJGIC4UnS95G6MvsizIVlkRRuCVI2cNaEUkE5usQtER&#10;ujVZPh6/zjrAyiNIFQLd3g1Ovkr4da1k/FjXQUVmSk7cYtox7dt+z1ZLUTQofKvlkYb4BxZWaEef&#10;nqHuRBRsh/o3KKslQoA6jiTYDOpaS5VyoGwm41+yeWyFVykXEif4s0zh/8HKD/sHZLoq+WLCmROW&#10;avSJVBOuMYrRHQnU+VBQ3KN/wD7F4O9Bfg3MwbqlMHWLCF2rREW0Unz204PeCPSUbbv3UBG82EVI&#10;Wh1qtD0gqcAOqSRP55KoQ2SSLiez+dU0n3MmyZfn08X0at5zykRxeu4xxLcKLOsPJUdin+DF/j7E&#10;IfQUkuiD0dVGG5MMbLZrg2wvqD82aR3Rw2WYcawr+fWciPwdYpzWnyCsjtToRltS+hwkil63N65K&#10;bRiFNsOZsjOOkjxpN9RgC9UT6YgwdDFNHR1awO+cddTBJQ/fdgIVZ+ado1pcT2azvuWTQTLmZOCl&#10;Z3vpEU4SVMkjZ8NxHYcx2XnUTUs/TVLuDm6pfrVOyvb8BlZHstSlqTbHierH4NJOUT/mfvUMAAD/&#10;/wMAUEsDBBQABgAIAAAAIQDw4hQF3wAAAAoBAAAPAAAAZHJzL2Rvd25yZXYueG1sTI/BTsMwEETv&#10;SPyDtUjcqN2UQhriVAhUJI5teuG2iU0SiNdR7LSBr2c5wW1HM5p9k29n14uTHUPnScNyoUBYqr3p&#10;qNFwLHc3KYgQkQz2nqyGLxtgW1xe5JgZf6a9PR1iI7iEQoYa2hiHTMpQt9ZhWPjBEnvvfnQYWY6N&#10;NCOeudz1MlHqTjrsiD+0ONin1tafh8lpqLrkiN/78kW5zW4VX+fyY3p71vr6an58ABHtHP/C8IvP&#10;6FAwU+UnMkH0GlbLlLdEDclmDYID69t7Pip20kSBLHL5f0LxAwAA//8DAFBLAQItABQABgAIAAAA&#10;IQC2gziS/gAAAOEBAAATAAAAAAAAAAAAAAAAAAAAAABbQ29udGVudF9UeXBlc10ueG1sUEsBAi0A&#10;FAAGAAgAAAAhADj9If/WAAAAlAEAAAsAAAAAAAAAAAAAAAAALwEAAF9yZWxzLy5yZWxzUEsBAi0A&#10;FAAGAAgAAAAhAMSxUtAdAgAAPwQAAA4AAAAAAAAAAAAAAAAALgIAAGRycy9lMm9Eb2MueG1sUEsB&#10;Ai0AFAAGAAgAAAAhAPDiFAXfAAAACgEAAA8AAAAAAAAAAAAAAAAAdwQAAGRycy9kb3ducmV2Lnht&#10;bFBLBQYAAAAABAAEAPMAAACDBQAAAAA=&#10;"/>
        </w:pict>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:i/>
          <w:noProof/>
        </w:rPr>
        <w:pict>
          <v:shape id="AutoShape 83" o:spid="_x0000_s1030" type="#_x0000_t32" style="position:absolute;left:0;text-align:left;margin-left:139.5pt;margin-top:14.75pt;width:153pt;height:.05pt;z-index:251661312;visibility:visible" o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF&#10;90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA&#10;0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD&#10;OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893&#10;SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y&#10;JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl&#10;bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR&#10;JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY&#10;22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i&#10;OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA&#10;IQAmWhCRIgIAAD8EAAAOAAAAZHJzL2Uyb0RvYy54bWysU8GO2jAQvVfqP1i+s0kgUIgIq1UCvWy7&#10;SLv9AGM7xKpjW7YhoKr/3rEJaGkvVdUcnLE98+bNzPPy8dRJdOTWCa1KnD2kGHFFNRNqX+Jvb5vR&#10;HCPniWJEasVLfOYOP64+flj2puBj3WrJuEUAolzRmxK33psiSRxteUfcgzZcwWWjbUc8bO0+YZb0&#10;gN7JZJyms6TXlhmrKXcOTuvLJV5F/Kbh1L80jeMeyRIDNx9XG9ddWJPVkhR7S0wr6ECD/AOLjggF&#10;SW9QNfEEHaz4A6oT1GqnG/9AdZfophGUxxqgmiz9rZrXlhgea4HmOHNrk/t/sPTrcWuRYCWejzFS&#10;pIMZPR28jqnRfBIa1BtXgF+ltjaUSE/q1Txr+t0hpauWqD2P3m9nA8FZiEjuQsLGGUiz679oBj4E&#10;EsRunRrbBUjoAzrFoZxvQ+EnjygcZot8kqUwOwp3s8k04pPiGmqs85+57lAwSuy8JWLf+korBcPX&#10;NouJyPHZ+UCMFNeAkFfpjZAyakAq1Jd4MR1PY4DTUrBwGdyc3e8qadGRBBXFb2Bx52b1QbEI1nLC&#10;1oPtiZAXG5JLFfCgNKAzWBeZ/Fiki/V8Pc9H+Xi2HuVpXY+eNlU+mm2yT9N6UldVnf0M1LK8aAVj&#10;XAV2V8lm+d9JYng8F7HdRHtrQ3KPHvsFZK//SDrONozzIoydZuetvc4cVBqdhxcVnsH7Pdjv3/3q&#10;FwAAAP//AwBQSwMEFAAGAAgAAAAhAONiAFTeAAAACQEAAA8AAABkcnMvZG93bnJldi54bWxMj0Fv&#10;wjAMhe9I+w+RkXZBI6VSGS1NEZq0w44DpF1D47WFxqmalHb8+pnTdrOfn56/l+8m24ob9r5xpGC1&#10;jEAglc40VCk4Hd9fNiB80GR06wgV/KCHXfE0y3Vm3EifeDuESnAI+UwrqEPoMil9WaPVfuk6JL59&#10;u97qwGtfSdPrkcNtK+MoWkurG+IPte7wrcbyehisAvRDsor2qa1OH/dx8RXfL2N3VOp5Pu23IAJO&#10;4c8MD3xGh4KZzm4g40WrIH5NuUvgIU1AsCHZJCycH8IaZJHL/w2KXwAAAP//AwBQSwECLQAUAAYA&#10;CAAAACEAtoM4kv4AAADhAQAAEwAAAAAAAAAAAAAAAAAAAAAAW0NvbnRlbnRfVHlwZXNdLnhtbFBL&#10;AQItABQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAAAAAAAAAAAAAAC8BAABfcmVscy8ucmVsc1BL&#10;AQItABQABgAIAAAAIQAmWhCRIgIAAD8EAAAOAAAAAAAAAAAAAAAAAC4CAABkcnMvZTJvRG9jLnht&#10;bFBLAQItABQABgAIAAAAIQDjYgBU3gAAAAkBAAAPAAAAAAAAAAAAAAAAAHwEAABkcnMvZG93bnJl&#10;di54bWxQSwUGAAAAAAQABADzAAAAhwUAAAAA&#10;"/>
        </w:pict>
      </w:r>
      <w:r w:rsidR="005B29E1" w:rsidRPr="00996C38">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:i/>
        </w:rPr>
        <w:t xml:space="preserve">                                  </w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:i/>
        </w:rPr>
        <w:t xml:space="preserve">                 <xsl:value-of select="nama_jalan"/></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRPr="00996C38" w:rsidRDefault="005F38CF" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
        </w:rPr>
        <w:t><xsl:value-of select="arah_kiri"/></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRDefault="00756042" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:noProof/>
        </w:rPr>
        <w:pict>
          <v:shape id="AutoShape 85" o:spid="_x0000_s1028" type="#_x0000_t32" style="position:absolute;left:0;text-align:left;margin-left:362.25pt;margin-top:6.2pt;width:0;height:42.7pt;flip:y;z-index:251663360;visibility:visible" o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF&#10;90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA&#10;0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD&#10;OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893&#10;SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y&#10;JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl&#10;bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR&#10;JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY&#10;22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i&#10;OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA&#10;IQCyLZz6JQIAAEYEAAAOAAAAZHJzL2Uyb0RvYy54bWysU02P0zAQvSPxH6zc23yQljZqulolLZcF&#10;Ku3C3bWdxMKxLdttWiH+O2OnLV24IEQOztieefNm5nn1cOoFOjJjuZJllE6TCDFJFOWyLaMvL9vJ&#10;IkLWYUmxUJKV0ZnZ6GH99s1q0AXLVKcEZQYBiLTFoMuoc04XcWxJx3psp0ozCZeNMj12sDVtTA0e&#10;AL0XcZYk83hQhmqjCLMWTuvxMloH/KZhxH1uGsscEmUE3FxYTVj3fo3XK1y0BuuOkwsN/A8seswl&#10;JL1B1dhhdDD8D6ieE6OsatyUqD5WTcMJCzVANWnyWzXPHdYs1ALNsfrWJvv/YMmn484gTstoAe2R&#10;uIcZPR6cCqnRYuYbNGhbgF8ld8aXSE7yWT8p8s0iqaoOy5YF75ezhuDUR8SvQvzGakizHz4qCj4Y&#10;EoRunRrTo0Zw/dUHenDoCDqF8Zxv42Enh8h4SOB0lmfZMkwuxoVH8HHaWPeBqR55o4ysM5i3nauU&#10;lKABZUZ0fHyyzvP7FeCDpdpyIYIUhERDGS1n2SzQsUpw6i+9mzXtvhIGHbEXU/hCsXBz72bUQdIA&#10;1jFMNxfbYS5GG5IL6fGgLqBzsUa1fF8my81is8gneTbfTPKkrieP2yqfzLfp+1n9rq6qOv3hqaV5&#10;0XFKmfTsrspN879TxuUNjZq7affWhvg1eugXkL3+A+kwYj/VUR97Rc87cx09iDU4Xx6Wfw33e7Dv&#10;n//6JwAAAP//AwBQSwMEFAAGAAgAAAAhADJfHJrcAAAACQEAAA8AAABkcnMvZG93bnJldi54bWxM&#10;j8FOhDAQhu8mvkMzJt7cIsGFZSkbY6LxYEhc9d6lI6B0irQL7Ns7xoMeZ/4v/3xT7BbbiwlH3zlS&#10;cL2KQCDVznTUKHh9ub/KQPigyejeESo4oYddeX5W6Ny4mZ5x2odGcAn5XCtoQxhyKX3dotV+5QYk&#10;zt7daHXgcWykGfXM5baXcRStpdUd8YVWD3jXYv25P1oFX5Se3hI5ZR9VFdYPj08NYTUrdXmx3G5B&#10;BFzCHww/+qwOJTsd3JGMF72CNE5uGOUgTkAw8Ls4KNikGciykP8/KL8BAAD//wMAUEsBAi0AFAAG&#10;AAgAAAAhALaDOJL+AAAA4QEAABMAAAAAAAAAAAAAAAAAAAAAAFtDb250ZW50X1R5cGVzXS54bWxQ&#10;SwECLQAUAAYACAAAACEAOP0h/9YAAACUAQAACwAAAAAAAAAAAAAAAAAvAQAAX3JlbHMvLnJlbHNQ&#10;SwECLQAUAAYACAAAACEAsi2c+iUCAABGBAAADgAAAAAAAAAAAAAAAAAuAgAAZHJzL2Uyb0RvYy54&#10;bWxQSwECLQAUAAYACAAAACEAMl8cmtwAAAAJAQAADwAAAAAAAAAAAAAAAAB/BAAAZHJzL2Rvd25y&#10;ZXYueG1sUEsFBgAAAAAEAAQA8wAAAIgFAAAAAA==&#10;"/>
        </w:pict>
      </w:r>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRDefault="00756042" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:noProof/>
        </w:rPr>
        <w:pict>
          <v:shape id="AutoShape 84" o:spid="_x0000_s1027" type="#_x0000_t32" style="position:absolute;left:0;text-align:left;margin-left:336.75pt;margin-top:9.8pt;width:49.5pt;height:0;z-index:251662336;visibility:visible" o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF&#10;90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA&#10;0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD&#10;OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893&#10;SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y&#10;JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl&#10;bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR&#10;JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY&#10;22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i&#10;OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA&#10;IQAu7w/tIAIAADwEAAAOAAAAZHJzL2Uyb0RvYy54bWysU02P2jAQvVfqf7B8hyQ0sBARVqsEetl2&#10;kXb7A4ztJFYd27INAVX97x2bD7HtparKwYwzM2/ezDwvH4+9RAdundCqxNk4xYgrqplQbYm/vW1G&#10;c4ycJ4oRqRUv8Yk7/Lj6+GE5mIJPdKcl4xYBiHLFYErceW+KJHG04z1xY224AmejbU88XG2bMEsG&#10;QO9lMknTWTJoy4zVlDsHX+uzE68iftNw6l+axnGPZImBm4+njecunMlqSYrWEtMJeqFB/oFFT4SC&#10;ojeomniC9lb8AdULarXTjR9T3Se6aQTlsQfoJkt/6+a1I4bHXmA4ztzG5P4fLP162FokWIkfFhgp&#10;0sOOnvZex9JonocBDcYVEFeprQ0t0qN6Nc+afndI6aojquUx+u1kIDkLGcm7lHBxBsrshi+aQQyB&#10;AnFax8b2ARLmgI5xKafbUvjRIwofZ5P5bAqro1dXQoprnrHOf+a6R8EosfOWiLbzlVYKNq9tFquQ&#10;w7PzgRUprgmhqNIbIWUUgFRoKPFiOpnGBKelYMEZwpxtd5W06ECChOIvtgie+zCr94pFsI4Ttr7Y&#10;ngh5tqG4VAEP+gI6F+uskR+LdLGer+f5KJ/M1qM8revR06bKR7NN9jCtP9VVVWc/A7UsLzrBGFeB&#10;3VWvWf53eri8nLPSboq9jSF5jx7nBWSv/5F0XGzY5VkVO81OW3tdOEg0Bl+eU3gD93ew7x/96hcA&#10;AAD//wMAUEsDBBQABgAIAAAAIQCA5w9V3QAAAAkBAAAPAAAAZHJzL2Rvd25yZXYueG1sTI/BTsMw&#10;EETvSPyDtUi9IOo0qAkNcaqqEgeOtJW4uvGSpI3XUew0oV/PIg7luDNPszP5erKtuGDvG0cKFvMI&#10;BFLpTEOVgsP+7ekFhA+ajG4doYJv9LAu7u9ynRk30gdedqESHEI+0wrqELpMSl/WaLWfuw6JvS/X&#10;Wx347Ctpej1yuG1lHEWJtLoh/lDrDrc1lufdYBWgH5aLaLOy1eH9Oj5+xtfT2O2Vmj1Mm1cQAadw&#10;g+G3PleHgjsd3UDGi1ZBkj4vGWVjlYBgIE1jFo5/gixy+X9B8QMAAP//AwBQSwECLQAUAAYACAAA&#10;ACEAtoM4kv4AAADhAQAAEwAAAAAAAAAAAAAAAAAAAAAAW0NvbnRlbnRfVHlwZXNdLnhtbFBLAQIt&#10;ABQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAAAAAAAAAAAAAAC8BAABfcmVscy8ucmVsc1BLAQIt&#10;ABQABgAIAAAAIQAu7w/tIAIAADwEAAAOAAAAAAAAAAAAAAAAAC4CAABkcnMvZTJvRG9jLnhtbFBL&#10;AQItABQABgAIAAAAIQCA5w9V3QAAAAkBAAAPAAAAAAAAAAAAAAAAAHoEAABkcnMvZG93bnJldi54&#10;bWxQSwUGAAAAAAQABADzAAAAhAUAAAAA&#10;"/>
        </w:pict>
      </w:r>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRPr="00996C38" w:rsidRDefault="00C44FA1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:i/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:i/>
        </w:rPr>
        <w:t><xsl:value-of select="batas_kiri"/></w:t>
      </w:r>
      <w:r w:rsidR="005F38CF">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:i/>
        </w:rPr>
        <w:t></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRPr="00996C38" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:i/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00996C38">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:i/>
        </w:rPr>
        <w:t xml:space="preserve">                              </w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:i/>
        </w:rPr>
        <w:t xml:space="preserve">                       </w:t>
      </w:r>
      <w:r w:rsidR="005F38CF">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:i/>
        </w:rPr>
        <w:t><xsl:value-of select="batas_bawah"/></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRPr="00420C0A" w:rsidRDefault="005B29E1" w:rsidP="005B29E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
     
  </xsl:if>
    <w:p w:rsidR="00C66CD3" w:rsidRDefault="00C66CD3" w:rsidP="00C44FA1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="142"/>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="142"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <xsl:if test="gambar_denah != ''">
    <w:p w:rsidR="00C66CD3" w:rsidRDefault="00C66CD3" w:rsidP="00C44FA1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="142"/>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="142"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:noProof/>
        </w:rPr>
        <w:drawing>
          <wp:inline distT="0" distB="0" distL="0" distR="0">
            <wp:extent cx="5734050" cy="2943225"/>
            <wp:effectExtent l="19050" t="0" r="0" b="0"/> 
            <wp:docPr id="1" name="Picture 0" descr="gambar_denah.png"/>
            <wp:cNvGraphicFramePr>
              <a:graphicFrameLocks xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" noChangeAspect="1"/>
            </wp:cNvGraphicFramePr>
            <a:graphic xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main">
              <a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/picture">
                <pic:pic xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture">
                  <pic:nvPicPr>
                    <pic:cNvPr id="0" name="gambar_denah.png"/>
                    <pic:cNvPicPr/>
                  </pic:nvPicPr>
                  <pic:blipFill>
                    <a:blip r:embed="rId6" cstate="print">
                      <a:extLst>
                        <a:ext uri="">
                          <a14:useLocalDpi xmlns:a14="http://schemas.microsoft.com/office/drawing/2010/main" xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape" xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk" xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas" xmlns="" val="0"/>
                        </a:ext>
                      </a:extLst>
                    </a:blip>
                    <a:srcRect/>
                    <a:stretch>
                      <a:fillRect/>
                    </a:stretch>
                  </pic:blipFill>
                  <pic:spPr>
                    <a:xfrm>
                      <a:off x="0" y="0"/>
                      <a:ext cx="5734050" cy="2943225"/>
                    </a:xfrm>
                    <a:prstGeom prst="rect">
                      <a:avLst/>
                    </a:prstGeom>
                  </pic:spPr>
                </pic:pic>
              </a:graphicData>
            </a:graphic>
          </wp:inline>
        </w:drawing>
      </w:r>
    </w:p>
  </xsl:if>
    <w:p w:rsidR="00C66CD3" w:rsidRDefault="005B29E1" w:rsidP="00C44FA1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="142"/>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="142"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
      </w:r>
    </w:p>
    <w:p w:rsidR="005B29E1" w:rsidRDefault="005B29E1" w:rsidP="00C44FA1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="142"/>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="142"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Demikian</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Surat</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Keterangan</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>ini kami buat</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>dengan</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>sebenar</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> – </w:t>
      </w:r>
      <w:r w:rsidRPr="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>benarnya</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>untuk di pergunakan</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>sebagaimana</w:t>
      </w:r>
      <w:r w:rsidR="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00C44FA1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>mestinya.</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00C44FA1" w:rsidRDefault="00C44FA1" w:rsidP="00C44FA1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="142"/>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="142"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblStyle w:val="TableGrid"/>
        <w:tblW w:w="0" w:type="auto"/>
        <w:tblInd w:w="142" w:type="dxa"/>
        <w:tblBorders>
          <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="5133"/>
        <w:gridCol w:w="5174"/>
      </w:tblGrid>
      <w:tr w:rsidR="00C44FA1" w:rsidTr="00C44FA1">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="5224" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00C44FA1" w:rsidRDefault="00C44FA1" w:rsidP="00C44FA1">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="5225" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00C44FA1" w:rsidRDefault="00C44FA1" w:rsidP="00C44FA1">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t xml:space="preserve">Grinting, </w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t><xsl:value-of select="dt_surat_keterangan"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00C44FA1" w:rsidRDefault="00C44FA1" w:rsidP="00C44FA1">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t><xsl:value-of select="ttd_jabatan"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00C44FA1" w:rsidRDefault="00C44FA1" w:rsidP="00C44FA1">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00C44FA1" w:rsidRDefault="00C44FA1" w:rsidP="00C44FA1">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00C44FA1" w:rsidRDefault="00C44FA1" w:rsidP="00C44FA1">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00C44FA1" w:rsidRDefault="00C44FA1" w:rsidP="00C44FA1">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00C44FA1" w:rsidRDefault="00C44FA1" w:rsidP="00C44FA1">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:bCs/>
              </w:rPr>
              <w:t><xsl:value-of select="ttd_nama"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
    </w:tbl>
    <w:p w:rsidR="00C44FA1" w:rsidRPr="00C44FA1" w:rsidRDefault="00C44FA1" w:rsidP="00C44FA1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="142"/>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="142"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00DF50A9" w:rsidRPr="00D956EA" w:rsidRDefault="00DF50A9" w:rsidP="00D956EA">
      <w:bookmarkStart w:id="0" w:name="_GoBack"/>
      <w:bookmarkEnd w:id="0"/>
    </w:p>
    <w:sectPr w:rsidR="00DF50A9" w:rsidRPr="00D956EA" w:rsidSect="00DF50A9">
      <w:pgSz w:w="12240" w:h="20160" w:code="5"/>
      <w:pgMar w:top="899" w:right="927" w:bottom="1079" w:left="1080" w:header="720" w:footer="720" w:gutter="0"/>
      <w:cols w:space="720"/>
      <w:docGrid w:linePitch="360"/>
    </w:sectPr>
	</xsl:for-each>
  </w:body>
</w:document>
</xsl:template>
</xsl:stylesheet>
