<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<w:document xmlns:ve="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml">
  <w:body>
	<xsl:for-each select="root/row">
    <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="006B1371">
      <w:pPr>
        <w:jc w:val="right"/>
      </w:pPr>
      <w:r>
        <w:t>TURUNAN</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="006B1371">
      <w:pPr>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:b/>
          <w:u w:val="single"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00AC4167">
        <w:rPr>
          <w:b/>
          <w:u w:val="single"/>
        </w:rPr>
        <w:t>KUTIPAN DAFTAR BUKU C</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="006B1371">
      <w:pPr>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:b/>
          <w:u w:val="single"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="006B1371">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
      </w:pPr>
      <w:r>
        <w:tab/>
      </w:r>
    </w:p>
    <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="006B1371">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:u w:val="single"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00AC4167">
        <w:rPr>
          <w:u w:val="single"/>
        </w:rPr>
        <w:t xml:space="preserve">DESA  </w:t>
      </w:r>
      <w:r w:rsidRPr="00C64CA5">
        <w:rPr>
          <w:b/>
          <w:bCs/>
          <w:u w:val="single"/>
        </w:rPr>
        <w:t>GRINTING</w:t>
      </w:r>
      <w:r w:rsidR="00D7597F">
        <w:rPr>
          <w:u w:val="single"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="00F81650">
        <w:rPr>
          <w:u w:val="single"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00AC4167">
        <w:rPr>
          <w:u w:val="single"/>
        </w:rPr>
        <w:t>KECAMATAN</w:t>
      </w:r>
      <w:r w:rsidR="00D7597F">
        <w:rPr>
          <w:u w:val="single"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="00F81650">
        <w:rPr>
          <w:u w:val="single"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00C64CA5">
        <w:rPr>
          <w:b/>
          <w:bCs/>
          <w:u w:val="single"/>
        </w:rPr>
        <w:t>BULAKAMBA</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="006B1371">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="006B1371" w:rsidRPr="003B21ED" w:rsidRDefault="006B1371" w:rsidP="006B1371">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:rPr>
          <w:lang w:val="id-ID"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:t>Nama</w:t>
      </w:r>
      <w:r w:rsidR="00D7597F">
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:t xml:space="preserve">pemilik Tanah  </w:t>
      </w:r>
      <w:r w:rsidR="00F81650">
        <w:rPr>
          <w:b/>
          <w:i/>
        </w:rPr>
        <w:t><xsl:value-of select="nama_pemilik"/></w:t>
      </w:r>
      <w:r w:rsidR="00D7597F">
        <w:rPr>
          <w:b/>
          <w:i/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:t>No.C</w:t>
      </w:r>
      <w:r w:rsidR="00F81650">
        <w:rPr>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t xml:space="preserve">  <xsl:value-of select="no_c"/></w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t xml:space="preserve">  </w:t>
      </w:r>
      <w:r>
        <w:t>Tempat</w:t>
      </w:r>
      <w:r w:rsidR="00D7597F">
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:t>tinggal</w:t>
      </w:r>
      <w:r w:rsidR="00D7597F">
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:t>Desa</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:lang w:val="id-ID"/>
        </w:rPr>
        <w:t>Grinting</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="006B1371">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
      </w:pPr>
      <w:r>
        <w:t>.</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="006B1371">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
      </w:pPr>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblW w:w="10314" w:type="dxa"/>
        <w:tblBorders>
          <w:top w:val="single" w:sz="4" w:space="0" w:color="auto"/>
          <w:left w:val="single" w:sz="4" w:space="0" w:color="auto"/>
          <w:bottom w:val="single" w:sz="4" w:space="0" w:color="auto"/>
          <w:right w:val="single" w:sz="4" w:space="0" w:color="auto"/>
          <w:insideH w:val="single" w:sz="4" w:space="0" w:color="auto"/>
          <w:insideV w:val="single" w:sz="4" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLayout w:type="fixed"/>
        <w:tblLook w:val="01E0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="817"/>
        <w:gridCol w:w="709"/>
        <w:gridCol w:w="649"/>
        <w:gridCol w:w="791"/>
        <w:gridCol w:w="540"/>
        <w:gridCol w:w="430"/>
        <w:gridCol w:w="992"/>
        <w:gridCol w:w="236"/>
        <w:gridCol w:w="844"/>
        <w:gridCol w:w="720"/>
        <w:gridCol w:w="610"/>
        <w:gridCol w:w="708"/>
        <w:gridCol w:w="612"/>
        <w:gridCol w:w="467"/>
        <w:gridCol w:w="1189"/>
      </w:tblGrid>
      <w:tr w:rsidR="006B1371" w:rsidTr="004A6DAB">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="4928" w:type="dxa"/>
            <w:gridSpan w:val="7"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:t>SAWAH</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="236" w:type="dxa"/>
            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRPr="00AD5A3D" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:left="-3" w:firstLine="3"/>
              <w:rPr>
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </w:rPr>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="5150" w:type="dxa"/>
            <w:gridSpan w:val="7"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:t>TANAH KERING</w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
      <w:tr w:rsidR="006B1371" w:rsidTr="004A6DAB">
        <w:trPr>
          <w:trHeight w:val="465"/>
        </w:trPr>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="817" w:type="dxa"/>
            <w:vMerge w:val="restart"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="008E77CA">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
            <w:r>
              <w:t>No.</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
            <w:r>
              <w:t>Persil dan</w:t>
            </w:r>
            <w:r w:rsidR="00D7597F">
              <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r>
              <w:t>huruf bagia persil</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="709" w:type="dxa"/>
            <w:vMerge w:val="restart"/>
            <w:textDirection w:val="btLr"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:ind w:left="113" w:right="113"/>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:t>Klas</w:t>
            </w:r>
            <w:r w:rsidR="00D7597F">
              <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r>
              <w:t>Desa</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:ind w:left="113" w:right="113"/>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:ind w:left="113" w:right="113"/>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:ind w:left="113" w:right="113"/>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:t>Kls</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:ind w:left="113" w:right="113"/>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:t>Ds</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2410" w:type="dxa"/>
            <w:gridSpan w:val="4"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:t>Menurut</w:t>
            </w:r>
            <w:r w:rsidR="00D7597F">
              <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r>
              <w:t>Daftar</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:t>Perincian</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="992" w:type="dxa"/>
            <w:vMerge w:val="restart"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:r>
              <w:t>Sebab</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:r>
              <w:t>dan</w:t>
            </w:r>
            <w:r w:rsidR="00D7597F">
              <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r>
              <w:t>Tgl</w:t>
            </w:r>
            <w:r w:rsidR="00D7597F">
              <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r>
              <w:t>perubahan</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="236" w:type="dxa"/>
            <w:vMerge w:val="restart"/>
            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="844" w:type="dxa"/>
            <w:vMerge w:val="restart"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
            <w:r>
              <w:t>No.</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
            <w:r>
              <w:t>Persil dan</w:t>
            </w:r>
            <w:r w:rsidR="00D7597F">
              <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r>
              <w:t>huruf bagia persil</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="720" w:type="dxa"/>
            <w:vMerge w:val="restart"/>
            <w:textDirection w:val="btLr"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:ind w:left="113" w:right="113"/>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:t>Klas</w:t>
            </w:r>
            <w:r w:rsidR="00D7597F">
              <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r>
              <w:t>Desa</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:ind w:left="113" w:right="113"/>
              <w:jc w:val="center"/>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:ind w:left="113" w:right="113"/>
            </w:pPr>
            <w:r>
              <w:t>Klas desa</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:left="113" w:right="113"/>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2397" w:type="dxa"/>
            <w:gridSpan w:val="4"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:t>Menurut Daftar</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:t>Perincian</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1189" w:type="dxa"/>
            <w:vMerge w:val="restart"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:r>
              <w:t>Sebab</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="006B1371" w:rsidRPr="00A11EF5" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
            <w:r>
              <w:t>dan</w:t>
            </w:r>
            <w:r w:rsidR="00D7597F">
              <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r>
              <w:t>Tgl</w:t>
            </w:r>
            <w:r w:rsidR="00D7597F">
              <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r>
              <w:t>perubahan</w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
      <w:tr w:rsidR="006B1371" w:rsidTr="004A6DAB">
        <w:trPr>
          <w:trHeight w:val="420"/>
        </w:trPr>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="817" w:type="dxa"/>
            <w:vMerge/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="709" w:type="dxa"/>
            <w:vMerge/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB"/>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1440" w:type="dxa"/>
            <w:gridSpan w:val="2"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:t>Luas</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:t>Milik</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="970" w:type="dxa"/>
            <w:gridSpan w:val="2"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:t>Pajak</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="992" w:type="dxa"/>
            <w:vMerge/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB"/>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="236" w:type="dxa"/>
            <w:vMerge/>
            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="844" w:type="dxa"/>
            <w:vMerge/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="720" w:type="dxa"/>
            <w:vMerge/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1318" w:type="dxa"/>
            <w:gridSpan w:val="2"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:t>Luas</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:t>Milik</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1079" w:type="dxa"/>
            <w:gridSpan w:val="2"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
            <w:r>
              <w:t>Pajak</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1189" w:type="dxa"/>
            <w:vMerge/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
          </w:p>
        </w:tc>
      </w:tr>
      <w:tr w:rsidR="006B1371" w:rsidTr="004A6DAB">
        <w:trPr>
          <w:trHeight w:val="574"/>
        </w:trPr>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="817" w:type="dxa"/>
            <w:vMerge/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="709" w:type="dxa"/>
            <w:vMerge/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB"/>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="649" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
            <w:r>
              <w:t>ha</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="791" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
            <w:r>
              <w:t>M</w:t>
            </w:r>
            <w:r w:rsidRPr="00AD5A3D">
              <w:rPr>
                <w:vertAlign w:val="superscript"/>
              </w:rPr>
              <w:t>2</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="540" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
            <w:r>
              <w:t>Rp</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="430" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
            <w:r>
              <w:t>$</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="992" w:type="dxa"/>
            <w:vMerge/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB"/>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="236" w:type="dxa"/>
            <w:vMerge/>
            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="844" w:type="dxa"/>
            <w:vMerge/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="720" w:type="dxa"/>
            <w:vMerge/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="610" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
            <w:r>
              <w:t>ha</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="708" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
            <w:r>
              <w:t>M</w:t>
            </w:r>
            <w:r w:rsidRPr="00AD5A3D">
              <w:rPr>
                <w:vertAlign w:val="superscript"/>
              </w:rPr>
              <w:t>2</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="612" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
            <w:r>
              <w:t>Rp</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="467" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
            <w:r>
              <w:t>$</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1189" w:type="dxa"/>
            <w:vMerge/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="004A6DAB">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
          </w:p>
        </w:tc>
      </w:tr>
      <xsl:for-each select="list_kutipan_detail/p">
      <w:tr w:rsidR="006B1371" w:rsidTr="003F3B7E">
        <w:trPr>
          <w:trHeight w:val="1890"/>
        </w:trPr>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="817" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRPr="00A70A20" w:rsidRDefault="00A70A20" w:rsidP="003F3B7E">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t><xsl:value-of select="s_persil"/></w:t>
            </w:r>
            <w:r w:rsidR="00BB4FD6">
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="709" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRPr="00A70A20" w:rsidRDefault="00A70A20" w:rsidP="003F3B7E">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t><xsl:value-of select="s_kelas"/></w:t>
            </w:r>
            <w:r w:rsidR="00BB4FD6">
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="649" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRPr="00A739CE" w:rsidRDefault="00A70A20" w:rsidP="003F3B7E">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t><xsl:value-of select="s_luas_ha"/></w:t>
            </w:r>
            <w:r w:rsidR="00BB4FD6">
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="791" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRPr="00A70A20" w:rsidRDefault="00A70A20" w:rsidP="003F3B7E">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t><xsl:value-of select="s_luas"/></w:t>
            </w:r>
            <w:r w:rsidR="00BB4FD6">
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="540" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRPr="00A739CE" w:rsidRDefault="00A70A20" w:rsidP="003F3B7E">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t><xsl:value-of select="s_pajak"/></w:t>
            </w:r>
            <w:r w:rsidR="00BB4FD6">
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="430" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRPr="00A739CE" w:rsidRDefault="00A70A20" w:rsidP="003F3B7E">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t><xsl:value-of select="s_pajak_usd"/></w:t>
            </w:r>
            <w:r w:rsidR="00BB4FD6">
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="992" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRPr="00A70A20" w:rsidRDefault="00A70A20" w:rsidP="003F3B7E">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t><xsl:value-of select="s_sbb_tgl_ubah"/></w:t>
            </w:r>
            <w:r w:rsidR="00BB4FD6">
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="236" w:type="dxa"/>
            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="003F3B7E">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="844" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="003F3B7E">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t><xsl:value-of select="d_persil"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="006B1371" w:rsidRPr="00BA6AF5" w:rsidRDefault="006B1371" w:rsidP="003F3B7E">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="720" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRPr="001B2B56" w:rsidRDefault="006B1371" w:rsidP="003F3B7E">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t><xsl:value-of select="d_kelas"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="610" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRPr="00A739CE" w:rsidRDefault="00A70A20" w:rsidP="003F3B7E">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t><xsl:value-of select="d_luas_ha"/></w:t>
            </w:r>
            <w:r w:rsidR="00BB4FD6">
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="708" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRPr="001B2B56" w:rsidRDefault="006B1371" w:rsidP="003F3B7E">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t><xsl:value-of select="d_luas"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="612" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRPr="00A739CE" w:rsidRDefault="00A70A20" w:rsidP="003F3B7E">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t><xsl:value-of select="d_pajak"/></w:t>
            </w:r>
            <w:r w:rsidR="00BB4FD6">
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="467" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRPr="00A70A20" w:rsidRDefault="00A70A20" w:rsidP="003F3B7E">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t><xsl:value-of select="d_pajak_usd"/></w:t>
            </w:r>
            <w:r w:rsidR="00BB4FD6">
              <w:rPr>
                <w:b/>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1189" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="006B1371" w:rsidRPr="003A6EE6" w:rsidRDefault="006B1371" w:rsidP="003F3B7E">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="003A6EE6">
              <w:rPr>
                <w:sz w:val="20"/>
                <w:szCs w:val="20"/>
              </w:rPr>
              <w:t><xsl:value-of select="d_sbb_tgl_ubah"/></w:t>
            </w:r>
           
          </w:p>
        </w:tc>
      </w:tr>
      </xsl:for-each>
    </w:tbl>
    <w:p w:rsidR="006B1371" w:rsidRDefault="006B1371" w:rsidP="006B1371">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
      </w:pPr>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblStyle w:val="TableGrid"/>
        <w:tblW w:w="0" w:type="auto"/>
        <w:tblInd w:w="18" w:type="dxa"/>
        <w:tblBorders>
          <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="4680"/>
        <w:gridCol w:w="5751"/>
      </w:tblGrid>
      <w:tr w:rsidR="00D7597F" w:rsidTr="00D7597F">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="4680" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00D7597F" w:rsidRDefault="00D7597F" w:rsidP="006B1371">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="5751" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00D7597F" w:rsidRDefault="00D7597F" w:rsidP="00D7597F">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:left="4320" w:hanging="4320"/>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:t>Turunan telah disesuaikan dengan “Daftar Aslinya”</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00D7597F" w:rsidRDefault="00D7597F" w:rsidP="00D7597F">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:t xml:space="preserve">GRINTING, TANGGAL </w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:lang w:val="id-ID"/>
              </w:rPr>
              <w:t>,</w:t>
            </w:r>
            <w:r w:rsidR="00F81650">
              <w:t><xsl:value-of select="dt_kutipan"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00D7597F" w:rsidRDefault="00F81650" w:rsidP="00D7597F">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:t><xsl:value-of select="ttd_jabatan"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00D7597F" w:rsidRDefault="00D7597F" w:rsidP="00D7597F">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:left="4320" w:hanging="4320"/>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00D7597F" w:rsidRDefault="00D7597F" w:rsidP="00D7597F">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:left="4320" w:hanging="4320"/>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00D7597F" w:rsidRDefault="00D7597F" w:rsidP="00D7597F">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:left="4320" w:hanging="4320"/>
            </w:pPr>
            <w:r>
              <w:tab/>
            </w:r>
            <w:r>
              <w:tab/>
              <w:t xml:space="preserve"></w:t>
            </w:r>
            
          </w:p>
          <w:p w:rsidR="00D7597F" w:rsidRDefault="00F81650" w:rsidP="00D7597F">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:left="4320" w:hanging="4320"/>
              <w:jc w:val="center"/>
            </w:pPr>
            <w:r>
              <w:t><xsl:value-of select="ttd_nama"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
    </w:tbl>
    <w:p w:rsidR="00D7597F" w:rsidRDefault="00D7597F" w:rsidP="006B1371">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="006B1371" w:rsidRPr="003A6EE6" w:rsidRDefault="006B1371" w:rsidP="00D7597F">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
      </w:pPr>
      <w:r>
        <w:tab/>
      </w:r>
    </w:p>
    <w:p w:rsidR="00DF50A9" w:rsidRPr="00D956EA" w:rsidRDefault="00DF50A9" w:rsidP="00D956EA">
      <w:bookmarkStart w:id="0" w:name="_GoBack"/>
      <w:bookmarkEnd w:id="0"/>
    </w:p>
    <w:sectPr w:rsidR="00DF50A9" w:rsidRPr="00D956EA" w:rsidSect="00DF50A9">
      <w:pgSz w:w="12240" w:h="20160" w:code="5"/>
      <w:pgMar w:top="899" w:right="927" w:bottom="1079" w:left="1080" w:header="720" w:footer="720" w:gutter="0"/>
      <w:cols w:space="720"/>
      <w:docGrid w:linePitch="360"/>
    </w:sectPr>
	</xsl:for-each>
  </w:body>
</w:document>
</xsl:template>
</xsl:stylesheet>
