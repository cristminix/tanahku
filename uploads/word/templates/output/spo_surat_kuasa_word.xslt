<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<w:document xmlns:ve="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml">
  <w:body>
	<xsl:for-each select="root/row">
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="right"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:i/>
          <w:iCs/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:i/>
          <w:iCs/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Lampiran- 1</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t xml:space="preserve">Kepada </w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t>Yth.</w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t>Kepala Kantor Pertanahan</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t>Kabupaten</w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Brebes</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t>di .</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
          <w:u w:val="single"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:bCs/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
          <w:u w:val="single"/>
        </w:rPr>
        <w:t>BREBES</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Dengan</w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>hormat,</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRDefault="00BE04CD" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve">Yang bertanda </w:t>
      </w:r>
      <w:r w:rsidR="00686F75" w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>tangan</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="00686F75" w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>di</w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="00686F75" w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>bawah</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="00686F75" w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>ini :</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="003537A7" w:rsidRDefault="003537A7" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblStyle w:val="TableGrid"/>
        <w:tblW w:w="0" w:type="auto"/>
        <w:tblBorders>
          <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="2178"/>
        <w:gridCol w:w="2790"/>
        <w:gridCol w:w="4226"/>
      </w:tblGrid>
      <w:tr w:rsidR="00D02764" w:rsidTr="00BE04CD">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2178" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00D02764" w:rsidRDefault="00D02764" w:rsidP="00D02764">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:firstLine="540"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Nama</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00D02764" w:rsidRDefault="00D02764" w:rsidP="00D02764">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:firstLine="540"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Umur</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00D02764" w:rsidRDefault="00D02764" w:rsidP="00D02764">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:firstLine="540"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Pekerjaan</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00D02764" w:rsidRDefault="00D02764" w:rsidP="00D02764">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:firstLine="540"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Nomor KTP</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2790" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00D02764" w:rsidRDefault="00D02764" w:rsidP="00686F75">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>: 1.<xsl:value-of select="nama_1"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00D02764" w:rsidRDefault="00D02764" w:rsidP="00686F75">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve">: </w:t>
            </w:r>
            <w:r w:rsidRPr="00D02764">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t><xsl:value-of select="umur_1"/></w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"> Th</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00D02764" w:rsidRDefault="00D02764" w:rsidP="00686F75">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="pekerjaan_1"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00D02764" w:rsidRDefault="00D02764" w:rsidP="00686F75">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="nomor_ktp_1"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="4226" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00D02764" w:rsidRDefault="00D02764" w:rsidP="00D02764">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>: 2.<xsl:value-of select="nama_2"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00D02764" w:rsidRDefault="00D02764" w:rsidP="00D02764">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve">: </w:t>
            </w:r>
            <w:r w:rsidRPr="00D02764">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t><xsl:value-of select="umur_2"/></w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t></w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"> Th</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00D02764" w:rsidRDefault="00D02764" w:rsidP="00D02764">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="pekerjaan_2"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00D02764" w:rsidRDefault="00D02764" w:rsidP="00D02764">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="nomor_ktp_2"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
      <w:tr w:rsidR="00D02764" w:rsidTr="00BE04CD">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2178" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00D02764" w:rsidRDefault="00D02764" w:rsidP="00D02764">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:firstLine="540"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Alamat</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="7016" w:type="dxa"/>
            <w:gridSpan w:val="2"/>
          </w:tcPr>
          <w:p w:rsidR="00D02764" w:rsidRDefault="00D02764" w:rsidP="00686F75">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="alamat"/> , RT <xsl:value-of select="rt"/> RW <xsl:value-of select="rw"/>, Kecamatan Bulakamba Kabupaten Brebes</w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
    </w:tbl>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Dalam</w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>hal</w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>ini</w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>bertindak</w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>untuk</w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>dan</w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>atas</w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>nama</w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>diri</w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>sendiri/selaku</w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>kuasa</w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>dari :</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="003537A7" w:rsidRPr="00420C0A" w:rsidRDefault="003537A7" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="2160"/>
        </w:tabs>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t>Nama</w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t>:</w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> <xsl:value-of select="nama_kuasa"/></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1620"/>
        </w:tabs>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t>Umur</w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t xml:space="preserve">: </w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="umur_kuasa"/> Th</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1620"/>
        </w:tabs>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t>Pekerjaan</w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t xml:space="preserve">: </w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="pekerjaan_kuasa"/></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1620"/>
        </w:tabs>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t>Nomor KTP</w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t xml:space="preserve">: </w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="nomor_ktp_kuasa"/></w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve">  </w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t>Alamat</w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t xml:space="preserve">: </w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>
          <xsl:value-of select="alamat_kuasa"/> RT <xsl:value-of select="rt_kuasa"/> RW  <xsl:value-of select="rw_kuasa"/> Kecamatan Bulakamba Kabupaten Brebes
         </w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Berdasarkan</w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>surat</w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>kuasa</w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> Nomor <xsl:value-of select="dasar_nomor_surat_kuasa"/> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve">tanggal </w:t>
      </w:r>
      <w:r w:rsidR="00D02764">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="dasar_tgl_surat_kuasa"/></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Dengan</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>ini</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>mengajukan</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>permohonan :</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <xsl:for-each select="list_permohonan/p">
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t><xsl:value-of select="."/></w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
    </w:p>
    </xsl:for-each>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00505593" w:rsidRDefault="00686F75" w:rsidP="00505593">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="0"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
          <w:vertAlign w:val="superscript"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Atas</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>sebidang</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>tanah</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="0078522C">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="jenis_tanah"/></w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> C. No </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"><xsl:value-of select="no_c"/> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve">Persil </w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="persil"/></w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Klas</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> <xsl:value-of select="kelas"/></w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve">  </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Luas</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
          <w:u w:val="single"/>
        </w:rPr>
        <w:t>+</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="luas_tanah"/></w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>M</w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
          <w:vertAlign w:val="superscript"/>
        </w:rPr>
        <w:t xml:space="preserve">2 </w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
          <w:vertAlign w:val="superscript"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Yang terletak di</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"><xsl:value-of select="letak_tanah"/> Desa Grinting </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Kacamatan</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Bulakamba</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Kabupaten</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Brebes</w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Untuk</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>melengkapi</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>permohonan</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>dimaksud,</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>bersama</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>ini kami lampirkan :</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <xsl:for-each select="list_lampiran/p">
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:numPr>
          <w:ilvl w:val="0"/>
          <w:numId w:val="13"/>
        </w:numPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="567"/>
        </w:tabs>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="."/></w:t>
      </w:r>
      
    </w:p>
    </xsl:for-each>
   
    <w:p w:rsidR="00505593" w:rsidRPr="00420C0A" w:rsidRDefault="00505593" w:rsidP="00505593">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="851"/>
        </w:tabs>
        <w:ind w:left="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Demikian</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>permohonan kami, dan kami menjamin :</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:numPr>
          <w:ilvl w:val="0"/>
          <w:numId w:val="14"/>
        </w:numPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Bahwa</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>bidang</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>tanah</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>tersebut</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>benar</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> – </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>benar</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>saya</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>kuasai</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>secara</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>fisik</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>dan</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>tidak</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>dalam</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>sengketa</w:t>
      </w:r>
      <w:r w:rsidR="00BE04CD">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>.</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00505593">
      <w:pPr>
        <w:numPr>
          <w:ilvl w:val="0"/>
          <w:numId w:val="14"/>
        </w:numPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="709" w:hanging="349"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Bahwa</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>semua</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>lampiran</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>tersebut</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>benar</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>adanya, apabila</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>terdapat</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>ketidak</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>benaran yang berakibat</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Perdata</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>dan</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>atau</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Pidana</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>menjadi</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> tanggung </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>jawab</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>saya</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>dan</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>permohonan</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>tersebut</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>batal</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>dengan</w:t>
      </w:r>
      <w:r w:rsidR="00505593">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>sendirinya</w:t>
      </w:r>
      <w:r w:rsidR="00BE04CD">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>.</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="360"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblStyle w:val="TableGrid"/>
        <w:tblW w:w="0" w:type="auto"/>
        <w:tblInd w:w="360" w:type="dxa"/>
        <w:tblBorders>
          <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="3708"/>
        <w:gridCol w:w="3325"/>
        <w:gridCol w:w="3056"/>
      </w:tblGrid>
      <w:tr w:rsidR="00FD48E5" w:rsidTr="00FD48E5">
        <w:trPr>
          <w:trHeight w:val="1710"/>
        </w:trPr>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="3708" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00FD48E5" w:rsidRDefault="00FD48E5" w:rsidP="00686F75">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="540"/>
                <w:tab w:val="left" w:pos="1800"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="6381" w:type="dxa"/>
            <w:gridSpan w:val="2"/>
          </w:tcPr>
          <w:p w:rsidR="00FD48E5" w:rsidRDefault="00FD48E5" w:rsidP="00FD48E5">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="540"/>
                <w:tab w:val="left" w:pos="1800"/>
              </w:tabs>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve">Grinting, </w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t><xsl:value-of select="dt_surat_kuasa"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00FD48E5" w:rsidRPr="00420C0A" w:rsidRDefault="00FD48E5" w:rsidP="00FD48E5">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="540"/>
                <w:tab w:val="left" w:pos="1800"/>
              </w:tabs>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Hormat kami,</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00FD48E5" w:rsidRPr="00420C0A" w:rsidRDefault="00FD48E5" w:rsidP="00FD48E5">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="540"/>
                <w:tab w:val="left" w:pos="1800"/>
              </w:tabs>
              <w:ind w:left="360"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00FD48E5" w:rsidRPr="00420C0A" w:rsidRDefault="00FD48E5" w:rsidP="00FD48E5">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="540"/>
                <w:tab w:val="left" w:pos="1800"/>
              </w:tabs>
              <w:ind w:left="360"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00FD48E5" w:rsidRPr="003913E8" w:rsidRDefault="00FD48E5" w:rsidP="00FD48E5">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="540"/>
                <w:tab w:val="left" w:pos="1800"/>
              </w:tabs>
              <w:ind w:left="72"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:i/>
                <w:iCs/>
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="003913E8">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:i/>
                <w:iCs/>
                <w:sz w:val="16"/>
                <w:szCs w:val="16"/>
              </w:rPr>
              <w:t>Materai Rp 6.000</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00FD48E5" w:rsidRDefault="00FD48E5" w:rsidP="00686F75">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="540"/>
                <w:tab w:val="left" w:pos="1800"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
          </w:p>
        </w:tc>
      </w:tr>
      <w:tr w:rsidR="00FD48E5" w:rsidTr="00FD48E5">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="3708" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00FD48E5" w:rsidRDefault="00FD48E5" w:rsidP="00686F75">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="540"/>
                <w:tab w:val="left" w:pos="1800"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="3325" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00FD48E5" w:rsidRDefault="00FD48E5" w:rsidP="00FD48E5">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="540"/>
                <w:tab w:val="left" w:pos="1800"/>
              </w:tabs>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>1.<xsl:value-of select="nama_1"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="3056" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00FD48E5" w:rsidRDefault="00FD48E5" w:rsidP="00FD48E5">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="540"/>
                <w:tab w:val="left" w:pos="1800"/>
              </w:tabs>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>2. <xsl:value-of select="nama_2"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
    </w:tbl>
    <w:p w:rsidR="00FD48E5" w:rsidRPr="00420C0A" w:rsidRDefault="00FD48E5" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="360"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="003913E8" w:rsidRDefault="00686F75" w:rsidP="00FD48E5">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="360"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:i/>
          <w:iCs/>
          <w:sz w:val="16"/>
          <w:szCs w:val="16"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="360"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:i/>
          <w:iCs/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="360"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00686F75" w:rsidRPr="00420C0A" w:rsidRDefault="00686F75" w:rsidP="00686F75">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="360"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
          <w:u w:val="single"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve">          </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
    </w:p>
    <w:p w:rsidR="00DF50A9" w:rsidRPr="00D956EA" w:rsidRDefault="00DF50A9" w:rsidP="00D956EA">
      <w:bookmarkStart w:id="0" w:name="_GoBack"/>
      <w:bookmarkEnd w:id="0"/>
    </w:p>
    <w:sectPr w:rsidR="00DF50A9" w:rsidRPr="00D956EA" w:rsidSect="00DF50A9">
      <w:pgSz w:w="12240" w:h="20160" w:code="5"/>
      <w:pgMar w:top="899" w:right="927" w:bottom="1079" w:left="1080" w:header="720" w:footer="720" w:gutter="0"/>
      <w:cols w:space="720"/>
      <w:docGrid w:linePitch="360"/>
    </w:sectPr>
	</xsl:for-each>
  </w:body>
</w:document>
</xsl:template>
</xsl:stylesheet>
