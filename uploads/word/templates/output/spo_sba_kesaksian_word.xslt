<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<w:document xmlns:ve="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml">
  <w:body>
	<xsl:for-each select="root/row">
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
        </w:rPr>
        <w:t>BERITA ACARA KESAKSIAN</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="00402B4C" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:noProof/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:pict>
          <v:line id="Line 59" o:spid="_x0000_s1026" style="position:absolute;left:0;text-align:left;z-index:251659264;visibility:visible" from="0,3.6pt" to="495pt,3.6pt" o:gfxdata="UEsDBBQABgAIAAAAIQC2gziS/gAAAOEBAAATAAAAW0NvbnRlbnRfVHlwZXNdLnhtbJSRQU7DMBBF&#10;90jcwfIWJU67QAgl6YK0S0CoHGBkTxKLZGx5TGhvj5O2G0SRWNoz/78nu9wcxkFMGNg6quQqL6RA&#10;0s5Y6ir5vt9lD1JwBDIwOMJKHpHlpr69KfdHjyxSmriSfYz+USnWPY7AufNIadK6MEJMx9ApD/oD&#10;OlTrorhX2lFEilmcO2RdNtjC5xDF9pCuTyYBB5bi6bQ4syoJ3g9WQ0ymaiLzg5KdCXlKLjvcW893&#10;SUOqXwnz5DrgnHtJTxOsQfEKIT7DmDSUCaxw7Rqn8787ZsmRM9e2VmPeBN4uqYvTtW7jvijg9N/y&#10;JsXecLq0q+WD6m8AAAD//wMAUEsDBBQABgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAX3JlbHMvLnJl&#10;bHOkkMFqwzAMhu+DvYPRfXGawxijTi+j0GvpHsDYimMaW0Yy2fr2M4PBMnrbUb/Q94l/f/hMi1qR&#10;JVI2sOt6UJgd+ZiDgffL8ekFlFSbvV0oo4EbChzGx4f9GRdb25HMsYhqlCwG5lrLq9biZkxWOiqY&#10;22YiTra2kYMu1l1tQD30/bPm3wwYN0x18gb45AdQl1tp5j/sFB2T0FQ7R0nTNEV3j6o9feQzro1i&#10;OWA14Fm+Q8a1a8+Bvu/d/dMb2JY5uiPbhG/ktn4cqGU/er3pcvwCAAD//wMAUEsDBBQABgAIAAAA&#10;IQBi0DF+HwIAADwEAAAOAAAAZHJzL2Uyb0RvYy54bWysU8GO2jAQvVfqP1i+QxIWWIgIqyqBXmiL&#10;tPQDjO0Qax3bsg0BVf33jg1BbHupqubgjO2Z5zczbxYv51aiE7dOaFXgbJhixBXVTKhDgb/v1oMZ&#10;Rs4TxYjUihf4wh1+WX78sOhMzke60ZJxiwBEubwzBW68N3mSONrwlrihNlzBZa1tSzxs7SFhlnSA&#10;3spklKbTpNOWGaspdw5Oq+slXkb8uubUf6trxz2SBQZuPq42rvuwJssFyQ+WmEbQGw3yDyxaIhQ8&#10;eoeqiCfoaMUfUK2gVjtd+yHVbaLrWlAec4BssvS3bF4bYnjMBYrjzL1M7v/B0q+nrUWCFXj+hJEi&#10;LfRoIxRHk3moTWdcDi6l2tqQHT2rV7PR9M0hpcuGqAOPHHcXA3FZiEjehYSNM/DCvvuiGfiQo9ex&#10;UOfatgESSoDOsR+Xez/42SMKh9PRbDpJoW20v0tI3gca6/xnrlsUjAJLIB2ByWnjfCBC8t4lvKP0&#10;WkgZ2y0V6go8ec4mAbo1kLxvhNqBBN4ihNNSsOAeAp097Etp0YkECcUv5gk3j25WHxWL8A0nbHWz&#10;PRHyagMdqQIeJAcEb9ZVIz/m6Xw1W83Gg/FouhqM06oafFqX48F0nT1PqqeqLKvsZ6CWjfNGMMZV&#10;YNfrNRv/nR5uk3NV2l2x98Ik79FjBYFs/4+kY3dDQ6/S2Gt22dq+6yDR6HwbpzADj3uwH4d++QsA&#10;AP//AwBQSwMEFAAGAAgAAAAhAP+Zu8jbAAAABAEAAA8AAABkcnMvZG93bnJldi54bWxMj0FPwkAQ&#10;he8k/IfNmHghsBUTLLVbQohePJgAHuS2dMe2sTtbdhda/fWOXPT45U3e+yZfDbYVF/ShcaTgbpaA&#10;QCqdaahS8LZ/nqYgQtRkdOsIFXxhgFUxHuU6M66nLV52sRJcQiHTCuoYu0zKUNZodZi5DomzD+et&#10;joy+ksbrnsttK+dJspBWN8QLte5wU2P5uTtbBWYbwtNmSL/vX/3L6fSeTg79fqLU7c2wfgQRcYh/&#10;x/Crz+pQsNPRnckE0SrgR6KChzkIDpfLhPl4ZVnk8r988QMAAP//AwBQSwECLQAUAAYACAAAACEA&#10;toM4kv4AAADhAQAAEwAAAAAAAAAAAAAAAAAAAAAAW0NvbnRlbnRfVHlwZXNdLnhtbFBLAQItABQA&#10;BgAIAAAAIQA4/SH/1gAAAJQBAAALAAAAAAAAAAAAAAAAAC8BAABfcmVscy8ucmVsc1BLAQItABQA&#10;BgAIAAAAIQBi0DF+HwIAADwEAAAOAAAAAAAAAAAAAAAAAC4CAABkcnMvZTJvRG9jLnhtbFBLAQIt&#10;ABQABgAIAAAAIQD/mbvI2wAAAAQBAAAPAAAAAAAAAAAAAAAAAHkEAABkcnMvZG93bnJldi54bWxQ&#10;SwUGAAAAAAQABADzAAAAgQUAAAAA&#10;" strokeweight="4.5pt">
            <v:stroke linestyle="thinThick"/>
          </v:line>
        </w:pict>
      </w:r>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="0044568E" w:rsidP="00D6293F">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="540"/>
          <w:tab w:val="left" w:pos="1134"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t>Pada</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>hari</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>ini</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="dt_hari"/></w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>tanggal</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="00F869C5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t></w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="00F869C5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="dt_indo"/></w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve">   kami    yang </w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>bertanda</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>tangan</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>dibawah</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>ini :</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="180"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="180" w:hanging="180"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>1. Nama</w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t xml:space="preserve">: </w:t>
      </w:r>
      <w:r w:rsidR="00F869C5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="nama_saksi_1"/></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="00D6293F" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="180"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="180" w:hanging="180"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve">    </w:t>
      </w:r>
      <w:r w:rsidR="0044568E" w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Umur</w:t>
      </w:r>
      <w:r w:rsidR="0044568E" w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidR="0044568E" w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t xml:space="preserve">: </w:t>
      </w:r>
      <w:r w:rsidR="00F869C5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"><xsl:value-of select="umur_saksi_1"/> </w:t>
      </w:r>
      <w:r w:rsidR="0044568E" w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Th</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="180"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="180" w:hanging="180"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Pekerjaan</w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t xml:space="preserve">: </w:t>
      </w:r>
      <w:r w:rsidR="00F869C5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="pekerjaan_saksi_1"/></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="180"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="180" w:hanging="180"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Alamat</w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t xml:space="preserve">: </w:t>
      </w:r>
      <w:r w:rsidR="00F869C5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="alamat_saksi_1"/></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="180"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="180" w:hanging="180"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="180"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="180" w:hanging="180"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>2. Nama</w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t xml:space="preserve">: </w:t>
      </w:r>
      <w:r w:rsidR="00F869C5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="nama_saksi_2"/></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="00D6293F" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="180"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="180" w:hanging="180"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve">    </w:t>
      </w:r>
      <w:r w:rsidR="0044568E" w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Umur</w:t>
      </w:r>
      <w:r w:rsidR="0044568E" w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidR="0044568E" w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t xml:space="preserve">: </w:t>
      </w:r>
      <w:r w:rsidR="00F869C5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="umur_saksi_2"/></w:t>
      </w:r>
      <w:r w:rsidR="0044568E" w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> Th</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="180"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="180" w:hanging="180"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Pekerjaan</w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t xml:space="preserve">: </w:t>
      </w:r>
      <w:r w:rsidR="00F869C5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="pekerjaan_saksi_2"/></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="180"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="180" w:hanging="180"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Alamat</w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t xml:space="preserve">: </w:t>
      </w:r>
      <w:r w:rsidR="00F869C5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="alamat_saksi_2"/></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00D6293F" w:rsidRPr="00BA6AF5" w:rsidRDefault="00D6293F" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="180"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="180" w:hanging="180"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Dengan</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>ini</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>memberikan</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>kesaksian (jika</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>perlu</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>diangkat</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>sumpah) bahwa :</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRDefault="0044568E" w:rsidP="00D6293F">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="270"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="270" w:hanging="180"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>1. Sebidang</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>tanah</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="00F869C5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"><xsl:value-of select="jenis_tanah"/> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>tercantum</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>dalam</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>buku</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>leter</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve">C. No </w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="00F869C5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="no_c"/></w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> Persil </w:t>
      </w:r>
      <w:r w:rsidR="00F869C5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="persil"/></w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Klas</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="00F869C5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="kelas"/></w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve">  </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Luas</w:t>
      </w:r>
      <w:r w:rsidR="00F869C5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
          <w:u w:val="single"/>
        </w:rPr>
        <w:t>+</w:t>
      </w:r>
      <w:r w:rsidR="00F869C5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="luas_tanah"/></w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>M</w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
          <w:vertAlign w:val="superscript"/>
        </w:rPr>
        <w:t xml:space="preserve">2 </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Yang terletak di</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="00F869C5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="letak_tanah"/></w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Desa</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> Grinting</w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>,</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Kacamatan</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Bulakamba</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Kabupaten</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Brebes</w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>,</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>benar</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>benar</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>secara</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>fisik</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>dikuasai</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve">oleh : </w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00D6293F" w:rsidRPr="00BA6AF5" w:rsidRDefault="00D6293F" w:rsidP="00D6293F">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="270"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="270" w:hanging="180"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:strike/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t>Benar</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>benar</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>secara</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>fisik dimiliki</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t></w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> /</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:strike/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>dikuasai</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:strike/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:strike/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>oleh</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00F869C5" w:rsidRPr="00BA6AF5" w:rsidRDefault="00F869C5" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblStyle w:val="TableGrid"/>
        <w:tblW w:w="0" w:type="auto"/>
        <w:tblBorders>
          <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="2178"/>
        <w:gridCol w:w="2790"/>
        <w:gridCol w:w="4226"/>
      </w:tblGrid>
      <w:tr w:rsidR="00D6293F" w:rsidTr="00062B80">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2178" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00D6293F" w:rsidRDefault="00D6293F" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:firstLine="540"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Nama</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00D6293F" w:rsidRDefault="00D6293F" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:firstLine="540"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Umur</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00D6293F" w:rsidRDefault="00D6293F" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:firstLine="540"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Pekerjaan</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00D6293F" w:rsidRDefault="00D6293F" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:firstLine="540"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Nomor KTP</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2790" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00D6293F" w:rsidRDefault="00D6293F" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>: 1.<xsl:value-of select="nama_1"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00D6293F" w:rsidRDefault="00D6293F" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve">: </w:t>
            </w:r>
            <w:r w:rsidRPr="00D02764">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t><xsl:value-of select="umur_1"/></w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"> Th</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00D6293F" w:rsidRDefault="00D6293F" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="pekerjaan_1"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00D6293F" w:rsidRDefault="00D6293F" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="nomor_ktp_1"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="4226" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00D6293F" w:rsidRDefault="00D6293F" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>: 2.<xsl:value-of select="nama_2"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00D6293F" w:rsidRDefault="00D6293F" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve">: </w:t>
            </w:r>
            <w:r w:rsidRPr="00D02764">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t><xsl:value-of select="umur_2"/></w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t></w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"> Th</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00D6293F" w:rsidRDefault="00D6293F" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="pekerjaan_2"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00D6293F" w:rsidRDefault="00D6293F" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="nomor_ktp_2"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
      <w:tr w:rsidR="00D6293F" w:rsidTr="00062B80">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2178" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00D6293F" w:rsidRDefault="00D6293F" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:firstLine="540"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Alamat</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00D6293F" w:rsidRDefault="00D6293F" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:firstLine="540"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Seluas</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="7016" w:type="dxa"/>
            <w:gridSpan w:val="2"/>
          </w:tcPr>
          <w:p w:rsidR="00D6293F" w:rsidRDefault="00D6293F" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="alamat"/> , RT <xsl:value-of select="rt"/> RW <xsl:value-of select="rw"/>, Kecamatan Bulakamba Kabupaten Brebes</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00D6293F" w:rsidRDefault="00D6293F" w:rsidP="00103317">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve">: </w:t>
            </w:r>
            <w:r w:rsidR="00103317">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t><xsl:value-of select="luas_tanah"/></w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r w:rsidRPr="00B5735E">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve">m2  </w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
    </w:tbl>
    <w:p w:rsidR="0044568E" w:rsidRDefault="0044568E" w:rsidP="00D6293F">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="2160"/>
        </w:tabs>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="2160"/>
        </w:tabs>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>dengan</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>batas</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> – </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>batas</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>sebagai</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>berikut :</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblStyle w:val="TableGrid"/>
        <w:tblW w:w="0" w:type="auto"/>
        <w:tblBorders>
          <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLayout w:type="fixed"/>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="2898"/>
        <w:gridCol w:w="2610"/>
        <w:gridCol w:w="1754"/>
        <w:gridCol w:w="3016"/>
      </w:tblGrid>
      <w:tr w:rsidR="00D6293F" w:rsidTr="00062B80">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2898" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00D6293F" w:rsidRDefault="00D6293F" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:firstLine="540"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Sebelah Utara</w:t>
            </w:r>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:tab/>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2610" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00D6293F" w:rsidRPr="00FA22A6" w:rsidRDefault="00D6293F" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="1800"/>
              </w:tabs>
              <w:ind w:left="72" w:hanging="72"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>:</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"> <xsl:value-of select="batas_utara"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1754" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00D6293F" w:rsidRPr="00FA22A6" w:rsidRDefault="00D6293F" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="1800"/>
              </w:tabs>
              <w:ind w:left="72" w:hanging="72"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Sebelah</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Timur</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:tab/>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="3016" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00D6293F" w:rsidRPr="00FA22A6" w:rsidRDefault="00D6293F" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="1800"/>
              </w:tabs>
              <w:ind w:left="72" w:hanging="72"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="batas_timur"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
      <w:tr w:rsidR="00D6293F" w:rsidTr="00062B80">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2898" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00D6293F" w:rsidRPr="00BA6AF5" w:rsidRDefault="00D6293F" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:ind w:firstLine="540"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Sebelah Selatan</w:t>
            </w:r>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:tab/>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2610" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00D6293F" w:rsidRDefault="00D6293F" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve">: </w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t><xsl:value-of select="batas_selatan"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1754" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00D6293F" w:rsidRDefault="00D6293F" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve">Sebelah Barat  </w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="3016" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00D6293F" w:rsidRDefault="00D6293F" w:rsidP="00062B80">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="batas_barat"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
    </w:tbl>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="540" w:hanging="540"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="180"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:ind w:left="180" w:hanging="180"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>2. Kepemilikan</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>/</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>penguasaan</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>tanah</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>tersebut</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve">berdasarkan </w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="proses_perolehan_asal_tanah"/></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="0044568E" w:rsidP="00D6293F">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
        </w:tabs>
        <w:ind w:left="270"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
        <w:t>Dilaksanakan</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>secara</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>lisan</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>/ tertulis yang tanda</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>buktinya</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>hilang,</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>berasal</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>dari</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>saudara</w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="00D6293F" w:rsidRPr="00103317">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="nama_pemilik_asal_tanah"/></w:t>
      </w:r>
      <w:r w:rsidR="00D6293F">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Tahun</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="00103317" w:rsidRPr="00103317">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t><xsl:value-of select="tgl_asal_tanah_sejak"/></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="0044568E" w:rsidP="00103317">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>Demikian</w:t>
      </w:r>
      <w:r w:rsidR="00103317">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>berita</w:t>
      </w:r>
      <w:r w:rsidR="00103317">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>acara</w:t>
      </w:r>
      <w:r w:rsidR="00103317">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>ini</w:t>
      </w:r>
      <w:r w:rsidR="00103317">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>dibuat</w:t>
      </w:r>
      <w:r w:rsidR="00103317">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>dengan</w:t>
      </w:r>
      <w:r w:rsidR="00103317">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>sebenarnya</w:t>
      </w:r>
      <w:r w:rsidR="00103317">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>untuk</w:t>
      </w:r>
      <w:r w:rsidR="00103317">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>melengkapi</w:t>
      </w:r>
      <w:r w:rsidR="00103317">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>surat</w:t>
      </w:r>
      <w:r w:rsidR="00103317">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>pernyataan</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>yang</w:t>
      </w:r>
      <w:r w:rsidR="00103317">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>bersangkutan</w:t>
      </w:r>
      <w:r w:rsidR="00103317">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>guna</w:t>
      </w:r>
      <w:r w:rsidR="00103317">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>mengajukan</w:t>
      </w:r>
      <w:r w:rsidR="00103317">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>permohonan</w:t>
      </w:r>
      <w:r w:rsidR="00103317">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:t>sertifikat</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblStyle w:val="TableGrid"/>
        <w:tblW w:w="0" w:type="auto"/>
        <w:tblInd w:w="108" w:type="dxa"/>
        <w:tblBorders>
          <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="4680"/>
        <w:gridCol w:w="5661"/>
      </w:tblGrid>
      <w:tr w:rsidR="00103317" w:rsidTr="00F869C5">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="4680" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00103317" w:rsidRDefault="00103317" w:rsidP="00103317">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Mengetahui,</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00103317" w:rsidRDefault="00103317" w:rsidP="00103317">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t><xsl:value-of select="ttd_jabatan"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00103317" w:rsidRDefault="00103317" w:rsidP="00103317">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00103317" w:rsidRDefault="00103317" w:rsidP="00103317">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00103317" w:rsidRDefault="00103317" w:rsidP="00103317">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00103317" w:rsidRDefault="00103317" w:rsidP="00103317">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00103317" w:rsidRDefault="00103317" w:rsidP="00103317">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00103317" w:rsidRDefault="00103317" w:rsidP="00103317">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00103317" w:rsidRDefault="00103317" w:rsidP="00103317">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00103317" w:rsidRDefault="00103317" w:rsidP="00103317">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:bCs/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t><xsl:value-of select="ttd_nama"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="5661" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00103317" w:rsidRDefault="00103317" w:rsidP="00103317">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Yang memberikan</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>Kesaksian</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00103317" w:rsidRDefault="00103317" w:rsidP="00103317">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00103317" w:rsidRDefault="00103317" w:rsidP="00103317">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00103317" w:rsidRDefault="00103317" w:rsidP="00103317">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00103317" w:rsidRDefault="00103317" w:rsidP="00103317">
            <w:pPr>
              <w:pStyle w:val="ListParagraph"/>
              <w:numPr>
                <w:ilvl w:val="0"/>
                <w:numId w:val="158"/>
              </w:numPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00103317">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve">( </w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"><xsl:value-of select="nama_saksi_1"/> </w:t>
            </w:r>
            <w:r w:rsidRPr="00103317">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>)</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00103317" w:rsidRDefault="00103317" w:rsidP="00103317">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00103317" w:rsidRDefault="00103317" w:rsidP="00103317">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00103317" w:rsidRDefault="00103317" w:rsidP="00103317">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00103317" w:rsidRPr="00103317" w:rsidRDefault="00103317" w:rsidP="00103317">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00103317" w:rsidRPr="00103317" w:rsidRDefault="00103317" w:rsidP="00103317">
            <w:pPr>
              <w:pStyle w:val="ListParagraph"/>
              <w:numPr>
                <w:ilvl w:val="0"/>
                <w:numId w:val="158"/>
              </w:numPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve">( </w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t xml:space="preserve"><xsl:value-of select="nama_saksi_2"/> </w:t>
            </w:r>
            <w:r w:rsidRPr="00BA6AF5">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
              <w:t>)</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00103317" w:rsidRDefault="00103317" w:rsidP="00103317">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:sz w:val="22"/>
                <w:szCs w:val="22"/>
              </w:rPr>
            </w:pPr>
          </w:p>
        </w:tc>
      </w:tr>
    </w:tbl>
    <w:p w:rsidR="00103317" w:rsidRPr="00BA6AF5" w:rsidRDefault="00103317" w:rsidP="0044568E">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="0044568E" w:rsidP="00103317">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00BA6AF5">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
        <w:tab/>
      </w:r>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00BA6AF5" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="22"/>
          <w:szCs w:val="22"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00420C0A" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="0044568E" w:rsidRPr="00420C0A" w:rsidRDefault="0044568E" w:rsidP="0044568E">
      <w:pPr>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00DF50A9" w:rsidRPr="00D956EA" w:rsidRDefault="00DF50A9" w:rsidP="00D956EA">
      <w:bookmarkStart w:id="0" w:name="_GoBack"/>
      <w:bookmarkEnd w:id="0"/>
    </w:p>
    <w:sectPr w:rsidR="00DF50A9" w:rsidRPr="00D956EA" w:rsidSect="00DF50A9">
      <w:pgSz w:w="12240" w:h="20160" w:code="5"/>
      <w:pgMar w:top="899" w:right="927" w:bottom="1079" w:left="1080" w:header="720" w:footer="720" w:gutter="0"/>
      <w:cols w:space="720"/>
      <w:docGrid w:linePitch="360"/>
    </w:sectPr>
	</xsl:for-each>
  </w:body>
</w:document>
</xsl:template>
</xsl:stylesheet>
