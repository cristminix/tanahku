<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<w:document xmlns:ve="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml">
  <w:body>
	<xsl:for-each select="root/row">
    <w:p w:rsidR="00E045E1" w:rsidRPr="00420C0A" w:rsidRDefault="00E045E1" w:rsidP="00E045E1">
      <w:pPr>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRPr="00420C0A" w:rsidRDefault="00E045E1" w:rsidP="00E045E1">
      <w:pPr>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRPr="00420C0A" w:rsidRDefault="00E045E1" w:rsidP="00E045E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRPr="00420C0A" w:rsidRDefault="00E045E1" w:rsidP="00E045E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRPr="00E17B48" w:rsidRDefault="00E045E1" w:rsidP="00E045E1">
      <w:pPr>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:b/>
          <w:sz w:val="28"/>
          <w:szCs w:val="28"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:b/>
          <w:noProof/>
          <w:sz w:val="28"/>
          <w:szCs w:val="28"/>
        </w:rPr>
        <w:drawing>
          <wp:anchor distT="0" distB="0" distL="114300" distR="114300" simplePos="0" relativeHeight="251659264" behindDoc="0" locked="0" layoutInCell="1" allowOverlap="1">
            <wp:simplePos x="0" y="0"/>
            <wp:positionH relativeFrom="column">
              <wp:posOffset>16510</wp:posOffset>
            </wp:positionH>
            <wp:positionV relativeFrom="paragraph">
              <wp:posOffset>21590</wp:posOffset>
            </wp:positionV>
            <wp:extent cx="685800" cy="664210"/>
            <wp:effectExtent l="0" t="0" r="0" b="2540"/>
            <wp:wrapNone/>
            <wp:docPr id="90" name="Picture 74" descr="Brebes1"/>
            <wp:cNvGraphicFramePr>
              <a:graphicFrameLocks xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main" noChangeAspect="1"/>
            </wp:cNvGraphicFramePr>
            <a:graphic xmlns:a="http://schemas.openxmlformats.org/drawingml/2006/main">
              <a:graphicData uri="http://schemas.openxmlformats.org/drawingml/2006/picture">
                <pic:pic xmlns:pic="http://schemas.openxmlformats.org/drawingml/2006/picture">
                  <pic:nvPicPr>
                    <pic:cNvPr id="0" name="logo.png" descr="Brebes1"/>
                    <pic:cNvPicPr>
                      <a:picLocks noChangeAspect="1" noChangeArrowheads="1"/>
                    </pic:cNvPicPr>
                  </pic:nvPicPr>
                  <pic:blipFill>
                    <a:blip r:embed="rId6" cstate="print">
                      <a:extLst>
                        <a:ext uri="">
                          <a14:useLocalDpi xmlns:a14="http://schemas.microsoft.com/office/drawing/2010/main" xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape" xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk" xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup" xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml" xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" xmlns:w10="urn:schemas-microsoft-com:office:word" xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas" xmlns="" val="0"/>
                        </a:ext>
                      </a:extLst>
                    </a:blip>
                    <a:srcRect/>
                    <a:stretch>
                      <a:fillRect/>
                    </a:stretch>
                  </pic:blipFill>
                  <pic:spPr bwMode="auto">
                    <a:xfrm>
                      <a:off x="0" y="0"/>
                      <a:ext cx="685800" cy="664210"/>
                    </a:xfrm>
                    <a:prstGeom prst="rect">
                      <a:avLst/>
                    </a:prstGeom>
                    <a:noFill/>
                    <a:ln>
                      <a:noFill/>
                    </a:ln>
                  </pic:spPr>
                </pic:pic>
              </a:graphicData>
            </a:graphic>
          </wp:anchor>
        </w:drawing>
      </w:r>
      <w:r w:rsidRPr="00E17B48">
        <w:rPr>
          <w:b/>
          <w:sz w:val="28"/>
          <w:szCs w:val="28"/>
        </w:rPr>
        <w:t>PEMERINTAH KABUPATEN BREBES</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRPr="00E17B48" w:rsidRDefault="00E045E1" w:rsidP="00E045E1">
      <w:pPr>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:b/>
          <w:sz w:val="28"/>
          <w:szCs w:val="28"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00E17B48">
        <w:rPr>
          <w:b/>
          <w:sz w:val="28"/>
          <w:szCs w:val="28"/>
        </w:rPr>
        <w:t>KECAMATAN BULAKAMBA</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRDefault="00E045E1" w:rsidP="00E045E1">
      <w:pPr>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:b/>
          <w:sz w:val="32"/>
          <w:szCs w:val="32"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00E17B48">
        <w:rPr>
          <w:b/>
          <w:sz w:val="32"/>
          <w:szCs w:val="32"/>
        </w:rPr>
        <w:t>DESA GRINTING</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRDefault="00E045E1" w:rsidP="00E045E1">
      <w:pPr>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:b/>
          <w:sz w:val="32"/>
          <w:szCs w:val="32"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRPr="00E17B48" w:rsidRDefault="00E045E1" w:rsidP="00E045E1">
      <w:pPr>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:b/>
          <w:sz w:val="32"/>
          <w:szCs w:val="32"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRPr="00420C0A" w:rsidRDefault="00E045E1" w:rsidP="00E045E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="28"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="28"/>
        </w:rPr>
        <w:t>SURAT KETERANGAN</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="28"/>
        </w:rPr>
        <w:t xml:space="preserve"> KEPEMILIKAN TANAH</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRPr="00420C0A" w:rsidRDefault="00E045E1" w:rsidP="00E045E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve">Nomor : </w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t><xsl:value-of select="nomor_surat"/></w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRPr="00420C0A" w:rsidRDefault="00E045E1" w:rsidP="00E045E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="28"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRPr="00420C0A" w:rsidRDefault="00E045E1" w:rsidP="00E045E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="center"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:sz w:val="28"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRPr="00420C0A" w:rsidRDefault="00E045E1" w:rsidP="00E045E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="540"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
        <w:t>Kepala</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Desa</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t>Grinting</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Kecamatan</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t>Bulakamba</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Kabupaten</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t>Brebes</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRDefault="00E045E1" w:rsidP="00E045E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Dengan</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>ini</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>menerangkan</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>bahwa</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>sebidang</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>tanah</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>terletak di:</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00E045E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblStyle w:val="TableGrid"/>
        <w:tblW w:w="0" w:type="auto"/>
        <w:tblInd w:w="108" w:type="dxa"/>
        <w:tblBorders>
          <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="1938"/>
        <w:gridCol w:w="283"/>
        <w:gridCol w:w="8120"/>
      </w:tblGrid>
      <w:tr w:rsidR="00892C02" w:rsidTr="00892C02">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1938" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00F850E6">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>Jalan</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t xml:space="preserve"> </w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00F850E6">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>RT/RW</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t xml:space="preserve"> </w:t>
            </w:r>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>Desa/Kelurahan</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00F850E6">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:tab/>
              <w:t>Kecamatan</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00F850E6">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>Kabupaten</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="283" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00F850E6">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>:</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00F850E6">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>:</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00F850E6">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>:</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00F850E6">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>:</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00F850E6">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>:</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="8120" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00F850E6">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:bCs/>
              </w:rPr>
              <w:t><xsl:value-of select="jalan_t"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00892C02">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t xml:space="preserve">RT </w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:bCs/>
              </w:rPr>
              <w:t><xsl:value-of select="rt_t"/></w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t xml:space="preserve"> RW </w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:bCs/>
              </w:rPr>
              <w:t><xsl:value-of select="rw_t"/></w:t>
            </w:r>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t xml:space="preserve"> </w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00892C02">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:bCs/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:bCs/>
              </w:rPr>
              <w:t>Grinting</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00892C02">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:bCs/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:bCs/>
              </w:rPr>
              <w:t>Bulakamba</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00892C02">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:bCs/>
              </w:rPr>
              <w:t>Brebes</w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
    </w:tbl>
    <w:p w:rsidR="001D042B" w:rsidRDefault="001D042B" w:rsidP="001D042B">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRPr="00420C0A" w:rsidRDefault="00E045E1" w:rsidP="001D042B">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="540"/>
          <w:tab w:val="left" w:pos="1800"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Dengan</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>batas</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> – </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>batas</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>sebagai</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>berikut :</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRPr="00420C0A" w:rsidRDefault="00E045E1" w:rsidP="00E045E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="6450"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblStyle w:val="TableGrid"/>
        <w:tblW w:w="0" w:type="auto"/>
        <w:tblBorders>
          <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLayout w:type="fixed"/>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="2178"/>
        <w:gridCol w:w="2610"/>
        <w:gridCol w:w="1890"/>
        <w:gridCol w:w="3600"/>
      </w:tblGrid>
      <w:tr w:rsidR="00F52E9C" w:rsidRPr="00F52E9C" w:rsidTr="00F52E9C">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2178" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00F52E9C" w:rsidRPr="00F52E9C" w:rsidRDefault="00F52E9C" w:rsidP="00F52E9C">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00F52E9C">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>Sebelah Utara</w:t>
            </w:r>
            <w:r w:rsidRPr="00F52E9C">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:tab/>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2610" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00F52E9C" w:rsidRPr="00F52E9C" w:rsidRDefault="00F52E9C" w:rsidP="00F850E6">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="1800"/>
              </w:tabs>
              <w:ind w:left="72" w:hanging="72"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00F52E9C">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="batas_utara"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1890" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00F52E9C" w:rsidRPr="00F52E9C" w:rsidRDefault="00F52E9C" w:rsidP="00F850E6">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="1800"/>
              </w:tabs>
              <w:ind w:left="72" w:hanging="72"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00F52E9C">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>Sebelah Timur</w:t>
            </w:r>
            <w:r w:rsidRPr="00F52E9C">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:tab/>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="3600" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00F52E9C" w:rsidRPr="00F52E9C" w:rsidRDefault="00F52E9C" w:rsidP="00F850E6">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="1800"/>
              </w:tabs>
              <w:ind w:left="72" w:hanging="72"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00F52E9C">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="batas_timur"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
      <w:tr w:rsidR="00F52E9C" w:rsidRPr="00F52E9C" w:rsidTr="00F52E9C">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2178" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00F52E9C" w:rsidRPr="00F52E9C" w:rsidRDefault="00F52E9C" w:rsidP="00F52E9C">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00F52E9C">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>Sebelah Selatan</w:t>
            </w:r>
            <w:r w:rsidRPr="00F52E9C">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:tab/>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="2610" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00F52E9C" w:rsidRPr="00F52E9C" w:rsidRDefault="00F52E9C" w:rsidP="00F850E6">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00F52E9C">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="batas_selatan"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1890" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00F52E9C" w:rsidRPr="00F52E9C" w:rsidRDefault="00F52E9C" w:rsidP="00F850E6">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00F52E9C">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t xml:space="preserve">Sebelah Barat  </w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="3600" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00F52E9C" w:rsidRPr="00F52E9C" w:rsidRDefault="00F52E9C" w:rsidP="00F850E6">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="6450"/>
              </w:tabs>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00F52E9C">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>: <xsl:value-of select="batas_barat"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
    </w:tbl>
    <w:p w:rsidR="00E045E1" w:rsidRPr="00420C0A" w:rsidRDefault="00E045E1" w:rsidP="00E045E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRPr="00420C0A" w:rsidRDefault="00E045E1" w:rsidP="001D042B">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="990"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="left" w:pos="1980"/>
          <w:tab w:val="left" w:pos="3600"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
        <w:t>Yang tercatat</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>dalam</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Buku</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>leter C Desa</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Nomor</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t><xsl:value-of select="no_c"/></w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> Persil</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t><xsl:value-of select="persil"/></w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Klas</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t><xsl:value-of select="kelas"/></w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Luas</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:u w:val="single"/>
        </w:rPr>
        <w:t>+</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:t><xsl:value-of select="luas_tanah"/></w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> M</w:t>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:vertAlign w:val="superscript"/>
        </w:rPr>
        <w:t>2</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="001D042B" w:rsidRDefault="001D042B" w:rsidP="00E045E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRDefault="00E045E1" w:rsidP="00E045E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Adalah</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>benar</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> – </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>benar</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>milik :</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRDefault="00E045E1" w:rsidP="00E045E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblStyle w:val="TableGrid"/>
        <w:tblW w:w="10868" w:type="dxa"/>
        <w:tblInd w:w="108" w:type="dxa"/>
        <w:tblBorders>
          <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="1890"/>
        <w:gridCol w:w="360"/>
        <w:gridCol w:w="8618"/>
      </w:tblGrid>
      <w:tr w:rsidR="00892C02" w:rsidTr="00892C02">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="1890" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00E045E1">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t xml:space="preserve">Nama </w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00E045E1">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t xml:space="preserve">Umur </w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00E045E1">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t xml:space="preserve">Pekerjaan </w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00E045E1">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>Alamat</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="360" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00E045E1">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>:</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00E045E1">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>:</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00E045E1">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>:</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00E045E1">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>:</w:t>
            </w:r>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="8618" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00E045E1">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t xml:space="preserve"><xsl:value-of select="nama_pemilik"/> </w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00E045E1">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t><xsl:value-of select="umur_pemilik"/> Th</w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00E045E1">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="lowKashida"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t><xsl:value-of select="pekerjaan_pemilik"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00892C02">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="900"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="4320"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t><xsl:value-of select="alamat_pemilik"/> RT <xsl:value-of select="rt_pemilik"/> RW <xsl:value-of select="rw_pemilik"/>,Kecamatan Bulakamba Kabupaten Brebes</w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
    </w:tbl>
    <w:p w:rsidR="001D042B" w:rsidRDefault="001D042B" w:rsidP="00892C02">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:jc w:val="both"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRDefault="00E045E1" w:rsidP="00E045E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="both"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Yang diperoleh</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>dari</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> <xsl:value-of select="nama_pemilik_asal"/> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>melalui</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>jual</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>beli</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>dengan</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Akte</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Jual</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> B</w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>eli</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve">Nomor </w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>:</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRPr="00420C0A" w:rsidRDefault="00892C02" w:rsidP="00E045E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="both"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t><xsl:value-of select="nomor_akta"/></w:t>
      </w:r>
      <w:r w:rsidR="00E045E1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>,</w:t>
      </w:r>
      <w:r w:rsidR="001D042B">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidR="00E045E1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve">tertanggal </w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t><xsl:value-of select="tgl_akta"/></w:t>
      </w:r>
      <w:r w:rsidR="00B07EB4">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t></w:t>
      </w:r>
      <w:r>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t></w:t>
      </w:r>
      <w:r w:rsidR="00E045E1">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRPr="00420C0A" w:rsidRDefault="00E045E1" w:rsidP="00E045E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRDefault="00E045E1" w:rsidP="00892C02">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="142"/>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00892C02">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Demikian</w:t>
      </w:r>
      <w:r w:rsidR="00892C02">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00892C02">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Surat</w:t>
      </w:r>
      <w:r w:rsidR="00892C02">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00892C02">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>Keterangan</w:t>
      </w:r>
      <w:r w:rsidR="00892C02">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00892C02">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>ini kami buat</w:t>
      </w:r>
      <w:r w:rsidR="00892C02">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00892C02">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>dengan</w:t>
      </w:r>
      <w:r w:rsidR="00892C02">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00892C02">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>sebenar</w:t>
      </w:r>
      <w:r w:rsidR="00892C02">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> -  </w:t>
      </w:r>
      <w:r w:rsidRPr="00892C02">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>benarnya</w:t>
      </w:r>
      <w:r w:rsidR="00892C02">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00892C02">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>untuk</w:t>
      </w:r>
      <w:r w:rsidR="00892C02">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00892C02">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>menjadi</w:t>
      </w:r>
      <w:r w:rsidR="00892C02">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00892C02">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>periksa</w:t>
      </w:r>
      <w:r w:rsidR="00892C02">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00892C02">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>guna</w:t>
      </w:r>
      <w:r w:rsidR="00892C02">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t xml:space="preserve"> </w:t>
      </w:r>
      <w:r w:rsidRPr="00892C02">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:t>seperlunya</w:t>
      </w:r>
    </w:p>
    <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00892C02">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="142"/>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:tbl>
      <w:tblPr>
        <w:tblStyle w:val="TableGrid"/>
        <w:tblW w:w="0" w:type="auto"/>
        <w:tblBorders>
          <w:top w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:left w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:bottom w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:right w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideH w:val="none" w:sz="0" w:space="0" w:color="auto"/>
          <w:insideV w:val="none" w:sz="0" w:space="0" w:color="auto"/>
        </w:tblBorders>
        <w:tblLook w:val="04A0"/>
      </w:tblPr>
      <w:tblGrid>
        <w:gridCol w:w="5224"/>
        <w:gridCol w:w="5225"/>
      </w:tblGrid>
      <w:tr w:rsidR="00892C02" w:rsidTr="00892C02">
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="5224" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00892C02">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
          </w:p>
        </w:tc>
        <w:tc>
          <w:tcPr>
            <w:tcW w:w="5225" w:type="dxa"/>
          </w:tcPr>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00892C02">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t xml:space="preserve">Grinting, </w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>T</w:t>
            </w:r>
            <w:r w:rsidRPr="00420C0A">
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t>anggal</w:t>
            </w:r>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t xml:space="preserve"> <xsl:value-of select="dt_surat_keterangan"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00892C02">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:bCs/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
              <w:t><xsl:value-of select="ttd_jabatan"/></w:t>
            </w:r>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00892C02">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:bCs/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00892C02">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:bCs/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00892C02">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:bCs/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00892C02">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:bCs/>
              </w:rPr>
            </w:pPr>
          </w:p>
          <w:p w:rsidR="00892C02" w:rsidRDefault="00892C02" w:rsidP="00892C02">
            <w:pPr>
              <w:tabs>
                <w:tab w:val="right" w:pos="142"/>
                <w:tab w:val="right" w:pos="180"/>
                <w:tab w:val="right" w:pos="1440"/>
                <w:tab w:val="right" w:pos="2160"/>
                <w:tab w:val="left" w:pos="5580"/>
              </w:tabs>
              <w:spacing w:line="276" w:lineRule="auto"/>
              <w:jc w:val="center"/>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
              </w:rPr>
            </w:pPr>
            <w:r>
              <w:rPr>
                <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
                <w:b/>
                <w:bCs/>
              </w:rPr>
              <w:t><xsl:value-of select="ttd_nama"/></w:t>
            </w:r>
          </w:p>
        </w:tc>
      </w:tr>
    </w:tbl>
    <w:p w:rsidR="00892C02" w:rsidRPr="00892C02" w:rsidRDefault="00892C02" w:rsidP="00892C02">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="142"/>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    
    <w:p w:rsidR="00E045E1" w:rsidRPr="00420C0A" w:rsidRDefault="00E045E1" w:rsidP="00E045E1">
      <w:pPr>
        <w:tabs>
          <w:tab w:val="right" w:pos="180"/>
          <w:tab w:val="right" w:pos="900"/>
          <w:tab w:val="right" w:pos="1440"/>
          <w:tab w:val="right" w:pos="2160"/>
          <w:tab w:val="left" w:pos="4320"/>
          <w:tab w:val="left" w:pos="5580"/>
        </w:tabs>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:ind w:left="4320" w:hanging="4320"/>
        <w:jc w:val="lowKashida"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
    </w:p>
    <w:p w:rsidR="00E045E1" w:rsidRPr="00420C0A" w:rsidRDefault="00E045E1" w:rsidP="00E045E1">
      <w:pPr>
        <w:spacing w:line="276" w:lineRule="auto"/>
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
      </w:pPr>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:tab/>
      </w:r>
      <w:r w:rsidRPr="00420C0A">
        <w:rPr>
          <w:rFonts w:ascii="Arial" w:hAnsi="Arial" w:cs="Arial"/>
          <w:b/>
          <w:bCs/>
        </w:rPr>
        <w:tab/>
      </w:r>
    </w:p>
    <w:p w:rsidR="00DF50A9" w:rsidRPr="00D956EA" w:rsidRDefault="00DF50A9" w:rsidP="00D956EA">
      <w:bookmarkStart w:id="0" w:name="_GoBack"/>
      <w:bookmarkEnd w:id="0"/>
    </w:p>
    <w:sectPr w:rsidR="00DF50A9" w:rsidRPr="00D956EA" w:rsidSect="00DF50A9">
      <w:pgSz w:w="12240" w:h="20160" w:code="5"/>
      <w:pgMar w:top="899" w:right="927" w:bottom="1079" w:left="1080" w:header="720" w:footer="720" w:gutter="0"/>
      <w:cols w:space="720"/>
      <w:docGrid w:linePitch="360"/>
    </w:sectPr>
	</xsl:for-each>
  </w:body>
</w:document>
</xsl:template>
</xsl:stylesheet>
